set deltae 0.001

source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl

array set match {
    alpha_x 0.59971622
    alpha_y -1.93937335
    beta_x  18.382571
    beta_y  64.450775
    emitt_x 6.6
    emitt_y 0.2
    charge  4.0e9
    sigma_z 44.0
    phase   0.0
    e_spread -1.0
}

#emittances unit is e-7
set charge $match(charge)
set e0 $e_initial

set n_slice 30
set n 100
set n_total [expr $n_slice*$n]


make_beam_particles $e0 $match(e_spread) $n_total

make_beam_many beam0 $n_slice $n
BeamRead -file particles.in -beam beam0

