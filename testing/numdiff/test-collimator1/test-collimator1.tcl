#################################################################################################################################
#
# Collimator test on CLIC BDS beam
# Geometric wake: diffractive regime
# Resistive wake: intermediate regime
#
#################################################################################################################################

set base_dir .
set script_dir ../../script_dir

set match(charge) 4.0e9

source $script_dir/element_test.tcl

