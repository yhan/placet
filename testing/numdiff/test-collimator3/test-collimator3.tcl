#################################################################################################################################
#
# Collimator test on CLIC BDS beam
# Geometric wake: inductive regime
# Resistive wake: short-range regime
#
#################################################################################################################################

set base_dir .
set script_dir ../../script_dir

set match(charge) 4.0e9

source $script_dir/element_test.tcl
