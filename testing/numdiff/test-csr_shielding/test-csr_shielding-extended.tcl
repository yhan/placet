set sbend_synrad 0
set csr 1
set csr_charge [expr 8.4e-9]
set csr_nbins 800
set csr_nsectors 50
set csr_savesectors 0
set csr_filterorder 3
set csr_nhalffilter 50

set csr_shielding 1
set csr_shielding_n_images 16
set csr_shielding_height 0.04

set csr_shielding_width 0.08

set script_dir ../../script_dir

source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl

set e_initial 2.38
set e0 $e_initial
set length 1.5
set rho 5

BeamlineNew
Girder
Sbend -name "TESTBEND" -synrad $sbend_synrad -csr $csr -csr_charge $csr_charge -csr_nbins $csr_nbins -csr_nsectors $csr_nsectors -csr_savesectors $csr_savesectors -length $length -angle [expr $length/$rho] -E1 0 -E2 0 -six_dim 1 -e0 $e0 -aperture_shape elliptic -aperture_x 100 -aperture_y 100 -csr_shielding $csr_shielding -csr_shielding_n_images $csr_shielding_n_images -csr_shielding_height $csr_shielding_height -csr_shielding_width $csr_shielding_width -csr_filterorder $csr_filterorder -csr_nhalffilter $csr_nhalffilter
TclCall -script "BeamDump -file beam_final.dat"

BeamlineSet -name testbend

set name beam0
array set match {beta_x 100.0 beta_y 100.0 alpha_x 0.0 alpha_y 0.0}

#set match(emitt_x) 1000.0
#set match(emitt_y) 1000.0
#set match(e_spread) 0.0
#set match(charge) $csr_charge
#set charge $match(charge)
#set match(sigma_z) 300.0

set match(emitt_x) 1000.0
set match(emitt_y) 1000.0
set match(e_spread) 0.0
set match(charge) $csr_charge
set charge $match(charge)
set match(sigma_z) 1000.0

set n_slice 6
set n 1000
set n_total [expr $n_slice*$n]

make_beam_many beam0 $n_slice $n
make_beam_particles $e_initial $match(e_spread) [expr $n_slice*$n]
BeamRead -file particles.in -beam $name

#Octave {
#    sigmaz=str2num('$match(sigma_z)')
#    beamnew=placet_get_beam('$name');
#    n_tot=length(beamnew);
#    np= 55000;
#    beamnew(1:np,4)=normrnd(-2*sigmaz,sigmaz,np,1);
#    beamnew(np+1:end,4)=normrnd(3*sigmaz,sigmaz,n_tot-np,1);
#    placet_set_beam(beamnew);
#}

Octave {
    sigmaz=str2num('$match(sigma_z)')
    beamnew=placet_get_beam('$name');
    n_tot=length(beamnew);
    for ite=1:n_tot
       value=10*sigmaz;
       while(abs(value)>3*sigmaz)
         value=sigmaz*randn();
       end
       beamnew(ite,4)=value;
    end
    % beamnew(1:end,4)=normrnd(0,sigmaz,n_tot,1);
    % beamnew(1:end,4)=sigmaz*rand(n_tot,1);
    placet_set_beam(beamnew); 
    format long;
    [e b1]=placet_test_no_correction('testbend','$name', "None");
    out=[str2num('$csr_shielding_height') mean(b1(:,1)) rms(b1(:,1)-mean(b1(:,1)))]
}

TestNoCorrection -beam $name -emitt_file emitt.dat -survey Zero -machines 1
