##
# In this example we first track the beam up to the solenoid map.
# After dumping the beam, we then proceed to track without synrad, and without
# solenoid map on through the solenoid part of the beamline.
# We then make use of backwards tracking, with solenoid on. synrad still off.
# Finally we track forward with both synrad and solenoid
#
# This gives us:
#  - The ideal beam distribution without any effects from synrad or solenoids.
#  - The beam effect of the solenoid ignoring the synrad, to be compared with
#  - The effect of solenoid and the synrad
#
# What we in the end are looking for is the effect of the synchrotron radiation on
# the luminosity.

set e_initial 1496.0
set e0 $e_initial
set script_dir .

# Step length in the IR tracking routine
set step 0.05
# Number of slices
set n_slice 2
# Number of particles per slice
set n 10

set synrad 0
set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1
set scale 1.0

source $script_dir/clic_basic_single.tcl

proc save_beam {name} {

    BeamDump -file $name 
}


array set match {
    alpha_x 0
    alpha_y 0
    beta_x  66.14532014 
    beta_y  17.92472388
}
##############################
# CHECK EMITTANCE BEFORE RUN #
##############################
set match(emitt_x) 6.800
set match(emitt_y) 0.200
set match(charge) 4e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set n_total [expr $n_slice*$n] 
source $script_dir/clic_beam.tcl


FirstOrder 1

set solmap ildantinobuck.txt


# Track beam through without solenoids..
BeamlineNew
source bds.ffs.last.tcl
set name2 particles.ffs.last.out
TclCall -script {save_beam $name2} 
BeamlineSet -name test.ffs.last
BeamlineUse -name test.ffs.last

make_beam_many beam6 $n_slice $n
set n_total [expr $n*$n_slice]
exec echo "[expr $e0-2] -0.0001 0. -8.0 0.0002 0.0" > beam.in
for {set i 1} {$i < $n_total} {incr i} {
    set energy [expr $e0-2+$i*4.0/$n_total]
    set z [expr -8+$i*16.0/$n_total]
    exec echo "$energy -0.0001 0. $z 0.0002 0.0" >> beam.in
}
BeamRead -file beam.in -beam beam6

# First we make reference distribution with TestNoCorrection,
# and compare to TestIntRegion..
puts "TestIntRegion w/o map"
TestIntRegion -beam beam6 -survey Zero -step 0.001
exec mv $name2 particles.testintregion.out
puts "TestNoCorrection w/o map"
TestNoCorrection -beam beam6 -survey Zero
exec mv $name2 particles.testintregion.ref

puts " TestIntRegion forwards... "
TestIntRegion -beam beam6 -emitt_file emitt.dat -survey Zero -angle 0.01 -step $step -synrad 0 -filename $solmap
# want to compare this file after instead of just the one particle..
exec mv $name2 $name2.bak

puts " TestIntRegion forwards single track... "
make_beam_many beam7 1 1
exec echo "$e0 -0.0001 0. 0.0 0.0002 0.0" > beam.in
BeamRead -file beam.in -beam beam7
TestIntRegion -beam beam7 -emitt_file emitt.dat -survey Zero -angle 0.01 -step $step -synrad 0 -filename $solmap -backward 1 -writefirst 1
exec mv $name2.bak $name2

