/*
* Program to read in a mad8, madx or bmad lattice file, translate to
* AML and get attributes.
*/

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <map>

#include <tcl.h>
#include <tk.h>

#include "UAP/BasicUtilities.hpp"
#include "UAP/UAPUtilities.hpp"

#include "Bmad/MAD8Parser.hpp"
#include "Bmad/MADXParser.hpp"
#include "Bmad/BmadParser.hpp"
#include "Bmad/XSIFParser.hpp"
#include "AML/AMLReader.hpp"
#include "AML/AMLLatticeExpander.hpp"

#define DEBUG false

using namespace BasicUtilities;
using namespace std;

#define e_charge 1.6021892e-19
#define c_light 2.99792458e8

extern "C" {
extern int Importlatticeaml_Init(Tcl_Interp *interp);

  extern int ImportLatticeAML_Init(Tcl_Interp *interp);
}

  typedef std::map<std::string, std::string> Attrs;

  struct cmpAttr {
    int operator () (const Attrs &a, const Attrs &b ) const
    {
      Attrs &_a=const_cast<Attrs&>(a);
      Attrs &_b=const_cast<Attrs&>(b);
      return atof(_a["superimpose-offset"].c_str()) - atof(_a["length"].c_str())/2 <= atof(_b["superimpose-offset"].c_str()) - atof(_b["length"].c_str())/2;
    }
  };

/****************************************************************************************************************
*						print_elmnt							*
*****************************************************************************************************************
function to write element in tcl file "file" from attrs map of attributes. last_element is an internal variable used to
verify if a drift need to be added between last and current element*/

  double print_elmnt(Attrs attrs,double last_element_pos, ostream &file)
  {  
    if(DEBUG) cerr<<"DEB:print_element"<<endl;
    double dist=atof(attrs["superimpose-offset"].c_str())-atof(attrs["length"].c_str())/2-last_element_pos;
    if(dist>1e-4) file<<"Drift -name AUTO -length "<<dist<<endl;

    std::vector<std::string> type;
    for(std::map<std::string,std::string>::iterator itr=attrs.begin();itr!=attrs.end(); ++itr)
    {
      if(itr->second=="true") 
      {
	type.push_back(itr->first);
      }
    }
    if (type.size()>1) 
    {
      cerr<<"\nWARNING : Unsuported multiple componant detected : ";
      for(int i=0;i<type.size();++i) 
      {
	cerr<<type[i]<<" ";
	attrs[type[i]]="";
      }
      cerr<<".\nFirst keeped for element : "<<attrs["name"];
    }

    if(attrs["girder"]=="true") file<<"Girder"<<endl;

    if(attrs["drift"]=="true") file<<"Drift -name "<<attrs["name"];

    if(attrs["ab_multipole"]=="true")
    {
      file<<"Multipole -name "<<attrs["name"];
     if(attrs["ab_multipole-an"]==attrs["ab_multipole-bn"] && attrs["ab_multipole-an"]!="" &&
          attrs["ab_multipole-an_value"]==attrs["ab_multipole-an_value"] && attrs["ab_multipole-an_value"]!="")
	file<<" -strength [expr "<<atof(attrs["ab_multipole-an_value"].c_str())*atof(attrs["lenght"].c_str())<<"*$e0]"
            <<" -type "<<attrs["ab_multipole-an"];
      if(attrs["methods-n_step"]!="") file<<" -steps "<<attrs["methods-n_step"];
      if(attrs["orientation-tilt"]!="") file<<" -tilt "<<attrs["orientation-tilt"];
    }

    if(attrs["bend"]=="true")
    {
//      if(attrs["bend-f_int1"]!="" && attrs["bend-h_gap1"]!="")
      file<<"Sbend -name "<<attrs["name"];
      if(attrs["bend-g"]!="") 
        file<<" -angle "<<atof(attrs["bend-g"].c_str())*atof(attrs["length"].c_str());
      if(attrs["bend-b"]!="") 
	file<<" -angle [expr "<<atof(attrs["bend-b"].c_str())*atof(attrs["length"].c_str())*e_charge/c_light<<"*$e0]";
      file<<" -e0 $e0";
      if(attrs["bend-e1"]!="") file<<" -E1 "<< attrs["bend-e1"];
      if(attrs["bend-e2"]!="") file<<" -E2 "<< attrs["bend-e2"];
      if(attrs["thick_multipole-k1"]!="") 
	file<<" -K [expr "<<atof(attrs["thick_multipole-k1"].c_str())*atof(attrs["length"].c_str())<<"*$e0]";
      if(attrs["thick_multipole-k2"]!="") 
	file<<" -K2 [expr "<<atof(attrs["thick_multipole-k2"].c_str())*atof(attrs["length"].c_str())<<"*$e0]";
      if(attrs["thick_multipole-B1"]!="") 
	file<<" -K [expr "<<atof(attrs["thick_multipole-B1"].c_str());
      if(attrs["thick_multipole-B2"]!="") 
	file<<" -K2 [expr "<<atof(attrs["thick_multipole-B2"].c_str());
      if(attrs["bend-f_int1"]!="" || attrs["bend-f_int2"]!="" || attrs["bend-h_gap1"]!="" || attrs["bend-h_gap2"]!="")
	cerr<<"\nWARNING : f_int or/and h_gap parameter(s) not translated";
//      if(attrs["bend-f_int1"]!="" && attrs["bend-h_gap1"]!="")
    }

    if(attrs["octupole"]=="true")
    {
      file<<"Multipole -name "<<attrs["name"];
      file<<" -type 4 ";
      if(attrs["octupole-k_value"]!="")
	file<<" -strength [expr "<<atof(attrs["octupole-k_value"].c_str())*atof(attrs["length"].c_str())<<"*$e0]";
      if(attrs["octupole-b_value"]!="")
	file<<" -strength "<<atof(attrs["octupole-b_value"].c_str());
      if(attrs["methods-n_step"]!="")
	file<<" -steps "<<attrs["methods-n_step"];
      if(attrs["orientation-tilt"]!="") file<<" -tilt ";
    }

    if(attrs["quadrupole"]=="true")
    {
      file<<"Quadrupole -name "<<attrs["name"];
      if(attrs["quadrupole-k_value"]!="")
	file<<" -strength [expr "<<atof(attrs["quadrupole-k_value"].c_str())*atof(attrs["length"].c_str())<<"*$e0]";
      if(attrs["quadrupole-b_value"]!="")
	file<<" -strength "<<atof(attrs["quadrupole-b_value"].c_str());
      if(attrs["orientation-tilt"]!="") file<<" -tilt ";
    }    

    if(attrs["sextupole"]=="true")
    {
      file<<"Multipole -name "<<attrs["name"];
      file<<" -type 3 ";
      if(attrs["sextupole-k_value"]!="")
	file<<" -strength [expr "<<atof(attrs["sextupole-k_value"].c_str())*atof(attrs["length"].c_str())<<"*$e0]";
      if(attrs["sextupole-b_value"]!="")
	file<<" -strength "<<atof(attrs["sextupole-b_value"].c_str());
      if(attrs["methods-n_step"]!="")
	file<<" -steps "<<attrs["methods-n_step"];
      if(attrs["orientation-tilt"]!="") file<<" -tilt ";
    }

    if(attrs["beambeam"]=="true") file<<"#Beambeam :\nDrift -name "<<attrs["name"];
    if(attrs["custom"]=="true")file<<"#custom :\nDrift -name "<<attrs["name"];
    if(attrs["elseparator"]=="true")file<<"#elseparator :\nDrift -name "<<attrs["name"];
    if(attrs["kicker"]=="true") file<<"#Kicker :\nDrift -name "<<attrs["name"];
    if(attrs["marker"]=="true") file<<"Marker -name "<<attrs["name"];
    if(attrs["match"]=="true") file<<"#Match :\nDrift -name "<<attrs["name"];

    if(attrs["multipole"]=="true") 
    {
      file<<"Multipole -name "<<attrs["name"];
      if(attrs["multipole-n_value"]!="") file<<" -type "<<attrs["multipole-n_value"];
      if(attrs["multipole-kl_value"]!="")
	file<<" -strength [expr "<<atof(attrs["multipole-kl_value"].c_str())*atof(attrs["length"].c_str())<<"*$e0]";
      if(attrs["multipole-bl_value"]!="")
	file<<" -strength "<<atof(attrs["multipole-bl_value"].c_str());
      if(attrs["methods-n_step"]!="")
	file<<" -steps "<<attrs["methods-n_step"];
      if(attrs["orientation-tilt"]!="") file<<" -tilt ";
    }

    if(attrs["patch"]=="true") file<<"#Patch :\nDrift -name "<<attrs["name"];
    if(attrs["rf_cavity"]=="true") file<<"#Rf_cavity :\nDrift -name "<<attrs["name"];

    if(attrs["solenoid"]=="true")
      file<<"#Solenoid not implemented :\nSolenoid -name "<<attrs["name"]; 

    if(attrs["taylor_map"]=="true") file<<"#Taylor_map :\nDrift -name "<<attrs["name"];
    if(attrs["thick_multipole"]=="true") file<<"#Thick_multipole :\nDrift -name "<<attrs["name"];
    if(attrs["wiggler"]=="true") file<<"#wiggler :\nDrift -name "<<attrs["name"];

    if(attrs["orientation-xoffset"]!="") file<<" -x "<<attrs["orientation-xoffset"];
    if(attrs["orientation-yoffset"]!="") file<<" -y "<<attrs["orientation-yoffset"];
    if(attrs["orientation-soffset"]!="") file<<" -s "<<attrs["orientation-soffset"];
    if(attrs["orientation-xpitch"]!="") file<<" -xp "<<attrs["orientation-xpitch"];
    if(attrs["orientation-ypitch"]!="") file<<" -yp "<<attrs["orientation-ypitch"];
    if(attrs["orientation-roll"]!="") file<<" -roll "<<attrs["orientation-roll"];
    if(attrs["aperture-x_limit"]!="") file<<" -aperture_x "<<attrs["aperture-x_limit"];
    if(attrs["aperture-y_limit"]!="") file<<" -aperture_y "<<attrs["aperture-y_limit"];
    if(attrs["aperture-shape"]=="ELLIPTICAL") file<<" -aperture_shape elliptic";
    if(attrs["aperture-shape"]=="RECTANGULAR") file<<" -aperture_shape rectangular";
    if(attrs["aperture-shape"]=="DIAMOND") file<<" -aperture_shape elliptic";

    if(attrs["length"]!="") file<<" -length "<<attrs["length"];

//prise en compte de la perte d'energie par rayonement synchrotron
    if(attrs["bend"]=="true")
      file<<"\nset e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]";

    file<<endl;

    return atof(attrs["superimpose-offset"].c_str()) + atof(attrs["length"].c_str())/2;
  }

/****************************************************************************************************************
*						process_orientation						*
*****************************************************************************************************************
function to set orientation of element pointed by node in attrs map of attributes.*/

  void process_orientation(UAPNode * node, Attrs &attrs)
  {
    if(DEBUG) cerr<<"DEB:process_orientation"<<endl;
    if(node==NULL)
    { 
   //   cout<<"No orientation"<<endl;
      return;
    }
    if(node->getName()!="orientation")
    {
      cerr<<"\nERROR : should be an orientation node, it is an "<<node->getName()<<" node : "<<attrs["name"];
      return;
    }
    if (node!=NULL)
    attrs["orientation-origin"] = node->getAttributeString("origin");
    if (node->getChildByName("x_offset")!=NULL)
      attrs["orientation-xoffset"] = node->getChildByName("x_offset")->getAttributeString("design");
    if (node->getChildByName("y_offset")!=NULL)
      attrs["orientation-yoffset"] = node->getChildByName("y_offset")->getAttributeString("design");
    if (node->getChildByName("s_offset")!=NULL)
      attrs["orientation-soffset"] = node->getChildByName("s_offset")->getAttributeString("design");
    if (node->getChildByName("x_pitch")!=NULL)
      attrs["orientation-xpitch"] = node->getChildByName("x_pitch")->getAttributeString("design");
    if (node->getChildByName("y_pitch")!=NULL)
      attrs["orientation-ypitch"] = node->getChildByName("y_pitch")->getAttributeString("design");
    if (node->getChildByName("tilt")!=NULL)
      attrs["orientation-tilt"] = node->getChildByName("tilt")->getAttributeString("design");
    if (node->getChildByName("roll")!=NULL)
      attrs["orientation-roll"] = node->getChildByName("roll")->getAttributeString("design");
    if(attrs["orientation-origin"]!="" && attrs["orientation-origin"]!="CENTER") 
      cerr<<"\nERROR : origin != CENTER not implemented";
  }

/****************************************************************************************************************
*						store_elemnt							*
*****************************************************************************************************************
function to set attribute of element pointed by elmnt_node in attrs map of attributes. If inherit==true no error 
is outputed for misssing attributes and drift type is not set by default.*/

  void store_elmnt(UAPNode * elmnt_node, Attrs &attrs, bool inherit=false)
  {
    if(DEBUG) cerr<<"DEB:store_elmnt"<<endl;
    AttributeList attributes = elmnt_node->getAttributes();
    for (AttributeListCIter ia = attributes.begin(); ia != attributes.end(); ia++) 
    {
      if(ia->getName()=="");
      else if(ia->getName()=="name")
      {
	attrs["name"]=ia->getValue();
      }else if(ia->getName()!="inherit" && ia->getName()!="ref")
	cerr<<"\nERROR : Unespected "<<ia->getName()<<"="<<ia->getValue();
    }  
    NodeList childrens = elmnt_node->getChildren();
    for(NodeListIter it =childrens.begin(); it!=childrens.end(); ++it)
    {
      if((*it)->getName()=="ab_multipole") 
      {
	attrs["ab_multipole"] = "true";
	if ((*it)->getChildByName("a")!=NULL)
	{
	  attrs["ab_multipole-an"] = (*it)->getChildByName("a")->getAttributeString("n");
	  attrs["ab_multipole-an_value"] = (*it)->getChildByName("a")->getAttributeString("design");
	}
	if ((*it)->getChildByName("b")!=NULL)
	{
	  attrs["ab_multipole-bn"] = (*it)->getChildByName("b")->getAttributeString("n");
	  attrs["ab_multipole-bn_value"] = (*it)->getChildByName("b")->getAttributeString("design");
	}
	if(attrs["ab_multipole-an"]==attrs["ab_multipole-bn"] 
             && attrs["ab_multipole-an_value"]==attrs["ab_multipole-bn_value"] 
	     && attrs["ab_multipole-an"]!="" && attrs["ab_multipole-an_value"]!="") ;
	else cerr<<"\nERROR : ab_multipole not compatible";
      }

      if((*it)->getName()=="beambeam")
      {
	attrs["beambeam"]="true";
	cerr<<"\nERROR : beambeam not implemented";
      }

      if((*it)->getName()=="bend") 
      {
	char angle[20]="";
	attrs["bend"]="true";
	if((*it)->getChildByName("g")!=NULL)
  	  attrs["bend-g"]=(*it)->getChildByName("g")->getAttributeString("design");
	if((*it)->getChildByName("B")!=NULL)
  	  attrs["bend-b"]=(*it)->getChildByName("B")->getAttributeString("design");
	if((*it)->getChildByName("e1")!=NULL)
  	  attrs["bend-e1"]=(*it)->getChildByName("e1")->getAttributeString("design");
	if((*it)->getChildByName("e2")!=NULL)
  	  attrs["bend-e2"]=(*it)->getChildByName("e2")->getAttributeString("design");
	if((*it)->getChildByName("f_int1")!=NULL)
  	  attrs["bend-f_int1"]=(*it)->getChildByName("f_int1")->getAttributeString("design");
	if((*it)->getChildByName("f_int2")!=NULL)
	  attrs["bend-f_int2"]=(*it)->getChildByName("f_int2")->getAttributeString("design");
	if((*it)->getChildByName("h_gap1")!=NULL)
	  attrs["bend-h_gap1"]=(*it)->getChildByName("h_gap1")->getAttributeString("design");
	if((*it)->getChildByName("h_gap2")!=NULL)
	  attrs["bend-h_gap2"]=(*it)->getChildByName("h_gap2")->getAttributeString("design");
	if(elmnt_node->getChildByName("length")!=NULL)
	  attrs["bend-length"]=elmnt_node->getChildByName("length")->getAttributeString("design");
	if(attrs["bend-length"].find_first_of("[]()")==attrs["bend-length"].npos 
            && attrs["bend-g"].find_first_of("[]()")==attrs["bend-g"].npos 
            && attrs["bend-b"].find_first_of("[]()")==attrs["bend-b"].npos);
	else cerr<<"\nERROR : problem in numerical evaluation :\n"<<attrs["bend-length"]<<" | "
        	 <<attrs["bend-g"]<<" | "<<attrs["bend-b"];
      }

      if((*it)->getName()=="custom")
      {
	attrs["custom"]="true";
	cerr<<"\nERROR : custom not implemented";
      }

      if((*it)->getName()=="elseparator")
      {
	attrs["elseparator"]="true";
	cerr<<"\nERROR : elseparator not implemented";
      }

      if((*it)->getName()=="kicker")
      {
	attrs["kicker"]="true";
	cerr<<"\nERROR : kicker not implemented";
      }

      if((*it)->getName()=="marker")
      {
	attrs["marker"]="true";
      }

      if((*it)->getName()=="match")
      {
	attrs["match"]="true";
	cerr<<"\nERROR : match not implemented";
      }

      if((*it)->getName()=="multipole") 
      {
	attrs["multipole"]="true";
	if((*it)->getChildByName("kl")!=NULL)
	  attrs["multipole-kl_value"]=(*it)->getChildByName("kl")->getAttributeString("design");
	if((*it)->getChildByName("Bl")!=NULL)
	  attrs["multipole-bl_value"]=(*it)->getChildByName("Bl")->getAttributeString("design");
	if((*it)->getChildByName("n")!=NULL)
	  attrs["multipole-n_value"]=(*it)->getChildByName("n")->getAttributeString("design");
	if(attrs["multipole-kl_value"]!="");
	else if(attrs["multipole-bl_value"]!="");
	else if(!inherit)
	  cerr<<"\nWARNING : strength not found for element : "<<elmnt_node->getAttributeString("name");      
      }

      if((*it)->getName()=="octupole")
      {
	attrs["octupole"]="true";
	process_orientation((*it)->getChildByName("orientation"), attrs);
	if((*it)->getChildByName("k")!=NULL)
	  attrs["octupole-k_value"]=(*it)->getChildByName("k")->getAttributeString("design");
	if((*it)->getChildByName("B")!=NULL)
	  attrs["octupole-b_value"]=(*it)->getChildByName("B")->getAttributeString("design");
	if(attrs["octupole-k_value"]!="");
	else if(attrs["octupole-b_value"]!="");
	else if(!inherit)
	  cerr<<"\nWARNING : strength not found for element : "<<elmnt_node->getAttributeString("name");      
       }

      if((*it)->getName()=="patch")
      {
	attrs["path"]="true";
	cerr<<"\nERROR : path not implemented";
      }

      if((*it)->getName()=="quadrupole")
      {
	attrs["quadrupole"]="true";
	process_orientation((*it)->getChildByName("orientation"), attrs);
	if((*it)->getChildByName("k")!=NULL)
	  attrs["quadrupole-k_value"]=(*it)->getChildByName("k")->getAttributeString("design");
	if((*it)->getChildByName("B")!=NULL)
	  attrs["quadrupole-b_value"]=(*it)->getChildByName("B")->getAttributeString("design");
	if(attrs["quadrupole-k_value"]!="");
	else if(attrs["quadrupole-b_value"]!="");
	else if(!inherit)
	  cerr<<"\nWARNING : strength not found for element : "<<elmnt_node->getAttributeString("name");      
       }

      if((*it)->getName()=="rf_cavity")
      {
	attrs["rf_cavity"]="true";
	cerr<<"\nERROR : rf_cavity not implemented";
      }

      if((*it)->getName()=="sextupole")
      {
	attrs["sextupole"]="true";
	process_orientation((*it)->getChildByName("orientation"), attrs);
	if((*it)->getChildByName("k")!=NULL)
	  attrs["sextupole-k_value"]=(*it)->getChildByName("k")->getAttributeString("design");
	if((*it)->getChildByName("B")!=NULL)
	  attrs["sextupole-b_value"]=(*it)->getChildByName("B")->getAttributeString("design");
	if(attrs["sextupole-k_value"]!="");
	else if(attrs["sextupole-b_value"]!="");
	else if(!inherit)
	  cerr<<"\nWARNING : strength not found for element : "<<elmnt_node->getAttributeString("name");      
       }

      if((*it)->getName()=="solenoid")
      {
	attrs["solenoid"]="true";
	cerr<<"\nERROR : solenoid not implemented";
      }

      if((*it)->getName()=="taylor_map")
      {
	attrs["taylor_map"]="true";
	cerr<<"\nERROR : taylor_map not implemented";
      }

      if((*it)->getName()=="thick_multipole")
      {
	if((*it)->getChildByName("k")!=NULL)
	{
	  if((*it)->getChildByName("k")->getAttributeString("n")=="1")
            attrs["thick_multipole-k1"]=(*it)->getChildByName("k")->getAttributeString("design");
	  else if((*it)->getChildByName("k")->getAttributeString("n")=="2")
            attrs["thick_multipole-k2"]=(*it)->getChildByName("k")->getAttributeString("design");
	  else if((*it)->getChildByName("k")->getAttributeString("n")!="")
            cerr<<"\nERROR : thick_multipole is just implemented for n=1 & n=2";
	}
	if((*it)->getChildByName("B")!=NULL)
	{
	  if((*it)->getChildByName("B")->getAttributeString("n")=="1")
            attrs["thick_multipole-B1"]=(*it)->getChildByName("B")->getAttributeString("design");
	  else if((*it)->getChildByName("B")->getAttributeString("n")=="2")
            attrs["thick_multipole-B2"]=(*it)->getChildByName("B")->getAttributeString("design");
	  else if((*it)->getChildByName("B")->getAttributeString("n")!="")
            cerr<<"\nERROR : thick_multipole is just implemented for n=1 & n=2";
	}
      }

      if((*it)->getName()=="wiggler")
      {
	attrs["wiggler"]="true";
	cerr<<"\nERROR : wiggler not implemented";
      }

      if(!inherit)
      {
	int nb_type=0;
	for(std::map<std::string,std::string>::iterator itr=attrs.begin();itr!=attrs.end(); ++itr)
	{
	  if(itr->second=="true") nb_type++;
	}  
	if(nb_type==0) attrs["drift"] ="true";
      }

//			  General attributes

      if((*it)->getName()=="aperture")
      {
	if((*it)->getChildByName("x_limit")!=NULL)
	  attrs["aperture-x_limit"]=(*it)->getChildByName("x_limit")->getAttributeString("design");
	if((*it)->getChildByName("y_limit")!=NULL)
	  attrs["aperture-y_limit"]=(*it)->getChildByName("y_limit")->getAttributeString("design");
	if((*it)->getChildByName("xy_limit")!=NULL)
	  attrs["aperture-xy_limit"]=(*it)->getChildByName("xy_limit")->getAttributeString("design");
	attrs["aperture-at"]=(*it)->getAttributeString("at");
	attrs["aperture-shape"]=(*it)->getAttributeString("shape");
	attrs["aperture-side"]=(*it)->getAttributeString("side");
      }
      if((*it)->getName()=="description")
      {
	if((*it)->getChildByName("type")!=NULL)
	  attrs["description-type"]=(*it)->getChildByName("type")->getAttributeString("tag");
	if((*it)->getChildByName("alias")!=NULL)
	  attrs["description-alias"]=(*it)->getChildByName("alias")->getAttributeString("tag");
	if((*it)->getChildByName("ref")!=NULL)
	  attrs["description-ref"]=(*it)->getChildByName("ref")->getAttributeString("tag");
	if((*it)->getChildByName("doc")!=NULL)
	  attrs["description-doc"]=(*it)->getChildByName("doc")->getAttributeString("tag");
	if((*it)->getChildByName("mad_element")!=NULL)
	  attrs["description-mad_element"]=(*it)->getChildByName("mad_element")->getAttributeString("tag");
      }
      if((*it)->getName()=="doc");
      if((*it)->getName()=="field_table");
      if((*it)->getName()=="floor");
      if((*it)->getName()=="instrument");
      if((*it)->getName()=="length") attrs["length"]=(*it)->getAttributeString("design") ;
      if((*it)->getName()=="methods")
      {
	if((*it)->getChildByName("tracking")!=NULL)
	  attrs["methods-tracking"] = (*it)->getChildByName("tracking")->getAttributeString("value");
	if((*it)->getChildByName("transfer_map_calc")!=NULL)
	  attrs["methods-transfer_map_calc"] = (*it)->getChildByName("transfer_map_calc")->getAttributeString("value");
	if((*it)->getChildByName("n_step")!=NULL)
	  attrs["methods-n_step"] = (*it)->getChildByName("n_step")->getAttributeString("value");
	if((*it)->getChildByName("relative_tolerance")!=NULL)
	  attrs["methods-relative_tolerance"] = (*it)->getChildByName("relative_tolerance")->getAttributeString("value");
	if((*it)->getChildByName("absolute_tolerance")!=NULL)
	  attrs["methods-absolute_tolerance"] = (*it)->getChildByName("absolute_tolerance")->getAttributeString("value");
	if((*it)->getChildByName("integrator_order")!=NULL)
	  attrs["methods-integrator_order"] = (*it)->getChildByName("integrator_order")->getAttributeString("value");
      }
      if((*it)->getName()=="orientation")
      {
	process_orientation(*it, attrs);
      }
      if((*it)->getName()=="state") 
	attrs["state-is_on"] = (*it)->getAttributeString("is_on");

      if((*it)->getName()=="superimpose") 
      {
	attrs["superimpose-offset"]= (*it)->getAttributeString("offset");
	attrs["superimpose-ref_element"]= (*it)->getAttributeString("ref_element");
	attrs["superimpose-ele_origin"]= (*it)->getAttributeString("ele_origin");
	attrs["superimpose-ref_origin"]= (*it)->getAttributeString("ref_origin");
      }
    }
  }

/****************************************************************************************************************
*							look_sector						*
*****************************************************************************************************************
function which return a pointer to the sector named name going down the tree from curent_node. If not found 
return NULL. args==true disable verbose, level is an internal variable used for recursivity.*/

  UAPNode * look_sector(UAPNode* curent_node, std::string name, bool args=false, int level=0)
  {
    if(DEBUG) cerr<<"DEB:look_sector "<<name<<" in "
                  <<curent_node->getAttributeString("name")<<endl;
    UAPNode * result = NULL;
    if(curent_node->getName()=="sector")
    {
      AttributeList attributes = curent_node->getAttributes();
      for (AttributeListCIter ia = attributes.begin(); ia != attributes.end(); ia++) 
	if(ia->getName()=="name" && ia->getValue()==name) 
          return curent_node;
    }
    NodeList children = curent_node->getChildren();
    for(NodeListIter it =children.begin(); it!=children.end(); ++it)
    {
      result = look_sector(*it, name, args, level+1);
      if (result != NULL)
      {
	return result;
      }
    }
    if(level==0 && result==NULL)
    {
      if(!args) cerr<<"\nERROR: sector "<<name<<" not found!";
      return curent_node;
    }    
    return result;
  }

/****************************************************************************************************************
*							look_element						*
*****************************************************************************************************************
function which return a pointer to the element named name going down the tree from curent_node. If not found 
return NULL. args==true disable verbose, level is an internal variable used for recursivity.*/

  UAPNode * look_element(UAPNode* curent_node, std::string name, bool args=false, int level=0)
  {
    if(DEBUG) cerr<<"DEB:lookelement"<<endl;
    UAPNode * result = NULL;
    if(curent_node->getName()=="element")
    {
      AttributeList attributes = curent_node->getAttributes();
      for (AttributeListCIter ia = attributes.begin(); ia != attributes.end(); ia++) 
	if(ia->getName()=="name" && ia->getValue()==name)
          return curent_node;
    }
    NodeList children = curent_node->getChildren();
    for(NodeListIter it =children.begin(); it!=children.end(); ++it)
    {
      result = look_element(*it, name, args, level+1);
      if (result != NULL) return result;
    }
    if(level ==0 && result==NULL )
    {
      if(!args)cerr<<"\nERROR: element "<<name<<" not found!";
      return curent_node;
    }
    else return result;
  }


/****************************************************************************************************************
*							getLabNode						*
*****************************************************************************************************************
function which return a pointer to the lab node going up the tree from node. If not found return NULL.*/

  UAPNode * getLabNode(UAPNode * node)
  {
    if(DEBUG) cerr<<"DEB:getlabnode"<<endl;
    UAPNode *return_node=NULL;
    if (node->getName()=="laboratory") 
    {
      return node;
    }
    UAPNode *parent=node->getParent();
    if(parent==NULL) return node;
    else return_node=getLabNode(parent);
    return return_node;
  }

/****************************************************************************************************************
*							process_element						*
*****************************************************************************************************************
function filling attrs map for the element pointed by elemnt_node. Manages inherit et reference.*/

  void process_element(UAPNode *elmnt_node, std::map<std::string,std::string> &attrs)
  {
    if(DEBUG) cerr<<"DEB:process_element"<<endl;
    if(elmnt_node->getName()!="element") 
      cerr<<"\nERROR : "<<elmnt_node->getAttributeString("name")
          <<" expected to be element but is "<<elmnt_node->getName();
    AttributeList attributes = elmnt_node->getAttributes();
    UAPNode * lab = getLabNode(elmnt_node);
    for (AttributeListCIter ia = attributes.begin(); ia != attributes.end(); ia++) 
    {
      if(ia->getName() == "name" && elmnt_node->getAttributeString("inherit")=="") 
      {
	store_elmnt(elmnt_node, attrs);
      }
      else if(ia->getName() == "ref")
      { 
	store_elmnt(elmnt_node, attrs, true);
	process_element(look_element(lab,ia->getValue()), attrs);
      }
      else if(ia->getName() == "inherit")
      {
	UAPNode * herit_node = look_element(lab,ia->getValue());
	store_elmnt(herit_node, attrs, true);
	store_elmnt(elmnt_node, attrs);
      } 
      else if(ia->getName() != "name")
        cerr<<"\nERROR : "<<ia->getName()<<" attribute unknow for element : "
	    <<elmnt_node->getAttributeString("name");
    }
  }

/****************************************************************************************************************
*							process_sector						*
*****************************************************************************************************************
function which develop the layout from aml_rep. It calls process_element for each element of the sector. 
Manages multi-level sectors, multi-level reflections, references, multi-level args to replace elements by others 
(look AML documentation for futher information about argument of sectors)*/

  void process_sector(UAPNode *aml_rep,std::vector<Attrs> &line,
          std::vector <std::string> ref_args=std::vector<std::string>(),
          std::vector <std::string> name_args=std::vector<std::string>(),bool reflect = false)
  {
    if(DEBUG) cerr<<"DEB:process_sector : "<<aml_rep->getAttributeString("name")<<endl;
    if(aml_rep->getName()!="sector") 
      cerr<<"\nERROR : "<<aml_rep->getAttributeString("name")
          <<" expected to be sector but is "<<aml_rep->getName();
    NodeList elements=aml_rep->getChildren();
    UAPNode *lab = getLabNode(aml_rep);
    if(lab->getName()!="laboratory") cerr<<"laboratory node not found ..."<<endl;
    std::map<std::string,std::string> attrs;
    std::string args_replaced=aml_rep->getAttributeString("args");
    if(args_replaced != "")
      for(int i=0; i<args_replaced.length(); i++)
	if(args_replaced[i]==' ') 
	  args_replaced=args_replaced.substr(0,i)+args_replaced.substr(i+1);
    if(args_replaced!="")
    {
      for(int i=0; i<args_replaced.length(); i++)
      {
	int pos = args_replaced.find_first_of(',',i+1);
	name_args.push_back(args_replaced.substr(i,pos-i));
	if(pos<0) break;
	i=pos;
      }
    }
    if(aml_rep->getAttributeString("reflection")=="true" != reflect)
    {
//reflected sector
      for(NodeListRevIter it =elements.rbegin(); it!=elements.rend(); ++it)
      {
	attrs.clear();
	if ((*it)->getName()=="element")
	{
	  if(ref_args.size()==0 ) 
	  {
	    process_element(*it,attrs);
	    line.push_back(attrs);
	    continue;
	  }else
	  {
	    if(ref_args.size() != name_args.size())
	    {
	      cerr<<"\nERROR : number of args do not match ("<<ref_args.size() <<"!="
	          <<name_args.size() <<") for sector : "<<aml_rep->getAttributeString("name")
	          <<"->"<<(*it)->getAttributeString("ref");
	      process_element(*it,attrs);
	      line.push_back(attrs);
	      continue;
	    }
	    bool exchange=false;
	    for(int i=0;i<name_args.size();i++)
	    {
	      if(name_args[i]==(*it)->getAttributeString("ref"))
	      {
        	exchange=true;
		UAPNode *element_exchanged = look_element(lab,ref_args[i],true);
		if(element_exchanged == lab) element_exchanged = look_sector(lab,ref_args[i],true);
		int k=i;
		while(element_exchanged == lab && k>0)
		{
                  int j=k-1;
	          for(j=k-1;j>=0;j--)
		  {
		    if(ref_args[k]==name_args[j])
		    {
		      if(element_exchanged == lab) element_exchanged = look_element(lab,ref_args[j],true);
		      if(element_exchanged == lab) element_exchanged = look_sector(lab,ref_args[j], true);
		    }
		  }
		  k=j;
		}
                if(element_exchanged == lab)
		{
		  cerr<<"\nERROR: unresolved "<<(*it)->getAttributeString("ref");
		  continue;
		} 
		if(element_exchanged->getName()=="element")
		  process_element(element_exchanged, attrs);
		else if(element_exchanged->getName()=="sector")
		  process_sector(element_exchanged, line, ref_args, name_args);
		else 
		{
		  cerr<<"\nERROR: "
		      <<element_exchanged->getAttributeString("name")
		      <<"should be element or sector";
        	  continue;
		}
		line.push_back(attrs);
		continue;
	      }
	    }
	    if(!exchange)
	    {
	      process_element(*it, attrs);
	      line.push_back(attrs);
	      continue;
	    }
	  }
	}else if ((*it)->getName()=="sector")
	{
	  std::string repeat_str = (*it)->getAttributeString("repeat");
	  std::string args = (*it)->getAttributeString("args");
	  if(repeat_str == "") repeat_str="1";
	  int repeat = atoi(repeat_str.c_str());

	  if((*it)->getAttributeString("girder")!="")
	  {
	    attrs["girder"]="true";
	    line.push_back(attrs);
	    continue;
	  }

	  if((*it)->getAttributeString("ref")!="")
	  {
	    std::string ref = (*it)->getAttributeString("ref");
	    if(args!="")
	      for(int i=0; i<args.length(); i++)
		if(args[i]==' ') 
		  args=args.substr(0,i)+args.substr(i+1);
	    vector <std::string> cp_ref_args=ref_args;
	    if(args!="")
	    {
	      for(int i=0; i<args.length(); i++)
	      {
		int pos = args.find_first_of(',',i+1);
		cp_ref_args.push_back(args.substr(i,pos-i));
		if(pos<0) break;
		i=pos;
	      }
	    }
	    vector <std::string> cp2_ref_args, cp_name_args;
	    for(int i=0;i<repeat;i++)
	    {
	      cp2_ref_args=cp_ref_args;
	      cp_name_args=name_args;
	      process_sector(look_sector(lab, ref), line, cp2_ref_args, cp_name_args);
	    }
	  }else
	  {
	    vector <std::string> cp_ref_args, cp_name_args;
	    for(int i=0;i<repeat;i++)
	    {
	      cp_ref_args=ref_args;
	      cp_name_args=name_args;
	      process_sector(*it, line, cp_ref_args, cp_name_args);
	    }
	  }
	  AttributeList attributes = (*it)->getAttributes();
	  for (AttributeListCIter ia = attributes.begin(); ia != attributes.end(); ia++) 
	    if (ia->getName()!= "repeat" && ia->getName()!="girder" && ia->getName()!="ref" 
		&& ia->getName()!="args" && ia->getName()!="name" && ia->getName()!="reflection") 
	      cerr<<"\nERROR : "<<ia->getName()<<" attribute unknow for sector : "
		  <<(*it)->getAttributeString("name");

	  if((*it)->getAttributeString("girder")!="") line.push_back(attrs);
	}
      }
    }else
    {
//non-reflected sector
      for(NodeListIter it =elements.begin(); it!=elements.end(); ++it)
      {
	attrs.clear();
	if ((*it)->getName()=="element")
	{
	  if(ref_args.size()==0 ) 
	  {
	    process_element(*it,attrs);
	    line.push_back(attrs);
	    continue;
	  }else
	  {
	    if(ref_args.size() != name_args.size())
	    {
	      cerr<<"\nERROR : number of args do not match ("<<ref_args.size() <<"!="
	          <<name_args.size() <<") for sector : "<<aml_rep->getAttributeString("name")
	          <<"->"<<(*it)->getAttributeString("ref");
	      process_element(*it,attrs);
	      line.push_back(attrs);
	      continue;
	    }
	    bool exchange=false;
	    for(int i=0;i<name_args.size();i++)
	    {
	      if(name_args[i]==(*it)->getAttributeString("ref"))
	      {
        	exchange=true;
		UAPNode *element_exchanged = look_element(lab,ref_args[i],true);
		if(element_exchanged == lab) element_exchanged = look_sector(lab,ref_args[i],true);
		int k=i;
		while(element_exchanged == lab && k>0)
		{
                  int j=k-1;
	          for(j=k-1;j>=0;j--)
		  {
		    if(ref_args[k]==name_args[j])
		    {
		      if(element_exchanged == lab) element_exchanged = look_element(lab,ref_args[j],true);
		      if(element_exchanged == lab) element_exchanged = look_sector(lab,ref_args[j],true);
		    }
		  }
		  k=j;
		}
                if(element_exchanged == lab)
		{
		  cerr<<"\nERROR: unresolved "<<(*it)->getAttributeString("ref");
		  continue;
		} 
		if(element_exchanged->getName()=="element")
		  process_element(element_exchanged, attrs);
		else if(element_exchanged->getName()=="sector")
		  process_sector(element_exchanged, line, ref_args, name_args);
		else 
		{
		  cerr<<"\nERROR: "
		      <<element_exchanged->getAttributeString("name")
		      <<"should be element or sector";
		  continue;
		}
        	line.push_back(attrs);
	      }
	    }
	    if(!exchange)
	    {
	      process_element(*it, attrs);
	      line.push_back(attrs);
	      continue;
	    }
	  }
	}else if ((*it)->getName()=="sector")
	{
	  std::string repeat_str = (*it)->getAttributeString("repeat");
	  std::string args = (*it)->getAttributeString("args");
	  if(repeat_str == "") repeat_str="1";
	  int repeat = atoi(repeat_str.c_str());

	  if((*it)->getAttributeString("girder")!="")
	  {
	    attrs["girder"]="true";
	    line.push_back(attrs);
	    continue;
	  }

	  if((*it)->getAttributeString("ref")!="")
	  {
	    std::string ref = (*it)->getAttributeString("ref");
	    if(args!="")
	      for(int i=0; i<args.length(); i++)
		if(args[i]==' ') 
		  args=args.substr(0,i)+args.substr(i+1);
	    vector <std::string> cp_ref_args=ref_args;
	    if(args!="")
	    {
	      for(int i=0; i<args.length(); i++)
	      {
		int pos = args.find_first_of(',',i+1);
		cp_ref_args.push_back(args.substr(i,pos-i));
		if(pos<0) break;
		i=pos;
	      }
	    }
	    vector <std::string> cp2_ref_args, cp_name_args;
	    for(int i=0;i<repeat;i++)
	    {
	      cp2_ref_args=cp_ref_args;
	      cp_name_args=name_args;
	      UAPNode *look=look_sector(lab, ref);
	      process_sector(look, line, cp2_ref_args, cp_name_args);
	    }
	  }else
	  {
	    vector <std::string> cp_ref_args, cp_name_args;
	    for(int i=0;i<repeat;i++)
	    {
	      cp_ref_args=ref_args;
	      cp_name_args=name_args;
	      process_sector(*it, line, cp_ref_args, cp_name_args);
	    }
	  }
	  AttributeList attributes = (*it)->getAttributes();
	  for (AttributeListCIter ia = attributes.begin(); ia != attributes.end(); ia++) 
	    if (ia->getName()!= "repeat" && ia->getName()!="girder" && ia->getName()!="ref" 
		&& ia->getName()!="args" && ia->getName()!="name" && ia->getName()!="reflection") 
	      cerr<<"\nERROR : "<<ia->getName()<<" attribute unknow for sector : "
		  <<(*it)->getAttributeString("name");

	  if((*it)->getAttributeString("girder")!="") line.push_back(attrs);
	}
      }
    }
  }

/****************************************************************************************************************
*						ExpandAMLTree							*
*****************************************************************************************************************
function which creates the line vector (look process_sector function) and look for the sector names "sector" going 
down the tree from aml_rep. Return true if sector found and false otherwise.*/
  
  bool ExpandAMLTree(UAPNode *aml_rep,std::string sector,std::vector<Attrs> &line)
  {
    bool ok = true;

    string node_name = aml_rep->getName();
//    cout<<"element de type : "<<aml_rep->getName()<<endl;
    if (aml_rep->getName() == "sector")
    {
      AttributeList attributes = aml_rep->getAttributes();
      for (AttributeListCIter ia = attributes.begin(); ia != attributes.end(); ia++) 
      {
	if(ia->getName()=="name" && ia->getValue()==sector)
	{
       //   -liste
          cout<<"Sector "<<sector<<" found !\n";
	  process_sector(aml_rep,line);
  //	-sorting de la liste
  //	-on place le drifts qui manquent
  //	-scansion de la liste 
  //	     on imprime les attributes
	}
//	else cout<<"skip "<<ia->getValue()<<endl;
      }
    }
    NodeList children = aml_rep->getChildren();
    for(NodeListIter it =children.begin(); it!=children.end(); ++it)
    {
      if (!ExpandAMLTree(*it, sector,line)) ok = false;
    }   
    return ok;
  }


/****************************************************************************************************************
*						AMLRepToPLACETFile						*
****************************************************************************************************************/

int tk_ImportLatticeAML(ClientData clientdata,Tcl_Interp *interp,int argc, const char *argv[])
{
  char *file_name_ptr=NULL;
  char *line_name_ptr=NULL;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: ImportLatticeAML -file file.mad -line line_to_import"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name_ptr,
     (char*)"File to load"},
    {(char*)"-line",TK_ARGV_STRING,(char*)NULL,
     (char*)&line_name_ptr,
     (char*)"Name of the line to be imported"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))!=TCL_OK){
    return error;
  } 
  if (!file_name_ptr) {
    Tcl_SetResult(interp,"You need to specify the file name", TCL_VOLATILE);
    return TCL_ERROR;
  }

  string file_name = file_name_ptr;
  string line_name = line_name_ptr ? line_name_ptr : file_name_ptr;
  
  cerr.precision(15);

  // Read in file and make an X representation and an AML representation tree.
  MADCore* reader;
  if (file_name.find(".madx") != string::npos) {
    reader = new MADXParser();
  } else if (file_name.find(".bmad") != string::npos) {
    reader = new BmadParser();
  } else if (file_name.find(".mad8")!= string::npos ) {
    reader = new MAD8Parser();
  } else if (file_name.find(".xsif") != string::npos){
    reader = new XSIFParser();
  } else {
    Tcl_SetResult(interp,"Unknown file extension. Supported extentions are : madx|bmad|mad8|xsif", TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  cout<<"read file \""<<file_name<<"\" and fill struture ... "<<endl;

  UAPNode* UAPRoot = reader->XFileToAMLRep (file_name);

  cout<<"done"<<endl;
  
  string::size_type ext=file_name.find_last_of(".");
  file_name.erase(ext);
  file_name.append(".aml0");

  AMLReader * aml_reader;
  aml_reader->AMLRepToAMLFile (UAPRoot,file_name,false);
  
  cout<<"removings comments ..."<<endl;
  
  std::string command="cat ";
  command += file_name;
  command += " | sed -e 's/&quote;/``/g'| grep -v '<\\/\\?xi:include'> tmp; mv tmp ";
  command += file_name;
  printf("%s\n", command.c_str());
  system(command.c_str());

  cout<<"\nEVALUATE !!!!!!!!\n";

  UAPNode* root = aml_reader->AMLFileToAMLRep(file_name);
  UAPUtilities utilities(root);
  utilities.evaluate();
     
  cout<<"\nEND EVALUATE !!!!!!!\n"<<endl;

  ext=file_name.find_last_of(".");
  file_name.erase(ext);
  file_name.append(".aml");
 
  aml_reader->AMLRepToAMLFile (root,file_name); 
  
    if (root->getName() == "UAP_root") 
      root = root->getChildByName("AML_representation");

    if (!root || (root->getName() != "AML_representation")) {
      cerr<<"root argument is not <AML_representation> or its parent"<<endl;
      return 0;
    }

    UAPNode* lab = root->getChildByName("laboratory");

    if (!lab || lab->getName() != "laboratory") {
      cerr<<"lab argument is not <laboratory> or its parent"<<endl;
      return 0;
    }

    std::vector<Attrs> line;

    ExpandAMLTree(lab,line_name,line);
    bool need_sort = false;
    for(int i=0;i<line.size();i++) if ((line[i])["superimpose-offset"]!="") need_sort=true;
    if(need_sort)std::sort(line.begin(), line.end(), cmpAttr());
  
  
    
  ostringstream element_str;
  

  double end_current_line,end_previous_line=0;
  Tcl_Eval(interp, "Girder");
  Tcl_Eval(interp, "SetReferenceEnergy $e0");
  for (int i=0; i<line.size(); i++) 
  {
    Attrs &attrs=line[i];
    end_current_line = print_elmnt(attrs,end_previous_line, element_str);
    
    //std::cerr << element_str.str() << std::endl;
    
    Tcl_Eval(interp, const_cast<char*>(element_str.str().c_str()));
        
    end_previous_line = end_current_line;
  }
  
  return TCL_OK;
}


int
Importlatticeaml_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"ImportLatticeAML",tk_ImportLatticeAML,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"ImportLatticeAML","0.1");
  return TCL_OK;
}

