#ifndef LIBXSIF_H
#define LIBXSIF_H

#include "libxsif_helper.h"

#ifdef __cplusplus  /*  C++ compatibility  */
extern "C" {
#endif

enum {
	MAD_DRIFT = 1 ,  MAD_RBEND = 2 , MAD_SBEND = 3 ,
	MAD_WIGG  = 4 ,  MAD_QUAD  = 5 , MAD_SEXT  = 6 ,
	MAD_OCTU  = 7 ,  MAD_MULTI = 8 , MAD_SOLN  = 9 ,
	MAD_RFCAV = 10,  MAD_SEPA  = 11, MAD_ROLL  = 12,
	MAD_ZROT  = 13,  MAD_HKICK = 14, MAD_VKICK = 15,
	MAD_HMON  = 16,  MAD_VMON  = 17, MAD_MONI  = 18, 
	MAD_MARK  = 19,  MAD_ECOLL = 20, MAD_RCOLL = 21,
	MAD_QUSE  = 22,  MAD_GKICK = 23, MAD_ARBIT = 24,
	MAD_MTWIS = 25,  MAD_MATR  = 26, MAD_LCAV =  27,
	MAD_INST =  28,  MAD_BLMO  = 29, MAD_PROF =  30,
	MAD_WIRE =  31,  MAD_SLMO  = 32, MAD_IMON =  33,
	MAD_DIMU =  34,  MAD_YROT  = 35, MAD_SROT =  36,
	MAD_BET0 =  37,  MAD_BEAM  = 38, MAD_KICKMAD = 39
};

static const char *MAD_ELEM_NAMES[40] = {
	NULL,
	"DRIFT",	"RBEND",	"SBEND",
	"WIGG",  	"QUAD", 	"SEXT",
	"OCTU",  	"MULTI", 	"SOLN",
	"RFCAV",  	"SEPA", 	"ROLL",
	"ZROT",  	"HKICK", 	"VKICK",
	"HMON",  	"VMON", 	"MONI", 
	"MARK",  	"ECOLL", 	"RCOLL",
	"QUSE",  	"GKICK", 	"ARBIT",
	"MTWIS",  	"MATR", 	"LCAV" ,
	"INST" ,  	"BLMO", 	"PROF" ,
	"WIRE" ,  	"SLMO", 	"IMON" ,
	"DIMU" ,  	"YROT", 	"SROT" ,
	"BET0" ,  	"BEAM", 	"KICKMAD"
};

extern signed int xsif_io_setup_(char *, char *, char *, signed int *, signed int *, signed int *, signed int *, unsigned int *, unsigned int *, int, int, int );
extern signed int xsif_cmd_loop_(void * );
extern void	  xsif_io_close_();

extern signed int parchk_(unsigned int * );
extern void	  parord_(unsigned int * );
extern void	  parevl_();

extern signed int xsif_elements_MP_npos1;
extern signed int xsif_elements_MP_npos2;


/*
extern void xsif_size_pars_
extern void xsif_inout_
extern void xsif_constants_
extern void xsif_elem_pars_
extern void xsif_elements_
extern void xsif_interfaces_
extern void xsif_header_
extern void xsif_io_close_
extern void xsif_allocate_initial_
extern void xsif_mem_manage_
extern void xsif_release_mem_
extern void xsif_stack_search_
*/

#ifdef __cplusplus  /*  C++ compatibility  */
}
#endif

#endif /* LIBXSIF_H */
 
