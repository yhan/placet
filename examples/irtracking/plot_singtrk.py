import pylab as p


data=p.loadtxt('singtrk.dat')
ildata=p.loadtxt('ildantinobuck.txt')

s=data[:,3]
xmin=max([min(s),min(-ildata[:,0])])
xmax=min([max(s),max(-ildata[:,0])])

def reduce(x,xmin,xmax,y=None):
    i=0
    while(x[i]<xmin):
        i+=1
    j=i
    while(x[j]<xmax):
        j+=1
    if y!=None:
        return x[i:j],y[i:j]
    return x[i:j]

s,data=reduce(s,xmin,xmax,data)
x=data[:,1]
y=data[:,2]
b=data[:,6:9]
s_ild,ildata=reduce(ildata[:,0],-xmax,-xmin,ildata)

p.plot(s,x,label='x')
#p.plot(s,y,label='y')
p.legend()

p.figure()
p.plot(s,b[:,0],label=r'$B_x$')
p.plot(s,b[:,1],label=r'$B_y$')
p.plot(s,b[:,2],label=r'$B_z$')
p.legend()

p.figure()
#plot 'ildantinobuck.txt' u (8.24-$1):3 w l t 'B_r', '' u (8.24-$1):4 w l ls 3 t 'B_z'
p.plot(-ildata[:,0],ildata[:,2],label=r'$B_r$')
p.plot(-ildata[:,0],ildata[:,3],label=r'$B_z$')
p.title('Original field map...')
p.legend()

p.show()
