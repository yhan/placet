Girder
Drift -name "OCTDRIFT" -length 2 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.248412 -strength [expr -3.026746849*$e0] -aperture_shape elliptic -aperture_x 0.00483 -aperture_y 0.00483
Girder
Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.248412 -strength [expr -3.026746849*$e0] -aperture_shape elliptic -aperture_x 0.00483 -aperture_y 0.00483
Girder
Drift -name "LX0" -length 0.248412 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.62834066 -strength [expr 0.06588700141*$e0] -aperture_shape elliptic -aperture_x 0.00469 -aperture_y 0.00469
# WARNING: Multipole options not defined. Multipole type 0 with 0 strength added (tracked as a drift of length 0).
Girder
Multipole -name "OCTF1" -synrad $mult_synrad -type 0 -length 0 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.62834066 -strength [expr 0.06588700141*$e0] -aperture_shape elliptic -aperture_x 0.00469 -aperture_y 0.00469
Girder
Drift -name "D1OD" -length 0.48412 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "OCTDRIFT" -length 2 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "DEC0" -synrad $mult_synrad -type 4 -length 0.496824 -strength [expr -1.0*-135.6855204*$e0] -aperture_shape elliptic -aperture_x 0.00359 -aperture_y 0.00359
Girder
Drift -name "LX0" -length 0.248412 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.248412 -strength [expr 10.89294757*$e0] -aperture_shape elliptic -aperture_x 0.00376 -aperture_y 0.00376
Girder
Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.248412 -strength [expr 10.89294757*$e0] -aperture_shape elliptic -aperture_x 0.00376 -aperture_y 0.00376
#Girder
#Drift -name "LX0" -length 0.248412 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "DD0" -synrad $mult_synrad -type 5 -length 0.248412 -strength [expr 38023298.32*$e0] -aperture_shape elliptic -aperture_x 0.00382 -aperture_y 0.00382
Girder
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.366266 -strength [expr -0.1587736629*$e0] -aperture_shape elliptic -aperture_x 0.00383 -aperture_y 0.00383
Girder
Multipole -name "OCTD1" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1000*$e0] -aperture_shape elliptic -aperture_x 0.00383 -aperture_y 0.00383
Girder
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.366266 -strength [expr -0.1587736629*$e0] -aperture_shape elliptic -aperture_x 0.00383 -aperture_y 0.00383
Girder
Drift -name "D0" -length 3.5026092 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
