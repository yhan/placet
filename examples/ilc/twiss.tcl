set scripts .
set lattice linac

source $scripts/init.tcl

proc HCORRECTOR { a b c d } {
}

proc VCORRECTOR { a b c d } {
}

proc Marker { a b } {
}

Girder
source $lattice/elin1.tcl
source $lattice/eund.tcl
source $lattice/elin2.tcl

BeamlineSet

source $lattice/elin1_beam.tcl

set n_total 50000
set n_slice 31
set n 11

source $scripts/tesla_beam.tcl

make_beam_slice beam0 $n_slice $n

TwissPlot -beam beam0 -file twiss.dat
