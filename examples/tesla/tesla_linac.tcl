#
# Select the main linac phases. Here, only one is chosen.
#

set ph1 -27.0
set ph2 5.0

#
# Define wavelength
#

set lambda [expr 0.3/1.3]

#
# Define gradient
#

set gradient [expr 23.4e-3]

#
# Define longrange wakefield
# uncomment next command if single bunch only
#

set CAV_modes {
    {1.6506 19.98 7e4}
    {1.6991 301.86 5e4}
    {1.7252 423.41 2e4}
    {1.7545 59.86 2e4}
    {1.7831 49.2 7.5e3}
    {1.7949 21.70 1e4}
    {1.8342 13.28 5e4}
    {1.8509 11.26 2.5e4}
    {1.8643 191.56 5e4}
    {1.8731 255.71 7e4}
    {1.8795 50.8 1e5}
    {2.5630 42.41 1e5}
    {2.5704 20.05 1e5}
    {2.5751 961.28 5e4}
}

#
# Initial energy
#

#set e0 5.0

#
# Define element parameters
#

set L_cav 1.036
set L_dcme 0.191
set L_dics 0.283
set L_duqp 0.247
set L_ddqp 0.171
set L_dims 0.382
set D_cav 0.28
set L_quad 0.666
set L_bpm 0.0

#
# Calculate resulting parameters
#

set e0 [expr $e0+6*$L_cav*$gradient*cos($ph1/180.0*acos(-1.0))]
set de [expr $L_cav*12*($gradient-0.0554385789318e-3)]
set de0 [expr $L_cav*12*($gradient)]
set de1 [expr $L_cav*12*-0.0504385789318e-3]

#
# Define the lattice
#

#
# define normalised focal strengths for the two sectors
#

set K1 [expr $L_quad*0.045231]
set K2 [expr $L_quad*0.03033]

#
# find initial twiss parameters
#

#array set match [MatchFodo -K1 $K1 -K2 -$K1 -l1 $L_quad -l2 $L_quad \
	-L [expr 0.5*$L_FODO]]

array set match {beta_x 89.309 beta_y 50.681 alpha_x -1.451 alpha_y 0.873}
array set match {beta_x 89.309 beta_y 50.681 alpha_x -1.451 alpha_y 0.873}
#array set match {beta_x 89.3843 beta_y 50.5679 alpha_x -1.3034 alpha_y 0.9577}

#
# Define structure
#

#
# prepare a list of the modes
#

set cav_modes {}
set N_mode 0
set scale [expr 1*0.3/(2.0*acos(-1.0)*1.3)/1.0]
foreach mode $CAV_modes {
    lappend cav_modes [expr 0.3/[lindex $mode 0]]
    lappend cav_modes [expr [lindex $mode 1]*$scale]
    lappend cav_modes [expr [lindex $mode 2]]
    incr N_mode
}

#
# use this list to create fields
#

WakeSet wakelong $cav_modes

#
# define structure
#

InjectorCavityDefine -lambda $lambda \
	-wakelong wakelong

#
# Routine to put one structure into the beambeline.
#

proc Structure {} {
    global L_cav CAV_modes gradient ph2

    AccCavity -l $L_cav -gradient $gradient -type 0 -phase $ph2
}

proc Structure0a {} {
    global L_cav CAV_modes gradient ph1

    AccCavity -l $L_cav -gradient $gradient -type 0 -phase $ph1
}

proc Structure0 {} {
    global L_cav CAV_modes gradient ph1

    AccCavity -l $L_cav -gradient $gradient -type 0 -phase $ph1
}

proc Structure1 {} {
    global L_cav CAV_modes gradient ph1

    AccCavity -l $L_cav -gradient 0.0 -type 0 -phase $ph1
}

#
# Define module
#

proc Module {} {
    global L_quad L_dcme L_dics

    Girder
    Drift -l $L_dcme
    for {set i 0} {$i<11} {incr i} {
	Structure
	Drift -l $L_dics
    }
    Structure
    Drift -l $L_dcme
}

proc Quad_Module {s} {
    global L_quad L_dcme L_dics L_duqp L_ddqp

    Girder
    Drift -l $L_dcme
    for {set i 0} {$i<5} {incr i} {
	Structure
	Drift -l $L_dics
    }
    Structure
    Drift -l $L_duqp
#    QuadBpm -l $L_quad -strength $s
    Bpm -length 0.0
    Quadrupole -l $L_quad -strength $s
    Drift -l $L_ddqp
    for {set i 0} {$i<5} {incr i} {
	Structure
	Drift -l $L_dics
    }
    Structure
    Drift -l $L_dcme
}

proc Module0 {} {
    global L_quad L_dcme L_dics

    Girder
    Drift -l $L_dcme
    for {set i 0} {$i<11} {incr i} {
	Structure0
	Drift -l $L_dics
    }
    Structure0
    Drift -l $L_dcme
}

proc Quad_Module0 {s} {
    global L_quad L_dcme L_dics L_duqp L_ddqp

    Girder
    Drift -l $L_dcme
    for {set i 0} {$i<5} {incr i} {
	Structure0
	Drift -l $L_dics
    }
    Structure0
    Drift -l $L_duqp
#    QuadBpm -l $L_quad -strength $s
    Bpm -length 0.0
    Quadrupole -l $L_quad -strength $s
    Drift -l $L_ddqp
    for {set i 0} {$i<5} {incr i} {
	Structure0
	Drift -l $L_dics
    }
    Structure0
    Drift -l $L_dcme
}

proc Module0a {} {
    global L_quad L_dcme L_dics

    Girder
    Drift -l $L_dcme
    for {set i 0} {$i<11} {incr i} {
	Structure0a
	Drift -l $L_dics
    }
    Structure0
    Drift -l $L_dcme
}

proc Quad_Module0a {s} {
    global L_quad L_dcme L_dics L_duqp L_ddqp

    Girder
#    ReferencePoint -sense -1
    Drift -l $L_dcme
    for {set i 0} {$i<5} {incr i} {
	Structure0a
	Drift -l $L_dics
    }
    Structure0
    Drift -l $L_duqp
#    Bpm -length 0.0
    Quadrupole -l $L_quad -strength $s
#    QuadBpm -l $L_quad -strength $s
    Drift -l $L_ddqp
    for {set i 0} {$i<5} {incr i} {
	Structure0
	Drift -l $L_dics
    }
    Structure0
    Drift -l $L_dcme
}

#
#
#

proc long_module0 {k} {
    global L_dims e0 L_d1 L_quad de ph1 de0 de1
    Quad_Module0 [expr $k]
    Drift -l $L_dims
    Module0
    Drift -l $L_dims
#    set e0 [expr $e0+2*($de0*cos($ph1/180.0*acos(-1.0))+$de1)]
    set e0 [expr $e0+2*$de*cos($ph1/180.0*acos(-1.0))]
} 

proc long_module0a {k} {
    global L_dims e0 L_d1 L_quad de ph1 de0 de1
    Quad_Module0a [expr $k]
    Drift -l $L_dims
    Module0
    Drift -l $L_dims
#    set e0 [expr $e0+2*($de0*cos($ph1/180.0*acos(-1.0))+$de1)]
    set e0 [expr $e0+2*$de*cos($ph1/180.0*acos(-1.0))]
} 

proc long_module1 {k} {
    global L_dims e0 L_d1 L_quad de ph2  de0 de1
    Quad_Module [expr $k]
    Drift -l $L_dims
    Module
    Drift -l $L_dims
#    set e0 [expr $e0+2*($de0*cos($ph2/180.0*acos(-1.0))+$de1)]
    set e0 [expr $e0+2*$de*cos($ph2/180.0*acos(-1.0))]
} 

proc long_module2 {k} {
    global L_dims e0 L_d1 L_quad de ph2  de0 de1
    Quad_Module [expr $k]
    Drift -l $L_dims
    Module
    Drift -l $L_dims
    Module
    Drift -l $L_dims
#    set e0 [expr $e0+3*($de0*cos($ph2/180.0*acos(-1.0))+$de1)]
    set e0 [expr $e0+3*$de*cos($ph2/180.0*acos(-1.0))]
} 

#
# Setup the lattice
#

for {set i 0} {$i<1} {incr i} {
    set eold $e0
    long_module0a [expr $eold*$K1]
    set eold $e0
    long_module0 [expr -$eold*$K1]
}

for {set i 1} {$i<12} {incr i} {
    set eold $e0
    long_module0 [expr $eold*$K1]
    set eold $e0
    long_module0 [expr -$eold*$K1]
}

for {set i 12} {$i<101} {incr i} {
    set eold $e0
    long_module1 [expr $eold*$K1]
    set eold $e0
    long_module1 [expr -$eold*$K1]
}

#
# matching between sectors
#

long_module1 [expr $e0*$K1]
long_module1 [expr $e0*$L_quad*-0.03749300]
long_module1 [expr $e0*$L_quad* 0.04123559]
long_module2 [expr $e0*$L_quad*-0.04562015]
long_module2 [expr $e0*$L_quad* 0.03612909]
long_module2 [expr $e0*$L_quad*-0.04063047]

#
# second sector
#

for {set i 0} {$i<73} {incr i} {
    set eold $e0
    long_module2 [expr $eold*$K2]
    set eold $e0
    long_module2 [expr -$eold*$K2]
}
Quad_Module [expr $e0*$K2]

Drift -length 20
Bpm -length 0.0
Drift -length 20
Bpm -length 0.0

