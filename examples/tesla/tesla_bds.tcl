set radiate 0
set radloss 1.40894859676626e-05

proc D20CM {} {
    global e0
    Girder
    Drift -l 0.2
}
proc D30CM {} {
    global e0
    Girder
    Drift -l 0.3
}
proc BEGL {} {
    global e0
    Drift -l 0.0
}
proc ENDL {} {
    global e0
    Drift -l 0.0
}
proc QLF1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  [expr 0.666/2]  -str [expr  0.03033  * 0.666/2 *$e0]
}
proc QLF1P {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  [expr 0.666/2]  -str [expr  -0.03033  * 0.666/2 *$e0]
}
proc DCM {} {
    global e0
    Girder
    Drift -l 7.993 
}
proc DCM2W {} {
    global e0
    Girder
    Drift -l 1.0 
}
proc WIGG1 {} {
    global e0
    Girder
    Drift -l   50.   
}
proc WIGG2 {} {
    global e0
    Girder
    Drift -l   50.   
}
proc DLTRGT {} {
    global e0
    Girder
    Drift -l  300.   
}
proc TARGET {} {
    global e0
    Girder
    Drift -l 0.0
}
proc WIGGLER {} {
    global e0
    Girder
    WIGG1
    Girder
    WIGG2
}
proc DLMA1E {} {
    global e0
    Girder
    Drift -l  2.0   
}
proc DLMA2 {} {
    global e0
    Girder
    Drift -l  2.029 
}
proc DLMA3 {} {
    global e0
    Girder
    Drift -l  5.696 
}
proc DLMA4 {} {
    global e0
    Girder
    Drift -l  4.435 
}
proc DLMA5 {} {
    global e0
    Girder
    Drift -l  3.260 
}
proc QLMA1E {} {
    global e0  radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr -4.589952E-02 * 1.5 *$e0]
}
proc QLMA2E {} {
    global e0  radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr  7.186191E-02 * 1.5 *$e0]
}
proc QLMA3E {} {
    global e0  radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr -5.767654E-02 * 1.5 *$e0]
}
proc QLMA4E {} {
    global e0  radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr  5.144621E-02 * 1.5 *$e0]
}
proc BPM_1010 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_1020 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_1030 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_1040 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc S_QLMA1E {} {
    global e0
    BPM_1010
    QLMA1E
}
proc S_QLMA2E {} {
    global e0
    BPM_1020
    QLMA2E
}
proc S_QLMA3E {} {
    global e0
    BPM_1030
    QLMA3E
}
proc S_QLMA4E {} {
    global e0
    BPM_1040
    QLMA4E
}
proc M2ASE {} {
    global e0
    DCM2W
    WIGGLER
    DLMA1E
    S_QLMA1E
    DLMA2
    S_QLMA2E
    DLMA3
    S_QLMA3E
    DLMA4
    S_QLMA4E
    DLMA5
}
proc M2ASEM {} {
    global e0
    QLF1
    DCM
    M2ASE
}
proc LI2TRGT {} {
    global e0
    QLF1E
    DLMA1E
    WIGGLER
    DLTRGT
    TARGET
}
proc DLMA1P {} {
    global e0
    Girder
    Drift -l   2.0 
}
proc DLMA2P {} {
    global e0
    Girder
    Drift -l   3.0 
}
proc DLMA3P {} {
    global e0
    Girder
    Drift -l  27.0 
}
proc DLMA4P {} {
    global e0
    Girder
    Drift -l   3.0 
}
proc DLMA5P {} {
    global e0
    Girder
    Drift -l   1.8 
}
proc QLMA1P {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr  0.711453E-01 * 1.5 *$e0]
}
proc QLMA2P {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr -0.576991E-01 * 1.5 *$e0]
}
proc QLMA3P {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr  0.978566E-01 * 1.5 *$e0]
}
proc QLMA4P {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr -0.661833E-01 * 1.5 *$e0]
}
proc S_QLMA1P {} {
    global e0
    BPM_1010
    QLMA1P
}
proc S_QLMA2P {} {
    global e0
    BPM_1020
    QLMA2P
}
proc S_QLMA3P {} {
    global e0
    BPM_1030
    QLMA3P
}
proc S_QLMA4P {} {
    global e0
    BPM_1040
    QLMA4P
}
proc M2ASP {} {
    global e0
    QLF1P
    DLMA1P
    S_QLMA1P
    DLMA2P
    S_QLMA2P
    DLMA3P
    S_QLMA3P
    DLMA4P
    S_QLMA4P
    DLMA5P
}
proc DLPA1 {} {
    global e0
    Girder
    Drift -l  0.37 
}
proc DLPA2 {} {
    global e0
    Girder
    Drift -l  1.55 
}
proc DLPA3 {} {
    global e0
    Girder
    Drift -l  1.00 
}
proc DLPA4A {} {
    global e0
    Girder
    Drift -l  0.5 
}
set ANG_BPA [expr 8.000e-3/12/4]
proc BLPA {} {
    global e0 ANG_BPA radloss  radiate
    Girder
    Sbend -l  1.8  -angle $ANG_BPA -e0 $e0 -synrad $radiate
#    set e0 [expr $e0-$radloss*pow($ANG_BPA,2)*pow($e0,4)/1.8]
}
proc MBLPA {} {
    global e0
}
set ANG_BNA [expr -7.266e-3/12/4]
proc BLNA {} {
    global e0 ANG_BNA radloss  radiate
    Girder
    Sbend -l  1.8  -angle  $ANG_BNA   -e0 $e0 -synrad $radiate
#    set e0 [expr $e0-$radloss*pow($ANG_BNA,2)*pow($e0,4)/1.8]
}
proc MBLNA {} {
    global e0
}
proc S_BLPA {} {
    global e0
    BLPA
    MBLPA
    D20CM
    BLPA
    MBLPA
    D30CM
    BLPA
    MBLPA
    D20CM
    BLPA
    MBLPA
}
proc S_BLNA {} {
    global e0
    BLNA
    MBLNA
    D20CM
    BLNA
    MBLNA
    D30CM
    BLNA
    MBLNA
    D20CM
    BLNA
    MBLNA
}
proc QLPA1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr +0.590000E-01 * 1.5 *$e0]
}
proc QLPA2 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr -0.455597E-01 * 1.5 *$e0]
}
proc SLPA {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -type 3 -l 0.5 -str [expr  4.039998*0.5*$e0]
}
proc SLNA {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -type 3 -l 0.5 -str [expr -3.876417*0.5*$e0]
}
proc BPM_2000 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2010 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2020 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2030 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc F_QLPA1 {} {
    global e0
    BPM_2000
    QLPA1
}
proc F_QLPA2 {} {
    global e0
    BPM_2010
    QLPA2
}
proc F_QLPA3 {} {
    global e0
    BPM_2020
    QLPA2
}
proc F_QLPA4 {} {
    global e0
    BPM_2030
    QLPA1
}
proc PACELL1 {} {
    global e0
    S_BLPA
    DLPA1
    S_BLPA
    DLPA1
    S_BLPA
    DLPA1
    S_BLPA
    DLPA1
    S_BLPA
    DLPA1
    S_BLNA
    DLPA2
}
proc S_G2000 {} {
    global e0
    F_QLPA1
    DLPA3
    F_QLPA2
    DLPA4A
    SLPA
    DLPA4A
    F_QLPA3
    DLPA3
    F_QLPA4
}
proc PACELL2 {} {
    global e0
    DLPA2
    S_BLNA
    DLPA1
    S_BLPA
    DLPA1
    S_BLPA
    DLPA1
    S_BLPA
    DLPA1
    S_BLPA
    DLPA1
    S_BLPA
}
proc PARC {} {
    global e0
    PACELL1
    S_G2000
    PACELL2
}
proc DLPN1 {} {
    global e0
    Girder
    Drift -l  0.850 
}
proc DLPN2 {} {
    global e0
    Girder
    Drift -l  1.174 
}
proc QLPN1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  2.0  -str [expr -0.415272E-01 * 2.0 *$e0]
}
proc QLPN2 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  3.0  -str [expr  0.735921E-01 * 3.0 *$e0]
}
proc QLPN3 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  2.0  -str [expr -0.556024E-01 * 2.0 *$e0]
}
proc BPM_2100 {} {
    global e0 radiate
    Girder
    Drift -l 0.0
}
proc BPM_2110 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2120 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc F_QLPN1 {} {
    global e0
    BPM_2100
    QLPN1
}
proc F_QLPN2 {} {
    global e0
    BPM_2100
    QLPN2
}
proc F_QLPN3 {} {
    global e0
    BPM_2100
    QLPN3
}
proc S_G2100 {} {
    global e0
    F_QLPN1
    DLPN2
    F_QLPN2
    DLPN2
    F_QLPN3
}
proc MPNS {} {
    global e0
    DLPN1
    S_G2100
    DLPN1
}
proc DLNA1 {} {
    global e0
    Girder
    Drift -l  0.37  
}
proc DLNA2 {} {
    global e0
    Girder
    Drift -l  1.55  
}
proc DLNA3 {} {
    global e0 
    Girder
    Drift -l  1.00  
}
proc DLNA4A {} {
    global e0
    Girder
    Drift -l  0.50  
}
proc QLNA1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr +0.540000E-01 * 1.5 *$e0]
}
proc QLNA2 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.5  -str [expr -0.402188E-01 * 1.5 *$e0]
}
proc BPM_2200 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2210 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2220 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2230 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc F_QLNA1 {} {
    global e0
    BPM_2200
    QLNA1
}
proc F_QLNA2 {} {
    global e0
    BPM_2210
    QLNA2
}
proc F_QLNA3 {} {
    global e0
    BPM_2220
    QLNA2
}
proc F_QLNA4 {} {
    global e0
    BPM_2230
    QLNA1
}
proc NACELL1 {} {
    global e0
    S_BLNA
    DLNA1
    S_BLNA
    DLNA1
    S_BLNA
    DLNA1
    S_BLNA
    DLNA1
    S_BLNA
    DLNA1
    S_BLPA
    DLNA2
}
proc S_G2200 {} {
    global e0
    F_QLNA1
    DLNA3
    F_QLNA2
    DLNA4A
    SLNA
    DLNA4A
    F_QLNA3
    DLNA3
    F_QLNA4
}
proc NACELL2 {} {
    global e0
    DLNA2
    S_BLPA
    DLNA1
    S_BLNA
    DLNA1
    S_BLNA
    DLNA1
    S_BLNA
    DLNA1
    S_BLNA
    DLNA1
    S_BLNA
}
proc NARC {} {
    global e0
    NACELL1
    S_G2200
    NACELL2
}
proc DLMC1 {} {
    global e0
    Girder
    Drift -l   0.57 
}
proc DLMC2 {} {
    global e0
    Girder
    Drift -l  10.0  
}
proc DLMC3 {} {
    global e0
    Girder
    Drift -l  17.627
}
proc DLMC4 {} {
    global e0
    Girder
    Drift -l   2.173
}
proc DLMC5 {} {
    global e0
    Girder
    Drift -l   5.15 
}
proc QLMC1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr  0.213012E-01 * 1.0 *$e0]
}
proc QLMC2 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr -0.348666E-01 * 1.0 *$e0]
}
proc QLMC3 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr  0.753957E-01 * 1.0 *$e0]
}
proc QLMC4 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr -0.521405E-01 * 1.0 *$e0]
}
proc BPM_2300 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2310 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2320 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_2330 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc S_QLMC1 {} {
    global e0
    BPM_2300
    QLMC1
}
proc S_QLMC2 {} {
    global e0
    BPM_2310
    QLMC2
}
proc F_QLMC3 {} {
    global e0
    BPM_2320
    QLMC3
}
proc F_QLMC4 {} {
    global e0
    BPM_2330
    QLMC4
}
proc S_G2320 {} {
    global e0
    F_QLMC3
    DLMC4
    F_QLMC4
}
proc M2CS {} {
    global e0
    DLMC1
    S_QLMC1
    DLMC2
    S_QLMC2
    DLMC3
    S_G2320
    DLMC5
}
proc MLI2CDSE {} {
    global e0
    BEGL
    M2ASEM
    PARC
    MPNS
    NARC
    M2CS
    ENDL
}
proc LI2CDSE {} {
    global e0
    BEGL
    M2ASE
    PARC
    MPNS
    NARC
    M2CS
    ENDL
}
proc LI2CDSP {} {
    global e0
    BEGL
    M2ASP
    PARC
    MPNS
    NARC
    M2CS
    ENDL
}
proc BETA_IN_CDS {} {
    global e0
}
proc BEGC {} {
    global e0
    Drift -l 0.0
}
proc ENDC {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BETA_IN_MESS {} {
    global e0
}
proc DMTCH {} {
    global e0
    Drift -l  -0.5     
}
proc DCMES1 {} {
    global e0
    Girder
    Drift -l   3.5     
}
proc DCMES1A {} {
    global e0
    Girder
    Drift -l   2.22    
}
proc DCMES1B {} {
    global e0
    Girder
    Drift -l   1.28    
}
proc DCMES1C {} {
    global e0
    Girder
    Drift -l   3.25    
}
proc DCMES1D {} {
    global e0
    Girder
    Drift -l   0.50    
}
proc DCMES2 {} {
    global e0
    Girder
    Drift -l  23.652   
}
proc DCMES3 {} {
    global e0
    Girder
    Drift -l   7.875   
}
proc DCMES4 {} {
    global e0
    Girder
    Drift -l   5.0     
}
proc DCMES5 {} {
    global e0
    Girder
    Drift -l   1.063   
}
proc DCMES6 {} {
    global e0
    Girder
    Drift -l  39.347   
}
proc DCMES7 {} {
    global e0
    Girder
    Drift -l   3.547   
}
proc DCMES8 {} {
    global e0
    Girder
    Drift -l  44.347   
}
proc DCMES9 {} {
    global e0
    Girder
    Drift -l   3.0     
}
proc DCMES10 {} {
    global e0
    Girder
    Drift -l   0.6     
}
proc DCMES20 {} {
    global e0
    Girder
    Drift -l   1.6     
}
proc DCMES30 {} {
    global e0
    Girder
    Drift -l   0.6     
}
proc DCMES40 {} {
    global e0
    Girder
    Drift -l   1.6     
}
proc BCMES1 {} {
    global e0 radloss radiate
    Girder
    Sbend -l 1.8 -angle  [expr 0.647499E-3/2]   -e0 $e0 -synrad $radiate
#    set e0 [expr $e0-$radloss*pow(0.647499E-3/2,2)*pow($e0,4)/1.8]
}
proc MBCMES1 {} {
    global e0
}
proc BCMES2 {} {
    global e0 radloss radiate
    Girder
    Sbend -l 1.8 -angle  0.556275E-4     -e0 $e0  -synrad $radiate
#    set e0 [expr $e0-$radloss*pow(0.556275E-4,2)*pow($e0,4)/1.8]
}
proc MBCMES2 {} {
    global e0
}
proc BCMES3 {} {
    global e0 radloss radiate
    Girder
    Sbend -l 1.8 -angle  [expr 0.649851E-3/2]   -e0 $e0  -synrad $radiate
#    set e0 [expr $e0-$radloss*pow(0.649851E-3/2,2)*pow($e0,4)/1.8]
}
proc MBCMES3 {} {
    global e0
}
proc BCMES4 {} {
    global e0 radloss radiate
    Girder
    Sbend -l 1.8 -angle -0.149778E-3     -e0 $e0  -synrad $radiate
#    set e0 [expr $e0-$radloss*pow(0.149778E-3,2)*pow($e0,4)/1.8]
}
proc MBCMES4 {} {
    global e0
}
proc BCMES1T {} {
    global e0
    BCMES1
    MBCMES1
    D20CM
    BCMES1
    MBCMES1
}
proc BCMES3T {} {
    global e0
    BCMES3
    MBCMES3
    D20CM
    BCMES3
    MBCMES3
}
proc QCMES1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.0 -str [expr  0.730378E-01   *1.0*$e0]
}
proc QCMES2 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.0 -str [expr -0.856892E-01   *1.0*$e0]
}
proc QCMES3 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 2.0 -str [expr  0.675331E-01   *2.0*$e0]
}
proc QCMES4 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.5 -str [expr -0.664194E-01   *1.5*$e0]
}
proc QCMES5 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.5 -str [expr  0.689465E-01   *1.5*$e0]
}
proc QCMES6 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.5 -str [expr -0.856035E-01   *1.5*$e0]
}
proc SCS1 {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -type 3 -l 0.5 -str [expr  5.0* 0.5*$e0]
    Multipole -synrad $radiate -type 3 -l 0.5 -str [expr  5.0* 0.5*$e0]
}
proc SCS2 {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -type 3 -l 0.5 -str [expr  5.0* 0.5*$e0]
    Multipole -synrad $radiate -type 3 -l 0.5 -str [expr  5.0* 0.5*$e0]
}
proc SC1 {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -type 3 -l 0.5 -str [expr -1.943573    *0.5    *$e0]
}
proc SC2 {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -type 3 -l 0.5 -str [expr  -3.267154   * 0.5   *$e0]
}
proc OC {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -type 4 -l 0.5 -str [expr  -1.28E3      * 0.5      *$e0/2.0]
}
proc ESPOI {} {
    global e0
    Girder
    Drift -l 0.0
}
proc TARGET {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3000 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3010 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3020 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3030 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3040 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3050 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3060 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3070 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3080 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3090 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3100 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3110 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3120 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3130 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3140 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3200 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3210 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3220 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3230 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3240 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3250 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_3260 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc S_SCS1 {} {
    global e0
    BPM_3000
    SCS1
}
proc M_QCMES1 {} {
    global e0
    BPM_3010
    QCMES1
}
proc M_QCMES2 {} {
    global e0
    BPM_3020
    QCMES2
}
proc S_G3010 {} {
    global e0
    M_QCMES1
    DCMES10
    BCMES1T
    DCMES10
    M_QCMES2
}
proc S_QCMES3 {} {
    global e0
    BPM_3030
    QCMES3
}
proc S_QCMES4 {} {
    global e0
    BPM_3040
    QCMES3
}
proc M_QCMES5 {} {
    global e0
    BPM_3050
    QCMES2
}
proc M_QCMES6 {} {
    global e0
    BPM_3060
    QCMES1
}
proc S_G3050 {} {
    global e0
    M_QCMES5
    DCMES4
    M_QCMES6
}
proc S_G3070 {} {
    global e0
    BPM_3070
    SC1
    DCMES1D
    OC
}
proc M_QCMES7 {} {
    global e0
    BPM_3080
    QCMES1
}
proc M_QCMES8 {} {
    global e0
    BPM_3090
    QCMES2
}
proc S_G3080 {} {
    global e0
    M_QCMES7
    DCMES20
    BCMES2
    MBCMES2
    DCMES20
    M_QCMES8
}
proc S_QCMES9 {} {
    global e0
    BPM_3100
    QCMES3
}
proc S_QCMES10 {} {
    global e0
    BPM_3110
    QCMES3
}
proc M_QCMES11 {} {
    global e0
    BPM_3120
    QCMES2
}
proc M_QCMES12 {} {
    global e0
    BPM_3130
    QCMES1
}
proc S_G3120 {} {
    global e0
    M_QCMES11
    DCMES30
    BCMES3T
    DCMES30
    M_QCMES12
}
proc S_SCS2 {} {
    global e0
    BPM_3140
    SCS2
}
proc MES {} {
    global e0
    DMTCH
    S_SCS1
    DCMES1A
    TARGET
    DCMES1B
    S_G3010
    DCMES2
    S_QCMES3
    DCMES3
    S_QCMES4
    DCMES2
    S_G3050
    DCMES1C
    S_G3070
    DCMES1C
    S_G3080
    DCMES2
    S_QCMES9
    DCMES3
    S_QCMES10
    DCMES2
    S_G3120
    DCMES1
    S_SCS2
    DMTCH
}
proc M_QCMES13 {} {
    global e0
    BPM_3200
    QCMES4
}
proc M_QCMES14 {} {
    global e0
    BPM_3210
    QCMES5
}
proc S_G3200 {} {
    global e0
    M_QCMES13
    DCMES5
    M_QCMES14
    DCMES40
    BCMES4
    MBCMES4
}
proc M_QCMES15 {} {
    global e0
    BPM_3220
    QCMES6
}
proc M_QCMES16 {} {
    global e0
    BPM_3230
    QCMES6
}
proc S_G3220 {} {
    global e0
    M_QCMES15
    DCMES7
    M_QCMES16
}
proc M_QCMES17 {} {
    global e0
    BPM_3240
    QCMES5
}
proc M_QCMES18 {} {
    global e0
    BPM_3250
    QCMES4
}
proc M_SC2 {} {
    global e0
    BPM_3260
    SC2
}
proc S_G3240 {} {
    global e0
    M_QCMES17
    DCMES5
    M_QCMES18
    M_SC2
}
proc ESP {} {
    global e0
    DCMES1
    S_G3200
    DCMES40
    DCMES6
    S_G3220
    DCMES8
    S_G3240
    DCMES9
    ESPOI
}
proc MESS {} {
    global e0
    BEGC
    MES
    ESP
}
proc BETA_IN_BCDS {} {
    global e0
}
proc DCBC1 {} {
    global e0
    Girder
    Drift -l  1.0     
}
proc DCBC2 {} {
    global e0
    Girder
    Drift -l  2.998   
}
proc DCBC3 {} {
    global e0
    Girder
    Drift -l 40.042   
}
proc DCBC6 {} {
    global e0
    Girder
    Drift -l  3.601   
}
proc DCBC4 {} {
    global e0
    Girder
    Drift -l 32.735   
}
proc DCBC5 {} {
    global e0
    Girder
    Drift -l  1.463   
}
proc BCBC {} {
    global e0 radloss radiate
    Girder
    Sbend -l  1.8  -angle [expr -0.191558E-2/3] -e0 $e0  -synrad $radiate
#    set e0 [expr $e0-$radloss*pow(0.191558E-2/3,2)*pow($e0,4)/1.8]
}
proc MBCBC {} {
    global e0
}
proc S_BCBC {} {
    global e0
    BCBC
    MBCBC
    D20CM
    BCBC
    MBCBC
    D20CM
    BCBC
    MBCBC
}
proc QCBC1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr  0.780531E-01 * 1.0 *$e0]
}
proc QCBC2 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr -0.824625E-01 * 1.0 *$e0]
}
proc QCBC3 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  2.0  -str [expr  0.767138E-01 * 2.0 *$e0]
}
proc XYSPOI {} {
    global e0
    Girder
    Drift -l 0.0
}
proc WS_1 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc WS_2 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc WS_3 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc WS_4 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4000 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4010 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4100 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4110 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4120 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4130 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4140 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4150 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4200 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4210 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4220 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4230 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4240 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4250 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4300 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4310 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4320 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4330 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4340 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4350 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4400 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4410 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4420 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_4430 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc M_QCBC1 {} {
    global e0
    BPM_4000
    QCBC1
}
proc M_QCBC2 {} {
    global e0
    BPM_4010
    QCBC2
}
proc S_G4000 {} {
    global e0
    M_QCBC1
    DCBC2
    M_QCBC2
}
proc M_QCBC3 {} {
    global e0
    BPM_4100
    QCBC3
}
proc M_QCBC4 {} {
    global e0
    BPM_4110
    QCBC3
}
proc S_G4100 {} {
    global e0
    M_QCBC3
    DCBC6
    M_QCBC4
}
proc M_QCBC5 {} {
    global e0
    BPM_4120
    QCBC2
}
proc M_QCBC6 {} {
    global e0
    BPM_4130
    QCBC1
}
proc M_QCBC7 {} {
    global e0
    BPM_4140
    QCBC1
}
proc M_QCBC8 {} {
    global e0
    BPM_4150
    QCBC2
}
proc S_G4120 {} {
    global e0
    M_QCBC5
    DCBC2
    M_QCBC6
    WS_1
    DCBC1
    XYSPOI
    DCBC1
    M_QCBC7
    DCBC2
    M_QCBC8
}
proc M_QCBC9 {} {
    global e0
    BPM_4200
    QCBC3
}
proc M_QCBC10 {} {
    global e0
    BPM_4210
    QCBC3
}
proc S_G4200 {} {
    global e0
    M_QCBC9
    DCBC6
    M_QCBC10
}
proc M_QCBC11 {} {
    global e0
    BPM_4220
    QCBC2
}
proc M_QCBC12 {} {
    global e0
    BPM_4230
    QCBC1
}
proc M_QCBC13 {} {
    global e0
    BPM_4240
    QCBC1
}
proc M_QCBC14 {} {
    global e0
    BPM_4250
    QCBC2
}
proc S_G4220 {} {
    global e0
    M_QCBC11
    DCBC2
    M_QCBC12
    WS_2
    DCBC1
    XYSPOI
    DCBC1
    M_QCBC13
    DCBC2
    M_QCBC14
}
proc M_QCBC15 {} {
    global e0
    BPM_4300
    QCBC3
}
proc M_QCBC16 {} {
    global e0
    BPM_4310
    QCBC3
}
proc S_G4300 {} {
    global e0
    M_QCBC15
    DCBC6
    M_QCBC16
}
proc M_QCBC17 {} {
    global e0
    BPM_4320
    QCBC2
}
proc M_QCBC18 {} {
    global e0
    BPM_4330
    QCBC1
}
proc M_QCBC19 {} {
    global e0
    BPM_4340
    QCBC1
}
proc M_QCBC20 {} {
    global e0
    BPM_4350
    QCBC2
}
proc S_G4320 {} {
    global e0
    M_QCBC17
    DCBC2
    M_QCBC18
    WS_3
    DCBC1
    XYSPOI
    DCBC1
    M_QCBC19
    DCBC2
    M_QCBC20
}
proc M_QCBC21 {} {
    global e0
    BPM_4400
    QCBC3
}
proc M_QCBC22 {} {
    global e0
    BPM_4410
    QCBC3
}
proc S_G4400 {} {
    global e0
    M_QCBC21
    DCBC6
    M_QCBC22
}
proc M_QCBC23 {} {
    global e0
    BPM_4420
    QCBC2
}
proc M_QCBC24 {} {
    global e0
    BPM_4430
    QCBC1
}
proc S_G4420 {} {
    global e0
    M_QCBC23
    DCBC2
    M_QCBC24
    WS_4
    DCBC1
    XYSPOI
}
proc BCDS {} {
    global e0
    DCBC1
    S_G4000
    DCBC4
    S_BCBC
    DCBC5
    S_G4100
    DCBC3
    S_G4120
    DCBC3
    S_G4200
    DCBC3
    S_G4220
    DCBC3
    S_G4300
    DCBC3
    S_G4320
    DCBC3
    S_G4400
    DCBC3
    S_G4420
}
proc CDS {} {
    global e0
    MESS
    BCDS
}
proc BETA_IN_FFS {} {
    global e0
}
proc BEGF {} {
    global e0
    Girder
    Drift -l 0.0
}
proc IP {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BETA_IN_BMS {} {
    global e0
}
proc DFB1 {} {
    global e0
    Girder
    Drift -l   1.0   
}
proc DFB2 {} {
    global e0
    Girder
    Drift -l   3.816 
}
proc DFB3 {} {
    global e0
    Girder
    Drift -l  28.410 
}
proc DFB4 {} {
    global e0
    Girder
    Drift -l  12.600 
}
proc DFB5 {} {
    global e0
    Girder
    Drift -l  10.224 
}
proc DFB6 {} {
    global e0
    Girder
    Drift -l   1.0   
}
proc DFB7 {} {
    global e0
    Girder
    Drift -l   1.0   
}
proc QFB1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr -.285935E-01 * 1.0 *$e0]
}
proc QFB2 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr  .491965E-01 * 1.0 *$e0]
}
proc QFB3 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr -.552811E-01 * 1.0 *$e0]
}
proc QFB4 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr  .389833E-01 * 1.0 *$e0]
}
proc QFB5 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr  .0          * 1.0 *$e0]
}
proc QFB6 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l  1.0  -str [expr  .0          * 1.0 *$e0]
}
proc BPM_5000 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_5010 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_5020 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_5030 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_5040 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc BPM_5050 {} {
    global e0
    Girder
    Drift -l 0.0
}
proc S_QFB1 {} {
    global e0
    BPM_5000
    QFB1
}
proc S_QFB2 {} {
    global e0
    BPM_5010
    QFB2
}
proc S_QFB3 {} {
    global e0
    BPM_5020
    QFB3
}
proc S_QFB4 {} {
    global e0
    BPM_5030
    QFB4
}
proc S_QFB5 {} {
    global e0
    BPM_5040
    QFB5
}
proc S_QFB6 {} {
    global e0
    BPM_5050
    QFB6
}
proc BMS {} {
    global e0
    DFB1
    S_QFB1
    DFB2
    S_QFB2
    DFB3
    S_QFB3
    DFB4
    S_QFB4
    DFB5
    S_QFB5
    DFB6
    S_QFB6
    DFB7
}

proc DFC1 {} {
    Girder
    Drift -l 19.8
}

proc DFC2 {} {
    Girder
    Drift -l 18.3
}

proc DFC3 {} {
    Girder
    Drift -l 0.5
}

proc DFC4 {} {
    Girder
    Drift -l 0.5
}

proc DFCQB {} {
    Girder
    Drift -l 0.9
}

proc DFCQS {} {
    Girder
    Drift -l 0.2
}

proc DFCSC {} {
    Girder
    Drift -l 0.5
}

set BFC_ANGLE [expr 0.7667E-3]

proc BFCH {} {
    global BFC_ANGLE e0 radloss radiate
    Girder
    Sbend -l 1.8 -angle [expr $BFC_ANGLE/10.0] -e0 $e0  -synrad $radiate
#    set e0 [expr $e0-$radloss*pow($BFC_ANGLE/10.0,2)*pow($e0,4)/1.8]
#    Drift -l 1.8
}

proc BFCV {} {
    global BFC_ANGLE e0 radloss radiate
    Girder
    Sbend -l 1.8 -angle [expr -$BFC_ANGLE/10.0] -e0 $e0  -synrad $radiate
#    set e0 [expr $e0-$radloss*pow($BFC_ANGLE/10.0,2)*pow($e0,4)/1.8]
#    Drift -l 1.8
}

proc BFCHT {} {
    BFCH
    Girder
    Drift -l 0.2
    BFCH
    Girder
    Drift -l 0.3
}

proc BFCVT {} {
    BFCV
    Girder
    Drift -l 0.2
    BFCV
    Girder
    Drift -l 0.3
}

proc S_BFCH {} {
    global BFC_ANGLE radloss e0 radiate
    BFCHT
    BFCHT
    BFCHT
    BFCHT
    BFCH
    Girder
    Drift -l 0.2
    BFCH
    set e0 [expr $e0-$radiate*$radloss*pow($BFC_ANGLE,2)*pow($e0,4)/18.0]
}

proc S_BFCV {} {
    global BFC_ANGLE radloss e0 radiate
    BFCVT
    BFCVT
    BFCVT
    BFCVT
    BFCV
    Girder
    Drift -l 0.2
    BFCV
    set e0 [expr $e0-$radiate*$radloss*pow($BFC_ANGLE,2)*pow($e0,4)/18.0]
}

set QFC_K1 0.0623999

proc QFC1 {} {
    global QFC_K1 e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 0.5 -str [expr $QFC_K1*$e0*0.5]
}

proc QFC2 {} {
    global QFC_K1 e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.0 -str [expr -$QFC_K1*$e0*1.0]
}

proc QFC3 {} {
    global QFC_K1 e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.0 -str [expr $QFC_K1*$e0*1.0]
}

proc QFC4 {} {
    global QFC_K1 e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 0.5 -str [expr -$QFC_K1*$e0*0.5]
}

proc SFCH {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -l 2.0 -str [expr 4.985*$e0*2.0] -type 3
#    Multipole -synrad $radiate -l 0.5 -str [expr 4.985*$e0*0.5] -type 3
#    Multipole -synrad $radiate -l 0.5 -str [expr 4.985*$e0*0.5] -type 3
#    Multipole -synrad $radiate -l 0.5 -str [expr 4.985*$e0*0.5] -type 3
#    Multipole -synrad $radiate -l 0.5 -str [expr 4.985*$e0*0.5] -type 3
}

proc SFCV {} {
    global e0 radiate
    Girder
    Multipole -synrad $radiate -l 2.0 -str [expr 17.32*$e0*2.0] -type 3
#    Multipole -synrad $radiate -l 0.5 -str [expr 17.32*$e0*0.5] -type 3
#    Multipole -synrad $radiate -l 0.5 -str [expr 17.32*$e0*0.5] -type 3
#    Multipole -synrad $radiate -l 0.5 -str [expr 17.32*$e0*0.5] -type 3
#    Multipole -synrad $radiate -l 0.5 -str [expr 17.32*$e0*0.5] -type 3
#    Drift -l 2.0
}

proc COLX {} {
    Girder
    Drift -l 1.0
}

proc COLY {} {
    Girder
    Drift -l 1.0
}

proc BPM_6000 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6010 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6020 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6022 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6024 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6030 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6040 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6050 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6060 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6062 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6064 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6070 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6080 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6090 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6100 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6102 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6104 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6110 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6120 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6130 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6140 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6142 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6144 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6150 {} {
    Girder
    Drift -l 0.0
}
proc BPM_6160 {} {
    Girder
    Drift -l 0.0
}

proc S_QFC1 {} {
    BPM_6000
    QFC1
}
proc S_QFC2 {} {
    BPM_6010
    QFC2
}
proc M_SFCH1 {} {
    BPM_6020
    SFCH
}
proc M_QFC3 {} {
    BPM_6022
    QFC3
}
proc M_SFCH2 {} {
    BPM_6024
    SFCH
}
proc S_G6020 {} {
    M_SFCH1
    DFCQS
    M_QFC3
    DFCQS
    M_SFCH2
    DFCSC
    COLX
}
proc S_QFC4 {} {
    BPM_6030
    QFC2
}
proc S_QFC5 {} {
    BPM_6040
    QFC3
}
proc S_QFC6 {} {
    BPM_6050
    QFC2
}
proc M_SFCH3 {} {
    BPM_6060
    SFCH
}
proc M_QFC7 {} {
    BPM_6062
    QFC3
}
proc M_SFCH4 {} {
    BPM_6064
    SFCH
}
proc S_G6060 {} {
    M_SFCH3
    DFCQS
    M_QFC7
    DFCQS
    M_SFCH4
    DFCSC
    COLX
}
proc S_QFC8 {} {
    BPM_6070
    QFC2
}
proc CCSH {} {
    S_QFC1
    DFCQB
    S_BFCH
    DFCQB
    S_QFC2
    DFC1
    S_G6020
    DFC2
    S_QFC4
    DFCQB
    S_BFCH
    DFCQB
    S_QFC5
    DFCQB
    S_BFCH
    DFCQB
    S_QFC6
    DFC1
    S_G6060
    DFC2
    S_QFC8
    DFCQB
    S_BFCH
    DFCQB
}

proc S_QFC9 {} {
    BPM_6080
    QFC1
    QFC4
}
proc S_QFC10 {} {
    BPM_6090
    QFC3
}
proc M_SFCV1 {} {
    BPM_6100
    SFCV
}
proc M_QCF11 {} {
    BPM_6102
    QFC2
}
proc M_SFCV2 {} {
    BPM_6104
    SFCV
}
proc S_G6100 {} {
    M_SFCV1
    DFCQS
    M_QCF11
    DFCQS
    M_SFCV2
    DFCSC
    COLY
}
proc S_QFC12 {} {
    BPM_6110
    QFC3
}
proc S_QFC13 {} {
    BPM_6120
    QFC2
}
proc S_QFC14 {} {
    BPM_6130
    QFC3
}
proc M_SFCV3 {} {
    BPM_6140
    SFCV
}
proc M_QCF15 {} {
    BPM_6142
    QFC2
}
proc M_SFCV4 {} {
    BPM_6144
    SFCV
}
proc S_G6140 {} {
    M_SFCV3
    DFCQS
    M_QCF15
    DFCQS
    M_SFCV4
    DFCSC
    COLY
}
proc S_QFC16 {} {
    BPM_6150
    QFC3
}
proc S_QFC17 {} {
    BPM_6160
    QFC4
}
proc CCSV {} {
    DFCQB
    S_BFCV
    DFCQB
    S_QFC10
    DFC1
    S_G6100
    DFC2
    S_QFC12
    DFCQB
    S_BFCV
    DFCQB
    S_QFC13
    DFCQB
    S_BFCV
    DFCQB
    S_QFC14
    DFC1
    S_G6140
    DFC2
    S_QFC16
    DFCQB
    S_BFCV
    DFCQB
    S_QFC17
}

proc CCS {} {
    CCSH
    S_QFC9
    CCSV
}

proc DFT12 {} {
    Girder
    Drift -l 0.7
}

proc DFT11 {} {
    Girder
    Drift -l 2.021
}

proc DFT10 {} {
    Girder
    Drift -l 3.703
}

proc COLL3 {} {
    Girder
    Drift -l 10.0
}

proc DFT9 {} {
    Girder
    Drift -l 73.04
}

proc DFT8 {} {
    Girder
    Drift -l 24.257
}

proc DFT7 {} {
    Girder
    Drift -l 8.703
}

proc COLL2 {} {
    Girder
    Drift -l 1.0
}


proc DFT6 {} {
    Girder
    Drift -l 99.0
}

proc SEPB {} {
    Girder
    Drift -l 12.0
}

proc COLL1 {} {
    Girder
    Drift -l 1.0
}

proc SEPA {} {
    Girder
    Drift -l 8.0
}

proc DFT5 {} {
    Girder
    Drift -l 1.5
}

proc MASK {} {
    Girder
    Drift -l 0.6
}

proc DFT4 {} {
    Girder
    Drift -l 1.794
}

proc DFT3 {} {
    Girder
    Drift -l 1.0
}

proc DFT2 {} {
    Girder
    Drift -l 0.4
}

proc DFT1 {} {
    Girder
    Drift -l 2.0
}

proc QFT6 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -length 0.5 -strength [expr 0.5*.242357E-01*$e0]
}

proc QFT5 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -length 0.5 -strength [expr 0.5*-.271388E-01*$e0]
}

proc QFT4 {} { 
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -length 1.5 -strength [expr 1.5*.118702E-01*$e0]
}

proc QFT3 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -length 1.5 -strength [expr 1.5*-.125288E-01*$e0]
}

proc QFT2 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.021 -strength [expr 1.021*0.29979*$e0]
}

proc QFT1 {} {
    global e0 radiate
    Girder
    Quadrupole -synrad $radiate -l 1.685 -strength [expr -1.685*0.29979*$e0]
}

proc S_GQ56 {} {
    QFT6
    DFT11
    QFT5
}

proc S_GQ34 {} {
    QFT4
    DFT8
    QFT3
}

proc F_DBLT {} {
    QFT2
    DFT3
    QFT1
}

proc S_IR {} {
    F_DBLT
    DFT2
    MASK
    DFT1
}

proc LUMON {} {
}

proc IP {} {
    Girder
    Quadrupole -l 0.0 -str 0.0
}

proc TELE {} {
    DFT12
    S_GQ56
    DFT10
    COLL3
    DFT9
    S_GQ34
    DFT7
    COLL2
    DFT6
    SEPB
    COLL1
    SEPA
    DFT5
    LUMON
    DFT4
    S_IR
    IP
}

proc FFS {} {
    BEGF
    BMS
    CCS
    TELE
}

Girder
LI2CDSE
CDS
BEGF
BMS
CCS
TELE
