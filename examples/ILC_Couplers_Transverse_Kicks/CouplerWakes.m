function Beam = CouplerWakes(Beam, filename, charge)
  wakes = load(filename);
  charge *=  1.602176462e-07; # conversion to pC
  S = wakes(:,1) * 1e6; # conversion to um
  Wx = wakes(:,2) * charge * 1e-9 * 1e6; # conversion to GV, then to urad
  Wy = wakes(:,3) * charge * 1e-9 * 1e6; # conversion to GV, then to urad
  h = (S(end) - S(1)) / (length(S)-1);
  if columns(Beam) == 6
    Zmean = mean(Beam(:,4));
    J = (Beam(:,4) - Zmean - S(1)) / h;
  else
    Zmean = sum(Beam(:,1) .* Beam(:,2)) / sum(Beam(:,2));
    J = (Beam(:,1) - Zmean - S(1)) / h;
  end
  j = floor(J);
  t = J - j;
  K1 = find(j >= length(S) - 1);
  K2 = find(j < 0);
  j(K1) = length(S) - 2;
  j(K2) = 0;
  t(K1) = 1;
  t(K2) = 0;
  if columns(Beam) == 6
    Beam(:,5) += (Wx(j+1).*(1-t) + Wx(j+2).*t) ./ Beam(:,1);
    Beam(:,6) += (Wy(j+1).*(1-t) + Wy(j+2).*t) ./ Beam(:,1);
  else
    Beam(:,5) += (Wx(j+1).*(1-t) + Wx(j+2).*t) ./ Beam(:,3);
    Beam(:,7) += (Wy(j+1).*(1-t) + Wy(j+2).*t) ./ Beam(:,3);
  end
end
