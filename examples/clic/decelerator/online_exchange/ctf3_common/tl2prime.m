function [quads, dipols, hcors, vcors, mons] = tl2prime()

prefix = 'CM';
  
amoni = 0;
adipo = 1;
aquad = 2;
ahcorr = 5;
avcorr = 6;


n=1;
device(n).name='BHL0100';      device(n).type=adipo;  device(n).madname='IBHL0100'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;
device(n).name='BHL0200';      device(n).type=adipo;  device(n).madname='IBHL0200'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;

device(n).name='BPM0110'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;
device(n).name='BPM0230'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;
device(n).name='BPM0270'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;

device(n).name='QFN0130-S';      device(n).type=aquad;  device(n).madname='CM.IQFN0130'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;
device(n).name='QDH0140';      device(n).type=aquad;  device(n).madname='CM.IQDH0140'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;
device(n).name='QFP0220';      device(n).type=aquad;  device(n).madname='CM.IQFP0220'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;
device(n).name='QDP0225';      device(n).type=aquad;  device(n).madname='CM.IQDP0225'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;
device(n).name='QFQ0260';      device(n).type=aquad;  device(n).madname='CM.IQFQ0260'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;
device(n).name='QDQ0265';      device(n).type=aquad;  device(n).madname='CM.IQDQ0265'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1;




device(n).name='DHD0240';      device(n).type=ahcorr;  device(n).madname='CM.IDHD0240'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1; 
device(n).name='DHD0290';      device(n).type=ahcorr;  device(n).madname='CM.IDHD0290'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1; 

device(n).name='DVD0240';      device(n).type=avcorr;  device(n).madname='CM.IDVD0240'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1; 
device(n).name='DVD0290';      device(n).type=avcorr;  device(n).madname='CM.IDVD0290'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CM'; n=n+1; 


nd = length(device);

ndipols = 0;
nquads  = 0;
nhcorrs  = 0;
nvcorrs  = 0;
nmons   = 0;

for i=1:nd
  switch device(i).type
    case adipo
      ndipols = ndipols + 1;
      dipols(ndipols).name = device(i).name;
      dipols(ndipols).isfesa = device(i).isfesa;
      dipols(ndipols).spos = device(i).spos;
      dipols(ndipols).faulty = device(i).faulty;
      dipols(ndipols).madname = device(i).madname;
      dipols(ndipols).prefix = device(i).prefix;
      
    case aquad
      nquads =  nquads + 1;
      quads(nquads).name = device(i).name;
      quads(nquads).isfesa = device(i).isfesa;
      quads(nquads).spos = device(i).spos;
      quads(nquads).faulty = device(i).faulty;
      quads(nquads).madname = device(i).madname;
      quads(nquads).prefix = device(i).prefix;
      
    case ahcorr
      nhcorrs =  nhcorrs + 1;
      hcors(nhcorrs).name = device(i).name;
      hcors(nhcorrs).isfesa = device(i).isfesa;
      hcors(nhcorrs).spos = device(i).spos;
      hcors(nhcorrs).faulty = device(i).faulty;
      hcors(nhcorrs).madname = device(i).madname;
      hcors(nhcorrs).prefix = device(i).prefix;

    case avcorr
      nvcorrs =  nvcorrs + 1;
      vcors(nvcorrs).name = device(i).name;
      vcors(nvcorrs).isfesa = device(i).isfesa;
      vcors(nvcorrs).spos = device(i).spos;
      vcors(nvcorrs).faulty = device(i).faulty;
      vcors(nvcorrs).madname = device(i).madname;
      vcors(nvcorrs).prefix = device(i).prefix;
      
     case amoni
       nmons = nmons + 1;
       mons(nmons).name  = device(i).name;
       mons(nmons).isfesa = device(i).isfesa;
       mons(nmons).spos = device(i).spos;
       mons(nmons).faulty = device(i).faulty;
       mons(nmons).madname = device(i).madname;
       mons(nmons).prefix = device(i).prefix;
       
   end
end  