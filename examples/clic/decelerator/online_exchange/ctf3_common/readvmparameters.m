function readvalue = readvmparameters(params,name)

filename = ['virtual.' params];
vmdb = importdata(filename,' ',0);
vmndevs = length(vmdb.textdata);

n=0;
for j=1:vmndevs
  vmname = char(vmdb.textdata(j));
  if (strcmpi(name,vmname))
    n = j;
    break
  end
end

if (n == 0)
  error('Can not find device %s in the list of devices. Java Control Error.\n',name);
  readvalues(i) = 1/n;
end;

readvalue = vmdb.data(n);
fprintf(1,'%s = %f in DB at n=%d\n', name, readvalue, n ); 
