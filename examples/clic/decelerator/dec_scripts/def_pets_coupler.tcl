### NB: only if the following parameter is set, the corresponding other parts of the code will be triggered
# [m]
set couplerlength 0.02

# Divide the cavity into n steps ( y, y' is calculated for every steps, several steps needed for large y')
set n_steps_coupler 1

# Define the longitudinal mode 
set beta_coupler_l 0.4528
set RQ_coupler 1e-10
set lambda_coupler_l 0.025

#
# Define the transverse modes
#

#    1             0.25                 5000                 15.42           1e-10
#                 w_t [V/(pC*m*mm)]     Q [-]               f [GHz]         beta_t [-]
foreach {no       w_t                  Q_t                  f_t             beta_t} {
    1            0.003996/0.02              1000                6.36           1e-10
    1            0.008022/0.02              300                7.98           1e-10
    1            0.04783/0.02              20.7                11.02           1e-10
    1            0.0542/0.02              18.3                11.96           1e-10
    1            0.004701/0.02             680                8.48           1e-10
    1            0.003691/0.02             700                8.94           1e-10
    1            0.003375/0.02             700                9.35           1e-10
    1            0.005105/0.02             480                10           1e-10
} {
    if { $is_one_minus_beta_included } {
	set Q_beta_factor [expr 1.0 / (1 - $beta_t*$beta_t_scaling ) ]
    } else {
	set Q_beta_factor 1.0
    }
    lappend modes_coupler_t_all "[expr $SI_c/(1e9*$f_t)] [expr 1e3*$w_t*$w_t_scaling_factor] [expr $Q_t*$Q_t_scaling_factor*$Q_beta_factor] [expr $beta_t*$beta_t_scaling]"
}

# Include only selected transverse modes for simulation
lappend modes_coupler_t [lindex $modes_coupler_t_all [expr 0]] 
#lappend modes_coupler_t [lindex $modes_coupler_t_all [expr 1]] 
#lappend modes_coupler_t [lindex $modes_coupler_t_all [expr 2]] 
#lappend modes_coupler_t [lindex $modes_coupler_t_all [expr 3]] 
#lappend modes_coupler_t [lindex $modes_coupler_t_all [expr 4]] 
#lappend modes_coupler_t [lindex $modes_coupler_t_all [expr 5]] 
puts "\'modes_coupler_t\' before ajustment: $modes_coupler_t"

#
# Adjust transverse modes
#

# LOOP PARAMETER
#  CHANGE PARAM IN MODE 1
#set ll [list [lindex $ll 0] [lreplace [lindex $ll 1] 1 1 $loop_param2]]  
#  CHANGE PARAM IN MODE 2
#set ll [list [lreplace [lindex $ll 0] 0 0 $loop_param2] [lindex $ll 1]]  

# CHANGE ALLE 4 PARAMETERS (simultanious scaling)
#set ll [list [lreplace [lindex $ll 0] 1 1 [expr $loop_param2*[lindex [lindex $ll 0] 1]] ] [lindex $ll 1]]  
#set ll [list [lreplace [lindex $ll 0] 2 2 [expr $loop_param2*[lindex [lindex $ll 0] 2]] ] [lindex $ll 1]]  
#set ll [list [lindex $ll 0] [lreplace [lindex $ll 1] 1 1 [expr $loop_param2*[lindex [lindex $ll 1] 1]] ]]
#set ll [list [lindex $ll 0] [lreplace [lindex $ll 1] 2 2 [expr $loop_param2*[lindex [lindex $ll 1] 2]] ]]

puts "\'modes_coupler_t\' after adjustent : $modes_coupler_t"



