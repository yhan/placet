#
# Script that loops main.tcl with two user defined parameter loops
#

# argsexamples:
#  ../res/
#  10
set g_deceleratorrootpath [lindex $argv 0]
set ext_param1 [lindex $argv 1]

# default parameters 
if {$argc < 1} {
    set g_deceleratorrootpath ".."
}
if {$argc < 2} {
    set ext_param1 1
}



# define placet executable here
set g_placetbin "placet-octave"


# clear and date output file
set cmd "date"
exec bash << $cmd > placet_output.txt

# main script, loop param1 (currently: Q, w_t scaling)
set g_n_runs 1
#set g_n_runs 140

    for {set run1 1} {$run1 <= $g_n_runs} {incr run1} {
    set g_loop_param1 $run1
    puts "Looping simulation with loop param1: $run1"

    # loops general parameter 
#	set g_param_list [list 0.000001 0.01 0.03 0.1 0.3]
#	set g_param_list [list 1.0 0.98 0.95 0.90 0.50 0]
	set g_param_list [list 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0 21.0]
	set g_param_list [list 1.0]
	set g_param_list [list 1.0 3.0 6.0 10.0 30.0 60.0 100.0 300.0 600.0 1000.0]

    set g_n_params [llength $g_param_list]
    for {set run2 1} {$run2 <= $g_n_params} {incr run2} {
	set g_loop_param2 [lindex $g_param_list [expr $run2-1]]
	puts "Looping simulation with loop param2: $g_loop_param2"
	set cmd "$g_placetbin $g_deceleratorrootpath/dec_scripts/main.tcl $g_deceleratorrootpath $g_loop_param1 $run1 $g_loop_param2 $run2 $ext_param1"
	puts $cmd
	exec bash << $cmd >>& placet_output.txt 
	set cmd "date"
	exec bash << $cmd >> placet_output.txt
    }
}

