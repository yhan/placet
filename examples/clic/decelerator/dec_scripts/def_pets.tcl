# Selected transverse modes for simulation
set new_modes_beta 1
set modes_beta_undamped 0
set new_modes_beta0 0
set old_modes_beta0 0
set sum_mode_old 0

#
# is the Q-factors given INCLUDING the factor (1-beta) ?   [ the effective damping is Q(1-beta) ]
#   if so, take it out, because it is included again in the PETS tracking element
#   (and it IS included in the slides from Igor, EA)
set is_one_minus_beta_included 1

if { $mysimname == "clic12" } {
    # strictly speaking "lattice-parameters", but defined here in order to calc E
    set n_PETS [expr 1372]
    set n_PETS [expr 1492]
    # Cavity active length
    set N_cell [expr 34.0]
    set length_cell 0.006253
} elseif { $mysimname == "tbl12" } {
    # strictly speaking "lattice-parameters", but defined here in order to calc E
    set n_PETS 16
    # Cavity active length
    set N_cell 128
    set length_cell 0.006253
} elseif { $mysimname == "tbts" } {
    set n_PETS 1
    # Cavity active length
    set N_cell 160
    set length_cell 0.006253
}   

# DEFINE CAVITY COUPLER 
if { $include_PETS_coupler } {
    source "$deceleratorrootpath/dec_scripts/def_pets_coupler.tcl"  
}

# [um]  NB: only used to calculate field non-uniformity (e.g. value does not matter in this example)
set cavityaperture [expr 23e3 / 2]

# Simulation parameters: divide the cavity into n steps ( y, y' is calculated for every steps, several steps needed for large y')
set n_steps $n_PETS_steps

# Simulation parameters: define specific cavity simulation parameters (normally not used)
set rf_kick 0
set rf_long 0
set rf_order 4
set rf_size 0


# Define the longitudinal mode 
# damping term: NB: power Q from IS is Q calculated without catch-up (1-beta), so divide by (1-beta), to compensate for mult. by (1-beta) in code
if { $clicnote_parameters } {
    set beta_l 0.4529
    set RQ [expr 2294.67e-0 / 2]  
    set fundamental_mode_Q [expr 1.0e20]
} elseif { $mysimname == "tbts" } {
    set beta_l [expr 0.459-0.0]
    set RQ [expr (2222.0e-0/2) / 1.0]  
    set fundamental_mode_Q [expr 7000.0e0/(1-$beta_l)]
} elseif { $mysimname == "tbl12" } {
    set beta_l [expr 0.459-0.0]
    set RQ [expr (2222.0e-0/2) / 1.0]  
    set fundamental_mode_Q [expr 7000.0e0/(1-$beta_l)]
} else {
    #  new and "coming" parameters
    #set beta_l [expr 0.459-0.0]
    #set RQ [expr (2222.0e-0/2) / 1.0]  
    set beta_l 0.4529
    set RQ [expr 2294.67e-0 / 2]  
    set fundamental_mode_Q [expr 7000.0e0/(1-$beta_l)]
    #set beta_l [expr 0.4529 / $RQ_vg_scaling_factor]
    #set RQ [expr 2294.67e-0 / 2 / $RQ_vg_scaling_factor]  
}
if { $ignore_wall_losses } {
    set fundamental_mode_Q [expr 1.0e20]
}
    
set lambda_l 0.025

# scale R/Q
set RQ [expr $RQ*$RQ_scaling_factor]

puts "\'lambda_l\' before ajustment: $lambda_l"


#
# Detuning of PETS resonance frequency
#

# CONFIG A - NO DETUNING
#    DETUNING [Hz]
  set detun 00.0e6
  set detun_N_cell 0
# END CONFIG A

# CONFIG B - E0 fixed
#    DETUNING [Hz]
#  set detun 125.0e6
#  set detun_N_cell 2
# END CONFIG B

# CONFIG B2 - l_PETS fixed
#    DETUNING [Hz]
#  set detun 125.0e6
#  set detun_N_cell 0
# END CONFIG B

# calculate final cavity length and detuning 
  set cavitylength [expr ($N_cell + $detun_N_cell)*$length_cell]
  # lattice parameter
  set dx_detuning $detun_N_cell*$length_cell
  set lambda_l [expr $lambda_l*(1 - $detun/($SI_c/$lambda_l + $detun))]

# EA VERY TEMP
#set cavitylength [expr 0.2333+0.000]

puts "\'lambda_l\' after ajustment: $lambda_l"


# - OBSOLETE 

# set cavitylength (length must be half-integer multiples of the following quantity in order to not bias Power production due to different phase
#set in_phase_travellength [expr 0.025*($beta_l/(1-$beta_l))]
# 1st harmonic
#set cavitylength [expr 10*$in_phase_travellength +  4*0.5*$in_phase_travellength + 0.000]
# 2nd harmonic
#set cavitylength [expr 10*$in_phase_travellength +  3*0.5*$in_phase_travellength]
#set cavitylength [expr 10*$in_phase_travellength +  27*0.5*$in_phase_travellength]
# sigma = 1000
#set cavitylength 0.2455
#set cavitylength 0.491
# sigma = 400
#set cavitylength 0.2534
#set cavitylength 0.24
#set cavitylength 0.2610
# DETUNED LAMBDA_L - 1st harm
#set cavitylength [expr 10*$in_phase_travellength +  5*0.5*$in_phase_travellength]
#set lambda_l 0.02475
# DETUNED LAMBDA_L - 2nd harm
#set cavitylength [expr 10*$in_phase_travellength +  6*0.5*$in_phase_travellength]
#set lambda_l 0.0246
# DETUNED LAMBDA_L - low beta_l (0.30)
#set cavitylength [expr 10*$in_phase_travellength +  28*0.5*$in_phase_travellength]
#set lambda_l 0.024925

# - OBSOLETE 


#
# Define the transverse modes
#


#    10            8.300e-00                004.6                15.46           1e-10
#    8             4.820e-00                003.8                13.40           1e-10
#    9             2.630e-00                006.2                15.46           1e-10
#                 w_t [V/(pC*m*mm)]     Q [-]                f [GHz]         beta_t [-]
foreach {no       w_t                  Q_t                  f_t             beta_t} {
    1            0.07373e-0               003.4                3.95           0.43
    2            0.10783e-0                005.5                6.92           0.67
    3            0.13885e-0                005.0                8.50           0.70
    4            3.9855e-00                006.82                12.01           0.67
    5            3.3693e-00                006.30               16.40           0.56
    6            0.0634e-00                527.0                27.41           0.18
    7            0.02256e-00                156.0                28.0           0.03
    8            0.03368e-00                943.0                32.82           0.02
    9            2.09e-0                 004.47                10.78           1e-10
    10            1.926e-00                004.16                12.54           1e-10
    11           3.7961e-00                004.79               16.24           1e-10
    12            0.043e-00                785.0                27.41           1e-10
    13            0.047e-00                116.0                28.0           1e-10
    14            0.031e-00                1180.0                32.0           1e-10
    15             0.045e-00                300.0                27.44           1e-10
    16            0.019e-00                180.0                28.05           1e-10
    17            0.017e-00                290.0                32.912          1e-10
    18            0.200e-00                085.0                39.12           1e-10
    19            0.030e-00                120.0                41.83           1e-10
    20            0.015e-00                380.0                48.91           1e-10
    21            0.850e-00                003.7                10.00           1e-10
    22            4.820e-00                003.8                13.40           1e-10
    23            2.630e-00                006.2                15.46           1e-10
    24            8.300e-00                004.6                15.46           1e-10
    25            0.560e-00                019.97                5.74           0.76e-0
    26            5.160e+00                085.58               15.57           0.62e-0
} {

    if { $is_one_minus_beta_included } {
	set Q_beta_factor [expr 1.0 / (1 - $beta_t*$beta_t_scaling ) ]
    } else {
	set Q_beta_factor 1.0
    }

    lappend modes_t_all "[expr $SI_c/(1e9*$f_t)] [expr 1e3*$w_t*$w_t_scaling_factor] [expr $Q_t*$Q_t_scaling_factor*$Q_beta_factor] [expr $beta_t*$beta_t_scaling]"
    set wedge_factor 2.0e0
    lappend modes_t_all_wedge "[expr $SI_c/(1e9*$f_t)] [expr 1e3*$w_t*$w_t_scaling_factor] [expr $Q_t*$wedge_factor*$Q_t_scaling_factor*$Q_beta_factor] [expr $beta_t*$beta_t_scaling]"
# Q x 2
#  lappend modes_t_all "[expr $SI_c/($f_t*1e9)] [expr 1e3*$w_t] [expr 2*$Q_t] $beta_t"
#   lappend modes_t_all "[expr $SI_c/((14.4)*1e9)] [expr 1e3*$w_t] [expr $Q_t*10] $beta_t"
# Q-scaling
#   lappend modes_t_all "[expr $SI_c/((4.6+0.4*$loop_param1)*1e9)] [expr 1e3*$w_t] [expr $Q_t*$loop_param2] $beta_t"
## w-scaling
#   lappend modes_t_all "[expr $SI_c/((9.6+0.4*$loop_param1)*1e9)] [expr 1e3*$w_t*$loop_param2] [expr $Q_t] $beta_t"

# long scan (Q-scaling)
#   lappend modes_t_all "[expr $SI_c/((4.6+0.4*$loop_param1)*1e9)] [expr 1e3*$w_t] [expr $Q_t*$loop_param2] $beta_t"

# long scan (fixed QT + Q-scaling )
   set start_f 4.6
#   set scale_f_t 15.46
   set scale_f_t 6.2
#   lappend modes_t_all "[expr $SI_c/(($start_f+0.4*$loop_param1)*1e9)] [expr 1e3*$w_t] [expr $loop_param2*$Q_t*((($start_f+0.4*$loop_param1))/$scale_f_t)] $beta_t"
# short scan (Q-scaling)
#   lappend modes_t_all "[expr $SI_c/((22.5+0.5*$loop_param1)*1e9)] [expr 1e3*$w_t] [expr $Q_t*$loop_param2] $beta_t"
}


# Include only selected transverse modes for simulation
if { $new_modes_beta } {
lappend modes_t [lindex $modes_t_all [expr 0]] 
lappend modes_t [lindex $modes_t_all [expr 1]] 
lappend modes_t [lindex $modes_t_all [expr 2]] 
lappend modes_t [lindex $modes_t_all [expr 3]] 
lappend modes_t [lindex $modes_t_all [expr 4]] 
lappend modes_t [lindex $modes_t_all [expr 5]] 
lappend modes_t [lindex $modes_t_all [expr 6]] 
lappend modes_t [lindex $modes_t_all [expr 7]] 
} elseif { $new_modes_beta0 } {
lappend modes_t [lindex $modes_t_all [expr 8]] 
lappend modes_t [lindex $modes_t_all [expr 9]] 
lappend modes_t [lindex $modes_t_all [expr 10]] 
lappend modes_t [lindex $modes_t_all [expr 11]] 
lappend modes_t [lindex $modes_t_all [expr 12]] 
lappend modes_t [lindex $modes_t_all [expr 13]] 
} elseif { $old_modes_beta0 } {
lappend modes_t [lindex $modes_t_all [expr 14]] 
lappend modes_t [lindex $modes_t_all [expr 15]] 
lappend modes_t [lindex $modes_t_all [expr 16]] 
lappend modes_t [lindex $modes_t_all [expr 17]] 
lappend modes_t [lindex $modes_t_all [expr 18]] 
lappend modes_t [lindex $modes_t_all [expr 19]] 
lappend modes_t [lindex $modes_t_all [expr 20]] 
lappend modes_t [lindex $modes_t_all [expr 21]] 
lappend modes_t [lindex $modes_t_all [expr 22]] 
} elseif { $modes_beta_undamped } {
lappend modes_t [lindex $modes_t_all [expr 24]] 
lappend modes_t [lindex $modes_t_all [expr 25]] 
} else {

if { $sum_mode_old } {
    ### NB: mode 9 (last) is the "sum mode" of all modes (for frequency scan)
    lappend modes_t [lindex $modes_t_all [expr 23]] 
} else {
#lappend modes_t [lindex $modes_t_all [expr 0]] 
#lappend modes_t [lindex $modes_t_all [expr 1]] 
#lappend modes_t [lindex $modes_t_all [expr 2]] 
#lappend modes_t [lindex $modes_t_all [expr 3]] 
#lappend modes_t [lindex $modes_t_all [expr 4]] 
#lappend modes_t [lindex $modes_t_all [expr 5]] 
}
}

lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 0]] 
lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 1]] 
lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 2]] 
lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 3]] 
lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 4]] 
lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 5]] 
lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 6]] 
lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 7]] 
lappend modes_t_wedge [lindex $modes_t_all_wedge [expr 8]] 


#puts "\'modes_t\' before ajustment: $modes_t"

#
# Adjust transverse modes
#

# LOOP PARAMETER
#  CHANGE PARAM IN MODE 1
#set ll [list [lindex $ll 0] [lreplace [lindex $ll 1] 1 1 $loop_param2]]  
#  CHANGE PARAM IN MODE 2
#set ll [list [lreplace [lindex $ll 0] 0 0 $loop_param2] [lindex $ll 1]]  

# CHANGE ALLE 4 PARAMETERS (simultanious scaling)
#set ll [list [lreplace [lindex $ll 0] 1 1 [expr $loop_param2*[lindex [lindex $ll 0] 1]] ] [lindex $ll 1]]  
#set ll [list [lreplace [lindex $ll 0] 2 2 [expr $loop_param2*[lindex [lindex $ll 0] 2]] ] [lindex $ll 1]]  
#set ll [list [lindex $ll 0] [lreplace [lindex $ll 1] 1 1 [expr $loop_param2*[lindex [lindex $ll 1] 1]] ]]
#set ll [list [lindex $ll 0] [lreplace [lindex $ll 1] 2 2 [expr $loop_param2*[lindex [lindex $ll 1] 2]] ]]

puts "\'modes_t\' after adjustent : $modes_t"


