#
# Extract y-pos from BPM readings
#
proc read_bpm {} {
    set bpm [BpmReadings]
    set res ""
    foreach x $bpm {
	lappend res [lindex $x 2]
    }
    return $res
}

#
# Extract matrix-element (NB: indexing starts with 0 )
#
proc elem_r_c {M r c} {
    return [lindex [lindex $M [expr $r]] [expr $c]]
}


#
# From DS: tools2
#
proc transform {r} {
    set res ""
    foreach x $r {
	lappend res [lindex $x 2]
    }
    return $res
}

namespace eval vector {

    namespace export add subtract combine

    proc add {r1 r0} {
	set res ""
	foreach y1 $r1 y0 $r0 {
	    lappend res [expr $y1+$y0]
	}
	return $res
    }

    proc subtract {r1 r0} {
	set res ""
	foreach y1 $r1 y0 $r0 {
	    lappend res [expr $y1-$y0]
	}
	return $res
    }

    proc subtract_and_add {r0 r1 r2} {
	set res $r0
	foreach y1 $r1 y2 $r2 {
	    lappend res [expr $y2-$y1]
	}
	return $res
    }

    proc combine {args} {
	set n [llength $args]
	set rall [lindex $args 0]
	for {set i 1} {$i<$n} {incr i} {
	    set rall [subtract_and_add $rall [lindex $args $i] [lindex $args 0]]
	}
	return $rall
    }
}

namespace eval matrix {

    namespace export combine subtract multiply_vector

    proc subtract {r1 r0} {
	set res ""
	foreach y1 $r1 y0 $r0 {
	    lappend res [vector_subtract $y1 $y0]
	}
	return $res
    }
    
    proc subtract_and_add {r0 r1 r2} {
	set res ""
	foreach tmp $r0 x1 $r1 x2 $r2 {
	    foreach y1 $x1 y2 $x2 {
		lappend tmp [expr $y2-$y1]
	    }
	    lappend res $tmp
	}
	return $res
    }

    proc combine {args} {
	set n [llength $args]
	set rall [lindex $args 0]
	for {set i 1} {$i<$n} {incr i} {
	    set rall [subtract_and_add $rall [lindex $args $i] [lindex $args 0]]
	}
	return $rall
    }
    
    proc multiply_vector {m v} {
	set res ""
	set tmp [lindex $m 0]
	foreach x $tmp {
	    lappend res "0.0"puts "R0: $R0"


	}
	foreach tmp $m y $v {
	    set tmp2 $res
	    set res ""
	    foreach x $tmp r $tmp2 {
		lappend res [expr $x*$y+$r]
	    }
	}
	return $res
    }
}




#
# old scripts, obselete  (EA)
proc old_response_proc {} {

# reads Octave (response) matrix from file
set R_filename "R0.dat"
set R0 {}

if {[file exists $R_filename] == 1} {
    set fileId [open $R_filename "r"]
    #set test [read $fileId]
    #puts "test: $test"
    set dummy [gets $fileId]
    set dummy [gets $fileId]
    set dummy [gets $fileId]
    set dummy [gets $fileId]
    set dummy [gets $fileId]
    while {![eof $fileId]} {
	set line [gets $fileId]
	lappend R0 $line
    }
    puts "R0: $R0"
    set row 3
    set col 3
    puts "R0: [lindex [lindex $R0 $row] $col]"
} else {
    puts "ERROR: response matrix not created\n\n"
}
}





# handly auxiliary routine
proc gauss_rand {} {
    # creates a gaussian (0,1) random variable from two uniform random variables
    # (Box-Muller algorithm)
    set x [expr { rand() }]
    set y [expr { rand() }]
    set z [expr sqrt(-2*log($x))*cos(2*acos(-1)*$y)]
    return $z
}
