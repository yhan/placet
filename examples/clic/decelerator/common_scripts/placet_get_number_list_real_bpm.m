% returns list of BPMs, excluding QuadBpms  (which are
% automatically included by default)
function B=placet_get_number_list_real_bpm(beamline)

B = placet_get_number_list(beamline, "bpm");

QB = placet_get_number_list(beamline, "quadbpm");
for n=1:length(QB)
  B(  find(B == QB(n)) ) = [];
end% for 

return;
