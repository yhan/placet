% importdata does not exist for octave! 
% we write something to mimic behaviour for the case needed... (EA)
function data = importdata(filename, dummy, dummy)

of = fopen(filename,'rb');

name = cell(1,1);
value(1) = 0;
n=1;
while(  1 )
  [name_, value_]= fscanf(of, '%s %g', "C");
  if(size(name_,1) == 0)
    break
  end% if
  name(:,n) = name_;
  value(:,n) = value_;
  n++;
end% while

data.textdata = name;
data.data = value;

fclose(of);
