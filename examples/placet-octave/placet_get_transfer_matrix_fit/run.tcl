#
# It calculates R# with the final doublet quadrupoles halved in intensity
#

ParallelThreads -num 2

set e_initial 1500
set e0 $e_initial
set script_dir .

array set args {
    deltae 0.001
    deltak 0.001
    six_dim 1
    sr 1
}
   
array set args $argv

set deltae $args(deltae)
set deltak $args(deltak)
set six_dim $args(six_dim)
set sr $args(sr)

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

set six_dim_sbend $six_dim
# this is not implemented yet
set six_dim_quad 0

SetReferenceEnergy $e0
Girder
source $script_dir/lattices/bds.match.linac4b_v_10_10_11
BeamlineSet -name test

array set match {
    alpha_x 0.59971622
    alpha_y -1.93937335
    beta_x  18.382571
    beta_y  64.450775
    emitt_x 6.6
    emitt_y 0.2
    charge  4.0e9
    sigma_z 44.0
    phase   0.0
    e_spread -1.0
}

set e0 $e_initial
set charge $match(charge)

set n_slice 50
set n 1000
set n_total [expr $n_slice*$n]
make_beam_many beam0 $n_slice $n


set n_total 3
set n_slice 1
set n 3
make_beam_many beam3 $n_slice $n

set n_total 4
set n_slice 1
set n 4
make_beam_many beam4 $n_slice $n

FirstOrder 1

Octave {
    [RF,TF] = placet_get_transfer_matrix_fit("test", "beam0", "None");
    RF
    printf("det(RF) =  %g\n\n", det(RF));
    
    RM = placet_get_transfer_matrix("test")
    printf("det(RM) =  %g\n\n", det(RM));
}

Octave {
    BI = placet_get_number_list("test", "bpm");
    out = placet_get_transfer_matrix_elements_fit("test", "beam0", "None", { "11", "12", "16", "43", "44", "166" }, 0, BI);
    save -text output_elements.dat out;
}
