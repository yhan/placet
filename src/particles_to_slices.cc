#include <limits>
#include <tk.h>

#include "particles_to_slices.h"
#include "beam.h"
#include "structures_def.h"
#include "lattice.h"
#include "girder.h"
#include "wakes.h"
#include "rmatrix.h"

void P2S::step_4d_0(BEAM* beam)
{
  placet_printf(VERBOSE, "ParticlesToSlices\n");
  if (beam->particle_beam==false) {
    placet_cout << ERROR << "in particles_to_slices: this is not a particle beam!" << endmsg;
    exit(1);
  }
  
  int nslices=beam->n_max/beam->bunches/beam->macroparticles;
  int nmacroparticles=beam->macroparticles;
  int nbunches=beam->bunches;
  
  double *ch,*x,*xp,*y,*yp,*z, *e;
  double *emin,*emax;
  double *corr;

  emin=new double[nslices];
  emax=new double[nslices];
  x=new double[nmacroparticles*nslices];
  xp=new double[nmacroparticles*nslices];
  y=new double[nmacroparticles*nslices];
  yp=new double[nmacroparticles*nslices];
  z=new double[nmacroparticles*nslices];
  e=new double[nmacroparticles*nslices];
  ch=new double[nmacroparticles*nslices];
  corr=new double[nmacroparticles*nslices*10];
		
  int particles_per_bunch=beam->slices/nbunches;
  
  for (int ib=0;ib<nbunches;++ib) {
    int m=ib*particles_per_bunch;
    double zmin;
    double zmax;
    {
      double zmean=0;
      for (int i=0;i<particles_per_bunch;++i) {
	zmean+=beam->particle[m++].z;
      }
      zmean/=particles_per_bunch;
      m=ib*particles_per_bunch;
      double tmp1=0;
      double tmp2=0;
      for (int i=0;i<particles_per_bunch;++i) {
	double tmp3=beam->particle[m++].z-zmean;
	tmp1+=tmp3*tmp3;
	tmp2+=tmp3;
      }
      double zrms=sqrt((tmp1-tmp2*tmp2/particles_per_bunch)/(particles_per_bunch-1));
      zmin=zmean-3*zrms;
      zmax=zmean+3*zrms;
    }
    
    double dzi=(zmax-zmin)/nslices;
    for (int j=0;j<nslices;++j) {
      emin[j]= std::numeric_limits<double>::infinity();
      emax[j]=-std::numeric_limits<double>::infinity();
    }
    
    m=ib*particles_per_bunch;
    for (int i=0;i<particles_per_bunch;++i) {
      PARTICLE &particle=beam->particle[m];
      int j=(int)((particle.z-zmin)/dzi);
      if ((j>=0)&&(j<nslices)) {
	if (particle.energy<emin[j]) emin[j]=particle.energy;
	if (particle.energy>emax[j]) emax[j]=particle.energy;
      }
      m++;
    }    
    
    for (int j=0;j<nmacroparticles*nslices;++j) {
      ch[j]=0.0;
      e[j]=0.0;
      x[j]=0.0;
      xp[j]=0.0;
      y[j]=0.0;
      yp[j]=0.0;
      for (int i=0;i<10;++i) {
	corr[j*10+i]=0.0;
      }
    }
    
    m=ib*particles_per_bunch;
    for (int i=0;i<particles_per_bunch;++i) {
      PARTICLE &particle=beam->particle[m];
      int j=(int)((particle.z-zmin)/dzi);
      if ((j>=0)&&(j<nslices)) {
	int k;
        if (emax[j]==emin[j])
          k=0;
        else
          k=(int)(nmacroparticles*(particle.energy-emin[j])/(emax[j]-emin[j]));
	if (k==nmacroparticles)
          --k;
	ch[j*nmacroparticles+k]+=1;
	x[j*nmacroparticles+k]+=particle.x;
	xp[j*nmacroparticles+k]+=particle.xp;
	y[j*nmacroparticles+k]+=particle.y;
	yp[j*nmacroparticles+k]+=particle.yp;
	e[j*nmacroparticles+k]+=particle.energy;
      }
      m++;
    }
    
    for (int j=0;j<nmacroparticles*nslices;++j) {
      if (ch[j]!=0) {
	x[j]/=ch[j];
	xp[j]/=ch[j];
	y[j]/=ch[j];
	yp[j]/=ch[j];
	e[j]/=ch[j];
      }
    }
    
    m=ib*particles_per_bunch;
    for (int i=0;i<particles_per_bunch;++i) {
      PARTICLE &particle=beam->particle[m];
      int j=(int)((particle.z-zmin)/dzi);
      if ((j>=0)&&(j<nslices)) {
        int k;
        if (emax[j]==emin[j])
          k=0;
        else
          k=(int)(nmacroparticles*(particle.energy-emin[j])/(emax[j]-emin[j]));	
	if (k==nmacroparticles)
          --k;
	double x1,xp1,y1,yp1;
	x1=particle.x-x[j*nmacroparticles+k];
	xp1=particle.xp-xp[j*nmacroparticles+k];
	y1=particle.y-y[j*nmacroparticles+k];
	yp1=particle.yp-yp[j*nmacroparticles+k];
	corr[(j*nmacroparticles+k)*10+0]+=x1*x1;
	corr[(j*nmacroparticles+k)*10+1]+=x1*xp1;
	corr[(j*nmacroparticles+k)*10+2]+=x1*y1;
	corr[(j*nmacroparticles+k)*10+3]+=x1*yp1;
	corr[(j*nmacroparticles+k)*10+4]+=xp1*xp1;
	corr[(j*nmacroparticles+k)*10+5]+=xp1*y1;
	corr[(j*nmacroparticles+k)*10+6]+=xp1*yp1;
	corr[(j*nmacroparticles+k)*10+7]+=y1*y1;
	corr[(j*nmacroparticles+k)*10+8]+=y1*yp1;
	corr[(j*nmacroparticles+k)*10+9]+=yp1*yp1;
      }
      m++;
    }
    
    for (int j=0;j<nmacroparticles*nslices;++j) {
      if (ch[j]!=0) {
        corr[j*10+0]/=ch[j];
	corr[j*10+1]/=ch[j];
	corr[j*10+2]/=ch[j];
	corr[j*10+3]/=ch[j];
	corr[j*10+4]/=ch[j];
	corr[j*10+5]/=ch[j];
	corr[j*10+6]/=ch[j];
	corr[j*10+7]/=ch[j];
	corr[j*10+8]/=ch[j];
	corr[j*10+9]/=ch[j];
      }
    }

    // fill "empty" macroparticles (energy bins)

    int last_j=-1;
    for (int j=0;j<nmacroparticles*nslices;++j) {
      if (ch[j]!=0.0) {
        if (j>last_j && last_j==-1) {
          int N=j+1;
          ch[j]/=N;
          for (int k=0;k<j;++k) {
            ch[k]=ch[j];
            x[k]=x[j];
            y[k]=y[j];
            xp[k]=xp[j];
            yp[k]=yp[j];
            e[k]=e[j];
            corr[k*10+0]=corr[j*10+0];
	    corr[k*10+1]=corr[j*10+1];
	    corr[k*10+2]=corr[j*10+2];
	    corr[k*10+3]=corr[j*10+3];
	    corr[k*10+4]=corr[j*10+4];
	    corr[k*10+5]=corr[j*10+5];
	    corr[k*10+6]=corr[j*10+6];
	    corr[k*10+7]=corr[j*10+7];
	    corr[k*10+8]=corr[j*10+8];
	    corr[k*10+9]=corr[j*10+9];
          }
        } else if (j>last_j) {
          int N=j-last_j;
          ch[last_j]/=N;
          for (int k=last_j+1;k<j;++k) {
            ch[k]=ch[last_j];
            x[k]=x[last_j];
            y[k]=y[last_j];
            xp[k]=xp[last_j];
            yp[k]=yp[last_j];
            e[k]=e[last_j];
            corr[k*10+0]=corr[last_j*10+0];
	    corr[k*10+1]=corr[last_j*10+1];
	    corr[k*10+2]=corr[last_j*10+2];
	    corr[k*10+3]=corr[last_j*10+3];
	    corr[k*10+4]=corr[last_j*10+4];
	    corr[k*10+5]=corr[last_j*10+5];
	    corr[k*10+6]=corr[last_j*10+6];
	    corr[k*10+7]=corr[last_j*10+7];
	    corr[k*10+8]=corr[last_j*10+8];
	    corr[k*10+9]=corr[last_j*10+9];
          }
        }
        last_j = j;
      }
    }
    if (last_j<nmacroparticles*nslices-1) {
      int N=nmacroparticles*nslices-last_j;
      ch[last_j]/=N;
      for (int k=last_j+1;k<nmacroparticles*nslices;++k) {
        ch[k]=ch[last_j];
        x[k]=x[last_j];
        y[k]=y[last_j];
        xp[k]=xp[last_j];
        yp[k]=yp[last_j];
        e[k]=e[last_j];
        corr[k*10+0]=corr[last_j*10+0];
        corr[k*10+1]=corr[last_j*10+1];
        corr[k*10+2]=corr[last_j*10+2];
        corr[k*10+3]=corr[last_j*10+3];
        corr[k*10+4]=corr[last_j*10+4];
        corr[k*10+5]=corr[last_j*10+5];
        corr[k*10+6]=corr[last_j*10+6];
        corr[k*10+7]=corr[last_j*10+7];
        corr[k*10+8]=corr[last_j*10+8];
        corr[k*10+9]=corr[last_j*10+9];
      }
    }
    
    m=ib*particles_per_bunch;
    if (center) zmin=-(zmax-zmin)/2;		
    for (int i=0;i<nmacroparticles*nslices;++i) {
      PARTICLE *particle_ptr = &beam->p_tmp[m];
      if (read_structure) {
	beam->z_position[ib*nslices+i/nmacroparticles]=zmin+(2*(i/nmacroparticles)+1)*dzi/2;
        particle_ptr->wgt=ch[i]/particles_per_bunch;
      }
      particle_ptr->energy=e[i];
      particle_ptr->x=x[i];
      particle_ptr->xp=xp[i];
      particle_ptr->y=y[i];
      particle_ptr->yp=yp[i];
      beam->sigma_xx[m].r11=corr[i*10+0];
      beam->sigma_xx[m].r12=corr[i*10+1];
      beam->sigma_xx[m].r21=corr[i*10+1];
      beam->sigma_xx[m].r22=corr[i*10+4];
      beam->sigma[m].r11=corr[i*10+7];
      beam->sigma[m].r12=corr[i*10+8];
      beam->sigma[m].r21=corr[i*10+8];
      beam->sigma[m].r22=corr[i*10+9];
      beam->sigma_xy[m].r11=corr[i*10+2];
      beam->sigma_xy[m].r12=corr[i*10+3];
      beam->sigma_xy[m].r21=corr[i*10+5];
      beam->sigma_xy[m].r22=corr[i*10+6];
      m++;
    }
  }	
  
  delete []emin;
  delete []emax;
  delete []x;
  delete []xp;
  delete []y;
  delete []yp;
  delete []z;
  delete []e;
  delete []ch;
  delete []corr;
  
  beam->particle_beam = false;
  
  PARTICLE *p=beam->p_tmp;
  beam->p_tmp=beam->particle;
  beam->particle=p;
  
  int __tmp=beam->n_max;
  beam->n_max=beam->slices;
  beam->slices=__tmp;
  
  int *__tmp_ptr=beam->particle_number_tmp;
  beam->particle_number_tmp=beam->particle_number;
  beam->particle_number=__tmp_ptr;
  
  if (read_structure) {
    wakes_prepare(beam);
  }
}

int tk_ParticlesToSlices(ClientData /*clientdata*/, Tcl_Interp *interp, int argc, char *argv[])
{
  int read_structure=0;
  int center=0;
  double e0=-1;  
  
  Tk_ArgvInfo table[]={
    {(char*)"-read_structure",TK_ARGV_INT,(char*)NULL,
     (char*)&read_structure,
     (char*)"If set to 1 the beam structure (weights and z-positions) are also read"},
    {(char *)"-center", TK_ARGV_CONSTANT, (char *) 1, 
     (char *) &center, 
     (char *)"Synchronize bunches to <Z>=0 [boolean]"},
    {(char*)"-e0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&e0,
     (char*)"Nominal energy [GeV]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK) {
    return error;
  }
  
  if (argc!=1) {
    Tcl_SetResult(interp,"Too many arguments to <ParticlesToSlices>", TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  inter_data.girder->add_element(new P2S(read_structure!=0,center==1, e0));
  return TCL_OK;
}
