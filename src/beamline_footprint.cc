#include <limits>
#include <cfloat>

#include "beamline.h"
#include "sbend.h"

#include "Frame.hh"

void BEAMLINE::save_footprint(std::ostream &stream, int start, int end, double x0, double y0, double z0, double xangle, double yangle, double zangle )
{
  //int old_precision = stream.precision(DBL_DIG);
  stream <<
    "# Beamline three-dimensional footprint, in 10 columns per element\n"
    "# 1. 'x' coordinate (center) [m]\n"
    "# 2. 'y' coordinate (center) [m]\n"
    "# 3. 'z' coordinate (center) [m]\n"
    "# 4. 's' position along the beam line (taken at element exit) [m]\n"
    "# 5. element name\n"
    "# 6. element type\n"
    "# 7. element length [m]\n"
    "# 8. euler angle alpha (aka phi) [rad]\n"
    "# 9. euler angle beta (aka theta) [rad]\n"
    "# 10. euler angle gamma (aka psi) [rad]\n";
  if (end==-1)
    end=n_elements-1;
  double s = 0.0;
  Frame frame;
  frame *= Frame(Vector3d(x0, y0, z0));
  frame *= Frame(Rotation(Axis_X, xangle));
  frame *= Frame(Rotation(Axis_Y, yangle));
  frame *= Frame(Rotation(Axis_Z, zangle));
  auto euler = frame.get_axes().get_euler_angles();
  stream << frame.get_origin() << ' ' << s << ' ' << "\"[BEGIN]\" MARKER 0 " << euler[0] << ' ' << euler[1] << ' ' << euler[2] << '\n';
  for (int i=start;i<=end;i++) {
    const ELEMENT &element_ref = *element[i];
    if (fabs(element_ref.get_length()) !=0.0) {
      s += element_ref.get_length();
      const Vector3d element_center = [&] () { 
	if (const SBEND *sbend = element_ref.sbend_ptr()) {
	  const double angle = sbend->get_angle();
	  if (angle!=0.0) {
	    const double rho = element_ref.get_length() / angle; // m
	    return Vector3d(rho * sin(angle/2.0), -rho * (1.0 - cos(angle/2.0)), 0.);
	  }
	}
	return Vector3d(element_ref.get_length()/2.0, 0., 0.);
      } ();
      euler = frame.get_axes().get_euler_angles();
      stream << frame * element_center << ' ' << s << " \"" << element_ref.get_name() << "\" " << typeid(element_ref).name() << ' ' << element_ref.get_length() << ' ' << euler[0] << ' ' << euler[1] << ' ' << euler[2] << '\n';
      if (const SBEND *sbend = element_ref.sbend_ptr()) {
	const double angle = sbend->get_angle();
	if (angle!=0.0) {
	  const double rho = element_ref.get_length() / angle; // m
	  if (fabs(sbend->get_tilt()) > std::numeric_limits<double>::epsilon()) {
	    frame *= Frame(Rotation(Axis_X,  sbend->get_tilt()));
	    frame *= Frame(Vector3d(rho * sin(angle), -rho * (1.0 - cos(angle)), 0.));
	    frame *= Frame(Rotation(Axis_Z, -angle));
	    frame *= Frame(Rotation(Axis_X, -sbend->get_tilt()));
	  } else {
	    frame *= Frame(Vector3d(rho * sin(angle), -rho * (1.0 - cos(angle)), 0.));
	    frame *= Frame(Rotation(Axis_Z, -angle));
	  }
	} else {
	  frame *= Frame(Vector3d(element_ref.get_length(), 0., 0.));
	}
      } else {
	frame *= Frame(Vector3d(element_ref.get_length(), 0., 0.));
      }
    } else {
      euler = frame.get_axes().get_euler_angles();
      stream << frame.get_origin() << ' ' << s << " \"" << element_ref.get_name() << "\" " << typeid(element_ref).name() << ' ' << element_ref.get_length() << ' ' << euler[0] << ' ' << euler[1] << ' ' << euler[2] << '\n';
    }
  }
  euler = frame.get_axes().get_euler_angles();
  stream << frame.get_origin() << ' ' << s << ' ' << "\"[END]\" MARKER 0 " << euler[0] << ' ' << euler[1] << ' ' << euler[2] << '\n';
  //stream.precision(old_precision);
}
