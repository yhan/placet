#include <cstdlib>
#include <limits>

#include "wakes.h"

#include "beam.h"
#include "short_range.h"
#include "structures_def.h"

void
wakes_prepare(BEAM *beam, const char* wakename, int iw) {
  int i,j,k=0,l,m;
  double *p,zsum,ztmp;
  
  // function was modified by Jochem Snuverink, please discuss if not working
  SHORT_RANGE* short_range = get_short_range(wakename);
  if (!short_range) {
    placet_cout << ERROR << "SHORT RANGE wakefield " << wakename << " not found in wakes_prepare" << endmsg;
    exit(1);
  }

  p=beam->field[iw].kick;
  for (i=0;i<beam->slices_per_bunch;++i) {
    zsum=0.0;
    m=0;
    for (j=0;j<i;++j) {
      p[k]=short_range->wake_x(beam->z_position[i]
			       -beam->z_position[j]);
      k++;
      ztmp=short_range->wake_z(beam->z_position[i]
			       -beam->z_position[j]);
      for (l=0;l<beam->macroparticles;++l) {
	zsum-=ztmp*beam->particle[m++].wgt;
      }
    }
    // is it correct, subtracting same position? - JS
    p[k]=short_range->wake_x(beam->z_position[j]
			     -beam->z_position[j]);
    k++;
    // is it correct, subtracting same position? - JS
    ztmp=0.5*short_range->wake_z(beam->z_position[j]
				 -beam->z_position[j]);
    for (l=0;l<beam->macroparticles;++l) {
      zsum-=ztmp*beam->particle[m++].wgt;
    }
    beam->field[iw].de[j]=zsum;
  }
  /* longitudinal field will be filled in cavity0.cc if s_long[iw][0]>1.0 */
  beam->s_long[iw][0]=2.0;
}
