/*
  Sets up a multi-bunch beam for the drive beam injector
*/

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beam.h"
#include "beamline.h"
#include "structures_def.h"

/* Function prototypes */

#include "rmatrix.h"
#include "errf.h"
#include "malloc.h"
#include "mb_inj.h"
#include "placeti3.h"
#include "quadwake.h"
#include "spline.h"

extern int data_nstep;
extern double zero_point;

extern INJECTOR_DATA injector_data;

BEAM *make_multi_bunch_inject(BEAMLINE *beamline,char *name,
			      SPLINE *beaml,
			      INJECT_BEAM_PARAM *param,int nt,int n_max)
{
  FILE *file;
  BEAM *bunch;
  int i,n,k,nmacro,j,nbunches;
  double wgtsum,ecut,sigma_e,delta,fact,zshift=0.0;
  float tmp1,tmp2,tmp3;
  double betax,alphax,epsx;
  double betay,alphay,epsy;
  char buffer[1000],*point;
  double *store,*ep,elow,ehigh,de,sumwgt;
  double charge,e0,lambda;

  lambda=injector_data.lambda[0];
  zshift=injector_data.lambda[0]*1e6*param->phase/360.0;
  nbunches=param->n_bunch;
  //scd remove?
  //  a0=injector_data.a0;
  charge=param->charge;
  e0=param->e0;
  //  grad=injector_data.gradient;
  //  grad=1.0;

  qsort(param->bunch,nt,sizeof(BUNCHINFO),&bunchinfo_cmp_z);

  sigma_e=param->espread;
  ecut=param->ecut;
  nmacro=param->n_macro;

  betax=param->betax;
  betay=param->betay;
  alphax=param->alphax;
  alphay=param->alphay;
  epsx=param->emittx;
  epsy=param->emitty;

  epsx*=fabs(EMASS/e0*1e12)*EMITT_UNIT;
  epsy*=fabs(EMASS/e0*1e12)*EMITT_UNIT;

  /* Open the file that defines the bunch and read the number of slices */

  file=read_file(name);

  fscanf(file,"%d",&n);

  /* scd to update for number of phases */
  if (nmacro<0) {
    fscanf(file,"%d",&nmacro);
  }
  //  bunch=bunch_make(nbunches,n,nmacro,beamline->n_phases,n_max);
  bunch=bunch_make(nbunches,n,nmacro,1,n_max);

  bunch->drive_data->param.inject=(INJECT_BEAM_PARAM*)
    xmalloc(sizeof(INJECT_BEAM_PARAM));
  *(bunch->drive_data->param.inject)=*(param);

  bunch->last_wgt=param->last_wgt;

  fscanf(file,"%g",&tmp1);
  wgtsum=0.0;
  de=2.0*ecut/(double)(nmacro);
  if (param->n_macro>0) {
    if (nmacro>1){
      if (param->d_energy) {
	store=param->eslice_wgt;
	ep=param->d_energy;
      }
      else {
        store=(double*)alloca(sizeof(double)*nmacro);
	ep=(double*)alloca(sizeof(double)*nmacro);
	sumwgt=0.0;
	elow=-ecut;
	for (k=0;k<nmacro;k++){
	  ehigh=elow+de;
	  ep[k]=0.5*(ehigh+elow)*sigma_e;
          placet_printf(VERYVERBOSE,"%g %g\n",elow,ehigh);
	  store[k]=0.0;
	  store[k]=gauss_bin(elow,ehigh);
	  elow=ehigh;
	  sumwgt+=store[k];
	}
	for (k=0;k<nmacro;k++){
	  store[k]/=sumwgt;
	  placet_printf(VERYVERBOSE,"wgt[%d]: %g\n",k,store[k]);
	}
      }
    }
    for (i=0;i<n;i++){
      fscanf(file,"%g %g %g",&tmp1,&tmp2,&tmp3);
      for (k=0;k<nmacro;k++){
	if (nmacro>1){
	  //	  delta=-ecut+((double)k+0.5)*de;
	  delta=ep[k];
	  //	  bunch_set_slice_energy(bunch,0,i,k,e0+delta*sigma_e);
	  bunch_set_slice_energy(bunch,0,i,k,e0+delta);
	  fact=store[k];
	}
	else{
	  delta=0.0;
	  bunch_set_slice_energy(bunch,0,i,k,e0);
	  fact=1.0;
	}
	bunch_set_slice_wgt(bunch,0,i,k,(double)tmp3*fact);
	wgtsum+=tmp3*fact;
#ifdef TWODIM
	bunch_set_slice_x(bunch,0,i,k,0.0);
	bunch_set_slice_xp(bunch,0,i,k,0.0);
	//      bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,
	//			 epsx/(1.0+delta*sigma_e/e0));
	bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,epsx);
	bunch_set_sigma_xy(bunch,0,i,k);
#endif
	bunch_set_slice_y(bunch,0,i,k,zero_point);
	bunch_set_slice_yp(bunch,0,i,k,0.0);
	//      bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,
	//			 epsy/(1.0+delta/e0));
	bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,epsy);
      }
      bunch->field->de[i]=tmp2*1e-3;
      bunch->z_position[i]=zshift+tmp1;
      // for (j=0;j<beamline->n_phases;j++){
      // 	bunch->acc_field[j][i]=grad*cos(bunch->z_position[i]*1e-6/lambda*TWOPI
      // 					+beamline->phase[j]*PI/180.0);
      // 	//	placet_printf(INFO,"long: %g %g %g %g %g %g\n",lambda,beamline->phase[j],grad,tmp1,bunch->acc_field[j][i],bunch->z_position[i]);
      // }
    }
  }
  else {
    for (i=0;i<n;i++){
      for (k=0;k<nmacro;++k) {
	point=fgets(buffer,1000,file);
	tmp1=strtod(point,&point);
	de=strtod(point,&point);
	tmp3=strtod(point,&point);
	tmp2=strtod(point,&point);
	bunch_set_slice_energy(bunch,0,i,k,de);
	bunch_set_slice_wgt(bunch,0,i,k,(double)tmp3);
#ifdef TWODIM
	bunch_set_slice_x(bunch,0,i,k,0.0);
	bunch_set_slice_xp(bunch,0,i,k,0.0);
	//      bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,
	//			 epsx/(1.0+delta*sigma_e/e0));
	bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,epsx);
	bunch_set_sigma_xy(bunch,0,i,k);
#endif
	bunch_set_slice_y(bunch,0,i,k,zero_point);
	bunch_set_slice_yp(bunch,0,i,k,0.0);
	//      bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,
	//			 epsy/(1.0+delta/e0));
	bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,epsy);
      }
      bunch->field->de[i]=tmp2*1e-3;
      bunch->z_position[i]=zshift+tmp1;
      // for (j=0;j<beamline->n_phases;j++){
      // 	bunch->acc_field[j][i]=grad*cos(bunch->z_position[i]*1e-6/lambda*TWOPI
      // 					+beamline->phase[j]*PI/180.0);
      // 	placet_printf(INFO,"%g %g\n",tmp1,bunch->acc_field[j][i]);
      // }
    }
  }

  /* Read the transverse wakefield kick */

  for (i=0;i<n*(n+1)/2;i++){
    fscanf(file,"%g",&tmp3);
    field_set_kick(bunch->field,i,tmp3*1e-3);
  }

  /* scd end */

  close_file(file);

  if (beaml){
    for (i=0;i<n;i++){
      /*
      bunch->field->de[i]=beaml->eval(bunch->z_position[i*nmacro])
	*1e-9*charge*1.6e-10*1e3;
	*/
      bunch->field->de[i]=beaml->eval(bunch->z_position[i])
	*1e-9*charge*1.6e-10*1e3;
      /*MV/pC*/
    }
  }
  /* better skip? ***? */
  /*
  for (i=0;i<n*nmacro;i++){
    bunch->particle[i].wgt/=wgtsum;
  }
  */

  k=n*nmacro;
  for (j=1;j<nbunches;j++){
    for (i=0;i<n;i++){
      bunch->z_position[i+j*n]=bunch->z_position[i]+param->bunch[j].z;
    }
    for (i=0;i<n*nmacro;i++){
      bunch->particle[k]=bunch->particle[i];
      bunch->sigma[k]=bunch->sigma[i];
      /*
      bunch->z_position[k]=bunch->z_position[i]+param->bunch[j].z;
      */
#ifdef TWODIM
      bunch->sigma_xx[k]=bunch->sigma_xx[i];
      bunch->sigma_xy[k]=bunch->sigma_xy[i];
#endif
      k++;
    }
  }
  k=n;
  for (j=1;j<nbunches;j++){
    for (i=0;i<n;i++){
      bunch->field->de[k]=bunch->field->de[i];
      // for (j1=0;j1<beamline->n_phases;j1++){
      // 	bunch->acc_field[j1][k]=bunch->acc_field[j1][i];
      // }
      k++;
    }
  }

  fact=1.0;
  bunch->drive_data->charge=(double*)xmalloc(sizeof(double)*nbunches);
  for (i=0;i<nbunches;i++){
    bunch->drive_data->charge[i]=param->bunch[i].ch;
  }
  k=0;
  for (j=0;j<nbunches;j++){
    for (i=0;i<n*nmacro;i++){
      bunch->particle[k].wgt*=param->bunch[j].ch;
      k++;
    }
    for (i=0;i<n;i++){
	bunch->field->de[j*n+i]*=param->bunch[j].ch;
	bunch->c_long[0][j*n+i]=cos(bunch->z_position[j*n+i]*1e-6/lambda
				    *TWOPI);
	bunch->s_long[0][j*n+i]=sin(bunch->z_position[j*n+i]*1e-6/lambda
				    *TWOPI);
    }
  }

  bunch->drive_data->along=(double*)realloc(bunch->drive_data->along,
					    sizeof(double)*nbunches*NSTEP);

  if (!injector_data.wake){
    placet_printf(ERROR,"injector data wake not defined! Run InjectorCavityDefine first");
    exit(1);
  }

  for (i=1;i<nbunches;i++){
    for (j=0;j<NSTEP;j++){
      bunch->drive_data->along[i+j*nbunches]=
	exp(-PI*(param->bunch[i].z-param->bunch[i-1].z)
	    *1e-6/(injector_data.wake->lambda[j]*injector_data.wake->Q[j]));
    }
  }
  bunch->drive_data->longrange_max=nbunches;
  for (j=0;j<NSTEP;j++){
    bunch->drive_data->along[j*nbunches]=injector_data.wake->a0[j]
      *(charge*ECHARGE*1e12)*1e-6*1e-3;
  }

  /*
  if (nmacro>1){
    field_adjust(bunch);
  }
  */

  bunch->which_field=7;
  if (nmacro>1) bunch->which_field=6;

  bunch->drive_data->empty_buckets=1;
  longrange_fill_band_new(bunch,injector_data.wake->lambda,NSTEP);

  quadrupole_wake_prepare(bunch,charge);
  //  quadrupole_wake_prepare_const(bunch,charge);
#ifdef TWODIM
  placet_cout << VERBOSE << "emitt_x at creation = " << emitt_x(bunch) << " particle sum = " << wgtsum << endmsg;
#endif
  placet_cout << VERBOSE << "emitt_y at creation = " << emitt_y(bunch) << " particle sum = " << wgtsum << endmsg;
  return bunch;
}
