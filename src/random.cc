#ifdef _OPENMP
#include <omp.h>
#endif
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <string>
#include <tcl.h>
#include <tk.h>

#include "placet_tk.h"
#include "select.h"
#include "random.hh"

//initialisation
RANDOM_NEW Misalignments(233);
RANDOM_NEW Instrumentation(1153);
RANDOM_NEW Groundmotion(2297);
RANDOM_NEW Cavity(3301);
RANDOM_NEW User(4111);
RANDOM_NEW Default(5051);
RANDOM_NEW Survey(6337);
RANDOM_NEW Select(7681);
PARALLEL_RNG Radiation(607);

 int PARALLEL_RNG::get_num_threads() {
    int nthreads=1;
#ifdef _OPENMP
#pragma omp parallel
    nthreads=omp_get_num_threads();
#endif
    return nthreads;
  }

 int PARALLEL_RNG::get_thread_num() {
#ifdef _OPENMP
    int threadnum = omp_get_thread_num();
    return threadnum;
#else
    return 0;
#endif
  }


//generalized reset function
int RANDOM_NEW::reset(std::string rng)
{
  if (rng=="Misalignments") Misalignments.SetSeed(233);
  else if(rng=="Radiation") Radiation.SetSeed(607);
  else if(rng=="Instrumentation") Instrumentation.SetSeed(1153);
  else if(rng=="Groundmotion") Groundmotion.SetSeed(2297);
  else if(rng=="Cavity") Cavity.SetSeed(3301);
  else if(rng=="User") User.SetSeed(4111);
  else if(rng=="Default") Default.SetSeed(5051);
  else if(rng=="Survey") Survey.SetSeed(6337);
  else if(rng=="Select") Select.SetSeed(7681);
  else if(rng=="all")
    {
      Misalignments.SetSeed(233);
      Radiation.SetSeed(607);
      Instrumentation.SetSeed(1153);
      Groundmotion.SetSeed(2297);
      Cavity.SetSeed(3301);
      User.SetSeed(4111);
      Default.SetSeed(5051);
      Survey.SetSeed(6337);
      Select.SetSeed(7681);
    }
  else return -1;
  return 0;
}

//generalized save function
int RANDOM_NEW::SaveAll(char *filename)
{
  FILE *file = fopen(filename,"w");
  if(file!=NULL) {              
    Misalignments.Save(file);
    Radiation.Save(file);
    Instrumentation.Save(file);
    Groundmotion.Save(file);
    Cavity.Save(file);
    User.Save(file);
    Default.Save(file);
    Survey.Save(file);
    Select.Save(file);
    fclose(file);
    return 0;
  }
  return -1;
}

//generalized load function
int RANDOM_NEW::LoadAll(char *filename)
{
  FILE *file = fopen(filename,"r");
  if(file!=NULL) {              
    Misalignments.Load(file);
    Radiation.Load(file);
    Instrumentation.Load(file);
    Groundmotion.Load(file);
    Cavity.Load(file);
    User.Load(file);
    Default.Load(file);
    Survey.Load(file);
    Select.Load(file);
    fclose(file);
    return 0;
  }
  return -1;
}

//__________TCL COMMENTS____________________________________________

int
Tcl_RandomCmd5(ClientData /*clientData*/,Tcl_Interp *interp,int /*argc*/,char * /*argv*/ [])
{
  char buf[20];
  snprintf(buf,20,"%g",User.Uniform());
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

int
Tcl_RandomCmdGauss(ClientData /*clientData*/,Tcl_Interp *interp,int /*argc*/,char * /*argv*/ [])
{
  char buf[20];
  snprintf(buf,20,"%g",User.Gauss());
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

int
Tcl_Random(ClientData /*clientData*/,Tcl_Interp *interp,int argc,char *argv[])
{
  int error;
  char *type="linear";
  Tk_ArgvInfo table[]={
    {(char*)"-type",TK_ARGV_STRING,(char*)NULL,
     (char*)&type,
     (char*)"distribution"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2){
    Tcl_SetResult(interp,"<Random> needs one variable name, e.g.: 'Random a'",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (strcmp(type,"linear")==0) {
    Tcl_CreateCommand(interp,argv[1],Tcl_RandomCmd5,
		      (ClientData)NULL,
		      (Tcl_CmdDeleteProc*)NULL);
  }
  else if (strcmp(type,"gaussian")==0) {
    Tcl_CreateCommand(interp,argv[1],Tcl_RandomCmdGauss,
		      (ClientData)NULL, 
		      (Tcl_CmdDeleteProc*)NULL);
  }
  else {
    Tcl_SetResult(interp,"type not known to <Tcl_Random>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  return TCL_OK;
}

int
Tcl_RandomSave(ClientData clientData,Tcl_Interp *interp,int argc,char *argv[])
{
  int error;
  char *init;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&init,
     (char*)"file name for random generator status storage"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2){
    Tcl_SetResult(interp,"Too many arguments to <RandomSave>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (RANDOM_NEW::SaveAll(init)!=0)    
    {return TCL_ERROR;}
  else  
    {return TCL_OK;}
}

int
Tcl_RandomLoad(ClientData clientData,Tcl_Interp *interp,int argc,char *argv[])
{
  int error;
  char *init;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&init,
     (char*)"file name for random generator status storage"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2){
    Tcl_SetResult(interp,"Too many arguments to <RandomLoad>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (RANDOM_NEW::LoadAll(init)!=0)
    {return TCL_ERROR;}
  else
    {return TCL_OK;}
}

int
Tcl_ParallelThreads(ClientData /*clientData*/,Tcl_Interp *interp,int argc,char *argv[])
{
  int error;
#ifdef _OPENMP
  int num = omp_get_max_threads();
#else
  inf num = 1;
#endif
  Tk_ArgvInfo table[]={
    {(char*)"-num",TK_ARGV_INT,(char*)NULL,
     (char*)&num,
     (char*)"Number of threads to be used for parallel calculations"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if (argc==1) {
    char buffer[256];
    snprintf(buffer, sizeof(buffer) / sizeof(*buffer), "%d", num);
    Tcl_SetResult(interp,buffer,TCL_VOLATILE);
    return TCL_OK;
  }
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <ParallelThreads>",TCL_VOLATILE);
    return TCL_ERROR;
  }
#ifdef _OPENMP
  omp_set_dynamic(0); 
  omp_set_num_threads(num);
#else
  placet_cout << WARNING << "ParallelThreads called, but PLACET was compiled without OpenMP support" << endmsg;
#endif
  return TCL_OK;
}

/**********************************************************************/
/*                      RandomReset                                   */
/**********************************************************************/

int Tcl_RandomReset(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  char *stream=NULL, *generator=NULL;
  int seed=-1;
  int error;
  Tk_ArgvInfo table[]={
    {(char*)"-seed",TK_ARGV_INT,(char*)NULL,
     (char*)&seed,
     (char*)"seed, integer value is expected e.g. 739 "},
    {(char*)"-stream",TK_ARGV_STRING,(char*)NULL,
     (char*)&stream,
     (char*)"stream, name of stream is expected, as a character string e.g.\"Misalignments\", for list see help "},
    {(char*)"-generator",TK_ARGV_STRING,(char*)NULL,
     (char*)&generator,
     (char*)"generator, generator type is expected, as a character string e.g. \"taus2\" "},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"This is a centralized command to set and reset all random number generators available in PLACET.\n Calling it without argument resets all RNGs to its default values.\n Giving a seed will set all RNGs to that seed.\n Giving a seed and a stream sets the seed of the selected stream.\n Giving a seed, a stream and a generator allows also to select the generation algorithm\n\n Available Streams with their default seeds are:\n Misalignments(233)\n Instrumentation(1153)\n Groundmotion(2297)\n Cavity(3301)\n User(4111)\n Default(5051)\n Survey(6337)\n Select(7681)\n Radiation(607)\n As default generation algorithm gsl_rng_taus2 is used"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <RandomReset>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  // check if there is a seed
  if (seed!=-1) {
    // check if there is a stream
    if(stream)
      {
  	// check if there is a generator
  	if(generator)
	  {
	            
	    //declare generator type
	    const gsl_rng_type * gen = gsl_rng_taus2;
        
	    if (!strcmp(generator,"taus2")) gen = gsl_rng_taus2;
	    else if(!strcmp(generator,"mt19937")) gen = gsl_rng_mt19937;
	    else if(!strcmp(generator,"gfsr4")) gen = gsl_rng_gfsr4;
	    else if(!strcmp(generator,"ranlxs0")) gen = gsl_rng_ranlxs0;
	    else if(!strcmp(generator,"ranlxs1")) gen = gsl_rng_ranlxs1;
	    else if(!strcmp(generator,"mrg")) gen = gsl_rng_mrg;
	    else if(!strcmp(generator,"ranlux")) gen = gsl_rng_ranlux;
	    else if(!strcmp(generator,"ranlxd1")) gen = gsl_rng_ranlxd1;
	    else{
	      Tcl_SetResult(interp,"Generator type not known",TCL_VOLATILE);
	      return TCL_ERROR;
	    }
        
	    //set generator and seed to stream
	    if (!strcmp(stream,"Misalignments")) Misalignments.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"Radiation")) Radiation.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"Instrumentation")) Instrumentation.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"Groundmotion")) Groundmotion.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"Cavity")) Cavity.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"User")) User.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"Default")) Default.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"Survey")) Survey.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"Select")) Select.SetGenerator(gen,seed);
	    else if(!strcmp(stream,"all"))
	      {
		Misalignments.SetGenerator(gen,seed);
		Radiation.SetGenerator(gen,seed);
		Instrumentation.SetGenerator(gen,seed);
		Groundmotion.SetGenerator(gen,seed);
		Cavity.SetGenerator(gen,seed);
		User.SetGenerator(gen,seed);
		Default.SetGenerator(gen,seed);
		Survey.SetGenerator(gen,seed);
		Select.SetGenerator(gen,seed);
	      }        
	    else {
	      Tcl_SetResult(interp,"Stream type not known",TCL_VOLATILE);
	      return TCL_ERROR;
	    }//stream not found
	    
	    placet_printf(INFO,"%s seed set to %i, generator set to %s \n", stream, seed, generator);
	  
	  }
  	
  	// no generator given
  	else
	  {
	    // set seed to stream, default generator
	    if (!strcmp(stream,"Misalignments")) Misalignments.SetSeed(seed);
	    else if(!strcmp(stream,"Radiation")) Radiation.SetSeed(seed);
	    else if(!strcmp(stream,"Instrumentation")) Instrumentation.SetSeed(seed);
	    else if(!strcmp(stream,"Groundmotion")) Groundmotion.SetSeed(seed);
	    else if(!strcmp(stream,"Cavity")) Cavity.SetSeed(seed);
	    else if(!strcmp(stream,"User")) User.SetSeed(seed);
	    else if(!strcmp(stream,"Default")) Default.SetSeed(seed);
	    else if(!strcmp(stream,"Survey")) Survey.SetSeed(seed);
	    else if(!strcmp(stream,"Select")) Select.SetSeed(seed);
	    else if(!strcmp(stream,"all"))
	      {
		Misalignments.SetSeed(seed);
		Radiation.SetSeed(seed);
		Instrumentation.SetSeed(seed);
		Groundmotion.SetSeed(seed);
		Cavity.SetSeed(seed);
		User.SetSeed(seed);
		Default.SetSeed(seed);
		Survey.SetSeed(seed);
		Select.SetSeed(seed);
	      }        
	    
	    else {
	      Tcl_SetResult(interp,"Stream type not known",TCL_VOLATILE);
	      return TCL_ERROR;
	    }//stream not found
	  
	  placet_printf(INFO,"%s seed set to %i \n", stream, seed);
	  
	  }
      }
  	
    //no stream given, set seed to all RNG
    else 
      {
	placet_printf(INFO,"All generator seeds set to %i \n",seed);
	Misalignments.SetSeed(seed);
	Radiation.SetSeed(seed);
	Instrumentation.SetSeed(seed);
	Groundmotion.SetSeed(seed);
	Cavity.SetSeed(seed);
	User.SetSeed(seed);
	Default.SetSeed(seed);
	Survey.SetSeed(seed);
	Select.SetSeed(seed);
      }
  }
  // reset all to default, because no seed given
  else{RANDOM_NEW::reset("all");}
  return TCL_OK;
}

int Rndm_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"Random",Tcl_Random,(ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(interp,"RandomLoad",Tcl_RandomLoad,(ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(interp,"RandomSave",Tcl_RandomSave,(ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(/* MANUAL::BEAMLINE, MANUAL::INSPECT, */ interp,"RandomReset",&Tcl_RandomReset, NULL, NULL);
  Tcl_PkgProvide(interp,"Rndm","1.0");
  Tcl_CreateCommand(interp,"ParallelThreads",Tcl_ParallelThreads,(ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  return TCL_OK;
}
