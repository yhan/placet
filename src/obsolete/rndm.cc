#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

//#include "rndm.h"

#define RNDM_EPS 6e-8
/*
static struct
{
  float u[97],c,cd,cm;
  int i,j;
} rndm5_store;

void rndmst5(int na1,int na2,int na3, int nb1)
{
  int i,j,nat;
  float s,t;
  rndm5_store.i=96;
  rndm5_store.j=32;
  for (i=0;i<97;i++)
    {
      s=0.0;
      t=0.5;
      for (j=0;j<24;j++)
	{
	  nat=(((na1*na2) % 179)*na3) % 179;
	  na1=na2;
	  na2=na3;
	  na3=nat;
	  nb1=(53*nb1+1) % 169;
	  if ((nb1*nat) % 64 >= 32)
	    {
	      s+=t;
	    }
	  t*=0.5;
	}
      rndm5_store.u[i]=s;
    }
  rndm5_store.c=    362436.0/16777216.0;
  rndm5_store.cd=  7654321.0/16777216.0;
  rndm5_store.cm= 16777213.0/16777216.0;
}

float rndm5()
{
  float temp;

 for (;;){
  temp=rndm5_store.u[rndm5_store.i]-rndm5_store.u[rndm5_store.j];
  if (temp<0.0)
    {
      temp+=1.0;
    }
  rndm5_store.u[rndm5_store.i]=temp;
  if (--rndm5_store.i<0) rndm5_store.i=96;
  if (--rndm5_store.j<0) rndm5_store.j=96;
  rndm5_store.c-=rndm5_store.cd;
  if (rndm5_store.c<0.0)
    {
      rndm5_store.c+=rndm5_store.cm;
    }
  temp-=rndm5_store.c;
  if (temp<0.0)
    {
      return temp+1.0;
    }
  else
    {
      if (temp>0.0) 
        return temp;
    }
}
}
*/
/*
static struct
{
  float u[97],c,cd,cm;
  int i,j;
} rndm5a_store;

void rndmst5a(int na1,int na2,int na3, int nb1)
{
  int i,j,nat;
  float s,t;
  rndm5a_store.i=96;
  rndm5a_store.j=32;
  for (i=0;i<97;i++)
    {
      s=0.0;
      t=0.5;
      for (j=0;j<24;j++)
	{
	  nat=(((na1*na2) % 179)*na3) % 179;
	  na1=na2;
	  na2=na3;
	  na3=nat;
	  nb1=(53*nb1+1) % 169;
	  if ((nb1*nat) % 64 >= 32)
	    {
	      s+=t;
	    }
	  t*=0.5;
	}
      rndm5a_store.u[i]=s;
    }
  rndm5a_store.c=    362436.0/16777216.0;
  rndm5a_store.cd=  7654321.0/16777216.0;
  rndm5a_store.cm= 16777213.0/16777216.0;
}

float rndm5a()
{
  float temp;

  temp=rndm5a_store.u[rndm5a_store.i]-rndm5a_store.u[rndm5a_store.j];
  if (temp<0.0)
    {
      temp+=1.0;
    }
  rndm5a_store.u[rndm5a_store.i]=temp;
  if (--rndm5a_store.i<0) rndm5a_store.i=96;
  if (--rndm5a_store.j<0) rndm5a_store.j=96;
  rndm5a_store.c-=rndm5a_store.cd;
  if (rndm5a_store.c<0.0)
    {
      rndm5a_store.c+=rndm5a_store.cm;
    }
  temp-=rndm5a_store.c;
  if (temp<0.0)
    {
      return temp+1.0;
    }
  else
    {
        return temp;
    }
}*/

static struct
{
  int i;
  float r[97];
  int ix1,ix2,ix3;
} rndm8_store;

void rndmst8(int idummy)
{
  static int m1=259200,ia1=7141,ic1=54773;
  static float rm1=1.0/259200.0;
  static int m2=134456,ia2=8121,ic2=28411;
  static float rm2=1.0/134456.0;
  static int m3=243000; //ia3=4561,ic3=51349
  static int ix1,ix2,ix3,i;
  // static float rm3=1.0/243000.0;

  if (idummy>0) idummy=-idummy;
  ix1=(ic1-idummy) % m1;
  if (ix1<0) ix1=-ix1; // potential overflow cure
  ix1=(ia1*ix1+ic1) % m1;
  ix2=ix1 % m2;
  ix1=(ia1*ix1+ic1) % m1;
  ix3=ix1 % m3;
  for (i=0;i<97;i++)
    {
      ix1=(ia1*ix1+ic1) % m1;
      ix2=(ia2*ix2+ic2) % m2;
      rndm8_store.r[i]=(ix1+ix2*rm2)*rm1;
    }
  rndm8_store.ix1=ix1;
  rndm8_store.ix2=ix2;
  rndm8_store.ix3=ix3;
}

float rndm8()
{
  static int m1=259200,ia1=7141,ic1=54773;
  static float rm1=1.0/259200.0;
  static int m2=134456,ia2=8121,ic2=28411;
  static float rm2=1.0/134456.0;
  static int m3=243000,ia3=4561,ic3=51349;
  static int i;
  static float rm3=1.0/243000.0;
  float help;

  rndm8_store.ix1=(ia1*rndm8_store.ix1+ic1) % m1;
  rndm8_store.ix2=(ia2*rndm8_store.ix2+ic2) % m2;
  rndm8_store.ix3=(ia3*rndm8_store.ix3+ic3) % m3;
  i=(int)((float)(97*rndm8_store.ix3)*rm3);
  help=rndm8_store.r[i];
  rndm8_store.r[i]=(rndm8_store.ix1+rndm8_store.ix2*rm2)*rm1;
  return help;
}
/*
static struct{
  int iset;
  float v1,v2;
} gasdev_data;

float gasdev_0()
{
  // returns a standard normal (Gaussian) random variable
  // Marsaglia polar method, based on Box-Muller transform:
  // http://en.wikipedia.org/wiki/Marsaglia_polar_method
  // it produces two independent numbers, of which one is stored for next use
  float r;
  if (gasdev_data.iset==0)
    {
      for (;;)
	{
          gasdev_data.v1=2.0*rndm()-1.0;
          gasdev_data.v2=2.0*rndm()-1.0;
          r=gasdev_data.v1*gasdev_data.v1+gasdev_data.v2*gasdev_data.v2;
          if ((r<=1.0) && (r!=0))
	    {
	      break;
	    }
        }
      gasdev_data.iset=1;
      r=sqrt(-2.0*log((double)r)/r);
      gasdev_data.v1*=r;
      gasdev_data.v2*=r;
      return gasdev_data.v1;
    }
  else
    {
      gasdev_data.iset=0;
      return gasdev_data.v2;
    }
}

float gasdev()
{
  float tmp;
  while(fabs(tmp=gasdev_0())>3.0) ;
  return tmp;
}

static struct{
  int iset;
  float v1,v2;
} gasdev_data2;

float gasdev2()
{
  // returns a standard normal (Gaussian) random variable
  // Marsaglia polar method, based on Box-Muller transform:
  // http://en.wikipedia.org/wiki/Marsaglia_polar_method
  // it produces two independent numbers, of which one is stored for next use
  float r;
  if (gasdev_data2.iset==0)
    {
      for (;;)
	{	
          gasdev_data2.v1=2.0*rndm5a()-1.0;
          gasdev_data2.v2=2.0*rndm5a()-1.0;
          r=gasdev_data2.v1*gasdev_data2.v1+gasdev_data2.v2*gasdev_data2.v2;
          if ((r<=1.0) && (r!=0))
	    {
	      break;
	    }
        }
      gasdev_data2.iset=1;
      r=sqrt(-2.0*log((double)r)/r);
      gasdev_data2.v1*=r;
      gasdev_data2.v2*=r;
      return gasdev_data2.v1;
    }
  else
    {
      gasdev_data2.iset=0;
      return gasdev_data2.v2;
    }
}
*/
/*
void rndmst()  //  sets all random number generators to default!!!
{
  //gasdev_data.iset=0;
  //gasdev_data2.iset=0;
  //  rndmst0(1);
  //  rndmst1(1);
  //  rndmst2(1);
  //  rndmst3(1);
  //rndmst5(12,34,56,78);
  //rndmst5a(12,34,56,78);
  //  rndmst6(1);
  //  rndmst7(1);
  //rndmst8(1);
}*/
/*
void rndm_save(char *name)
{
  FILE *file;
  file=fopen(name,"w");
  //  fwrite(&rndm0_store,sizeof(rndm0_store),1,file);
  //  fwrite(&rndm1_store,sizeof(rndm1_store),1,file);
  //  fwrite(&rndm2_store,sizeof(rndm2_store),1,file);
  //  fwrite(&rndm3_store,sizeof(rndm3_store),1,file);
  fwrite(&rndm5_store,sizeof(rndm5_store),1,file);
  fwrite(&rndm5a_store,sizeof(rndm5a_store),1,file);
  //  fwrite(&rndm6_store,sizeof(rndm6_store),1,file);
  //  fwrite(&rndm7_store,sizeof(rndm7_store),1,file);
  fwrite(&rndm8_store,sizeof(rndm8_store),1,file);
  fclose(file);
}

void rndm_load(char *name)
{
  FILE *file;
  if(file=fopen(name,"r")){
    //    fread(&rndm0_store,sizeof(rndm0_store),1,file);
    //    fread(&rndm1_store,sizeof(rndm1_store),1,file);
    //    fread(&rndm2_store,sizeof(rndm2_store),1,file);
    //    fread(&rndm3_store,sizeof(rndm3_store),1,file);
    fread(&rndm5_store,sizeof(rndm5_store),1,file);
    fread(&rndm5a_store,sizeof(rndm5a_store),1,file);
    //    fread(&rndm6_store,sizeof(rndm6_store),1,file);
    //    fread(&rndm7_store,sizeof(rndm7_store),1,file);
    fread(&rndm8_store,sizeof(rndm8_store),1,file);
    
    fclose(file);
  }
}
*/
#undef RNDM_EPS
