#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "ion.h"
#include "matrix.h"
#include "placet_tk.h"
#include "placeti3.h"
#include "quadrupole.h"
#include "rfkick.h"
//#include "twiss.h"
#include "step.h"

extern PetsWake drive_data;
extern INTER_DATA_STRUCT inter_data;

/**********************************************************************/
/*                      drift_step_half (not a command)               */
/**********************************************************************/

/*void drift_step_half(ELEMENT *element,BEAM *bunch)
{
  double length;
  R_MATRIX r1,r2;
  int i,n;

  length=0.5*element->length;
  r1.r11=1.0;
  r1.r12=length;
  r1.r21=0.0;
  r1.r22=1.0;
  r2.r11=1.0;
  r2.r12=0.0;
  r2.r21=length;
  r2.r22=1.0;
  for (i=0;i<bunch->slices;i++){
    bunch->particle[i].y+=length*bunch->particle[i].yp;
#ifdef TWODIM
    bunch->particle[i].x+=length*bunch->particle[i].xp;
#endif
    mult_M_M(&r1,bunch->sigma+i,bunch->sigma+i);
    mult_M_M(bunch->sigma+i,&r2,bunch->sigma+i);
#ifdef TWODIM
    mult_M_M(&r1,bunch->sigma_xx+i,bunch->sigma_xx+i);
    mult_M_M(bunch->sigma_xx+i,&r2,bunch->sigma_xx+i);
    mult_M_M(&r1,bunch->sigma_xy+i,bunch->sigma_xy+i);
    mult_M_M(bunch->sigma_xy+i,&r2,bunch->sigma_xy+i);
#endif
  }
}
*/
/**********************************************************************/
/*                      drift_step_half_0 (not a command)             */
/**********************************************************************/
/*
void drift_step_half_0(ELEMENT *element,BEAM *bunch)
{
  double length;
  int i,n;

  length=0.5*element->length;
  for (i=0;i<bunch->slices;i++){
    bunch->particle[i].y+=length*bunch->particle[i].yp;
#ifdef TWODIM
    bunch->particle[i].x+=length*bunch->particle[i].xp;
#endif
  }
}
*/
/**********************************************************************/
/*                      drift_step_quarter (not a command)            */
/**********************************************************************/
/*
void drift_step_quarter(ELEMENT *element,BEAM *bunch)
{
  double length;
  R_MATRIX r1,r2;
  int i,n;

  length=0.25*element->length;
  r1.r11=1.0;
  r1.r12=length;
  r1.r21=0.0;
  r1.r22=1.0;
  r2.r11=1.0;
  r2.r12=0.0;
  r2.r21=length;
  r2.r22=1.0;
  for (i=0;i<bunch->slices;i++){
    bunch->particle[i].y+=length*bunch->particle[i].yp;
#ifdef TWODIM
    bunch->particle[i].x+=length*bunch->particle[i].xp;
#endif
    mult_M_M(&r1,bunch->sigma+i,bunch->sigma+i);
    mult_M_M(bunch->sigma+i,&r2,bunch->sigma+i);
#ifdef TWODIM
    mult_M_M(&r1,bunch->sigma_xx+i,bunch->sigma_xx+i);
    mult_M_M(bunch->sigma_xx+i,&r2,bunch->sigma_xx+i);
    mult_M_M(&r1,bunch->sigma_xy+i,bunch->sigma_xy+i);
    mult_M_M(bunch->sigma_xy+i,&r2,bunch->sigma_xy+i);
#endif
  }
}
*/
/**********************************************************************/
/*                      cavity_step_half1 (not a command)             */
/**********************************************************************/
/*
void cavity_step_half1(ELEMENT *element,BEAM *bunch)
{
  double tmp,tmp2,factor,de0,x,y,delta,length,half_length,length_i,attenuation;
  double kick_y,kick_x,y_off,tmpy,lndelta,one2delta;
  float testy;
  R_MATRIX r1,r2;
  int i,j,k,m,n,j0,m2,nmacro,nf,nadd;
  double sumy,sumx,*p,gammap,*de,pos_x,pos_y,*rho_y,*force,multi_y,multi_x,
    *rho_a,*rho_b,multi_y_b,pos_yb,pos_xb,longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;

  factor=bunch->factor;
  length=element->length;
  half_length=0.5*length;
  length_i=1.0/length;
  de=bunch->acc_field[element->field];

  r1.r11=1.0;
  r1.r12=0.5*length;
  r1.r21=0.0;
  r2.r11=1.0;
  r2.r12=0.0;
  r2.r21=0.5*length;

  for (i=0;i<bunch->slices;i++){
    de0=de[i/bunch->macroparticles]
      +factor*bunch->field[0].de[i/bunch->macroparticles];
//    delta=0.5*de0/bunch->particle[i].energy;
if(drive_data.rf_long){
  de0*=rf_long(1.0,length,bunch->particle[i].y,bunch->particle[i].yp);
}
    delta=half_length*de0/bunch->particle[i].energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
    gammap=delta*length_i;
    bunch->particle[i].yp-=gammap*bunch->particle[i].y;
#endif
    tmp2=log(1.0+delta)/delta;
    tmp=1.0/(1.0+delta);
#else
//    gammap=0.5*de0/(bunch->particle[i].energy*element->length);
    gammap=delta*length_i;
#ifdef END_FIELDS
    bunch->particle[i].yp-=gammap*bunch->particle[i].y;
#endif
// scd 1.0-delta ? 
    tmp=(1.0-gammap*length);
#endif
    
#ifdef CAV_PRECISE
    bunch->particle[i].y+=half_length*bunch->particle[i].yp*tmp2;
#else
    bunch->particle[i].y+=half_length*bunch->particle[i].yp;
#endif
    bunch->particle[i].yp*=tmp;

#ifdef TWODIM
#ifdef END_FIELDS
    bunch->particle[i].xp-=gammap*bunch->particle[i].x;
#endif
#ifdef CAV_PRECISE
    bunch->particle[i].x+=half_length*bunch->particle[i].xp*tmp2;
#else
    bunch->particle[i].x+=half_length*bunch->particle[i].xp;
#endif
    bunch->particle[i].xp*=tmp;
#endif
    bunch->particle[i].energy+=half_length*de0;

// include the rotation due to the endfields
#ifdef CAV_PRECISE
    r1.r12=0.5*element->length*log(1.0+delta)/(delta);
    r2.r21=r1.r12;
#endif
    r1.r22=1.0/(1.0+delta);
    r2.r22=r1.r22;
    mult_M_M(&r1,bunch->sigma+i,bunch->sigma+i);
    mult_M_M(bunch->sigma+i,&r2,bunch->sigma+i);
#ifdef TWODIM
    mult_M_M(&r1,bunch->sigma_xx+i,bunch->sigma_xx+i);
    mult_M_M(bunch->sigma_xx+i,&r2,bunch->sigma_xx+i);
    mult_M_M(&r1,bunch->sigma_xy+i,bunch->sigma_xy+i);
    mult_M_M(bunch->sigma_xy+i,&r2,bunch->sigma_xy+i);
#endif
  }
}
*/
/**********************************************************************/
/*                      cavity_step_half2 (not a command)             */
/**********************************************************************/

void cavity_step_half2(ELEMENT *element,BEAM *bunch)
{
  double tmp,tmp2,factor,de0,x,y,delta,length,half_length,length_i,attenuation;
  double kick_y,kick_x,y_off,tmpy,lndelta,one2delta;
float testy;
  R_MATRIX r1,r2;
  int i,j,k,m,n,j0,m2,nmacro,nf,nadd;
  double sumy,sumx,*p,gammap,*de,pos_x,pos_y,*rho_y,*force,multi_y,multi_x,
    *rho_a,*rho_b,multi_y_b,pos_yb,pos_xb,longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;

  factor=bunch->factor;
  length=element->length;
  half_length=0.5*length;
  length_i=1.0/length;
  de=bunch->acc_field[element->field];

  r1.r11=1.0;
  r1.r12=0.5*length;
  r1.r21=0.0;
  r2.r11=1.0;
  r2.r12=0.0;
  r2.r21=0.5*length;

  for (i=0;i<bunch->slices;i++){
    de0=de[i/bunch->macroparticles]
      +factor*bunch->field[0].de[i/bunch->macroparticles];
if(drive_data.rf_long){
  de0*=rf_long(1.0,length,bunch->particle[i].y,bunch->particle[i].yp);
}
#ifdef CAV_PRECISE
    delta=half_length*de0/bunch->particle[i].energy;
#ifdef END_FIELDS
    gammap=delta/element->length;
#endif
    tmp2=log(1.0+delta)/delta;
    tmp=1.0/(1.0+delta);
#else
//    gammap=0.5*de0/(bunch->particle[i].energy*element->length);
    gammap=half_length*de0/(bunch->particle[i].energy*length);
    tmp=(1.0-gammap*length);
#endif

#ifdef CAV_PRECISE
    bunch->particle[i].y+=half_length*bunch->particle[i].yp*tmp2;
#else
    bunch->particle[i].y+=half_length*bunch->particle[i].yp;
#endif
#ifdef END_FIELDS
    bunch->particle[i].yp+=gammap*bunch->particle[i].y;
#endif
    bunch->particle[i].yp*=tmp;

#ifdef TWODIM
#ifdef CAV_PRECISE
    bunch->particle[i].x+=half_length*bunch->particle[i].xp*tmp2;
#else
    bunch->particle[i].x+=half_length*bunch->particle[i].xp;
#endif
#ifdef END_FIELDS
    bunch->particle[i].xp+=gammap*bunch->particle[i].x;
#endif
    bunch->particle[i].xp*=tmp;
#endif

    bunch->particle[i].energy+=half_length*de0;

#ifdef CAV_PRECISE
    r1.r12=0.5*element->length*log(1.0+delta)/(delta);
    r2.r21=r1.r12;
#endif
    r1.r22=1.0/(1.0+delta);
    r2.r22=r1.r22;

    mult_M_M(&r1,bunch->sigma+i,bunch->sigma+i);
    mult_M_M(bunch->sigma+i,&r2,bunch->sigma+i);
#ifdef TWODIM
    mult_M_M(&r1,bunch->sigma_xx+i,bunch->sigma_xx+i);
    mult_M_M(bunch->sigma_xx+i,&r2,bunch->sigma_xx+i);
    mult_M_M(&r1,bunch->sigma_xy+i,bunch->sigma_xy+i);
    mult_M_M(bunch->sigma_xy+i,&r2,bunch->sigma_xy+i);
#endif
  }
}

/**********************************************************************/
/*                      ion_data (structure)                          */
/**********************************************************************/

struct{
  double dist,charge,val,nb,dens;
  double rate_int,rate,s_old,f0;
} ion_data;

/**********************************************************************/
/*                      ion-plot (not a command)                      */
/**********************************************************************/

void ion_plot(FILE *file,BEAM *beam,int j,double s)
{
  int i,nc;
  double sigmax,sigmay,e,wgtsum,f,rate;

  nc=beam->slices/2;
  sigmax=0.0;
  sigmay=0.0;
  wgtsum=0.0;
  for (i=0;i<beam->slices;i++){
    sigmax+=beam->sigma_xx[i].r11*beam->particle[i].wgt;
    sigmay+=beam->sigma[i].r11*beam->particle[i].wgt;
    wgtsum+=beam->particle[i].wgt;
  }
  sigmax/=wgtsum;
  sigmax=sqrt(sigmax);
  sigmay/=wgtsum;
  sigmay=sqrt(sigmay);
  e=beam->particle[nc].energy;
  f=4.0/PI*sqrt(ion_data.val*RE*EMASS/28.0/(3.0*sigmay*(sigmax+sigmay)*1e-12));
  if (f<=ion_data.f0) {
    rate=ion_data.dens*ion_data.nb*ion_data.charge*RE*sigmay*sigmay
      /(sqrt(18.0)*sigmay*(sigmax+sigmay)*0.1*emitt_y(beam)*1e-7);
  }
  else{
    rate=0.0;
  }
  ion_data.rate_int+=0.5*(rate+ion_data.rate)*(s-ion_data.s_old);
  ion_data.rate=rate;
  ion_data.s_old=s;
  if (file){
    fprintf(file,"%d %g %g %g %g %g %g\n",j,s,e,sigmax,sigmay,f,
	    ion_data.rate_int);
  }
}

/**********************************************************************/
/*                      twiss_plot2 (not a command)                   */
/**********************************************************************/

void twiss_plot2(BEAMLINE *beamline,BEAM *bunch0)
{
  ELEMENT **element;
  int i,j=0,nc;
  double eps,beta,alpha,eps0,beta0,alpha0;
  FILE *file;
  double s=0.0,e,wgtsum;
  BEAM *bunch;

  file=fopen("test.dat","w");
  bunch=bunch_remake(bunch0);
  beam_copy(bunch0,bunch);
  nc=bunch->slices/2;
  element=beamline->element;
  while(element[j]!=NULL){
    s+=element[j]->length;
    switch(element[j]->type){
    case QUAD:
    case QUADBPM:
      quadrupole_step_half(element[j],bunch);
      break;
    case DRIFT:
    case BPM:
      drift_step_half(element[j],bunch);
      break;
    case CAV:
      cavity_step_half1(element[j],bunch);
      break;
    }
    eps=0.0;
    beta=0.0;
    alpha=0.0;
    for (i=0;i<bunch->slices;i++){
      eps+=(bunch->sigma[i].r11*bunch->sigma[i].r22
	    -bunch->sigma[i].r12*bunch->sigma[i].r21)*bunch->particle[i].wgt;
      beta+=bunch->sigma[i].r11*bunch->particle[i].wgt;
      alpha-=bunch->sigma[i].r12*bunch->particle[i].wgt;
    }
    eps=sqrt(eps);
    beta/=eps;
    alpha/=eps;
    eps0=(bunch->sigma[nc].r11*bunch->sigma[nc].r22
	  -bunch->sigma[nc].r12*bunch->sigma[nc].r21);
    beta0=bunch->sigma[nc].r11;
    alpha0=-bunch->sigma[nc].r12;
    eps0=sqrt(eps0);
    beta0/=eps0;
    alpha0/=eps0;
    e=bunch->particle[nc].energy;
    fprintf(file,"%d %g %g %g %g\n",j,s,e,beta0,alpha0);
    switch(element[j]->type){
    case QUAD:
    case QUADBPM:
      quadrupole_step_half(element[j],bunch);
      break;
    case DRIFT:
    case BPM:
      drift_step_half(element[j],bunch);
      break;
    case CAV:
      cavity_step_half1(element[j],bunch);
      break;
    }
    j++;
  }
  fclose(file);
  beam_delete(bunch);
}

/**********************************************************************/
/*                      ion_sum (not a command)                       */
/**********************************************************************/

double ion_sum(BEAMLINE *beamline,BEAM *bunch0,double val)
{
  ELEMENT **element;
  int i,j=0,nc;
  double sigmax,sigmay,f;
  FILE *file;
  double s=0.0,e,wgtsum,t=0.0;
  BEAM *bunch;
  double dist=0.2,n_part=4e9;

  file=fopen("test.dat","w");
  bunch=bunch_remake(bunch0);
  beam_copy(bunch0,bunch);
  nc=bunch->slices/2;
  element=beamline->element;
  while(element[j]!=NULL){
    s+=element[j]->length;
    switch(element[j]->type){
    case QUAD:
    case QUADBPM:
      quadrupole_step_half(element[j],bunch);
      break;
    case DRIFT:
    case BPM:
      drift_step_half(element[j],bunch);
      break;
    case CAV:
      cavity_step_half1(element[j],bunch);
      break;
    }
    sigmax=0.0;
    sigmay=0.0;
    wgtsum=0.0;
    for (i=0;i<bunch->slices;i++){
      sigmax+=bunch->sigma_xx[i].r11*bunch->particle[i].wgt;
      sigmay+=bunch->sigma[i].r11*bunch->particle[i].wgt;
      wgtsum+=bunch->particle[i].wgt;
    }
    sigmax/=wgtsum;
    sigmax=sqrt(sigmax);
    sigmay/=wgtsum;
    sigmay=sqrt(sigmay);
    e=bunch->particle[nc].energy;
    f=4.0/PI*sqrt(val*RE*EMASS/28.0/(3.0*sigmay*(sigmax+sigmay)*1e-12));
    if (f<=1.0) { t+=element[j]->length; }
    fprintf(file,"%d %g %g %g %g %g\n",j,s,e,sigmax,sigmay,f);
    switch(element[j]->type){
    case QUAD:
    case QUADBPM:
      quadrupole_step_half(element[j],bunch);
      break;
    case DRIFT:
    case BPM:
      drift_step_half(element[j],bunch);
      break;
    case CAV:
      cavity_step_half1(element[j],bunch);
      break;
    }
    j++;
  }
  fclose(file);
  beam_delete(bunch);
  return t;
}

/******   Tcl/Tk commands   ******/

/**********************************************************************/
/*                      IonPlot                                       */
/**********************************************************************/

int tk_IonPlot(ClientData clientdata,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  int error;
  char *beam_name=NULL,*name=NULL;
  double dist=0.0,charge=0.0,step=0.02;
  double p=1e-9,sigma_ion=2e6,k=1.38e-23,T=300.0,f0=1.0;
  char buffer[100];
  int nb=1;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the beam to use for the calculation"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the file to store the results"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,(char*)&charge,
     (char*)"Charge per bunch in number of particles"},
    {(char*)"-nb",TK_ARGV_INT,(char*)NULL,(char*)&nb,
     (char*)"Number of bunches"},
    {(char*)"-distance",TK_ARGV_FLOAT,(char*)NULL,(char*)&dist,
     (char*)"Distance between bunches in [m]"},
    {(char*)"-p",TK_ARGV_FLOAT,(char*)NULL,(char*)&p,
     (char*)"Pressure in [torr]"},
    {(char*)"-T",TK_ARGV_FLOAT,(char*)NULL,(char*)&T,
     (char*)"Temperture in [K]"},
    {(char*)"-step",TK_ARGV_FLOAT,(char*)NULL,(char*)&step,
     (char*)"Maximum step size in [m]"},
    {(char*)"-f0",TK_ARGV_FLOAT,(char*)NULL,(char*)&f0,
     (char*)"Relative frequency required for trapping"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  ion_data.val=dist*charge;
  ion_data.charge=charge;
  ion_data.nb=nb;
  ion_data.f0=f0;
  ion_data.dens=p*sigma_ion*1e-28/(k*T)*1e4/760.0*9.81;
  ion_data.rate_int=0.0;
  ion_data.rate=0.0;
  ion_data.s_old=0.0;
  BEAM* beam=get_beam(beam_name);
  int n1 = beam->slices/2;
  int n2 = n1;
  twiss_plot_step(inter_data.beamline,beam,name,step,n1,n2,&ion_plot);
  sprintf(buffer,"%g",ion_data.rate_int);
  Tcl_AppendResult(interp,buffer,NULL);
  return TCL_OK;
}

/**********************************************************************/
/*                      IonSum                                        */
/**********************************************************************/

int tk_IonSum(ClientData clientdata,Tcl_Interp *interp,int argc,
	      char *argv[])
{
  int error;
  char *beam_name=NULL,*name=NULL;
  double dist=0.0,charge=0.0,step=0.02;
  double p=1e-9,sigma_ion=2e6,k=1.38e-23,T=300.0,f0=1.0;
  int nb=1;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the beam to use for the calculation"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the file to store the results"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,(char*)&charge,
     (char*)"Charge per bunch in number of particles"},
    {(char*)"-nb",TK_ARGV_INT,(char*)NULL,(char*)&nb,
     (char*)"Number of bunches"},
    {(char*)"-distance",TK_ARGV_FLOAT,(char*)NULL,(char*)&dist,
     (char*)"Distance between bunches in [m]"},
    {(char*)"-p",TK_ARGV_FLOAT,(char*)NULL,(char*)&p,
     (char*)"Pressure in [torr]"},
    {(char*)"-T",TK_ARGV_FLOAT,(char*)NULL,(char*)&T,
     (char*)"Temperture in [K]"},
    {(char*)"-step",TK_ARGV_FLOAT,(char*)NULL,(char*)&step,
     (char*)"Maximum step size in [m]"},
    {(char*)"-f0",TK_ARGV_FLOAT,(char*)NULL,(char*)&f0,
     (char*)"Relative frequency required for trapping"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  ion_data.val=dist*charge;
  ion_data.charge=charge;
  ion_data.nb=nb;
  ion_data.f0=f0;
  ion_data.dens=p*sigma_ion*1e-28/(k*T)*1e4/760.0*9.81;
  ion_data.rate_int=0.0;
  ion_data.rate=0.0;
  ion_data.s_old=0.0;
  BEAM* beam = get_beam(beam_name);
  int n1 = beam->slices/2;
  int n2 = n1;
  twiss_plot_step(inter_data.beamline,beam,name,step,&ion_plot);
  return TCL_OK;
}
