#include "dl_element.h"
#include "function.h"
#include "matrix.h"

#include "placeti3.h"

struct QUADRUPOLE_DATA {
  double strength;
  double tilt;
  QUADRUPOLE_DATA() : strength(0.0), tilt(0.0) {}
};

bool dl_element_init(DLELEMENT &element, int &argc, char **argv )
{
  reinterpret_cast<QUADRUPOLE_DATA*>(element.data) = new QUADRUPOLE_DATA;
  QUADRUPOLE_DATA &dl_quad = *reinterpret_cast<QUADRUPOLE_DATA*>(element.data);
  element.attributes.add("strength", OPT_DOUBLE, &dl_quad.strength, opposite);
  element.attributes.add("tilt", OPT_DOUBLE, &dl_quad.tilt);
  element.attributes.add("tilt_deg", OPT_DOUBLE, &dl_quad.tilt, deg2rad, rad2deg);
  element.attributes.parse_args(argc, argv);
}

void dl_element_fini(DLELEMENT &element )
{
  delete reinterpret_cast<QUADRUPOLE_DATA*>(element.data);
}

void dl_element_step_in(DLELEMENT &element, BEAM *beam )
{
  QUADRUPOLE_DATA &dl_quad = *reinterpret_cast<QUADRUPOLE_DATA*>(element.data);
  double tilt = dl_quad.tilt;
  const double eps=std::numeric_limits<double>::epsilon();
  double _offset;
  if (fabs(_offset=0.5*element.geometry.length*element.offset.xp-element.offset.x)>eps || fabs(offset.xp)>eps) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.x+=_offset;
      particle.xp-=element.offset.xp;
    }
  }
  if (fabs(_offset=0.5*element.geometry.length*element.offset.yp-element.offset.y)>eps || fabs(offset.yp)>eps) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.y+=_offset;
      particle.yp-=element.offset.yp;
    }
  }
  if (fabs(element.offset.roll+tilt)>eps) {
    if (placet_switch.first_order || beam->particle_beam) {
      bunch_rotate_0(beam,element.offset.roll+tilt);
    }
    else{
      bunch_rotate(beam,element.offset.roll+tilt); 
    }
  }
}

void dl_element_step_out(DLELEMENT &element, BEAM *beam )
{
  QUADRUPOLE_DATA &dl_quad = *reinterpret_cast<QUADRUPOLE_DATA*>(element.data);
  double tilt = dl_quad.tilt;
  const double eps=std::numeric_limits<double>::epsilon();
  if (fabs(element.offset.roll+tilt)>eps) {
    if (placet_switch.first_order || beam->particle_beam) {
      bunch_rotate_0(beam,-element.offset.roll-tilt);
    }
    else{
      bunch_rotate(beam,-element.offset.roll-tilt);
    }
  }
  double _offset;
  if (fabs(_offset=0.5*element.geometry.length*element.offset.yp+element.offset.y)>eps || fabs(offset.yp)>eps) {
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      particle.y+=_offset;
      particle.yp+=element.offset.yp;
    }
  }
  if (fabs(_offset=0.5*element.geometry.length*element.offset.xp+element.offset.x)>eps || fabs(offset.xp)>eps) {
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      particle.x+=_offset;
      particle.xp+=element.offset.xp;
    }
  }
}

void dl_element_step_4d(DLELEMENT &element, BEAM *beam )
{
  QUADRUPOLE_DATA &dl_quad = *reinterpret_cast<QUADRUPOLE_DATA*>(element.data);
  double strength = dl_quad.strength;
  const double eps=std::numeric_limits<double>::epsilon();
  R_MATRIX r;
  R_MATRIX rxx;
  if (element.geometry.length>eps){
    double k0=strength/element.geometry.length;
    if (fabs(k0)<eps){
      element.drift_step_4d(beam);
      return;
    }
#ifdef LONGITUDINAL
    if (beam->macroparticles==1) {
      for (int i=0;i<beam->slices;i++){
        PARTICLE &particle=beam->particle[i];
	beam->z_position[i]+=element.geometry.length*0.5e-6
	  *(particle.yp*particle.yp
	    +particle.xp*particle.xp);
	beam->z_position[i]+=element.geometry.length*0.5*1e6
	  *(EMASS*EMASS)/(particle.energy
				*particle.energy);
      }
    }
#endif
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      double k=k0/particle.energy;
      if (k>0.0){
	double ksqrt=sqrt(k);
	double c=cos(ksqrt*element.geometry.length);
	double s=sin(ksqrt*element.geometry.length);
	r.r11=c;
	r.r12=s/ksqrt;
	r.r21=-s*ksqrt;
	r.r22=c;
	double tmp=particle.y*c+particle.yp*s/ksqrt;
	particle.yp=-s*ksqrt*particle.y+particle.yp*c;
	particle.y=tmp;
	c=cosh(ksqrt*element.geometry.length);
	s=sinh(ksqrt*element.geometry.length);
	rxx.r11=c;
	rxx.r21=s/ksqrt;
	rxx.r12=s*ksqrt;
	rxx.r22=c;
	tmp=particle.x*c+particle.xp*s/ksqrt;
	particle.xp=s*ksqrt*particle.x+particle.xp*c;
	particle.x=tmp;
	mult_M_M(beam->sigma_xx+i,&rxx,beam->sigma_xx+i);
	mult_M_M(beam->sigma_xy+i,&rxx,beam->sigma_xy+i);
	transpose_M(&rxx);
	mult_M_M(&rxx,beam->sigma_xx+i,beam->sigma_xx+i);
	mult_M_M(&r,beam->sigma_xy+i,beam->sigma_xy+i);      
	mult_M_M(&r,beam->sigma+i,beam->sigma+i);
	transpose_M(&r);
	mult_M_M(beam->sigma+i,&r,beam->sigma+i);
      } else {
	double ksqrt=sqrt(-k);
	double c=cosh(ksqrt*element.geometry.length);
	double s=sinh(ksqrt*element.geometry.length);
	r.r11=c;
	r.r12=s/ksqrt;
	r.r21=s*ksqrt;
	r.r22=c;
	double tmp=particle.y*c+particle.yp*s/ksqrt;
	particle.yp=s*ksqrt*particle.y+particle.yp*c;
	particle.y=tmp;
	c=cos(ksqrt*element.geometry.length);
	s=sin(ksqrt*element.geometry.length);
	rxx.r11=c;
	rxx.r12=s/ksqrt;
	rxx.r21=-s*ksqrt;
	rxx.r22=c;
	tmp=particle.x*c+particle.xp*s/ksqrt;
	particle.xp=-s*ksqrt*particle.x+particle.xp*c;
	particle.x=tmp;
	mult_M_M(&rxx,beam->sigma_xx+i,beam->sigma_xx+i);
	transpose_M(&rxx);
        mult_M_M(beam->sigma_xx+i,&rxx,beam->sigma_xx+i);
	mult_M_M(&r,beam->sigma_xy+i,beam->sigma_xy+i);      
	mult_M_M(beam->sigma_xy+i,&rxx,beam->sigma_xy+i);
	mult_M_M(&r,beam->sigma+i,beam->sigma+i);
	transpose_M(&r);
	mult_M_M(beam->sigma+i,&r,beam->sigma+i);
      }
    }
  } else {
    double k0=strength;
    if (fabs(k0)<eps){
      element.drift_step_4d(beam);
      return;
    }
#ifdef LONGITUDINAL
    if (beam->macroparticles==1) {
      for (int i=0;i<beam->slices;i++){
        PARTICLE &particle=beam->particle[i];
	beam->z_position[i]+=element.geometry.length*0.5e-6*(particle.yp*particle.yp+particle.xp*particle.xp);
	beam->z_position[i]+=element.geometry.length*0.5*1e6*(EMASS*EMASS)/(particle.energy*particle.energy);
      }
    }
#endif
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      double k=k0/particle.energy;
      r.r11=1.0;
      r.r12=0.0;
      r.r21=-k;
      r.r22=1.0;
      particle.yp-=k*particle.y;
      rxx.r11=1.0;
      rxx.r12=0.0;
      rxx.r21=k;
      rxx.r22=1.0;
      particle.xp+=k*particle.x;
      mult_M_M(&rxx,beam->sigma_xx+i,beam->sigma_xx+i);
      transpose_M(&rxx);
      mult_M_M(beam->sigma_xx+i,&rxx,beam->sigma_xx+i);
      mult_M_M(&r,beam->sigma_xy+i,beam->sigma_xy+i);      
      mult_M_M(beam->sigma_xy+i,&rxx,beam->sigma_xy+i);
      mult_M_M(&r,beam->sigma+i,beam->sigma+i);
      transpose_M(&r);
      mult_M_M(beam->sigma+i,&r,beam->sigma+i);
    }
  }
}

void dl_element_step_4d_0(DLELEMENT &element, BEAM *beam )
{
  QUADRUPOLE_DATA &dl_quad = *reinterpret_cast<QUADRUPOLE_DATA*>(element.data);
  double strength = dl_quad.strength;
  const double eps=std::numeric_limits<double>::epsilon();
  if(element.geometry.length>eps){
    double k0=strength/element.geometry.length;
    if (fabs(k0)<eps){
      element.drift_step_4d_0(beam);
      return;
    }
#ifdef LONGITUDINAL
    if (beam->macroparticles==1){
      for (int i=0;i<beam->slices;i++){
	beam->z_position[i]+=element.geometry.length*0.5e-6
	  *(beam->particle[i].yp*beam->particle[i].yp+beam->particle[i].xp*beam->particle[i].xp);
	beam->z_position[i]+=element.geometry.length*0.5*1e6*(EMASS*EMASS)/(beam->particle[i].energy*beam->particle[i].energy);
      }
    }
#endif
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      double k=k0/particle.energy;
      if (k>0.0){
	double ksqrt=sqrt(k);
	double c=cos(ksqrt*element.geometry.length);
	double s=sin(ksqrt*element.geometry.length);
	double tmp=particle.y*c+particle.yp*s/ksqrt;
	particle.yp=-s*ksqrt*particle.y+particle.yp*c;
	particle.y=tmp;
#ifdef TWODIM
	c=cosh(ksqrt*element.geometry.length);
	s=sinh(ksqrt*element.geometry.length);
	tmp=particle.x*c+particle.xp*s/ksqrt;
	particle.xp=s*ksqrt*particle.x+particle.xp*c;
	particle.x=tmp;
#endif
      } else{
	double ksqrt=sqrt(-k);
	double c=cosh(ksqrt*element.geometry.length);
	double s=sinh(ksqrt*element.geometry.length);
	double tmp=particle.y*c+particle.yp*s/ksqrt;
	particle.yp=s*ksqrt*particle.y+particle.yp*c;
	particle.y=tmp;
#ifdef TWODIM
	c=cos(ksqrt*element.geometry.length);
	s=sin(ksqrt*element.geometry.length);
	tmp=particle.x*c+particle.xp*s/ksqrt;
	particle.xp=-s*ksqrt*particle.x+particle.xp*c;
	particle.x=tmp;
#endif
      }
    }
  } else {
    double k0=strength;
    if (fabs(k0)<eps){
      element.drift_step_4d_0(beam);
      return;
    }
#ifdef LONGITUDINAL
    if (beam->macroparticles==1){
      for (int i=0;i<beam->slices;i++){
        PARTICLE &particle=beam->particle[i];
	beam->z_position[i]+=element.geometry.length*0.5e-6*(particle.yp*particle.yp+particle.xp*particle.xp);
	beam->z_position[i]+=element.geometry.length*0.5*1e6*(EMASS*EMASS)/(particle.energy*particle.energy);
      }
    }
#endif
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      double k=k0/particle.energy;
      particle.yp-=k*particle.y;
      particle.xp+=k*particle.x;
    }
  }
}
