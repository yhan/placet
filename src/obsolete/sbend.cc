int tk_Sbend(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	     char *argv[])
{
    int error;
    double length=0.0,e0=0.0,angle,e1=0.0,e2=0.0,k=0.0;
    SBEND *element;
    int synrad=1;
    char *capert=NULL;
    double apx=0.; double apy=0.;
    Tk_ArgvInfo table[]={
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
    (char*)"length of the cavity in meter"},
    {(char*)"-angle",TK_ARGV_FLOAT,(char*)NULL,(char*)&angle,
    (char*)"nominal bend angle"},
    {(char*)"-e0",TK_ARGV_FLOAT,(char*)NULL,(char*)&e0,
    (char*)"Nominal energy"},
    {(char*)"-K",TK_ARGV_FLOAT,(char*)NULL,(char*)&k,
    (char*)"Focusing strength"},
    {(char*)"-E1",TK_ARGV_FLOAT,(char*)NULL,(char*)&e1,
    (char*)"Angle of entrance"},
    {(char*)"-E2",TK_ARGV_FLOAT,(char*)NULL,(char*)&e2,
    (char*)"Angle of exit"},
    {(char*)"-synrad",TK_ARGV_INT,(char*)NULL,(char*)&synrad,
    (char*)"if not zero include synchrotron radiation"},
    {(char*)"-aperture",TK_ARGV_STRING,(char*)NULL,(char*)&capert,
    (char*)"aperture of the bending magnet"},
    {(char*)"-apx",TK_ARGV_FLOAT,(char*)NULL,(char*)&apx,
    (char*)"x_aperture for bending magnet"},
    {(char*)"-apy",TK_ARGV_FLOAT,(char*)NULL,(char*)&apy,
    (char*)"y_aperture for bending magnet"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
    }
    if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Sbend>",TCL_VOLATILE);
    return TCL_ERROR;
    }

    if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

    element=new SBEND(length, angle, e0, e1, e2, k);
    if (synrad) element->set_synrad();
    if(capert!=NULL && apx!=0. && apy!=0.) {
    element->set_aperture(capert, apx, apy);
    }
    inter_data.girder->add_element(element);
    return TCL_OK;
}

static inline void bend_simple(double F,double L,double Angle,double k0, double k2, double &x,double &xp, double &y,double &yp,double e)
{
  const int n=30;
  const double inv_e = 1./e;
  const double f=F/(n*e);
  const double kx=f*Angle/L;
  const double angle=Angle/n;
  const double rho_i=Angle/L;
  k0/=(n*e);
  const double l=L/n;
  double cw=1.0+0.5*(xp*xp+yp*yp);
  cw*=0.5*l*(1.0+x*rho_i);
  x+=xp*cw;
  y+=yp*cw;
#ifdef LONGITUDINAL
  // dz
  dz-=0.25*(xp*xp+yp*yp+EMASS*EMASS/(e*e)-x*angle)*l;
#endif
  k2/=n;
  for (int i=0;i<n;++i){
#ifdef LONGITUDINAL
    // dz
    dz+=0.25*(xp*xp+yp*yp+EMASS*EMASS/(e*e)-x*angle)*l;
#endif
    cw=1.0+0.5*xp*xp;
    double dx=((kx+k0)*x+f)*cw-angle;
    double dy=-k0*y*cw;
    cw+=0.5*yp*yp;
    xp-=dx;
    yp-=dy;
    cw*=l*(1.0+x*rho_i);
    x+=xp*cw;
    y+=yp*cw;
    if (k2!=0.0) {
      double kx0, ky0;
      multipole_kick(2,k2,0.0,1.0,x*1e6,y*1e6,kx0,ky0);
      kx0*=inv_e;
      ky0*=inv_e;
      xp+=kx0*1e-6;
      yp+=ky0*1e-6;
    }
  }
#ifdef LONGITUDINAL
  // dz
  dz+=0.25*(xp*xp+yp*yp+EMASS*EMASS/(e*e)-x*angle)*l;
#endif
  //  cw=1.0/sqrt(1.0-xp*xp-yp*yp);
  //  cw=1.0+0.5*(xp*xp+yp*yp);
  x-=xp*0.5*cw;
  y-=yp*0.5*cw;
}

static inline int
bend_simple_synrad_2(SBEND* sbend, double F,double L,double Angle,double K0, double K2,
		     double &x,double &xp,
                     double &y,double &yp,double &e)
{
  //  int i,n=30;
  const int n=15;
  int nphot=0;
  double f,kx,tx,ty,eloss,k0,tangle;
  double lifetime,cw;
  const double angle=Angle/n;
  const double rho_i=Angle/L;
  const double l=L/n;
  f=F/(n*e);
  k0=K0/(n*e);
  kx=f*Angle/L;
  cw=1.0+0.5*(xp*xp+yp*yp);
  double dummy = 0.5*l*(1.0+x*rho_i);
  x+=xp*dummy;
  y+=yp*dummy;
  lifetime=synrad_free_path();
#ifdef LONGITUDINAL
  const double EMASS2 = EMASS*EMASS;
  // dz
  dz-=0.25*(xp*xp+yp*yp+EMASS2/(e*e)-x*angle)*l;
#endif
  K2/=n;
  for (int i=0;i<n;++i){
#ifdef LONGITUDINAL
    // dz
    dz+=0.5*(xp*xp+yp*yp+EMASS2/(e*e)-x*angle)*l;
#endif
    cw=1.0+0.5*xp*xp;
    tx=(kx+k0)*x+f;
    ty=-k0*y;
    xp-=tx*cw-angle;
    yp-=ty*cw;
    cw+=0.5*yp*yp;
    dummy = l*(1.0+x*rho_i)*cw;
    x+=xp*dummy;
    y+=yp*dummy;
    tangle=sqrt(tx*tx+ty*ty);
    lifetime-=synrad_probability(e,tangle);
    while (lifetime<0.0) {
      lifetime+=synrad_free_path();
      eloss=synrad_eloss(e,tangle,l);
      e-=eloss;
      if (fabs(e)<std::numeric_limits<double>::epsilon())
        break;
      f=F/(n*e);
      kx=f*Angle/L;
      k0=K0/(n*e);
      nphot++;
      if(inter_data.track_photon) {
	ptracker->add_photon(x,y,xp,yp,eloss,i*l,sbend);
      }
    }
    if (fabs(e)<std::numeric_limits<double>::epsilon())
      break;
    if (K2!=0.0) {
      double kx0, ky0;
      multipole_kick(2,K2,0.0,1.0,x*1e6,y*1e6,kx0,ky0);
      const double inv_e = 1./e;
      kx0*=inv_e;
      ky0*=inv_e;
      xp+=kx0*1e-6;
      yp+=ky0*1e-6;
    }
  }
#ifdef LONGITUDINAL
  // dz
  dz+=0.25*(xp*xp+yp*yp+EMASS2/(e*e)-x*angle)*l;
#endif
  x-=xp*0.5*dummy;
  y-=yp*0.5*dummy;
  return nphot;
}
