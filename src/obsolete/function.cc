#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "function.h"

double pow_i(double x,int n)
{
  double res=1.0;
  while(n){
    if(n&1) res*=x;
    x*=x;
    n>>=1;
  }
  return res;
}

void pow_angle(double *c0,double *s0,int n)
{
  double s,c,c_old;
  s=*s0;
  c=*c0;
  n--;
  while(n){
    if (n&1){
      c_old=*c0;
      *c0=c* *c0-s* *s0;
      *s0=s*c_old+c* *s0;
    }
    c_old=c;
    c=c*c-s*s;
    s=s*c_old+c_old*s;
    n>>=1;
  }
  return;
}

void turn_angle(double c,double s,double *c0,double *s0)
{
  double tmp;
  tmp=*c0;
  *c0=c* *c0-s* *s0;
  *s0=s* tmp+c* *s0;
  return;
}

