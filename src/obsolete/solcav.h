#ifndef solcav_h
#define solcav_h

ELEMENT*
solcav_make(double length,double gradient,double bz,double phase);
void
solcav_particle_step(PARTICLE *particle,double length,double delta,double bz);
void
cavity_particle_step_end(PARTICLE *particle,double gradient);
void
solcav_particle_step_end(PARTICLE *particle,double gradient,
			 double bz);
void
cavity_sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double d_g);
void
solcav_sigma_step_end_new(R_MATRIX *sigma,R_MATRIX *sigma_xx,
			  R_MATRIX *sigma_xy,
			  double energy,double d_g,double d_bz);
void
solcav_sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double d_g,double d_bz);
void
solcav_sigma_step(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double length,double delta,double bz);
void
solcav_step(ELEMENT *element,BEAM *bunch);
void
solcav_step_4d_0(ELEMENT *element,BEAM *bunch);
void
solcav_step_twiss(ELEMENT *element,BEAM *beam,FILE *file,double step0,int j,
		    double s,int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int));

#endif
