#ifndef corrector_h
#define corrector_h

#include "option_info.h"

template <typename Type>
inline bool set_value(const option_info *option_table, const char *option_name, const Type &value )
{
	while(option_table->type!=OPT_END) {
		if (strcmp(option_table->name, option_name) == 0) {
			(*reinterpret_cast<Type*>(option_table->dest)) = value;
			return true;
		}
		option_table++;
	}
	return false;
}

template <typename Type>
inline bool add_value(const option_info *option_table, const char *option_name, const Type &value )
{
  while(option_table->type!=OPT_END) {
    if (strcmp(option_table->name, option_name) == 0) {
      (*reinterpret_cast<Type*>(option_table->dest)) += value;
      return true; 
    }
    option_table++; 
  }
  return false; 
}

#endif /* corrector_h */
