#ifndef file_stream_hh
#define file_stream_hh

#include <cstdio>
#include <string>
#include <vector>
#include <list>

#include "stream.hh"

class File_OStream : public OStream {

	FILE *file;
	
	size_t	length;
	
protected:

	inline bool writable() const { return file != NULL; }

public:

	explicit File_OStream(const char *path ) : length(0) { file=fopen(path, "w"); }
	explicit File_OStream(int fd ) : length(0) { file=::fdopen(fd, "w"); }
	File_OStream(FILE *f=NULL ) : file(f), length(0) {}
	
	~File_OStream() { if (file) fclose(file); }

	bool open(const char *path )
	{
		if (file) fclose(file);
		file=fopen(path, "w");
		length=0;
		return file!=NULL;
	}
	
	bool fdopen(int fd )
	{
		if (file) fclose(file);
		file=::fdopen(fd, "w");
		length=0;
		return file!=NULL;
	}
	
	void close()
	{
		fclose(file);
		file=NULL;
	}

	// output streams
	size_t get_length() const { return length; }
	void flush() { if (file) fflush(file); }
	
	// write methods
	inline size_t write(const char *ptr, size_t n )
	{
		size_t l = fwrite(ptr,sizeof(char),n,file);
		length += l*sizeof(char);
		return l;
	}
        
	inline size_t write(const char &ref )
	{
		if (fputc(ref,file) == EOF) return 0; 
		length += sizeof(char);
		return 1;
	}

	#define WRITE(TYPE)     inline size_t write(const TYPE *ptr, size_t n ) { size_t l=fwrite(ptr,sizeof(TYPE),n,file); length+=l*sizeof(TYPE); return l; }\
                                inline size_t write(const TYPE &ref )           { size_t l=fwrite(&ref,sizeof(TYPE),1,file); length+=sizeof(TYPE); return 1; }
	WRITE(float)
	WRITE(double)
	WRITE(long double)
	WRITE(signed short)
	WRITE(signed int)
	WRITE(signed long int)
	WRITE(unsigned short)
	WRITE(unsigned int)
	WRITE(unsigned long int)
	#undef WRITE

};

class File_IStream : public IStream {

	FILE *file;
	
protected:

	inline bool readable()	const { return file ? feof(file) == 0 : false; }

public:

	explicit File_IStream(const char *path ) { file=fopen(path, "r"); }
	explicit File_IStream(int fd ) { file=::fdopen(fd, "r"); }
	File_IStream(FILE *f=NULL) : file(f) {}

	~File_IStream() { if (file) fclose(file); }

	bool open(const char *path )
	{
		if (file) fclose(file);
		file=fopen(path, "r");
		return file!=NULL;
	}

	bool fdopen(int fd )
	{
		if (file) fclose(file);
		file=::fdopen(fd, "r");
		return file!=NULL;
	}
	
	void close()
	{
		fclose(file);
		file=NULL;
	}
	
	// input streams
	bool eof() const { return file ? feof(file) != 0 : true; }
	void rewind() { ::rewind(file); }
	
	// read methods
	inline size_t read(char *ptr, size_t n )
	{
		return fread(ptr,sizeof(char),n,file);
	}
	
	inline size_t read(char &c )
	{
		int __tmp=fgetc(file);
		if (__tmp==EOF) return 0;
		c=char(__tmp);
		return 1;
	}

	#define READ(TYPE)\
    inline size_t read(TYPE *ptr, size_t n )        { return fread(ptr,sizeof(TYPE),n,file); }\
    inline size_t read(TYPE &ref )                  { return fread(&ref,sizeof(TYPE),1,file); }

	READ(float)
	READ(double)
	READ(long double)
	READ(signed short)
	READ(signed int)
	READ(signed long int)
	READ(unsigned short)
	READ(unsigned int)
	READ(unsigned long int)
	#undef READ

};

class File_IOStream : public IOStream {

	FILE *file;
	
	size_t	length;
	
protected:

	inline bool writable() const { return file != NULL; }
	inline bool readable()	const { return file ? feof(file) == 0 : false; }

public:

	explicit File_IOStream(const char *path ) : length(0) { file=fopen(path, "a+"); }
	explicit File_IOStream(int fd ) : length(0) { file=fdopen(fd, "a+"); }
	File_IOStream(FILE *f=NULL ) : file(f), length(0) {}
	
	~File_IOStream() { if (file) fclose(file); }

	bool open(const char *path )
	{
		if (file) fclose(file);
		file=fopen(path, "a+");
		length=0;
		return file!=NULL;
	}
	
	void close()
	{
		fclose(file);
		file=NULL;
	}

	// output streams
	size_t get_length() const { return length; }
	void flush() { fflush(file); }
	
	// input streams
	bool eof() const { return file ? feof(file) != 0 : true; }
	void rewind() { ::rewind(file); }
	
	// write methods
	inline size_t write(const char *ptr, size_t n )
	{
		size_t l = fwrite(ptr,sizeof(char),n,file);
		length += l*sizeof(char);
		return l;
	}
        
	inline size_t write(const char &ref )
	{
		if (fputc(ref,file) == EOF) return 0; 
		length += sizeof(char);
		return 1;
	}

	#define WRITE(TYPE)\
    inline size_t write(const TYPE *ptr, size_t n ) { size_t l=fwrite(ptr, sizeof(TYPE),n,file); length+=l*sizeof(TYPE); return l; }\
    inline size_t write(const TYPE &ref )           { size_t l=fwrite(&ref,sizeof(TYPE),1,file); length+=sizeof(TYPE); return 1; }

	WRITE(float)
	WRITE(double)
	WRITE(long double)
	WRITE(signed short)
	WRITE(signed int)
	WRITE(signed long int)
	WRITE(unsigned short)
	WRITE(unsigned int)
	WRITE(unsigned long int)
	#undef WRITE
	
	// read methods
	inline size_t read(char *ptr, size_t n )
	{
		return fread(ptr,sizeof(char),n,file);
	}
	
	inline size_t read(char &c )
	{
		int __tmp=fgetc(file);
		if (__tmp==EOF) return 0;
		c=char(__tmp);
		return 1;
	}

	#define READ(TYPE)\
    inline size_t read(TYPE *ptr, size_t n )        { return fread(ptr,sizeof(TYPE),n,file); }\
    inline size_t read(TYPE &ref )                  { return fread(&ref,sizeof(TYPE),1,file); }

	READ(float)
	READ(double)
	READ(long double)
	READ(signed short)
	READ(signed int)
	READ(signed long int)
	READ(unsigned short)
	READ(unsigned int)
	READ(unsigned long int)
	#undef READ
};

#endif /* file_stream_hh */
