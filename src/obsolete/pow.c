#include "stdlib.h"
#include "stdio.h"
#include "math.h"

double
pow_i(double x,int n)
{
  double res=1.0;
  while(n){
    if(n%2) res*=x;
    x*=x;
    n/=2;
  }
  return res;
}

pow_angle(double *c0,double *s0,int n)
{
  double s,c,c_old;
  s=*s0;
  c=*c0;
  n--;
  while(n){
    if (n%2){
      c_old=*c0;
      *c0=c* *c0-s* *s0;
      *s0=s*c_old+c* *s0;
    }
    c_old=c;
    c=c*c-s*s;
    s=s*c_old+c_old*s;
    n/=2;
  }
  return;
}

turn_angle(double c,double s,double *c0,double *s0)
{
  *c0=c* *c0-s* *s0;
  *s0=s* *c0+c* *s0;
  return;
}

pow_angle_old(double *c0,double *s0,int n)
{
  double phi;
  phi=atan2(*c0,*s0);
  phi*=(double)n;
  *c0=cos(phi);
  *s0=sin(phi);
  return;
}

main()
{
  int i,j,k;
  double s,c,s0,c0;
  c=2.0*cos(0.01);
  s=2.0*sin(0.01);
  for(i=0;i<20000;i++){
    c0=cos(0.01);
    s0=sin(0.01);
    for (k=0;k<150;k++){
      /*      c=cos(0.01);
	      s=sin(0.01);*/
      for (j=0;j<21;j++){
	turn_angle(c0,s0,&c,&s);
      }
    }
  }
  exit(0);
}
