//old unused cases from void wake_kick(ELEMENT * /*element*/,BEAM *beam,double length)
extern PetsWake drive_data;

  case 2: {
    double *p=beam->field[0].kick;
    for (int j=0;j<beam->slices_per_bunch;j++){
      double sumy=0.0;
      double sumx=0.0;
      for (int i=0;i<=j;i++){
	double y=0.0;
	double x=0.0;
	for (int k=0;k<beam->macroparticles;k++){
	  y+=beam->particle[i*beam->macroparticles+k].y*
	    beam->particle[i*beam->macroparticles+k].wgt;
	  x+=beam->particle[i*beam->macroparticles+k].x*
	    beam->particle[i*beam->macroparticles+k].wgt;
	}
	sumy+=y * *p;
	sumx+=x * *p;
	p++;
      }
      for (int k=0;k<beam->macroparticles;k++){
	beam->particle[j*beam->macroparticles+k].yp+=sumy*factor/beam->particle[j*beam->macroparticles+k].energy;
	beam->particle[j*beam->macroparticles+k].xp+=sumx*factor/beam->particle[j*beam->macroparticles+k].energy;
      }
    }
  } break;
  case 44: {
    double *force=cavity_data->along;
    int m=0;
    int m2=0;
    double *rho_y=beam->rho_y[0];
    double *rho_yl=cavity_data->ypos;
    double *rho_x=beam->rho_x[0];
    double *rho_xl=cavity_data->xpos;
    for (int j=0;j<beam->bunches;j++){
      rho_yl[j]=0.0;
      rho_xl[j]=0.0;
      for (int i=0;i<beam->slices_per_bunch;i++){
	double sumy=0.0;
	double sumx=0.0;
	for (int k=0;k<beam->macroparticles;k++){
	  sumy+=beam->particle[m].y*beam->particle[m].wgt;
	  sumx+=beam->particle[m].x*beam->particle[m].wgt;
	  m++;
	}
	rho_y[i]=sumy;
	rho_yl[j]+=sumy;
	rho_x[i]=sumx;
	rho_xl[j]+=sumx;
      }
      double sumyl=0.0;
      double sumxl=0.0;
      for (int i=0;i<j;i++){
	sumyl+=rho_yl[i]*force[j-i];
	sumxl+=rho_xl[i]*force[j-i];
      }
      for (int i=0;i<beam->slices_per_bunch;i++){
	double sumy=0.0;
	double sumx=0.0;
	double *p=beam->field[0].kick;
	for (int k=0;k<=i;k++){
	  sumy+= rho_y[k] * *p;
	  sumx+= rho_x[k] * *p;
	  p++;
	}
	for (int k=0;k<beam->macroparticles;k++){
	  beam->particle[m2].yp+=(sumy+sumyl)*factor/beam->particle[m2].energy;
	  beam->particle[m2].xp+=(sumx+sumxl)*factor/beam->particle[m2].energy;
	  m2++;
	}
      }
    }
  } break;

  case 5:
    for (int k=0;k<beam->bunches;k++){
      double multi_y=0.0;
      double multi_y_b=0.0;
      double *rho_a=cavity_data->ypos;
      double *rho_b=cavity_data->yposb;
      double *force=cavity_data->along;
      for (int j=0;j<k;j++){
	multi_y+=rho_a[j]*force[k-j];
	multi_y_b+=rho_b[j]*force[k-j];
      }
      double *p=beam->field[0].kick;
      int m=k*beam->slices_per_bunch;
      double pos_y=0.0;
      double pos_yb=0.0;
      double pos_x=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	pos_y+=beam->particle[m+j].y*beam->particle[m+j].wgt*cavity_data->bf[m+j];
	pos_yb-=beam->particle[m+j].y*beam->particle[m+j].wgt*cavity_data->af[m+j];
	pos_x+=beam->particle[m+j].x*beam->particle[m+j].wgt;
	double sumy=0.0;
	double sumx=0.0;
	for (int i=0;i<=j;i++){
	  sumy+=beam->particle[m+i].y * *p;
	  sumx+=beam->particle[m+i].x * *p;
	  p++;
	}
	beam->particle[m+j].yp+=(sumy+multi_y*cavity_data->af[m+j]+multi_y_b*cavity_data->bf[m+j])*factor/beam->particle[m+j].energy;
	beam->particle[m+j].xp+=sumx*factor/beam->particle[m+j].energy;
      }
      cavity_data->ypos[k]=pos_y;
      cavity_data->yposb[k]=pos_yb;
      cavity_data->xpos[k]=pos_x;
    }
    break;
  case 8: {
    /* Initialise wakefields */
    double *rho_y=beam->rho_y[0];
    double multi_y=0.0;
    double multi_y_b=0.0;
    double *rho_x=beam->rho_x[0];
    double multi_x=0.0;
    double multi_x_b=0.0;
      /* set pointer af and bf to the appropriate cell number */
    
    double *af=cavity_data->af+step*beam->bunches*beam->slices_per_bunch;
    double *bf=cavity_data->bf+step*beam->bunches*beam->slices_per_bunch;
    double *along=cavity_data->along+step*beam->bunches;
    double longfact=along[0];
    
    /* set new cell number if different cells */
    
    step++;
    if (step>=NSTEP) step=0;
//    if (MODEL==0) step=0;
    
    /* loop over all bunches */
    
    for (int k=0;k<beam->bunches;k++){
      
      /* apply attenuation */

      if (k>0){
	multi_y*=along[k];
	multi_y_b*=along[k];
	multi_x*=along[k];
	multi_x_b*=along[k];
      }
      
      double *p=beam->field[0].kick;
      int m=k*beam->slices_per_bunch;
      double pos_y=0.0;
      double pos_yb=0.0;
      double pos_x=0.0;
      double pos_xb=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
	rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
	double s=af[m+j];  /*sin(beam->z_position[m+j]);*/
	double c=bf[m+j];  /*cos(beam->z_position[m+j]);*/
	/*
	  s=sin(beam->z_position[m+j]*TWOPI*1e-5);
	  c=cos(beam->z_position[m+j]*TWOPI*1e-5);
	*/
	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
	/*
	  Single bunch contribution
	 */
	double sumy=0.0;
	double sumx=0.0;
	for (int i=0;i<=j;i++){
	  sumy+=rho_y[i] * *p;
	  sumx+=rho_x[i] * *p;
	  p++;
	}
	beam->particle[m+j].yp+=(sumy+(multi_y*s+multi_y_b*c)*longfact)
	  *factor/beam->particle[m+j].energy;
	beam->particle[m+j].xp+=(sumx+(multi_x*s+multi_x_b*c)*longfact)
	  *factor/beam->particle[m+j].energy;
	}
      multi_y+=pos_y;
      multi_y_b+=pos_yb;
      multi_x+=pos_x;
      multi_x_b+=pos_xb;
    }
    }  break;
  case 9: {
    exit(17);
/*
    double multi_y=0.0;
    double multi_y_b=0.0;
    // scdtmp
    double *af=cavity_data->af+step*beam->bunches*beam->slices_per_bunch;
    double *bf=cavity_data->bf+step*beam->bunches*beam->slices_per_bunch;
    //      step++;
    //      if (step>15) step=0;
//      if (MODEL==0) step=0;
    double longfact=cavity_data->along[1];
    for (int k=0;k<beam->bunches;k++){
      multi_y*=ATTENUATION;
      multi_y_b*=ATTENUATION;
      // scdtmp
      //	if ((k%82)==0){
      //	  multi_y*=pow(ATTENUATION,22.0);
//	  multi_y_b*=pow(ATTENUATION,22.0);
      //	}
      int m=k*beam->slices_per_bunch;
      double pos_y=0.0;
      double pos_yb=0.0;
      double pos_x=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	  double s=af[m+j];//sin(beam->z_position[m+j]);
	  double c=bf[m+j];//cos(beam->z_position[m+j]);
	  pos_y+=beam->particle[m+j].y*beam->particle[m+j].wgt*c;
	  pos_yb-=beam->particle[m+j].y*beam->particle[m+j].wgt*s;
	  pos_x+=beam->particle[m+j].x*beam->particle[m+j].wgt;
	  double sumx=0.0;
          beam->particle[m+j].yp+=((multi_y+pos_y)*s+(multi_y_b+pos_yb)*c)
	    *longfact*factor/beam->particle[m+j].energy;
	  //          beam->particle[m+j].yp+=((multi_y)*s+(multi_y_b)*c)
	  //	    *longfact*factor/beam->particle[m+j].energy;
	  beam->particle[m+j].xp+=sumx*factor/beam->particle[m+j].energy;
      }
      multi_y+=pos_y;
      multi_y_b+=pos_yb;
      //placet_printf(INFO,"   %g %g\n",pos_y,pos_yb);
      cavity_data->xpos[k]=pos_x;
    }
*/
  }  break;
  case 11:
    for (int k=0;k<beam->bunches;k++){
      double multi_y=0.0;
      double multi_y_b=0.0;
      double *rho_a=cavity_data->ypos;
      double *rho_b=cavity_data->yposb;
      double multi_x=0.0;
	double multi_x_b=0.0;
	double *rho_a_x=cavity_data->xpos;
	double *rho_b_x=cavity_data->xposb;
	double *force=cavity_data->along;
	int j0=0;
	if (k>cavity_data->longrange_max){
	  j0=k-cavity_data->longrange_max;
	}
	for (int j=j0;j<k;j++){
	  multi_y+=rho_a[j]*force[k-j];
	  multi_y_b+=rho_b[j]*force[k-j];
	  multi_x+=rho_a_x[j]*force[k-j];
	  multi_x_b+=rho_b_x[j]*force[k-j];
	}
	int m=k*beam->slices_per_bunch;
	double pos_y=0.0;
	double pos_yb=0.0;
	double pos_x=0.0;
	double pos_xb=0.0;
	for (int j=0;j<beam->slices_per_bunch;j++){
// scd new
//	  s=sin(cavity_data.k_transverse*beam->z_position[m+j]);
//	  c=cos(cavity_data.k_transverse*beam->z_position[m+j]);
//	  s=cavity_data->af[m+j];
//	  c=cavity_data->bf[m+j];
	  pos_y+=beam->particle[m+j].y*beam->particle[m+j].wgt*cavity_data->bf[m+j];
	  pos_yb-=beam->particle[m+j].y*beam->particle[m+j].wgt*cavity_data->af[m+j];
	  pos_x+=beam->particle[m+j].x*beam->particle[m+j].wgt*cavity_data->bf[m+j];
	  pos_xb-=beam->particle[m+j].x*beam->particle[m+j].wgt*cavity_data->af[m+j];
	  
          beam->particle[m+j].yp+=((multi_y+pos_y*force[0])
				   *cavity_data->af[m+j]
				   +(multi_y_b+pos_yb*force[0])
				   *cavity_data->bf[m+j])
	    *factor/beam->particle[m+j].energy;
          beam->particle[m+j].xp+=((multi_x+pos_x*force[0])
				   *cavity_data->af[m+j]
				   +(multi_x_b+pos_xb*force[0])
				   *cavity_data->bf[m+j])
	    *factor/beam->particle[m+j].energy;
	}
	cavity_data->ypos[k]=pos_y;
	cavity_data->yposb[k]=pos_yb;
	cavity_data->xpos[k]=pos_x;
	cavity_data->xposb[k]=pos_xb;
    }
    break;
  case 13:
    for (int k=0;k<beam->bunches;k++){
      double multi_y=0.0;
      double multi_y_b=0.0;
      double *rho_a=cavity_data->ypos;
      double *rho_b=cavity_data->yposb;
      double *force=cavity_data->along;
      // scd new 26.1.98
      double y_off=0.0;
      
      int j0=0;
      if (k>cavity_data->longrange_max){
	j0=k-cavity_data->longrange_max;
      }
      for (int j=j0;j<k;j++){
	multi_y+=rho_a[j]*force[k-j];
	multi_y_b+=rho_b[j]*force[k-j];
      }
      // scd new 26.1.98
      double kick_y=0.0;
      for (int j=0;j<k;j++){
	kick_y+=cavity_data->yoff[j]*cavity_data->blong[k-j];
      }
      
      int m=k*beam->slices_per_bunch;
      double pos_y=0.0;
      double pos_yb=0.0;
      double pos_x=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	pos_y+=beam->particle[m+j].y*beam->particle[m+j].wgt
	  *cavity_data->bf[m+j];
	pos_yb-=beam->particle[m+j].y*beam->particle[m+j].wgt
	  *cavity_data->af[m+j];
	y_off+=beam->particle[m+j].y*beam->particle[m+j].wgt;
	pos_x+=beam->particle[m+j].x*beam->particle[m+j].wgt;
	beam->particle[m+j].yp+=((multi_y+pos_y*force[0])
				 *cavity_data->af[m+j]
				 +(multi_y_b+pos_yb*force[0])
				 *cavity_data->bf[m+j]
				 +kick_y)
	  *factor/beam->particle[m+j].energy;
	
	//ASK DANIEL : sumx was not initialized. I set it to zero.
	double sumx=0.0;
	beam->particle[m+j].xp+=sumx*factor/beam->particle[m+j].energy;
      }
      cavity_data->ypos[k]=pos_y;
      cavity_data->yposb[k]=pos_yb;
      cavity_data->yoff[k]=y_off;
      cavity_data->xpos[k]=pos_x;
    }
    break;
    case 15:
      for (int k=0;k<beam->bunches;k++){
	double multi_y=0.0;
	double multi_y_b=0.0;
	double *rho_a=cavity_data->ypos;
	double *rho_b=cavity_data->yposb;
	double *force=cavity_data->along;
	int j0=0;
	if (k>cavity_data->longrange_max){
	  j0=k-cavity_data->longrange_max;
	}
	for (int j=j0;j<k;j++){
	  multi_y+=rho_a[j]*force[k-j];
	  multi_y_b+=rho_b[j]*force[k-j];
	}
	int m=k*beam->slices_per_bunch;
	double pos_y=0.0;
	double pos_yb=0.0;
	for (int j=0;j<beam->slices_per_bunch;j++){
	  pos_y+=beam->particle[m+j].wgt*cavity_data->bf[m+j];
	  pos_yb-=beam->particle[m+j].wgt*cavity_data->af[m+j];
	  
          beam->particle[m+j].yp+=((multi_y+pos_y*force[0])
				   *cavity_data->af[m+j]
				   +(multi_y_b+pos_yb*force[0])
				   *cavity_data->bf[m+j])
	    *factor*pow(beam->particle[m+j].y,3)/beam->particle[m+j].energy;
	  
	  //ASK DANIEL : sumx was not initialized. I set it to zero.
	  double sumx=0.0;
	  beam->particle[m+j].xp+=sumx*factor/beam->particle[m+j].energy;
	}
	cavity_data->ypos[k]=pos_y;
	cavity_data->yposb[k]=pos_yb;
	
	//ASK DANIEL : pos_x was not initialized. I set it to zero.
	double pos_x=0.0;
	cavity_data->xpos[k]=pos_x;
      }
      break;
  case 122: {
    int n=beam->slices_per_bunch;
    double *rho_y=beam->rho_y[0];
    for (int i=0;i<n;i++){
      rho_y[i]=beam->particle[i].y;
    }
    double *p=beam->field[0].kick;
    for (int i=0;i<n;i++){
      double sumy=0.0;
      for (int j=0;j<=i;j++){
	sumy+=rho_y[j]* *p;
	p++;
      }
      beam->particle[i].yp+=
	sumy*factor/beam->particle[i].energy;
    }
  } break;
  case 112: {
    int nf=beam->macroparticles*beam->slices_per_bunch;
    int nadd=nf/2;
    int nmacro=beam->macroparticles;
    double half_length=0.5*length;
    for (int k=0;k<beam->bunches;k++){
      double multi_y=0.0;
      double multi_y_b=0.0;
      double *rho_a=cavity_data->ypos;
      double *rho_b=cavity_data->yposb;
      double *rho_a2=cavity_data->ypos2;
      double *rho_b2=cavity_data->yposb2;
      double multi_x=0.0;
      double multi_x_b=0.0;
      double *rho_a_x=cavity_data->xpos;
      double *rho_b_x=cavity_data->xposb;
      double *force=cavity_data->along;
      int j0=0;
      if (k>cavity_data->longrange_max){
	j0=k-cavity_data->longrange_max;
	}
      for (int j=j0;j<k;j++){
	multi_y+=(rho_a[j]
		  +0.5*(1.0-drive_data.speed*(beam->z_position[j*nf+nadd]
					      -beam->z_position[j*nf+nadd]))
		  *(rho_a2[j]-rho_a[j]))
	  *force[k-j];
	multi_y_b+=(rho_b[j]
		      +0.5*(1.0-drive_data.speed*(beam->z_position[k*nf+nadd]
						  -beam->z_position[j*nf+nadd]))
		      *(rho_b2[j]-rho_b[j]))
	  *force[k-j];
	multi_x+=rho_a_x[j]*force[k-j];
	multi_x_b+=rho_b_x[j]*force[k-j];
      }
      int m=k*beam->slices_per_bunch;
	double pos_y=0.0;
	double pos_yb=0.0;
	double pos_y2=0.0;
	double pos_yb2=0.0;
	double pos_x=0.0;
	double pos_xb=0.0;
	for (int j=0;j<beam->slices_per_bunch;j++){
	  double tmpy=0.0;
	  double tmpy2=0.0;
	  double tmpx=0.0;
	  for (int i=0;i<nmacro;i++){
	    tmpy+=(beam->particle[nmacro*(m+j)+i].y-half_length*
		   beam->particle[nmacro*(m+j)+i].yp)
	      *beam->particle[nmacro*(m+j)+i].wgt;
	    tmpx+=beam->particle[nmacro*(m+j)+i].x
	      *beam->particle[nmacro*(m+j)+i].wgt;
	  }
	  pos_y+=tmpy*cavity_data->bf[m+j];
	  pos_yb-=tmpy*cavity_data->af[m+j];
	  pos_x+=tmpx*cavity_data->bf[m+j];
	  pos_xb-=tmpx*cavity_data->af[m+j];
	  for (int i=0;i<nmacro;i++){
	    beam->particle[nmacro*(m+j)+i].yp+=((multi_y+(2.0*pos_y+pos_y2)/3.0
						 *force[0])
						*cavity_data->af[m+j]
						+(multi_y_b+(2.0*pos_yb+pos_yb2)/3.0
						  *force[0])
						*cavity_data->bf[m+j])
	      *factor/beam->particle[m+j].energy;
	    tmpy2+=(beam->particle[nmacro*(m+j)+i].y
		    +half_length*beam->particle[nmacro*(m+j)+i].yp)
	      *beam->particle[nmacro*(m+j)+i].wgt;
	    beam->particle[nmacro*(m+j)+i].xp+=((multi_x+pos_x*force[0])
						*cavity_data->af[m+j]
						+(multi_x_b+pos_xb*force[0])
						*cavity_data->bf[m+j])
	      *factor/beam->particle[m+j].energy;
	  }
	  pos_y2+=tmpy2*cavity_data->bf[m+j];
	  pos_yb2-=tmpy2*cavity_data->af[m+j];
	}
	cavity_data->ypos[k]=pos_y;
	cavity_data->yposb[k]=pos_yb;
	rho_a2[k]=pos_y2;
	rho_b2[k]=pos_yb2;
	cavity_data->xpos[k]=pos_x;
	cavity_data->xposb[k]=pos_xb;
    }
  } break;
