#ifndef vector_hh
#define vector_hh

#include <iostream>
#include <cstddef>
#include <cstdarg>
#include <cmath>

#include "stream.hh"

template <size_t N> class Vector {
	
  double data[N];

public:

  inline explicit Vector(double arg ) { for (size_t i=0; i<N; i++) data[i] = arg; }
  inline Vector(const double *v )  { for (size_t i=0; i<N; i++) data[i] = v[i]; }
  inline Vector() {}
	
  Vector(double v0, double v1, ... ) 
  {
    data[0] = v0;	
		
    if (N >= 2) {
      data[1] = v1;
      if (N >= 3) {
	va_list ap;
	va_start(ap, v1);
	for (size_t i = 2; i < N; i++) data[i] = va_arg(ap, double);
	va_end(ap);
      }
    }
  }

  inline void clear() { for (size_t i = 0; i < N; i++)	data[i] = 0; }

  inline const double &operator [] (size_t i )	const	{ return data[i]; }
  inline double &operator [] (size_t i )							{ return data[i]; }
	
  friend double abs(const Vector &v )
  {
    double sum = 0;
    for (size_t i = 0; i < N; i++) {
      double x = v.data[i];
      sum += x * x;
    }
    return sqrt(sum);
  }

  const Vector &operator += (const Vector &v )											{ for (size_t i = 0; i < N; i++) data[i] += v.data[i]; return *this; } 
  const Vector &operator -= (const Vector &v )											{ for (size_t i = 0; i < N; i++) data[i] -= v.data[i]; return *this; } 

  friend Vector operator + (const Vector &a, const Vector &b )			{ Vector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] + b.data[i]; return result; }
  friend Vector operator - (const Vector &a, const Vector &b )			{ Vector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] - b.data[i]; return result; }
  friend Vector operator - (const Vector &v )												{ Vector result; for (size_t i = 0; i < N; i++) result.data[i] = -v.data[i]; return result; }
	
  friend double operator * (const Vector &a, const Vector &b )			{ double result = a.data[0] * b.data[0]; for (size_t i = 1; i < N; i++) result += a.data[i] * b.data[i]; return result; }
  friend Vector operator * (const double &a, const Vector &b )			{ Vector result; for (size_t i = 0; i < N; i++) result.data[i] = a * b.data[i]; return result; }
  friend Vector operator * (const Vector &a, const double &b )			{ Vector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] * b; return result; }

  friend Vector operator / (const Vector &a, const double &b )			{ Vector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] / b; return result; }
		
  friend std::ostream &operator << (std::ostream &stream, const Vector &v )	{ for (size_t i = 0; i < N; i++) stream << "\t" << v[i]; return stream; }

  friend OStream &operator << (OStream &stream, const Vector &v )		{ if (stream) stream.write(v.data, N); return stream; }
  friend IStream &operator >> (IStream &stream, Vector &v )					{ if (stream) stream.read(v.data, N); return stream; }

};

#endif /* vector_hh */
