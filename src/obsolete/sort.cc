void
correct_swap(BEAM *beam,int i)
{
  PARTICLE p_tmp;
  R_MATRIX r_tmp;
  double z_tmp;

  //  placet_printf(INFO,"change: %d %g %g\n",i,beam->z_position[i-1],beam->z_position[i]);
  r_tmp=beam->sigma[i-1];
  beam->sigma[i-1]=beam->sigma[i];
  beam->sigma[i]=r_tmp;
#ifdef TWODIM
  r_tmp=beam->sigma_xx[i-1];
  beam->sigma_xx[i-1]=beam->sigma_xx[i];
  beam->sigma_xx[i]=r_tmp;
  r_tmp=beam->sigma_xy[i-1];
  beam->sigma_xy[i-1]=beam->sigma_xy[i];
  beam->sigma_xy[i]=r_tmp;
#endif
  p_tmp=beam->particle[i-1];
  beam->particle[i-1]=beam->particle[i];
  beam->particle[i]=p_tmp;
  z_tmp=beam->z_position[i-1];
  beam->z_position[i-1]=beam->z_position[i];
  beam->z_position[i]=z_tmp;
}

void
sort_beam_2(BEAM *beam)
{
  int i,j,n;

  n=beam->slices;
  for (j=1;j<n;j++){
    for (i=j-1;i>=0;i--){
      if (beam->z_position[i]<beam->z_position[j]) {
	break;
      }
    }
    exchange(beam,i+1,j);
  }
}
