#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <limits>
#include <typeinfo>
#include <readline/readline.h>
#include <readline/history.h>

#include <fnmatch.h>

#include <octave/config.h>
#include <octave/octave.h>
#include <octave/symtab.h>
#include <octave/parse.h>
#include <octave/unwind-prot.h>
#include <octave/toplev.h>
#include <octave/error.h> 
#include <octave/quit.h>
#include <octave/variables.h>
#include <octave/sighandlers.h>
#include <octave/sysdep.h>
#include <octave/defun.h>
#include <octave/ov-struct.h>
#include <octave/ov-cell.h>

//#include "octave_embed.h"
#include "octave.hh"

#include "beam.h"
#include "beamline.h"
#include "girder.h"
#include "multipole.h"
#include "rmatrix.h"
#include "dipole.h"
#include "link.h"
#include "bpm.h"

#include "placeti3.h"
#include "placet_tk.h"
#include "octave_placet.h"

DEFUN(placet_get_bpm_readings, args, nargout,
"     Get the bpm readings.\n\n\
Usage:\n\n\
     [R,S,I] = placet_get_bpm_readings(\"beamline\", bpm, correct)\n\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     bpm:           vector of bpm index position [INTEGER]. Default is: all bpms\n\
     correct:       neglect the BPM resolution [BOOL]. Default is: false\n\n\
Output:\n\
     - R = [x0, y0; x1, y1; ... ; xN, yN] : bpm readings [um]\n\
     - S = [z0, z1, ... , zN] bpm longitudinal position [m]\n\
     - I = [i0, i1, ... , iN] bpm index position [INTEGER]\n")
{
  if (args.length()<1 || args.length()>3) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_get_bpm_readings: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  
  if (args.length()>=2) {
    if (!args(1).is_real_type() || args(1).matrix_value().rows()!=1) {
      error("placet_get_bpm_readings: parameter 'bpm' (arg 2) must be an integer row-vector");
      return octave_value();
    }
  }
  
  if (args.length()==3) {
    if (!args(2).is_bool_type()) {
      error("placet_get_bpm_readings: parameter 'correct' (arg 3) must be a be boolean");
      return octave_value();
    }
  }

  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  
  ELEMENT **element=beamline->element;
  RowVector I;
  if (args.length()>=2) {
    I = RowVector(args(1).vector_value());
    for(int i=0; i<I.length(); i++) {
      if (floor(I(i))!=I(i)) {
        error("placet_get_bpm_readings: 'bpm' must be an integer vector");
        return octave_value();
      }
      else if (!((element[(int)floor(I(i))]->is_bpm()) || (element[(int)floor(I(i))]->is_quadbpm()))) {
        error("placet_get_bpm_readings: index is not a bpm");
        return octave_value();
      }
    }
  } else {
    std::vector<int> indexes;
    for (int i=0; i<beamline->n_elements; i++) {
      if ((element[i]->is_bpm()) || (element[i]->is_quadbpm())) {
         indexes.push_back(i);
      }
    }
    I.resize(indexes.size());
    for (size_t i=0; i<indexes.size(); i++) I(i) = indexes[i];
  }
    
  bool correct;
  if (args.length()==3)   correct = args(2).bool_value();
  else                    correct = false;
  
  Matrix R(I.length(), 2);
  RowVector S(I.length());
  if (correct) {
    double s=0.0;
    ELEMENT **element = beamline->element;
    for (int i=0, j=0; j<I.length(); i++, j++) {
      while(i<I(j))
        s+=element[i++]->get_length();
      s+=0.5*element[i]->get_length();
      S(j) = s;
      R(j, 0) = element[i]->bpm_ptr()->get_x_reading_exact();
      R(j, 1) = element[i]->bpm_ptr()->get_y_reading_exact();
      s+=0.5*element[i]->get_length();
    }
  } else {
    double s=0.0;
    ELEMENT **element = beamline->element;
    for (int i=0, j=0; j<I.length(); i++, j++) {
      while(i<I(j))
        s+=element[i++]->get_length();
      s+=0.5*element[i]->get_length();
      if (element[i]->is_quadbpm()) {
        S(j) = s;
        R(j, 0) = element[i]->bpm_ptr()->get_x_reading_exact();
        R(j, 1) = element[i]->bpm_ptr()->get_y_reading_exact();
      } else if (element[i]->is_bpm()) {
        S(j) = s;
        R(j, 0) = element[i]->bpm_ptr()->get_x_reading();
        R(j, 1) = element[i]->bpm_ptr()->get_y_reading();
      }
      s+=0.5*element[i]->get_length();
    }
  }
  octave_value_list retval;
  retval(0) = R;
  if (nargout>1) retval(1) = S;
  if (nargout>2) retval(2) = I;
  return retval;
}

DEFUN(placet_get_name_number_list, args, ,"\
     Get the index-number-list of the elements whose name\n\
     matches the patterns.\n\n\
Usage:\n\n\
     I = placet_get_name_number_list(\"beamline\", pattern1, [pattern2, ...])\n\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     patterns:      shell wildcard pattern [STRING]\n\n\
Output:\n\
     I:             row-vector with the indexes [INTEGER]\n")
{
  if (args.length()<2) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_get_name_number_list: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }

  for (int i=1; i< args.length(); i++) {
    if (!args(i).is_string()) {
      error("placet_get_name_number_list: expecting 'pattern%d' to be a string", i+1);
      return octave_value();
    }
  }
  
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  
  std::string patterns[args.length()-1];
  for (int i=0;i<args.length()-1;i++) {
  	patterns[i]=args(i+1).string_value();
  }
  
  std::vector<int> indexes;
  ELEMENT **element=beamline->element;
  for (int i=0; i<beamline->n_elements; i++) {
    for (int pattern=0;pattern<args.length()-1;pattern++) {
      if (fnmatch(patterns[pattern].c_str(),element[i]->get_name(),0)==0) {
        indexes.push_back(i);
        break;
      }
    }
  }
  RowVector I(indexes.size());
  for (size_t i=0; i<indexes.size(); i++) I(i) = indexes[i];
  return octave_value(I);
}

DEFUN(placet_get_number_list, args, ,
"     Get the index number list of the elements whose type is 'type'.\n\n\
Usage:\n\n\
     I = placet_get_number_list(\"beamline\", type1, [type2, type3, ...])\n\n\
Input\n\
     beamline:      beamline name [STRING]\n\
     types:         requested types [STRING]. 'type' can be:\n\
      - \"quadrupole\"\n\
      - \"multipole\"\n\
      - \"decapole\"\n\
      - \"octupole\"\n\
      - \"sextupole\"\n\
      - \"dipole\"\n\
      - \"cavity\"\n\
      - \"drift\"\n\
      - \"sbend\"\n\
      - \"bpm\"\n\n\
Output:\n\
     I:             row-vector with the indexes [INTEGER]\n")
{
  if (args.length()<2) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_get_name_number_list: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  
  std::vector<int> indexes;
  for (int i=0; i<beamline->n_elements; i++) {
    for (int j=1; j<args.length(); j++) {
      std::string type = args(j).string_value();
      ELEMENT *element = beamline->element[i];
      if ((type == "quadrupole" && element->is_quad()) ||
          (type == "cavity_pets" && element->is_cavity_pets()) ||
          (type == "multipole" && element->is_multipole()) ||
          (type == "decapole" && element->is_multipole() && element->multipole_ptr()->get_field() == 5) ||
          (type == "octupole" && element->is_multipole() && element->multipole_ptr()->get_field() == 4) ||
          (type == "sextupole" && element->is_multipole() && element->multipole_ptr()->get_field() == 3) ||
          (type == "dipole" && element->is_dipole()) ||
          (type == "cavity" && element->is_cavity()) ||
          (type == "drift" && element->is_drift()) ||
          (type == "sbend" && element->is_sbend()) ||
          (type == "quadbpm" && element->is_quadbpm()) ||
          (type == "bpm" && element->is_bpm()) ||
	  (type == "magnet" && (element->is_quad() || element->is_quadbpm() || element->is_multipole() || element->is_sbend()))) {
        indexes.push_back(i);
        break;
      }
    }
  }
  RowVector I(indexes.size());
  for (size_t i=0; i<indexes.size(); i++) I(i) = indexes[i];
  return octave_value(I);
}

DEFUN(placet_vary_corrector, args, ,
"Usage:\n\n\
     placet_vary_corrector(\"beamline\", index, [dx, dy])\n\n\
\n\
     Vary corrector at position 'index' in beamline 'beamline' by 'dx' and 'dy'\n") 
{
  if (args.length()!=3) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_vary_corrector: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_vary_corrector: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  if (!args(2).is_real_matrix() || args(2).matrix_value().columns()!=2) {
    error("placet_vary_corrector: parameters '[x,y]' (arg 3) must be a 2-column real matrix");
    return octave_value();
  }
  if (args(1).matrix_value().length()!=args(2).matrix_value().rows() || args(2).matrix_value().columns()!=2) {
    error("placet_vary_corrector: length(arg 2) must be equal to rows(arg 3) and columns(arg 3) must be equal to 2");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix corrections(args(2).matrix_value());
  Matrix indexes(args(1).matrix_value());
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_vary_corrector: 'index' must be an integer vector");
      return octave_value();
    }
  }
  try {
    for(int i=0; i<indexes.length(); i++) {
      int index = int(indexes(i));
      if (index<0 || index>=beamline->n_elements) {
        error("placet_vary_corrector: invalid index '%d'", index);
        return octave_value();
      } 
      double  x = corrections(i,0);
      double  y = corrections(i,1);
      beamline->element[index]->correct_x(x);
      beamline->element[index]->correct_y(y);
    }
  } catch (std::string str ) {
    error("placet_vary_corrector: %s", str.c_str());
  }
  return octave_value();
}

DEFUN(placet_vary_hcorrector, args, ,
"Usage:\n\n\
     placet_vary_hcorrector(\"beamline\", index, dx)\n\n\
\n\
     Vary the horizontal corrector at position 'index' in beamline 'beamline' by 'dx'\n") 
{
  if (args.length()!=3) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_vary_hcorrector: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_vary_hcorrector: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  if (!args(2).is_real_type()) {
    error("placet_vary_hcorrector: parameter 'dy' (arg 3) must be a real matrix or a scalar");
    return octave_value();
  }
  if (args(1).matrix_value().length()!=args(2).matrix_value().length() && args(2).matrix_value().length()!=1) {
    error("placet_vary_hcorrector: length(arg 3) must be either equal to length(arg 2) or 1");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix corrections(args(2).matrix_value());
  Matrix indexes(args(1).matrix_value());
  bool vector = corrections.length()>1;
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_vary_hcorrector: 'index' must be an integer vector");
      return octave_value();
    }
  }
  try {
    for(int i=0; i<indexes.length(); i++) {
      int index = int(indexes(i));
      if (index<0 || index>=beamline->n_elements) {
        error("placet_vary_hcorrector: invalid index '%d'", index);
        return octave_value();
      } 
      beamline->element[index]->correct_x(corrections(vector?i:0));
    }
  } catch (std::string str ) {
    error("placet_vary_hcorrector: %s", str.c_str());
  }
  return octave_value();
}

DEFUN(placet_vary_vcorrector, args, ,
"Usage:\n\n\
     placet_vary_vcorrector(\"beamline\", index, dy)\n\n\
\n\
     Vary the vertical corrector at position 'index' in beamline 'beamline' by 'dy'\n") 
{
  if (args.length()!=3) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_vary_vcorrector: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_vary_vcorrector: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  if (!args(2).is_real_type()) {
    error("placet_vary_vcorrector: parameter 'dy' (arg 3) must be a real matrix or a scalar");
    return octave_value();
  }
  if (args(1).matrix_value().length()!=args(2).matrix_value().length() && args(2).matrix_value().length()!=1) {
    error("placet_vary_vcorrector: length(arg 3) must be either equal to length(arg 2) or 1");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix corrections(args(2).matrix_value());
  Matrix indexes(args(1).matrix_value());
  bool vector = corrections.length()>1;
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_vary_vcorrector: 'index' must be an integer vector");
      return octave_value();
    }
  }
  try {
    for(int i=0; i<indexes.length(); i++) {
      int index = int(indexes(i));
      if (index<0 || index>=beamline->n_elements) {
        error("placet_vary_vcorrector: invalid index '%d'", index);
        return octave_value();
      } 
      beamline->element[index]->correct_y(corrections(vector?i:0));
    }
  } catch (std::string str ) {
    error("placet_vary_vcorrector: %s", str.c_str());
  }
  return octave_value();
}

DEFUN(placet_element_enable_hcorrector, args, /*nargout*/,
"     Allow to use an element as an horizontal corrector.\n\n\
Usage:\n\n\
     placet_element_enable_hcorrector(\"beamline\", index, \"leverage\", [step_size])\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     index:         index position (or vector of indexes) of the element [INTEGER]\n\
     leverage:      correction leverage name [STRING]\n\
     step_size:     corrector step size [DOUBLE]\n")
{
  if (args.length()<4) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_element_enable_hcorrector: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_element_enable_hcorrector: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  if (!args(2).is_string()) {
    error("placet_element_enable_hcorrector: expecting 'leverage' (arg 3) to be a string");
    return octave_value();
  }
  if (!(args(3).is_real_type() || args(3).is_string())) {
    error("placet_element_enable_hcorrector: option 'step_size' (arg 4) must be a scalar matrix or a string vector");
    return octave_value();
  }
  if (args(1).matrix_value().length()!=args(3).matrix_value().length() && args(3).matrix_value().length()!=1) {
    error("placet_element_enable_hcorrector: length(arg 4) must be either equal to length(arg 2) or 1");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix indexes = args(1).matrix_value();
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_element_enable_hcorrector: 'index' must be an integer vector");
      return octave_value();
    }
  }
  std::string leverage_name = args(2).string_value();
  Matrix values(args(3).matrix_value());
  bool vector = values.length()>1;
  if (vector) {
    if (args(1).vector_value().length() != args(3).vector_value().length()) {
      error("placet_element_enable_hcorrector: 'index' (arg 3) and 'value' (arg 4) must have the same number or elements");
      return octave_value();
    }
  }
  for (int i=0; i<indexes.length(); i++) {
    int index = int(indexes(i));
    if (index<0 || index>=beamline->n_elements) {
      error("placet_element_enable_hcorrector: invalid index '%d'", index);
      return octave_value();
    }
    beamline->element[index]->enable_hcorr(leverage_name.c_str(), values(vector?i:0));
  }
  return octave_value();
}

DEFUN(placet_element_enable_vcorrector, args, /*nargout*/,
"     Allow to use an element as a vertical corrector.\n\n\
Usage:\n\n\
     placet_element_enable_vcorrector(\"beamline\", index, \"leverage\", step_size)\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     index:         index position (or vector of indexes) of the element [INTEGER]\n\
     leverage:      correction leverage name [STRING]\n\
     step_size:     corrector step size [DOUBLE]\n")
{
  if (args.length()<4) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_element_enable_vcorrector: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_element_enable_vcorrector: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  if (!args(2).is_string()) {
    error("placet_element_enable_vcorrector: expecting 'leverage' (arg 3) to be a string");
    return octave_value();
  }
  if (!(args(3).is_real_type() || args(3).is_string())) {
    error("placet_element_enable_vcorrector: option 'step_size' (arg 4) must be a scalar matrix or a string vector");
    return octave_value();
  }
  if (args(1).matrix_value().length()!=args(3).matrix_value().length() && args(3).matrix_value().length()!=1) {
    error("placet_element_enable_vcorrector: length(arg 4) must be either equal to length(arg 2) or 1");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix indexes = args(1).matrix_value();
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_element_enable_vcorrector: 'index' must be an integer vector");
      return octave_value();
    }
  }
  std::string leverage_name = args(2).string_value();
  Matrix values(args(3).matrix_value());
  bool vector = values.length()>1;
  if (vector) {
    if (args(1).vector_value().length() != args(3).vector_value().length()) {
      error("placet_element_enable_vcorrector: 'index' (arg 3) and 'value' (arg 4) must have the same number or elements");
      return octave_value();
    }
  }
  for (int i=0; i<indexes.length(); i++) {
    int index = int(indexes(i));
    if (index<0 || index>=beamline->n_elements) {
      error("placet_element_enable_vcorrector: invalid index '%d'", index);
      return octave_value();
    }
    beamline->element[index]->enable_vcorr(leverage_name.c_str(), values(vector?i:0));
  }
  return octave_value();
}

DEFUN(placet_element_disable_hcorrector, args, /*nargout*/,
"     Disable the use of an element as an horizontal corrector.\n\n\
Usage:\n\n\
     placet_element_disable_hcorrector(\"beamline\", index)\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     index:         index position (or vector of indexes) of the element [INTEGER]\n")
{
  if (args.length()<2) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_element_disable_hcorrector: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_element_disable_hcorrector: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix indexes = args(1).matrix_value();
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_element_disable_hcorrector: 'index' must be an integer vector");
      return octave_value();
    }
  }
  for (int i=0; i<indexes.length(); i++) {
    int index = int(indexes(i));
    beamline->element[index]->disable_hcorr();
  }
  return octave_value();
}

DEFUN(placet_element_disable_vcorrector, args, /*nargout*/,
"     Disable the use of an element as a vertical corrector.\n\n\
Usage:\n\n\
     placet_element_disable_vcorrector(\"beamline\", index)\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     index:         index position (or vector of indexes) of the element [INTEGER]\n")
{
  if (args.length()<2) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_element_disable_vcorrector: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_element_disable_vcorrector: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix indexes = args(1).matrix_value();
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_element_disable_vcorrector: 'index' must be an integer vector");
      return octave_value();
    }
  }
  for (int i=0; i<indexes.length(); i++) {
    int index = int(indexes(i));
    beamline->element[index]->disable_vcorr();
  }
  return octave_value();
}

DEFUN(placet_test_no_correction, args, nargout,
"     Track \"beam\" through \"beamline\" (from 'start' to 'end')\n\n\
Usage:\n\n\
     placet_test_no_correction(\"beamline\", \"beam\", \"survey\")\n\
     placet_test_no_correction(\"beamline\", \"beam\", \"survey\", nmachines)\n\
     placet_test_no_correction(\"beamline\", \"beam\", \"survey\", nmachines, start, end)\n") 
{

  if (args.length()!=3 && args.length()!=4 && args.length()!=6) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_test_no_correction: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  
  if (!args(1).is_string()) {
    error("placet_test_no_correction: expecting 'beam' (arg 2) to be a string");
    return octave_value();
  }
  
  if (!args(2).is_string()) {
    error("placet_test_no_correction: expecting 'survey' (arg 3) to be a string");
    return octave_value();
  }
  
  if (args.length()>=4) {
    if (!args(3).is_real_scalar() || floor(args(3).scalar_value())!=args(3).scalar_value()) {
      error("placet_test_no_correction: parameter 'nmachines' (arg 4) must be an integer");
      return octave_value();
    }
  }
  
  if (args.length()==6) {
    if (!args(4).is_real_scalar() || floor(args(4).scalar_value())!=args(4).scalar_value()) {
      error("placet_test_no_correction: parameter 'start' (arg 5) must be an integer");
      return octave_value();
    }
    
    if (!args(5).is_real_scalar() || floor(args(5).scalar_value())!=args(5).scalar_value()) {
      error("placet_test_no_correction: parameter 'end' (arg 6) must be an integer");
      return octave_value();
    }
  }
  
  std::string beamline_name = args(0).string_value();
  std::string beam_name = args(1).string_value();
  std::string survey_name = args(2).string_value();
  
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
    
  BEAM *beam=get_beam(beam_name.c_str());
  
  void (*survey)(BEAMLINE*) = (void (*)(BEAMLINE *))survey_find(survey_name.c_str());
  if (!survey) {
    error("placet_test_no_correction: survey '%s' not found", survey_name.c_str());
    return octave_value();
  }
  int machines = args.length()>=4 ? args(3).int_value() : 1;
  int start = 0;
  int end = beamline->n_elements;
  if (args.length()==6) {
    start = args(4).int_value();
    end = args(5).int_value() == -1 ? beamline->n_elements : args(5).int_value()+1;
    if (end > beamline->n_elements) {
      error("placet_test_no_correction: parameter 'end' cannot be greater than the number of elements in 'beamline'");
      return octave_value();
    } else if (start >= end) {
      error("placet_test_no_correction: parameter 'start' cannot be greater than parameter 'end'");
      return octave_value();
    }
  }
  return octave_test_no_correction(beamline, beam, machines, survey, start, end, nargout);
}

DEFUN(placet_element_get_attribute, args, /*nargout*/,
"     Get an attribute of an element.\n\n\
Usage:\n\n\
     x = placet_element_get_attribute(\"beamline\", index, \"attribute\")\n\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     index:         index position (or vector of positions) of element [INTEGER]\n\
     attribute:     attribute name [STRING]\n")
{
  if (args.length()<3) {
    print_usage();
    return octave_value();
  }

  if (!args(0).is_string()) {
    error("placet_element_get_attribute: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }

  if (!args(1).is_real_type()) {
    error("placet_element_get_attribute: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }

  if (!args(2).is_string()) {
    error("placet_element_get_attribute: expecting 'attribute' (arg 3) to be a string");
    return octave_value();
  }

  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  
  Matrix indexes = args(1).matrix_value();
  if (indexes.length()==0) {
    error("placet_element_get_attribute: 'index' is empty");
    return octave_value();
  }
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_element_get_attribute: 'index' must be an integer matrix");
      return octave_value();
    }
  }
  
  string_vector attributes(args(2).all_strings());
  bool attribute_name_vector = attributes.length()>1;
  if (attribute_name_vector) {
    if (indexes.length() != attributes.length()) {
      error("placet_element_get_attribute: 'index' (arg 2) and 'attribute' (arg 3) must have the same number or elements");
      return octave_value();
    }
  }

  std::string _attribute_name = args(2).string_value();
  if (_attribute_name == "type_name") {
    string_vector retval(indexes.length());
    for (int i=0; i<indexes.length(); i++) {
      int index = int(indexes(i));
      if (index<0 || index>=beamline->n_elements) {
        error("placet_element_get_attribute: invalid index '%d'", index);
        return octave_value();
      } 
      ELEMENT *element=beamline->element[index];
      if      (element->is_quad()) retval(i)="quadrupole";
      else if (element->is_multipole()) retval(i)="multipole";
      else if (element->is_quadbpm()) retval(i)="quad_bpm";
      else if (element->is_dipole()) retval(i)="dipole";
      else if (element->is_cavity()) retval(i)="cavity";
      else if (element->is_cavity_pets()) retval(i)="cavity_pets";
      else if (element->is_sbend()) retval(i)="sbend";
      else if (element->is_drift()) retval(i)="drift";
      else if (element->is_bpm()) retval(i)="bpm";
      else retval(i)=typeid(*element).name();
    }
    return octave_value(retval);
  } else {
    Matrix retval_real(indexes.rows(), indexes.columns());
    ComplexMatrix retval_complex(indexes.rows(), indexes.columns());
    string_vector retval_string;
    option_type   attribute_type = OPT_NONE;
    for (int i=0; i<indexes.length(); i++) {
      std::string attribute_name = attributes(attribute_name_vector?i:0);
      int index = int(indexes(i));
      if (index<0 || index>=beamline->n_elements) {
        error("placet_element_get_attribute: invalid index '%d'", index);
        return octave_value();
      } 
      option_set &attributes = beamline->element[index]->get_attributes_table();
      if (option_info *attribute = attributes.find(attribute_name.c_str())) {
        attribute_type = attribute->type();
        switch(attribute_type) {
	case OPT_INT:
	  retval_real(i) = attribute->get_int();
          break;
	case OPT_BOOL:
	  retval_real(i) = attribute->get_bool();
          break;
	case OPT_DOUBLE:
	  retval_real(i) = attribute->get_double();
          break;
	case OPT_COMPLEX:
	  retval_complex(i) = attribute->get_complex();
          break;
	case OPT_STRING:
	case OPT_CHAR_PTR:
	  if (i==0) retval_string.resize(indexes.length());
	  retval_string(i) = attribute->get_string();
          break;
	case OPT_NONE: error("attribute type none!"); break;
	default: error("attribute type unknown!"); break;
        }
      } else {
        error("placet_element_get_attribute: unknown attribute `%s' in element %d", attribute_name.c_str(), index);
        return octave_value();
      }
    }
    switch(attribute_type) {
    case OPT_INT:
    case OPT_BOOL:
    case OPT_DOUBLE:  return octave_value(retval_real);
    case OPT_COMPLEX: return octave_value(retval_complex);
    case OPT_CHAR_PTR:
    case OPT_STRING:  return octave_value(retval_string);
    case OPT_NONE: error("attribute type none!"); break;
    default: error("attribute type unknown!"); break;
    }
  }
  return octave_value();
}

DEFUN(placet_element_set_attribute, args, /*nargout*/,
"     Set an attribute of an element.\n\n\
Usage:\n\n\
     x = placet_element_set_attribute(\"beamline\", index, \"attribute\", value)\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     index:         index position (or vector of indexes) of the element [INTEGER]\n\
     attribute:     attribute name [STRING]\n\
     value:         new value (or vector of new values) to set the attribute to [DOUBLE]\n")
{
  if (args.length()<4) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_element_set_attribute: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_element_set_attribute: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  if (!args(2).is_string()) {
    error("placet_element_set_attribute: expecting 'attribute' (arg 3) to be a string");
    return octave_value();
  }
  if (!(args(3).is_real_type() || args(3).is_complex_type() || args(3).is_string())) {
    error("placet_element_set_attribute: option 'value' (arg 4) must be a scalar matrix or a string vector");
    return octave_value();
  }
  int value_length = args(3).is_string() ? args(3).all_strings().length() : args(3).matrix_value().length();
  if (args(1).matrix_value().length()!=value_length && value_length!=1) {
    error("placet_element_set_attribute: length(arg 4) must be either equal to length(arg 2) or 1");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix indexes = args(1).matrix_value();
  if (indexes.length()==0) {
    error("placet_element_set_attribute: 'index' is empty");
    return octave_value();
  }
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_element_set_attribute: 'index' must be an integer vector");
      return octave_value();
    }
  }
  string_vector attributes(args(2).all_strings());
  bool attribute_name_vector = attributes.length()>1;
  if (attribute_name_vector) {
    if (args(1).vector_value().length() != attributes.length()) {
      error("placet_element_set_attribute: 'index' (arg 2) and 'attribute' (arg 3) must have the same number or elements");
      return octave_value();
    }
  }
  if (args(3).is_string()) {
    string_vector values(args(3).all_strings());
    bool vector = values.length()>1;
    if (vector) {
      if (args(1).vector_value().length() != args(3).all_strings().length()) {
	error("placet_element_set_attribute: 'index' (arg 2) and 'value' (arg 4) must have the same number or elements");
	return octave_value();
      }
    }
    for (int i=0; i<indexes.length(); i++) {
      int index = int(indexes(i));
      if (index<0 || index>=beamline->n_elements) {
        error("placet_element_set_attribute: invalid index '%d'", index);
        return octave_value();
      }
      std::string attribute_name = attributes(attribute_name_vector?i:0);
      beamline->element[index]->set_attribute(attribute_name.c_str(), values(vector?i:0));
    }
  } else if (args(3).is_complex_type()) {
    ComplexMatrix values(args(3).complex_matrix_value());
    bool vector = values.length()>1;
    if (vector) {
      if (args(1).vector_value().length() != args(3).vector_value().length()) {
        error("placet_element_set_attribute: 'index' (arg 3) and 'value' (arg 4) must have the same number or elements");
        return octave_value();
      }
    }
    for (int i=0; i<indexes.length(); i++) {
      int index = int(indexes(i));
      if (index<0 || index>=beamline->n_elements) {
        error("placet_element_set_attribute: invalid index '%d'", index);
        return octave_value();
      }
      option_set &attributes_table = beamline->element[index]->get_attributes_table();
      std::string attribute_name = attributes(attribute_name_vector?i:0);
      if (option_info *attribute = attributes_table.find(attribute_name.c_str())) {
        attribute->set(values(vector?i:0));
      }
    }
  } else {
    Matrix values(args(3).matrix_value());
    bool vector = values.length()>1;
    if (vector) {
      if (args(1).vector_value().length() != args(3).vector_value().length()) {
	error("placet_element_set_attribute: 'index' (arg 3) and 'value' (arg 4) must have the same number or elements");
	return octave_value();
      }
    }
    for (int i=0; i<indexes.length(); i++) {
      int index = int(indexes(i));
      if (index<0 || index>=beamline->n_elements) {
        error("placet_element_set_attribute: invalid index '%d'", index);
        return octave_value();
      } 
      option_set &attributes_table = beamline->element[index]->get_attributes_table();
      std::string attribute_name = attributes(attribute_name_vector?i:0);
      if (option_info *attribute = attributes_table.find(attribute_name.c_str())) {
        option_type attribute_type = attribute->type();
	if (attribute_type == OPT_INT) {
	  attribute->set(int(values(vector?i:0)));
	} else if (attribute_type == OPT_BOOL) {
	  attribute->set(bool(values(vector?i:0)==1));
	} else {
	  attribute->set(values(vector?i:0));
	}
      }
    }
  }
  return octave_value();
}

DEFUN(placet_element_get_attributes, args, /*nargout*/,
      "     List the attributes of one or more elements.\n\n\
Usage:\n\n\
     x = placet_element_get_attributes(\"beamline\", [index])\n\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     index:         index position (or vector of indexes) of element [INTEGER]\n")
{
  if (args.length()!=1 && args.length()!=2) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_element_get_attributes: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (args.length()==2 && !args(1).is_real_type()) {
    error("placet_element_get_attributes: parameter 'index' (arg 2) must be an integer type");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value(); 
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix indexes;
  if (args.length()==1) {
    indexes.resize(beamline->n_elements,1);
    for (int i=0;i<beamline->n_elements;i++)
      indexes(i)=i;
  } else {
    indexes=args(1).matrix_value();
    for(int i=0; i<indexes.length(); i++) {
      if (floor(indexes(i))!=indexes(i)) {
	error("placet_element_get_attributes: 'index' must be an integer vector");
	return octave_value();
      }
    }
  }
  Cell result(indexes.length(),1);
  for (int i=0; i<indexes.length(); i++) {
    Octave_map table;
    int index = int(indexes(i));
    if (index<0 || index>=beamline->n_elements) {
      error("placet_element_get_attributes: invalid index '%d'", index);
      return octave_value();
    } 
    ELEMENT *element=beamline->element[index];
    const char *type;
    if      (element->is_quad()) type="quadrupole";
    else if (element->is_multipole()) type="multipole";
    else if (element->is_dipole()) type="dipole";
    else if (element->is_cavity()) type="cavity";
    else if (element->is_sbend()) type="sbend";
    else if (element->is_drift()) type="drift";
    else if (element->is_bpm()) type="bpm";
    else type=typeid(*element).name();
    table.assign("type_name", type);
    const option_set &attributes = element->get_attributes_table();
    for (size_t j=0; j<attributes.size(); j++) {
      const option_info &attribute = attributes[j];
      option_type attribute_type = attribute.type();
      switch (attribute_type) {
      case OPT_INT: table.assign(attribute.get_name(), attribute.get_int()); break;
      case OPT_BOOL: table.assign(attribute.get_name(), attribute.get_bool() ? 1 : 0); break;
      case OPT_DOUBLE: table.assign(attribute.get_name(), attribute.get_double()); break;
      case OPT_COMPLEX: table.assign(attribute.get_name(), attribute.get_complex()); break;
      case OPT_STRING: table.assign(attribute.get_name(), attribute.get_string()); break; 
      case OPT_CHAR_PTR: table.assign(attribute.get_name(), attribute.get_char_ptr()); break;
      case OPT_NONE: error("attribute type none!"); break;
      default: error("attribute type unknown!"); break;
      }
    }
    result(i,0)=table;
  }
  return octave_value(result);
}

DEFUN(placet_get_beam, args, nargout,
      "     Return a beam as a matrix of single particles.\n\n\
Usage:\n\n\
     [B,S] = placet_get_beam()\n\
     [B,S] = placet_get_beam(\"beam\")\n\n\
Input\n\
     beam:          beam name [STRING]\n\n\
Output:\n\
     B:             column-like matrix of particles, each row being\n\
                    [E/GeV, x/um, y/um, z/um, x'/urad, y'/urad]\n\
     S:             longitudinal position of the beam along the beamline\n")
{
  if (args.length()>=2) {
    print_usage();
    return octave_value();
  }
  
  BEAM *beam=NULL;
  if (args.length()==1) {
    if (!args(0).is_string()) {
      error("placet_get_beam: expecting 'beam' (arg 1) to be a string");
      return octave_value();
    }
    std::string beam_name = args(0).string_value();
    beam=get_beam(beam_name.c_str());
  } else {
    beam=inter_data.bunch;
  }
  
  if (!beam) {
    error("placet_get_beam: beam not found");
    return octave_value();
  }

  size_t count=0;
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
    if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon())
      count++;
  }

  Matrix B;
  if (beam->particle_beam) {
    B.resize(count,6);
    for (int i=0,j=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
        B(j,0)=particle.energy;
        B(j,1)=particle.x;
        B(j,2)=particle.y;
        B(j,3)=particle.z;
        B(j,4)=particle.xp;
        B(j,5)=particle.yp;
        j++;
      }
    }
  } else {
    B.resize(count,17);
    int n=beam->slices/beam->bunches/beam->macroparticles;
    int nm=beam->macroparticles;
    int nb=beam->bunches;
    for (int j=0,l=0,m=0;j<nb;j++){
      for (int i=0;i<n;i++){
        for (int k=0;k<nm;k++){
          PARTICLE &particle=beam->particle[m];
          if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
            B(l,0)=beam->z_position[j*n+i];
	    B(l,1)=particle.wgt;
	    B(l,2)=particle.energy;
	    B(l,3)=particle.x;
	    B(l,4)=particle.xp;
	    B(l,5)=particle.y;
	    B(l,6)=particle.yp;
	    B(l,7)=beam->sigma_xx[m].r11;
	    B(l,8)=beam->sigma_xx[m].r12;
	    B(l,9)=beam->sigma_xx[m].r22;
	    B(l,10)=beam->sigma[m].r11;
	    B(l,11)=beam->sigma[m].r12;
	    B(l,12)=beam->sigma[m].r22;
	    B(l,13)=beam->sigma_xy[m].r11;
	    B(l,14)=beam->sigma_xy[m].r12;
	    B(l,15)=beam->sigma_xy[m].r21;
	    B(l,16)=beam->sigma_xy[m].r22;
            l++;
          }
          m++;
        }
      }
    }
  }
  octave_value_list retval;
  retval(0) = octave_value(B);
  if (nargout>1) retval(1) = octave_value(beam->s);
  return retval;
}

DEFUN(placet_set_beam, args, /*nargout*/,
      "     Set a beam from a matrix of single particles or slices.\n\n\
Usage:\n\n\
     B = placet_set_beam(B)\n\
     B = placet_set_beam(\"beam\", B)\n\n\
Input\n\
     beam:          beam name [STRING]\n\n\
     B:             column-like matrix of particles, each row being\n\
                    [E/GeV, x/um, y/um, z/um, x'/urad, y'/urad]\n")
{
  if (args.length()<1||args.length()>2) {
    print_usage();
    return octave_value();
  }
  
  BEAM *beam=NULL;
  Matrix B;
  if (args.length()==1) {
    if (args(0).matrix_value().columns()!=6 && args(0).matrix_value().columns()!=17) {
      error("placet_set_beam: expecting 'B' (arg 1) to be a 6- or 17-columns matrix");
      return octave_value(); 
    }
    B=args(0).matrix_value();
  }
  if (args.length()==2) {
    if (!args(0).is_string()) {
      error("placet_set_beam: expecting 'beam' (arg 1) to be a string");
      return octave_value();
    }
    if (args(1).matrix_value().columns()!=6 && args(1).matrix_value().columns()!=17) {
      error("placet_set_beam: expecting 'B' (arg 2) to be a 6- or 17-columns matrix");
    } 
    std::string beam_name = args(0).string_value();
    beam=get_beam(beam_name.c_str());
    B=args(1).matrix_value();
  } else {
    beam=inter_data.bunch;
  }
  
  if (!beam) {
    error("placet_set_beam: beam not found");
    return octave_value();
  }

  if (B.columns()==6) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.energy=B(i,0);
      particle.x=B(i,1);
      particle.y=B(i,2);
      particle.z=B(i,3);
      particle.xp=B(i,4);
      particle.yp=B(i,5);
    }
  } else {
    int n=beam->slices/beam->bunches/beam->macroparticles;
    int nm=beam->macroparticles;
    int nb=beam->bunches;
    for (int j=0,m=0;j<nb;j++){
      for (int i=0;i<n;i++){
        for (int k=0;k<nm;k++){
          PARTICLE &particle=beam->particle[m];
          beam->z_position[j*n+i]=B(m,0);
	  particle.wgt=B(m,1);
	  particle.energy=B(m,2);
	  particle.x=B(m,3);
	  particle.xp=B(m,4);
	  particle.y=B(m,5);
	  particle.yp=B(m,6);
	  beam->sigma_xx[m].r11=B(m,7);
	  beam->sigma_xx[m].r12=B(m,8);
	  beam->sigma_xx[m].r22=B(m,9);
	  beam->sigma[m].r11=B(m,10);
	  beam->sigma[m].r12=B(m,11);
	  beam->sigma[m].r22=B(m,12);
	  beam->sigma_xy[m].r11=B(m,13);
	  beam->sigma_xy[m].r12=B(m,14);
	  beam->sigma_xy[m].r21=B(m,15);
	  beam->sigma_xy[m].r22=B(m,16);
          m++;
        }
      }
    }
  }
  return octave_value();
}

DEFUN(placet_link_elements, args, /*nargout*/,
      "     Create a link between two elements. 'Linked' elements\n\
     share all properties of the respective 'targets'.\n\n\
Usage:\n\n\
     placet_link_elements(\"beamline\", target, link)\n\n\
Input:\n\
     beamline:      beamline name [STRING]\n\
     target:        index of the target element [INTEGER]\n\
     link:          index (or vector of indexes) of the link [INTEGER]\n")
{
  if (args.length()<3) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_link_elements: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  if (!args(1).is_real_type()) {
    error("placet_link_elements: parameter 'target' (arg 2) must be an integer type");
    return octave_value();
  }
  if (!args(2).is_real_type()) {
    error("placet_link_elements: parameter 'link' (arg 3) must be an integer type");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  Matrix indexes(args(1).matrix_value());
  for(int i=0; i<indexes.length(); i++) {
    if (floor(indexes(i))!=indexes(i)) {
      error("placet_link_elements: 'target' must be an integer vector");
      return octave_value();
    }
  }
  Matrix links(args(2).matrix_value());
  for(int i=0; i<links.length(); i++) {
    if (floor(links(i))!=links(i)) {
      error("placet_link_elements: 'link' must be an integer vector");
      return octave_value();
    }
  }
  if ((indexes.length()!=links.length())&&(indexes.length()!=1||links.length()==1)) {
    error("placet_link_elements: vectors 'target' and 'link' must have the same number of elements; or there must be many links to 1 target");
    return octave_value();
  }
  if (indexes.length()==1) {
    for(int i=0; i<links.length(); i++) {
      int n = int(indexes(0));
      int link_to_n = int(links(i));
      LINK *link = new LINK(beamline->element[n]);
      link->next = beamline->element[link_to_n]->next;
      GIRDER *girder=beamline->first;
      while (girder!=NULL) {
        ELEMENT *element=girder->element();
        while (element!=NULL){
          if (element->next==beamline->element[link_to_n]) {
            element->next=link;
          }
          element=element->next;
        }
        if (girder->element()==beamline->element[link_to_n]) {
          girder->set_first_element(link);
        }
        girder=girder->next();
      }
      delete beamline->element[link_to_n];
    }
  } else {
    for(int i=0; i<indexes.length(); i++) {
      int n = int(indexes(i)), link_to_n = int(links(i));
      LINK *link = new LINK(beamline->element[n]);
      link->next = beamline->element[link_to_n]->next;
      GIRDER *girder=beamline->first;
      while (girder!=NULL) {
        ELEMENT *element=girder->element();
        while (element!=NULL){
          if (element->next==beamline->element[link_to_n]) {
            element->next=link;
          }
          element=element->next;
        }
        if (girder->element()==beamline->element[link_to_n]) {
          girder->set_first_element(link);
        }
        girder=girder->next();
      }
      delete beamline->element[link_to_n];
    }
  }
  beamline_unset(beamline);
  beamline_set(beamline, NULL);
  return octave_value();
}

DEFUN(placet_beamline_refresh, args, /*nargout*/,
      "     Refresh a beamline structure. Useful when\n\
      you change elements' lengths\n\n\
Usage:\n\n\
     placet_beamline_refresh(\"beamline\")\n\n\
Input:\n\
     beamline:      beamline name [STRING]\n")
{
  if (args.length()!=1) {
    print_usage();
    return octave_value();
  }
  if (!args(0).is_string()) {
    error("placet_beamline_refresh: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  beamline_unset(beamline);
  beamline_set(beamline, NULL);
  return octave_value();
}

DEFUN(placet_get_response_matrix_fit, args, nargout,
"     Return the response matrices of 'beamline' to 'beam', using\n\
     selected 'bpms' and 'correctors.\n\n\
Usage:\n\n\
     [Rxx, Rxy, Ryx, Ryy, Rxxx, Rxxy, Rxyy, Ryxx, Ryyx, Ryyy] =\n\
      = placet_get_response_matrix_fit(\"beamline\", \"beam\", \"survey\", bpms, correctors )\n") 
{
  if (args.length()!=5) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_get_response_matrix_fit: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  
  if (!args(1).is_string()) {
    error("placet_get_response_matrix_fit: expecting 'beam' (arg 2) to be a string");
    return octave_value();
  }

  if (!args(2).is_string()) {
    error("placet_get_response_matrix_fit: expecting 'survey' (arg 3) to be a string");
    return octave_value();
  }
  
  if (!args(3).is_real_type()) {
    error("placet_get_response_matrix_fit: parameter 'bpms' (arg 4) must be an integer vector");
    return octave_value();
  }

  if (!args(4).is_real_type()) {
    error("placet_get_response_matrix_fit: parameter 'correctors' (arg 5) must be an integer vector");
    return octave_value();
  }

  std::string beamline_name = args(0).string_value();
  std::string beam_name = args(1).string_value();
  std::string survey_name = args(2).string_value();
  
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;
  
  BEAM *bunch=get_beam(beam_name.c_str());
  if (!bunch) bunch=inter_data.bunch;
  
  void (*survey)(BEAMLINE*) = (void (*)(BEAMLINE *))survey_find(survey_name.c_str());
  if (!survey) {
    error("placet_get_response_matrix_fit: survey '%s' not found", survey_name.c_str());
    return octave_value();
  }
  
  RowVector _bpms(args(3).vector_value());
  for(int i=0; i<_bpms.length(); i++) {
    if (floor(_bpms(i))!=_bpms(i)) {
      error("placet_get_response_matrix_fit: 'bpms' must be a vector of integer numbers");
      return octave_value();
    }
  }
  RowVector _correctors(args(4).vector_value());
  for(int i=0; i<_correctors.length(); i++) {
    if (floor(_correctors(i))!=_correctors(i)) {
      error("placet_get_response_matrix_fit: 'correctors' must be a vector of integer numbers");
      return octave_value();
    }
  }
  octave_value_list ret_value;
  if (int *Bpms = new int [ _bpms.length() ]) {
    if (int *Corrs = new int [ _correctors.length() ]) {
      for(int i=0; i<_bpms.length(); i++) Bpms[i] = int(_bpms(i));
      for(int i=0; i<_correctors.length(); i++) Corrs[i] = int(_correctors(i));
      ret_value = octave_get_response_matrix_fit(beamline, bunch, survey, Bpms, Corrs, _bpms.length(), _correctors.length(), nargout);
      delete []Corrs; 
    }
    delete []Bpms; 
  }
  return ret_value;
}

DEFUN(placet_get_transfer_matrix_fit, args, nargout,
"     Return the 1st and 2nd order transfer matrices from element 'start' to 'end',\n\
     fitting the beam 'beam' and using survey 'survey'.\n\n\
Usage:\n\n\
     [R, T] = placet_get_transfer_matrix_fit(\"beamline\", \"beam\", \"survey\", [start, end])\n\n\
Input\n\
     beamline:      beamline name [STRING]\n\
     beam:          beam name [STRING]\n\
     survey:        survey name [STRING]\n\
     start:         element to start from [INTEGER]\n\
     end:           element to end to (included) [INTEGER]\n\n\
Output:\n\
     R:             R transfer matrix 6x6\n\
     T:             T transfer matrix 6x6x6\n")
{
  if (args.length()<3) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_get_transfer_matrix_fit: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }

  if (!args(1).is_string()) {
    error("placet_get_transfer_matrix_fit: expecting 'beam' (arg 2) to be a string");
    return octave_value();
  }

  if (!args(2).is_string()) {
    error("placet_get_transfer_matrix_fit: expecting 'survey' (arg 3) to be a string");
    return octave_value();
  }
  
  if (args.length()>3) {
    if (!args(3).is_real_type()) {
      error("placet_get_transfer_matrix_fit: parameter 'start' (arg 4) must be an integer type");
      return octave_value();
    }
  }

  if (args.length()>4) {
    if (!args(4).is_real_type()) {
      error("placet_get_transfer_matrix_fit: parameter 'end' (arg 5) must be an integer type");
      return octave_value();
    }
  }

  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;

  std::string beam_name = args(1).string_value();    
  BEAM *bunch=get_beam(beam_name.c_str());
  if (!bunch) bunch=inter_data.bunch;

  std::string survey_name = args(2).string_value();  
  void (*survey)(BEAMLINE*) = (void (*)(BEAMLINE *))survey_find(survey_name.c_str());
  if (!survey) {
    error("placet_get_transfer_matrix_fit: survey '%s' not found", survey_name.c_str());
    return octave_value();
  }

  int start=0;
  int end=-1;

  if (args.length()>3)
    start=int(args(3).scalar_value());
    
  if (args.length()>4)
    end=int(args(4).scalar_value());
   
  if (end==-1 || end>=beamline->n_elements)
    end=beamline->n_elements-1;

  if (start > end) {
    error("placet_get_transfer_matrix_fit: parameter 'start' (arg 4) must be smaller or equal to paramter 'end' (arg 5)");
    return octave_value();
  }

  return octave_get_transfer_matrix_fit(beamline, bunch, survey, start, end, nargout);
}

DEFUN(placet_get_transfer_matrix, args, ,
"     Return the R matrix from element 'start' to 'end'.\n\n\
Usage:\n\n\
     I = placet_get_transfer_matrix(\"beamline\", [start, end])\n\n\
Input\n\
     beamline:      beamline name [STRING]\n\
     start:         element to start from [INTEGER]\n\
     end:           element to end to (included) [INTEGER]\n\n\
Output:\n\
     I:             R transfer matrix 6x6 \n")
{
  if (args.length()<1) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_get_transfer_matrix: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  
  if (args.length()>1) {
    if (!args(1).is_real_type()) {
      error("placet_get_transfer_matrix: parameter 'start' (arg 2) must be an integer type");
      return octave_value();
    }
  }

  if (args.length()>2) {
    if (!args(2).is_real_type()) {
      error("placet_get_transfer_matrix: parameter 'end' (arg 3) must be an integer type");
      return octave_value();
    }
  }

  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;

  int start=0;
  int end=-1;

  if (args.length()>1)
    start=int(args(1).scalar_value());
    
  if (args.length()>2)
    end=int(args(2).scalar_value());
   
  if (end==-1 || end>=beamline->n_elements)
    end=beamline->n_elements-1;

  if (start > end) {
    error("placet_get_transfer_matrix: parameter 'start' (arg 2) must be smaller or equal to paramter 'end' (arg 3)");
    return octave_value();
  }
  
  Andrea::Matrix<6,6> R = Andrea::Identity<6,6>();
  for (int i=start; i<=end; i++) {
    ELEMENT *element = beamline->element[i];
    R = element->get_transfer_matrix_6d() * R;
  }
  
  Matrix m(6,6);
  for (int i=0; i<6; i++) {
    for (int j=0; j<6; j++) {
      m(i,j)=R[i][j];
    }
  }
  return octave_value(m);
}

DEFUN(placet_girder_get_length, args, ,
"     Return a column-vector of girder lengths.\n\n\
Usage:\n\n\
     L = placet_girder_get_length(\"beamline\")\n\n\
Input\n\
     beamline:      beamline name [STRING]\n\n\
Output:\n\
     L:             column-vector of lengths\n")
{
  if (args.length()!=1) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_girder_get_length: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }
  
  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;

  size_t count=0;
  for (GIRDER *girder=beamline->first; girder; count++,girder=girder->next()){};
  
  ColumnVector V(count);

  count=0;
  for (GIRDER *girder=beamline->first; girder; girder=girder->next())
    V(count++) = girder->get_length()+girder->distance_to_prev_girder();

  return octave_value(V);
}

DEFUN(placet_girder_move, args, ,
"     Move the girders of a beamline.\n\n\
Usage:\n\n\
     placet_girder_move(\"beamline\", Delta)\n\n\
Input\n\
     beamline:      beamline name [STRING]\n\
     Delta:         4xN matrix with displacements [ dx1, dy1, dx2, dy2; ... ]\n")
{
  if (args.length()!=2) {
    print_usage();
    return octave_value();
  }
  
  if (!args(0).is_string()) {
    error("placet_girder_move: expecting 'beamline' (arg 1) to be a string");
    return octave_value();
  }

  if (args(1).matrix_value().columns()!=4) {
    error("placet_girder_move: expecting 'Delta' (arg 2) to be a 4-columns matrix");
    return octave_value();
  } 

  std::string beamline_name = args(0).string_value();
  BEAMLINE *beamline = get_beamline(beamline_name.c_str());
  if (!beamline) beamline = inter_data.beamline;

  Matrix D = args(1).matrix_value();

  int index=0;
  for (GIRDER *girder=beamline->first; index<D.rows() && girder; index++, girder=girder->next())
    girder->move_ends(D(index,0), D(index,2), D(index,1), D(index,3));

  return octave_value();
}
