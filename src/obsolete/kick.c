#include <cstdlib>
#include <cstdio>
#include <cmath>

#include "spline.h"
#include "errf.c"

#include <gsl/gsl_integration.h>

struct {
  SPLINE *spline;
} spline_integrate_data;

static double spline_integrate_f(double x, void * params )
{
  return spline_int(spline_integrate_data.spline,x);
}

double spline_integrate(SPLINE *spline,double a,double b)
{
	double result;

	if (gsl_integration_workspace *w = gsl_integration_workspace_alloc (1000))
	{
		spline_integrate_data.spline=spline;

		gsl_function F;

		F.function = &spline_integrate_f;
		F.params = NULL;

		double error;

		gsl_integration_qag(&F, a, b, 0, 1e-6, 1000, GSL_INTEG_GAUSS41, w, &result, &error); 
		gsl_integration_workspace_free(w);
	}
	
	return result;
}

/* calculates the longitudinal energy distribution */

struct{
  SPLINE *green,*charge;
  double z;
} fold_beamload_data;

static double beamloading_f(double x, void *params )
{
  if (x>fold_beamload_data.z) return 0.0;
  return spline_int(fold_beamload_data.green,fold_beamload_data.z-x)
    *spline_int(fold_beamload_data.charge,x);
}

double beamloading(SPLINE *green,SPLINE *charge,double a,double z)
{
	double result;

	if (gsl_integration_workspace *w = gsl_integration_workspace_alloc (1000))
	{
		fold_beamload_data.green=green;
		fold_beamload_data.charge=charge;
		fold_beamload_data.z=z;

		gsl_function F;

		F.function = &beamloading_f;
		F.params = NULL;

		double error;

		gsl_integration_qag(&F, a, b, 0, 1e-6, 1000, GSL_INTEG_GAUSS41, w, &result, &error); 
		gsl_integration_workspace_free(w);
	}
	
	return result;
}

struct{
  SPLINE *green,*charge;
  double z;
} fold_beamkick_data;

double
beamkick_f(double x)
{
  if (x>fold_beamkick_data.z) return 0.0;
  return spline_int(fold_beamkick_data.green,fold_beamkick_data.z-x)
    *spline_int(fold_beamkick_data.charge,x);
}

/*
double
beamkick(SPLINE *green,SPLINE *charge,double a,double b,double z)
{
  double res;
  fold_beamkick_data.green=green;
  fold_beamkick_data.charge=charge;
  fold_beamkick_data.z=z;
  qromb(&beamkick_f,a,b,&res);
  return res;
}
*/

double
beamkick(SPLINE *green,SPLINE *charge,double a,double b,double z)
{
  return spline_int(green,z-0.5*(a+b));
}

double
f_l(double x)
{
  double s;
  s=x*(0.625/2.998)*1e-3*1.5;
  return -pow(0.625/2.998*1.5,2)*1.6e-19*1e12*1e-6*250.0*exp(-0.85*sqrt(s));
}

double
f_t(double x)
{
  double s;
  s=x*(0.625/2.998*1.5)*1e-3;
  return pow(0.625/2.998*1.5,3)*1.6e-19*1e12*1e-6*1e3
    *5.45*(1.0-exp(-1.13*sqrt(s))*(1.0+1.13*sqrt(s)));
}

/*
  fills the array wk with the longitudinal beamloading and the array
  ch with the charge distribution
*/

wakefield_longitudinal_fill(SPLINE *sp_c,SPLINE *sp_l,double *wk,double *ch,
			    double a,double b,int n)
{
  int i,j;
  double dz,z;
  dz=fabs(a-b)/(double)n;
  z=a;
  for (i=0;i<n;i++){
    ch[i]=spline_integrate(sp_c,z,z+dz);
    z+=dz;
  }
  z=a+0.5*dz;
  for (i=0;i<n;i++){
    *wk++=beamloading(sp_l,sp_c,a,z);
    z+=dz;
  }
}

/*
  fills the array wk with the transverse wakefield
*/

wakefield_transverse_fill(SPLINE *sp_t,double *wk,double a,double b,int n)
{
  int i,j;
  double dz;
  dz=fabs(a-b)/(double)n;
  for (i=0;i<n;i++){
    for (j=0;j<i;j++){
      *wk++=spline_int(sp_t,dz*(i-j));
    }
    *wk++=0.0;
  }
}

main()
{
  SPLINE *sp_t,*sp_l,*sp_c;
  double x[1000],x2[1000],y[1000],z[1000],c[1000],s,ds,s2;
  int i,j,k=0;
  const double sigma_z=1500.0,cut_z=3.0;
  double wl[21],ch[21],wt[400];

  for (i=0;i<1000;i++){
    s=(double)(i-500)*10.0;
    x[i]=s;
    x2[i]=(double)i*10.0;
    y[i]=f_l(x2[i]);
    z[i]=f_t(x2[i]);
    c[i]=gauss(s/sigma_z)/sigma_z;
  }
  sp_l=spline_make(x2,0,y,0,1000);
  sp_t=spline_make(x2,0,z,0,1000);
  sp_c=spline_make(x,0,c,0,1000);
  wakefield_longitudinal_fill(sp_c,sp_l,wl,ch,-cut_z*sigma_z,cut_z*sigma_z,21);
  wakefield_transverse_fill(sp_t,wt,-cut_z*sigma_z,cut_z*sigma_z,21);
  ds=2.0*sigma_z*cut_z/21.0;
  s=-sigma_z*cut_z+0.5*ds;
  for (i=0;i<21;i++){
    printf("%g %g %g %g\n",-s,2e10*beamloading(sp_l,sp_c,-cut_z*sigma_z,s),
	   2e10*wl[i],ch[i]);
    s+=ds;
  }
  s=-sigma_z*cut_z+0.5*ds;
  k=0;
  for (i=0;i<21;i++){
    s2=-sigma_z*cut_z;
    for(j=0;j<i;j++){
      printf("%g %g\n",2e10*beamkick(sp_t,sp_c,s2,s2+ds,s),2e10*wt[k++]);
      s2+=ds;
    }
    printf("%g %g\n",0.0,wt[k++]);
    s+=ds;
  }
}
