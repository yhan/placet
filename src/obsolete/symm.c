#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "matrix.h" 

main()
{
  R_MATRIX rf,rd,rc,rk,rs;

  rf.r11=1.0;
  rf.r12=0.0;
  rf.r21=-0.7;
  rf.r22=1.0;

  rd.r11=1.0;
  rd.r12=0.0;
  rd.r21=0.35;
  rd.r22=1.0;

  rc.r11=1.0;
  rc.r12=1.0;
  rc.r21=0.0;
  rc.r22=1.0;

  rk.r11=1.0;
  rk.r12=0.0;
  rk.r21=-0.01;
  rk.r22=1.0;

  mult_M_M(&rk,&rc,&rs);
  mult_M_M(&rc,&rs,&rc);

  mult_M_M(&rc,&rd,&rs);
  mult_M_M(&rf,&rs,&rs);
  mult_M_M(&rc,&rs,&rs);
  mult_M_M(&rd,&rs,&rs);
  mult_M_M(&rs,&rs,&rs);
  printf("%g\n",determinante_M(&rs));
  print_M(&rs);
}
