double
dec_field(double t[],double w[],double f[],int n,
	  double k,double beta,double l, double lambda, double Q_long)
{
  int i,j=0;
  double max=0.0;
  for(i=0;i<n-1;i++){
    f[i]=f_new_point(t,w,&j,n,t[i],k,beta,l, lambda, Q_long);
    if (f[i]>max) {
      max=f[i];
    }
  }
  return max;
}

double
f_new_point(BEAM* beam,int *i,double t0,
	    double k,double beta,double l, double /*lambda*/, double /*Q_long*/)
{
  int j,n;
  double sum=0.0,l_eff;


  n=beam->slices;
  t0*=1e6;l*=1e6;k*=1e-6;
  while ((l<(t0-beam->z_position[*i])/(1.0-beta)*beta)&&((*i)<n)) {
    (*i)++;
  }
  j=(*i);
  while ((j<n)&&(t0-beam->z_position[j]>0.0)) {
    l_eff=l-beta*(t0-beam->z_position[j])/(1.0-beta);
    sum+=cos(k*(t0-beam->z_position[j]))*beam->particle[j].wgt*l_eff;
    j++;
  }
  if (t0==beam->z_position[j]) {
    l_eff=0.5*l;
    sum+=cos(k*(t0-beam->z_position[j]))*beam->particle[j].wgt*l_eff;
  }
  return sum*1e-6;
}
