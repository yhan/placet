#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "fftw.h"

#define TWOPI 6.283185307179586

#define Noff 500
#define Npoint 600
#define M 1

main()
{
  char buffer[1000],*point;
  FILE *f;
  int i,m=0,j;
  double dummy,wgt[M],y2,yp2,yyp;
  fftw_complex *in,*in2;
  fftw_plan p,p2;

  in=(fftw_complex*)malloc(sizeof(fftw_complex)*M*Npoint);
  p = fftw_create_plan_specific(Npoint,FFTW_FORWARD,
				FFTW_ESTIMATE | FFTW_IN_PLACE,
				in,M,NULL,M);
  in2=(fftw_complex*)malloc(sizeof(fftw_complex)*M*Npoint);
  p2 = fftw_create_plan_specific(Npoint,FFTW_FORWARD,
				FFTW_ESTIMATE | FFTW_IN_PLACE,
				in2,M,NULL,M);
  for (i=Noff;i<Noff+Npoint;i++){
    snprintf(buffer,1000,"b_%d\0",i);
    f=fopen(buffer,"r");
    for (j=0;j<M;j++){
      point=fgets(buffer,1000,f);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      wgt[j]=dummy;
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      in[m].re=dummy;
      in[m].im=0.0;
      dummy=strtod(point,&point);
      in2[m].re=dummy;
      in2[m].im=0.0;
      printf("%g %g %g\n",wgt[j],in[m].re,in2[m].re);
      m++;
    }
    fclose(f);
  }
  fftw(p,M,in,M,1,NULL,1,1);
  fftw(p2,M,in2,M,1,NULL,1,1);
  for (i=0;i<Npoint;i++){
      y2=0.0;
      yp2=0.0;
      yyp=0.0;
      for (j=0;j<M;j++){
	  y2+=in[i*M+j].re*in[i*M+j].re*wgt[j];
	  yp2+=in2[i*M+j].re*in2[i*M+j].re*wgt[j];
	  y2+=in[i*M+j].re*in2[i*M+j].re*wgt[j];
      }
      printf("%d %g %g %g %g\n",i,(y2*yp2-yyp*yyp),y2,yp2,yyp);
  }
}



