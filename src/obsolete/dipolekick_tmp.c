
extern struct{
  double beta_group_l,beta_group_t,a0,l_cav,lambda_transverse,
    lambda,gradient,q_value,shift,band;
  int steps;
  WAKE_DATA *wake;
} injector_data;

/* applies the transverse wakefield kick */

void
xdipole_kick(ELEMENT *element,BEAM *beam,double length)
{
  double tmp2,factor,x,y,half_length;
  double kick_y,y_off,tmpy,kick_x;
  static int step=0;
  int i,j,k,m,n,j0,m2,nmacro,nf,nadd,ks;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,multi_y,multi_x,
    *rho_a,*rho_b,multi_y_b,pos_yb,pos_xb,longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  half_length=0.5*length;
  
  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  for (step=0;step<injector_data.steps;step++){
    rho_y=beam->rho_y[0];
    multi_y=0.0;
    multi_y_b=0.0;
#ifdef TWODIM
    rho_x=beam->rho_x[0];
    multi_x=0.0;
    multi_x_b=0.0;
#endif
    /* set pointer af and bf to the appropriate cell number */
    
    af=beam->drive_data->af+step*beam->bunches*beam->slices_per_bunch;
    bf=beam->drive_data->bf+step*beam->bunches*beam->slices_per_bunch;
    along=beam->drive_data->along+step*beam->bunches;
    longfact=along[0];
  
    /* loop over all bunches */
  
    for (k=0;k<beam->bunches;k++){
      
      /* apply attenuation */
      
      if (k>0){
	multi_y*=along[k];
	multi_y_b*=along[k];
#ifdef TWODIM
	multi_x*=along[k];
	multi_x_b*=along[k];
#endif
      }
    
      m=k*beam->slices_per_bunch;
      pos_y=0.0;
      pos_yb=0.0;
#ifdef TWODIM
      pos_x=0.0;
      pos_xb=0.0;
#endif
      for (j=0;j<beam->slices_per_bunch;j++){
	rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
#ifdef TWODIM
	rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
#endif
	s=af[m+j];  /*sin(beam->z_position[m+j]);*/
	c=bf[m+j];  /*cos(beam->z_position[m+j]);*/
      /*
	s=sin(beam->z_position[m+j]*TWOPI*1e-5);
	c=cos(beam->z_position[m+j]*TWOPI*1e-5);
      */
	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
#endif
	beam->particle[m+j].yp+=(multi_y*s+multi_y_b*c)*longfact
	  *factor/beam->particle[m+j].energy;
#ifdef TWODIM
	beam->particle[m+j].xp+=(multi_x*s+multi_x_b*c)*longfact
	  *factor/beam->particle[m+j].energy;
#endif
      }
      multi_y+=pos_y;
      multi_y_b+=pos_yb;
#ifdef TWODIM
      multi_x+=pos_x;
      multi_x_b+=pos_xb;
#endif
    }
  }
  /*
    Single bunch contribution
  */
  
  for (k=0;k<beam->bunches;k++){
    m=k*beam->slices_per_bunch;
    p=beam->field->kick;
    for (j=0;j<beam->slices_per_bunch;j++){
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
#ifdef TWODIM
      rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      beam->particle[m+j].yp+=sumy*factor/beam->particle[m+j].energy;
#ifdef TWODIM
      beam->particle[m+j].xp+=sumx*factor/beam->particle[m+j].energy;
#endif
    }
  }
}

void
tdipole_kick(ELEMENT *element,BEAM *beam,double length)
{
  double tmp2,factor,x,y,half_length;
  double kick_y,y_off,tmpy,kick_x;
  static int step=0;
  int i,j,k,m,n,j0,m2,nmacro,nf,nadd,ks;
  double rndm_scale;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,multi_y,multi_x,
    *rho_a,*rho_b,multi_y_b,pos_yb,pos_xb,longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  half_length=0.5*length;
  
  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  for (step=0;step<injector_data.steps;step++){
    rndm_scale=element->v_tesla;
    rho_y=beam->rho_y[0];
    multi_y=0.0;
    multi_y_b=0.0;
#ifdef TWODIM
    rho_x=beam->rho_x[0];
    multi_x=0.0;
    multi_x_b=0.0;
#endif
    /* set pointer af and bf to the appropriate cell number */
    
    af=beam->drive_data->af+step*beam->bunches*beam->slices_per_bunch;
    bf=beam->drive_data->bf+step*beam->bunches*beam->slices_per_bunch;
    along=beam->drive_data->along+step*beam->bunches;
    longfact=along[0];
  
    /* loop over all bunches */
  
    for (k=0;k<beam->bunches;k++){
      
      /* apply attenuation */
      
      if (k>0){
	multi_y*=along[k];
	multi_y_b*=along[k];
#ifdef TWODIM
	multi_x*=along[k];
	multi_x_b*=along[k];
#endif
      }
    
      m=k*beam->slices_per_bunch;
      pos_y=0.0;
      pos_yb=0.0;
#ifdef TWODIM
      pos_x=0.0;
      pos_xb=0.0;
#endif
      for (j=0;j<beam->slices_per_bunch;j++){
	rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
#ifdef TWODIM
	rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
#endif
	s=sin(rndm_scale*beam->z_position[m+j]
	      *TWOPI*1e-6/injector_data.wake->lambda[step]);
	c=cos(rndm_scale*beam->z_position[m+j]
	      *TWOPI*1e-6/injector_data.wake->lambda[step]);

	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
#endif
	beam->particle[m+j].yp+=(multi_y*s+multi_y_b*c)*longfact
	  *factor/beam->particle[m+j].energy;
#ifdef TWODIM
	beam->particle[m+j].xp+=(multi_x*s+multi_x_b*c)*longfact
	  *factor/beam->particle[m+j].energy;
#endif
      }
      multi_y+=pos_y;
      multi_y_b+=pos_yb;
#ifdef TWODIM
      multi_x+=pos_x;
      multi_x_b+=pos_xb;
#endif
    }
  }
  /*
    Single bunch contribution
  */
  
  for (k=0;k<beam->bunches;k++){
    m=k*beam->slices_per_bunch;
    p=beam->field->kick;
    for (j=0;j<beam->slices_per_bunch;j++){
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
#ifdef TWODIM
      rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      beam->particle[m+j].yp+=sumy*factor/beam->particle[m+j].energy;
#ifdef TWODIM
      beam->particle[m+j].xp+=sumx*factor/beam->particle[m+j].energy;
#endif
    }
  }
}

void
dipole_kick_n_old(ELEMENT *element,BEAM *beam,double length)
{
  double tmp,factor,x,y,half_length;
  double kick_y,y_off,tmpy,kick_x;
  static int step=0;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,multi_y,multi_x,
    *rho_a,*rho_b,multi_y_b,pos_yb,pos_xb,longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  n_macro=beam->macroparticles;

  half_length=0.5*length;
  
  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  for (step=0;step<injector_data.steps;step++){
    //    rndm_scale=1.0+0.001*gasdev();
    rndm_scale=element->v_tesla;
    rho_y=beam->rho_y[0];
    multi_y=0.0;
    multi_y_b=0.0;
#ifdef TWODIM
    rho_x=beam->rho_x[0];
    multi_x=0.0;
    multi_x_b=0.0;
#endif
    /* set pointer af and bf to the appropriate cell number */
    
    af=beam->drive_data->af+step*beam->bunches*beam->slices_per_bunch;
    bf=beam->drive_data->bf+step*beam->bunches*beam->slices_per_bunch;
    along=beam->drive_data->along+step*beam->bunches;
    longfact=along[0];
  
    /* loop over all bunches */
  
    for (k=0;k<beam->bunches;k++){
      
      /* apply attenuation */
      
      if (k>0){
	multi_y*=along[k];
	multi_y_b*=along[k];
#ifdef TWODIM
	multi_x*=along[k];
	multi_x_b*=along[k];
#endif
      }
    
      m=k*beam->slices_per_bunch;
      pos_y=0.0;
      pos_yb=0.0;
#ifdef TWODIM
      pos_x=0.0;
      pos_xb=0.0;
#endif
      s=sin(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      c=cos(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      ds=sin(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      dc=cos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      for (j=0;j<beam->slices_per_bunch;j++){
	  rho_y[j]=0.0;
#ifdef TWODIM
	  rho_x[j]=0.0;
#endif
	for (i_m=0;i_m<n_macro;i_m++){
	  rho_y[j]+=beam->particle[(m+j)*n_macro+i_m].y
	    *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef TWODIM
	  rho_x[j]+=beam->particle[(m+j)*n_macro+i_m].x
	    *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
	}
	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
#endif
	for (i_m=0;i_m<n_macro;i_m++){
	  beam->particle[(m+j)*n_macro+i_m].yp+=
	    (multi_y*s+multi_y_b*c)*longfact
	    *factor/beam->particle[m+j].energy;
#ifdef TWODIM
	  beam->particle[(m+j)*n_macro+i_m].xp+=
	    (multi_x*s+multi_x_b*c)*longfact
	    *factor/beam->particle[m+j].energy;
#endif
	}
	tmp=dc*c-ds*s;
	s=ds*c+dc*s;
	c=tmp;
      }
      multi_y+=pos_y;
      multi_y_b+=pos_yb;
#ifdef TWODIM
      multi_x+=pos_x;
      multi_x_b+=pos_xb;
#endif
    }
  }
    /*
      Single bunch contribution
    */

  for (k=0;k<beam->bunches;k++){
    m=k*beam->slices_per_bunch;
    p=beam->field->kick;
    for (j=0;j<beam->slices_per_bunch;j++){
      rho_y[j]=0.0;
#ifdef TWODIM
      rho_x[j]=0.0;
#endif
      for (i_m=0;i_m<n_macro;i_m++) {
	rho_y[j]+=beam->particle[(m+j)*n_macro+i_m].y
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef TWODIM
	rho_x[j]+=beam->particle[(m+j)*n_macro+i_m].x
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
      }
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      for (i_m=0;i_m<n_macro;i_m++) {
	beam->particle[(m+j)*n_macro+i_m].yp+=
	  sumy*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#ifdef TWODIM
	beam->particle[(m+j)*n_macro+i_m].xp+=
	  sumx*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#endif
      }
    }
  }
}

void
dipole_kick_n(ELEMENT *element,BEAM *beam,double length)
{
  double tmp,factor,x,y,half_length;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,multi_y[injector_data.steps],multi_x[injector_data.steps],
    *rho_a,*rho_b,multi_y_b[injector_data.steps],pos_yb,pos_xb,longfact[injector_data.steps],s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,
    multi_x_b[injector_data.steps],tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  PARTICLE *particle;
  
  n_macro=beam->macroparticles;
  particle=beam->particle;
  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  half_length=0.5*length;
  
  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  rndm_scale=element->v_tesla;
  rho_y=beam->rho_y[0];
#ifdef TWODIM
  rho_x=beam->rho_x[0];
#endif
  for (step=0;step<injector_data.steps;step++){
    multi_y[step]=0.0;
    multi_y_b[step]=0.0;
#ifdef TWODIM
    multi_x[step]=0.0;
    multi_x_b[step]=0.0;
#endif
    longfact[step]=beam->drive_data->along[step*beam->bunches];
  }
  
  /* loop over all bunches */
  
  for (k=0;k<beam->bunches;k++){
    
    /* apply attenuation */
    
    if (k>0){
      for (step=0;step<injector_data.steps;step++){
	multi_y[step]*=beam->drive_data->along[step*beam->bunches+k];
	multi_y_b[step]*=beam->drive_data->along[step*beam->bunches+k];
#ifdef TWODIM
	multi_x[step]*=beam->drive_data->along[step*beam->bunches+k];
	multi_x_b[step]*=beam->drive_data->along[step*beam->bunches+k];
#endif
      }
    }
    
    particle=beam->particle+k*beam->slices_per_bunch*n_macro;
    p=beam->field->kick;
    for (j=0;j<beam->slices_per_bunch;j++){
      rho_y[j]=0.0;
#ifdef TWODIM
      rho_x[j]=0.0;
#endif
      for (i_m=0;i_m<n_macro;i_m++){
#ifdef TWODIM
	rho_x[j]+=particle->x*particle->wgt;
#endif
	rho_y[j]+=particle->y*particle->wgt;
	particle++;
      }
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      kick_y[j]=sumy;
#ifdef TWODIM
      kick_x[j]=sumx;
#endif
    }
    m=k*beam->slices_per_bunch;
    for (step=0;step<injector_data.steps;step++) {
      pos_y=0.0;
      pos_yb=0.0;
#ifdef TWODIM
      pos_x=0.0;
      pos_xb=0.0;
#endif
      s=sin(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      c=cos(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      ds=sin(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	     *TWOPI*1e-6/injector_data.wake->lambda[step]);
      dc=cos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	     *TWOPI*1e-6/injector_data.wake->lambda[step]);
      for (j=0;j<beam->slices_per_bunch;j++){
	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
#endif
	kick_y[j]+=(multi_y[step]*s+multi_y_b[step]*c)*longfact[step];
#ifdef TWODIM
	kick_x[j]+=(multi_x[step]*s+multi_x_b[step]*c)*longfact[step];
#endif
	tmp=dc*c-ds*s;
	s=ds*c+dc*s;
	c=tmp;
      }
      multi_y[step]+=pos_y;
      multi_y_b[step]+=pos_yb;
#ifdef TWODIM
      multi_x[step]+=pos_x;
      multi_x_b[step]+=pos_xb;
#endif
    }
    particle=beam->particle+k*beam->slices_per_bunch*n_macro;
    for (j=0;j<beam->slices_per_bunch;j++) {
      for (i_m=0;i_m<n_macro;i_m++){
#ifdef TWODIM
	particle->xp+=kick_x[j]*factor/particle->energy;
#endif
	particle->yp+=kick_y[j]*factor/particle->energy;
	particle++;
      }
    }
  }
}

void
dipole_kick(ELEMENT *element,BEAM *beam,double length)
{
  double tmp,factor,x,y,half_length;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,multi_y[injector_data.steps],multi_x[injector_data.steps],
    *rho_a,*rho_b,multi_y_b[injector_data.steps],pos_yb,pos_xb,longfact[injector_data.steps],s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,
    multi_x_b[injector_data.steps],tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  n_macro=beam->macroparticles;
  if (n_macro>1) {
      dipole_kick_n(element,beam,length);
      return;
  }

  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  half_length=0.5*length;
  
  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  rndm_scale=element->v_tesla;
  rho_y=beam->rho_y[0];
#ifdef TWODIM
  rho_x=beam->rho_x[0];
#endif
  for (step=0;step<injector_data.steps;step++){
    multi_y[step]=0.0;
    multi_y_b[step]=0.0;
#ifdef TWODIM
    multi_x[step]=0.0;
    multi_x_b[step]=0.0;
#endif
    longfact[step]=beam->drive_data->along[step*beam->bunches];
  }
  
  /* loop over all bunches */
  
  for (k=0;k<beam->bunches;k++){
    
    /* apply attenuation */
    
      if (k>0){
	  for (step=0;step<injector_data.steps;step++){
	      multi_y[step]*=beam->drive_data->along[step*beam->bunches+k];
	      multi_y_b[step]*=beam->drive_data->along[step*beam->bunches+k];
#ifdef TWODIM
	      multi_x[step]*=beam->drive_data->along[step*beam->bunches+k];
	      multi_x_b[step]*=beam->drive_data->along[step*beam->bunches+k];
#endif
	  }
      }
    
      m=k*beam->slices_per_bunch;
      p=beam->field->kick;
      for (j=0;j<beam->slices_per_bunch;j++){
	  rho_y[j]=beam->particle[m+j].y
	      *beam->particle[m+j].wgt;
#ifdef TWODIM
	  rho_x[j]=beam->particle[m+j].x
	      *beam->particle[m+j].wgt;
#endif
	  sumy=0.0;
#ifdef TWODIM
	  sumx=0.0;
#endif
	  for (i=0;i<=j;i++){
	      sumy+=rho_y[i] * *p;
#ifdef TWODIM
	      sumx+=rho_x[i] * *p;
#endif
	      p++;
	  }
	  kick_y[j]=sumy;
#ifdef TWODIM
	  kick_x[j]=sumx;
#endif
      }
      for (step=0;step<injector_data.steps;step++) {
	  pos_y=0.0;
	  pos_yb=0.0;
#ifdef TWODIM
	  pos_x=0.0;
	  pos_xb=0.0;
#endif
	  s=sin(rndm_scale*beam->z_position[m]
		*TWOPI*1e-6/injector_data.wake->lambda[step]);
	  c=cos(rndm_scale*beam->z_position[m]
		*TWOPI*1e-6/injector_data.wake->lambda[step]);
	  ds=sin(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
		 *TWOPI*1e-6/injector_data.wake->lambda[step]);
	  dc=cos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
		     *TWOPI*1e-6/injector_data.wake->lambda[step]);
	  for (j=0;j<beam->slices_per_bunch;j++){
	      pos_y+=rho_y[j]*c;
	      pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	      pos_x+=rho_x[j]*c;
	      pos_xb-=rho_x[j]*s;
#endif
	      kick_y[j]+=(multi_y[step]*s+multi_y_b[step]*c)*longfact[step];
#ifdef TWODIM
	      kick_x[j]+=(multi_x[step]*s+multi_x_b[step]*c)*longfact[step];
#endif
	      tmp=dc*c-ds*s;
	      s=ds*c+dc*s;
	      c=tmp;
	  }
	  multi_y[step]+=pos_y;
	  multi_y_b[step]+=pos_yb;
#ifdef TWODIM
	  multi_x[step]+=pos_x;
	  multi_x_b[step]+=pos_xb;
#endif
      }
      for (j=0;j<beam->slices_per_bunch;j++) {
	  beam->particle[m+j].yp+=
	      kick_y[j]*factor/beam->particle[m+j].energy;
#ifdef TWODIM
	  beam->particle[m+j].xp+=
	      kick_x[j]*factor/beam->particle[m+j].energy;
#endif
      }
  }
}
  
void
dipole_kick_old(ELEMENT *element,BEAM *beam,double length)
{
  double tmp,factor,x,y,half_length;
  double kick_y,y_off,tmpy,kick_x;
  static int step=0;
  int i,j,k,m,n,j0,m2,nf,nadd,ks;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,multi_y,multi_x,
    *rho_a,*rho_b,multi_y_b,pos_yb,pos_xb,longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  if (beam->macroparticles>1) {
      dipole_kick_n_old(element,beam,length);
      return;
  }

  half_length=0.5*length;
  
  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  for (step=0;step<injector_data.steps;step++){
    //    rndm_scale=1.0+0.001*gasdev();
    rndm_scale=element->v_tesla;
    rho_y=beam->rho_y[0];
    multi_y=0.0;
    multi_y_b=0.0;
#ifdef TWODIM
    rho_x=beam->rho_x[0];
    multi_x=0.0;
    multi_x_b=0.0;
#endif
    /* set pointer af and bf to the appropriate cell number */
    
    af=beam->drive_data->af+step*beam->bunches*beam->slices_per_bunch;
    bf=beam->drive_data->bf+step*beam->bunches*beam->slices_per_bunch;
    along=beam->drive_data->along+step*beam->bunches;
    longfact=along[0];
  
    /* loop over all bunches */
  
    for (k=0;k<beam->bunches;k++){
      
      /* apply attenuation */
      
      if (k>0){
	multi_y*=along[k];
	multi_y_b*=along[k];
#ifdef TWODIM
	multi_x*=along[k];
	multi_x_b*=along[k];
#endif
      }
    
      m=k*beam->slices_per_bunch;
      pos_y=0.0;
      pos_yb=0.0;
#ifdef TWODIM
      pos_x=0.0;
      pos_xb=0.0;
#endif
      s=sin(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      c=cos(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      ds=sin(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      dc=cos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      for (j=0;j<beam->slices_per_bunch;j++){
	rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
#ifdef TWODIM
	rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
#endif

	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
#endif
	beam->particle[m+j].yp+=(multi_y*s+multi_y_b*c)*longfact
	  *factor/beam->particle[m+j].energy;
#ifdef TWODIM
	beam->particle[m+j].xp+=(multi_x*s+multi_x_b*c)*longfact
	  *factor/beam->particle[m+j].energy;
	tmp=dc*c-ds*s;
	s=ds*c+dc*s;
	c=tmp;
#endif
      }
      multi_y+=pos_y;
      multi_y_b+=pos_yb;
#ifdef TWODIM
      multi_x+=pos_x;
      multi_x_b+=pos_xb;
#endif
    }
  }
    /*
      Single bunch contribution
    */

  for (k=0;k<beam->bunches;k++){
    m=k*beam->slices_per_bunch;
    p=beam->field->kick;
    for (j=0;j<beam->slices_per_bunch;j++){
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
#ifdef TWODIM
      rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      beam->particle[m+j].yp+=sumy*factor/beam->particle[m+j].energy;
#ifdef TWODIM
      beam->particle[m+j].xp+=sumx*factor/beam->particle[m+j].energy;
#endif
    }
  }
}

