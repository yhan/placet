#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <valarray>

#include <tcl.h>
#include <tk.h>

#include "sort_index.hh"
#include "short_range.h"

#include "beam.h"
#include "placet_print.h"
#include "spline.h"

#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_fft_complex.h>
#include <gsl/gsl_fft_real.h>

#define REAL(z,i) ((z)[2*(i)])
#define IMAG(z,i) ((z)[2*(i)+1])

// helper struct in anonymous namespace
namespace {
  struct sliceinfo {
    double x; // av xpos
    double y; // av ypos
    double z; // z pos (middle)
    double weight; // nr particles / charge
  };

  std::valarray<double> linspace(double xmin, double xmax, size_t N )
  {
    std::valarray<double> x(N);
    for(size_t n=0; n<N; n++)
      x[n] = xmin + ((xmax - xmin) * n) / (N-1);
    return x;
  }
}

Tcl_HashTable ShortRangeTable;

int ShortRange_Init(Tcl_Interp *interp)
{
  Tcl_InitHashTable(&ShortRangeTable,TCL_STRING_KEYS);
  return TCL_OK;
}

SHORT_RANGE* get_short_range(const char * short_range_name) {
  Tcl_HashEntry *entry=Tcl_FindHashEntry(&ShortRangeTable,short_range_name);
  if (entry==NULL) {
    placet_cout << ERROR << "Short range wake '" << short_range_name << "' not found" << endmsg;
    return NULL;
  }
  SHORT_RANGE* short_range=(SHORT_RANGE*)Tcl_GetHashValue(entry);
  return short_range;
}

SHORT_RANGE::SHORT_RANGE(SPLINE *sx,SPLINE *sy,SPLINE *sz,int fb,double sbl) : spline_x(sx), spline_y(sy), spline_z(sz), type(fb), spline_bunch_length(sbl) {
}

SHORT_RANGE::~SHORT_RANGE(){
  if (spline_x) delete spline_x;
  if (spline_y) delete spline_y;
  if (spline_z) delete spline_z;
}

double
SHORT_RANGE::wake_x(double dist)
{
  if (!spline_x) {
    placet_cout << ERROR << "Error in short_range: spline_x has not been defined" << endmsg;
    exit(1);
  }
  return spline_x->eval(dist);
}

double
SHORT_RANGE::wake_y(double dist)
{
  if (!spline_y) {
    placet_cout << ERROR << "Error in short_range: spline_y has not been defined" << endmsg;
    exit(1);
  }
  return spline_y->eval(dist);
}

double
SHORT_RANGE::wake_z(double dist)
{
  if (!spline_z) {
    placet_cout << ERROR << "Error in short_range: spline_z has not been defined" << endmsg;
    exit(1);
  }
  return spline_z->eval(dist);
}

std::pair<std::valarray<double>, std::valarray<double> > 
SHORT_RANGE::wake_x_convolve(BEAM *bunch, const std::vector<int> &index, double length, size_t Nslices ) const ///< returns a kick in urad * GeV
{
  double z_min = bunch->particle[*(index.begin())].z;
  double z_max = bunch->particle[*(index.end()-1)].z;
  // number of slices for wakefield computation (must be a power of two)
  std::valarray<double> z = linspace(z_min, z_max, Nslices);
  // data space for the convolution (must be a power of two, and twice the length of the functions to convolve)
  const size_t Nslices_conv = 2*Nslices;
  double data_Wx_real [Nslices_conv];
  double data_dQx_real[Nslices_conv];
  for (size_t i=0; i<Nslices; i++) data_dQx_real[i] = 0.0;
  // computes the charge distribution integrals and prepares the arrays for the FFTs
  for (int i=0, slice=0; i<index.size(); i++) {
    const PARTICLE &particle = bunch->particle[index[i]];
    while (slice<(Nslices-1) && particle.z>z[slice+1])
      slice++;
    data_dQx_real[slice] += particle.wgt * particle.x; // this stays in um, so the returned kick is in urad * GeV
  }
  const double Nparticles = bunch->drive_data->param.inject->charge;
  const double bunch_charge_pC = Nparticles * ECHARGE * 1e12;
  for (size_t i=0; i<Nslices; i++) {
    const double dz = (z[i] - z[0]) * 1e-6; // um to m
    data_Wx_real[i] = length * spline_x->eval(dz);
    data_dQx_real[i] *= bunch_charge_pC;
  }
  for (size_t i=Nslices; i<Nslices_conv; i++) {
    data_Wx_real[i] = 0.0;
    data_dQx_real[i] = 0.0;
  }
  // compute the convolution
  {
    // auxiliary arrays
    double aux_Wk[2*Nslices_conv];
    double aux_dQ[2*Nslices_conv];
    //////// Wx convolution
    gsl_fft_real_radix2_transform (data_Wx_real,  1, Nslices_conv);
    gsl_fft_real_radix2_transform (data_dQx_real, 1, Nslices_conv);
    gsl_fft_halfcomplex_radix2_unpack (data_Wx_real,  aux_Wk, 1, Nslices_conv);
    gsl_fft_halfcomplex_radix2_unpack (data_dQx_real, aux_dQ, 1, Nslices_conv);
    for (size_t i=0; i<Nslices_conv; i++) {
      std::complex<double> Wk_i = std::complex<double>(REAL(aux_Wk,i), IMAG(aux_Wk,i));
      std::complex<double> dQ_i = std::complex<double>(REAL(aux_dQ,i), IMAG(aux_dQ,i));
      std::complex<double> t = Wk_i * dQ_i;
      REAL(aux_Wk,i) = t.real();
      IMAG(aux_Wk,i) = t.imag();
    }
    gsl_fft_complex_radix2_inverse (aux_Wk, 1, Nslices_conv);
    for (size_t i=0; i<Nslices; i++) data_Wx_real[i] = REAL(aux_Wk,i) * 1e-9; // from e*V to e*GV
  }
  return std::pair<std::valarray<double>, std::valarray<double> >(z, std::valarray<double>(data_Wx_real, Nslices));
}

std::pair<std::valarray<double>, std::valarray<double> >
SHORT_RANGE::wake_y_convolve(BEAM *bunch, const std::vector<int> &index, double length, size_t Nslices ) const ///< returns a kick in urad * GeV
{
  double z_min = bunch->particle[*(index.begin())].z;
  double z_max = bunch->particle[*(index.end()-1)].z;
  // number of slices for wakefield computation (must be a power of two)
  std::valarray<double> z = linspace(z_min, z_max, Nslices);
  // data space for the convolution (must be a power of two, and twice the length of the functions to convolve)
  const size_t Nslices_conv = 2*Nslices;
  double data_Wy_real [Nslices_conv];
  double data_dQy_real[Nslices_conv];
  for (size_t i=0; i<Nslices; i++) data_dQy_real[i] = 0.0;
  // computes the charge distribution integrals and prepares the arrays for the FFTs
  for (size_t i=0, slice=0; i<index.size(); i++) {
    const PARTICLE &particle = bunch->particle[index[i]];
    while (slice<(Nslices-1) && particle.z>z[slice+1])
      slice++;
    data_dQy_real[slice] += particle.wgt * particle.y; // this stays in um, so the returned kick is in urad * GeV
  }
  const double Nparticles = bunch->drive_data->param.inject->charge;
  const double bunch_charge_pC = Nparticles * ECHARGE * 1e12;
  for (size_t i=0; i<Nslices; i++) {
    const double dz = (z[i] - z[0]) * 1e-6; // um to m
    data_Wy_real[i] = length * spline_y->eval(dz);
    data_dQy_real[i] *= bunch_charge_pC;
  }
  for (size_t i=Nslices; i<Nslices_conv; i++) {
    data_Wy_real[i] = 0.0;
    data_dQy_real[i] = 0.0;
  }
  // compute the convolution
  {
    // auxiliary arrays
    double aux_Wk[2*Nslices_conv];
    double aux_dQ[2*Nslices_conv];
    //////// Wy convolution
    gsl_fft_real_radix2_transform (data_Wy_real,  1, Nslices_conv);
    gsl_fft_real_radix2_transform (data_dQy_real, 1, Nslices_conv);
    gsl_fft_halfcomplex_radix2_unpack (data_Wy_real,  aux_Wk, 1, Nslices_conv);
    gsl_fft_halfcomplex_radix2_unpack (data_dQy_real, aux_dQ, 1, Nslices_conv);
    for (size_t i=0; i<Nslices_conv; i++) {
      std::complex<double> Wk_i = std::complex<double>(REAL(aux_Wk,i), IMAG(aux_Wk,i));
      std::complex<double> dQ_i = std::complex<double>(REAL(aux_dQ,i), IMAG(aux_dQ,i));
      std::complex<double> t = Wk_i * dQ_i;
      REAL(aux_Wk,i) = t.real();
      IMAG(aux_Wk,i) = t.imag();
    }
    gsl_fft_complex_radix2_inverse (aux_Wk, 1, Nslices_conv);
    for (size_t i=0; i<Nslices; i++) data_Wy_real[i] = REAL(aux_Wk,i) * 1e-9; // from e*V to e*GV
  }
  return std::pair<std::valarray<double>, std::valarray<double> >(z, std::valarray<double>(data_Wy_real, Nslices));
}

std::pair<std::valarray<double>, std::valarray<double> >
SHORT_RANGE::wake_z_convolve(BEAM *bunch, const std::vector<int> &index, double length, size_t Nslices ) const ///< returns an energy loss in GeV
{
  double z_min = bunch->particle[*(index.begin())].z;
  double z_max = bunch->particle[*(index.end()-1)].z;
  // number of slices for wakefield computation (must be a power of two)
  std::valarray<double> z = linspace(z_min, z_max, Nslices);
  // data space for the convolution (must be a power of two, and twice the length of the functions to convolve)
  const size_t Nslices_conv = 2*Nslices;
  double data_Wz_real[Nslices_conv];
  double data_dQ_real[Nslices_conv];
  for (size_t i=0; i<Nslices; i++) data_dQ_real[i] = 0.0;
  // computes the charge distribution integrals and prepares the arrays for the FFTs
  for (int i=0, slice=0; i<index.size(); i++) {
    const PARTICLE &particle = bunch->particle[index[i]];
    while (slice<(Nslices-1) && particle.z>z[slice+1])
      slice++;
    data_dQ_real[slice] += particle.wgt;
  }

  const double Nparticles = bunch->drive_data->param.inject->charge;
  const double bunch_charge_pC = Nparticles * ECHARGE * 1e12;
  for (size_t i=0; i<Nslices; i++) {
    const double dz = (z[i] - z[0]) * 1e-6; // um to m
    data_Wz_real[i] = length * spline_z->eval(dz) * (i==0 ? 0.5 : 1.0);
    data_dQ_real[i] *= bunch_charge_pC;
  }
  for (size_t i=Nslices; i<Nslices_conv; i++) {
    data_Wz_real[i] = 0.0;
    data_dQ_real[i] = 0.0;
  }
  // compute the convolution
  {
    // auxiliary arrays
    double aux_Wk[2*Nslices_conv];
    double aux_dQ[2*Nslices_conv];
    //////// Wz convolution
    gsl_fft_real_radix2_transform (data_Wz_real, 1, Nslices_conv);
    gsl_fft_real_radix2_transform (data_dQ_real, 1, Nslices_conv);
    gsl_fft_halfcomplex_radix2_unpack (data_Wz_real, aux_Wk, 1, Nslices_conv);
    gsl_fft_halfcomplex_radix2_unpack (data_dQ_real, aux_dQ, 1, Nslices_conv);
    for (size_t i=0; i<Nslices_conv; i++) {
      std::complex<double> Wk_i = std::complex<double>(REAL(aux_Wk,i), IMAG(aux_Wk,i));
      std::complex<double> dQ_i = std::complex<double>(REAL(aux_dQ,i), IMAG(aux_dQ,i));
      std::complex<double> t = Wk_i * dQ_i;
      REAL(aux_Wk,i) = t.real();
      IMAG(aux_Wk,i) = t.imag();
    }
    gsl_fft_complex_radix2_inverse (aux_Wk, 1, Nslices_conv);
    for (size_t i=0; i<Nslices; i++) data_Wz_real[i] = REAL(aux_Wk,i) * 1e-9; // from e*V to e*GV
  }
  return std::pair<std::valarray<double>, std::valarray<double> >(z, std::valarray<double>(data_Wz_real, Nslices));
}

void SHORT_RANGE::apply_wakefield_spline(BEAM* beam, double length) const {

  // apply the kick here, as no suitable method was found. should be put into more suitable place later?
  // 
  // no special case for macroparticles==1 (might reduce cpu)
  // needs to be generalised for more bunches

  if (beam->bunches>1) {
    placet_cout << WARNING << "ELEMENT: short range is not working with multi bunch" << endmsg;
  }

  const double conversion_factor_transv = 1e6 * ECHARGE * beam->drive_data->param.inject->charge;
  // 1e-3 [mm/um] * ECHARGE[C] * 1e12 [pC/C] * 1e-3 * [kV/V] * 1 [urad GeV / 1e kV] = [urad * GeV * pC*mm/V / um]

  const double conversion_factor_long = 1e3 * ECHARGE * beam->drive_data->param.inject->charge;
  // ECHARGE[C] * 1e12 [pC/C] * 1e-9[GeV / 1e*V] = [GeV * pC / V]

  // spline representation 'type'
  if (type==1) {
  
    /// store wakekicks for both sliced and particle beams
    // so that same code can be used for both beams
    double* wakekick_x = new double[beam->slices];
    // use alias for y if spline is same for both directions
    double* wakekick_y = (spline_x==spline_y) ? wakekick_x : new double[beam->slices];
    double* wakekick_z; 
    if (spline_z) wakekick_z = new double[beam->slices];

    if (beam->particle_beam) {
      /// get kick for each individual particle
      for (int j=0;j<beam->slices;j++) {
	PARTICLE &particle=beam->particle[j];
	wakekick_x[j] = spline_x->eval(particle.z*1e-6);
	if (spline_x != spline_y) {
	  wakekick_y[j] = spline_y->eval(particle.z*1e-6);
	}
	if (spline_z) wakekick_z[j] = spline_z->eval(particle.z*1e-6);
      }
    } else {
      int k = 0;
      /// for sliced beam, get kick for each slice
      for (int ibunch=0;ibunch<beam->bunches;ibunch++) {
	for (int j=0;j<beam->slices_per_bunch;j++) {
	  double zp=beam->z_position[ibunch*beam->slices_per_bunch + j];
	  double spline_x_eval = spline_x->eval(zp*1e-6);
	  double spline_y_eval, spline_z_eval;
	  if (spline_x != spline_y) {
	    spline_y_eval = spline_y->eval(zp*1e-6);
	  }
	  if (spline_z) spline_z_eval = spline_z->eval(zp*1e-6);
	  for (int i=0;i<beam->particle_number[j];i++) {
	    wakekick_x[k] = spline_x_eval;
	    if (spline_x != spline_y) {
	      wakekick_y[k] = spline_y_eval;
	    }
	    if (spline_z) wakekick_z[k] = spline_z_eval;
	    k++;
	  }
	}
      }	
    }
  
    double xpos_beam=0.0, ypos_beam=0.0, weight=0.0;
    
    for (int j=0;j<beam->slices;j++) {
      const PARTICLE &particle=beam->particle[j];
      xpos_beam+=particle.x*particle.wgt;
      ypos_beam+=particle.y*particle.wgt;
      weight+=particle.wgt;
    }
    xpos_beam/=weight;
    ypos_beam/=weight;
    // account for particle loss, weight is ratio of remaining particles
    // assumption: particle loss distribution is uniform
    const double conv_factor_wgt_adj_transv = conversion_factor_transv*weight;
    const double conv_factor_wgt_adj_long = conversion_factor_long*weight;
    const double conv_xpos = conv_factor_wgt_adj_transv*xpos_beam;
    const double conv_ypos = conv_factor_wgt_adj_transv*ypos_beam;
    for (int j=0;j<beam->slices;j++) {
      PARTICLE &particle=beam->particle[j];
      particle.xp+=conv_xpos*wakekick_x[j]/particle.energy;
      particle.yp+=conv_ypos*wakekick_y[j]/particle.energy;
      if (spline_z) particle.energy += wakekick_z[j] * conv_factor_wgt_adj_long;
    }

    delete[] wakekick_x;
    if (spline_x != spline_y) {delete[] wakekick_y;}
    if (spline_z) delete[] wakekick_z;

#ifdef HTGEN
    int imacro3=0; //haloparticles
    for (int j=0;j<beam->slices_per_bunch;j++) {//loop over slices
      // apply wakefield kick to the halo
      for (int i_m=0;i_m<beam->particle_number_sec[j];i_m++) {
	PARTICLE &particle=beam->particle_sec[imacro3];
	double wakekick_x = spline_x->eval(particle.z*1e-6);
	double wakekick_y = (spline_x==spline_y) ? wakekick_x : spline_y->eval(particle.z*1e-6);
	particle.xp+=conv_xpos*wakekick_x/particle.energy;
	particle.yp+=conv_ypos*wakekick_y/particle.energy;
	if (spline_z) {
	  double wakekick_z = spline_z->eval(particle.z*1e-6);
	  particle.energy += wakekick_z * conv_factor_wgt_adj_long;
	}
	imacro3++; 
      }
    }
#endif
    
  } else if (type==0) { // wakefield is not defined for full bunch

    // strategy:
    
    /*
      - artificial slicing with spline_bunch_length
      - loop over particles (ordered in z) and until bunchlength is reached. calculate their average position in x and y. store these position and nr particles
      - PROBLEMs: 
      - individual slices not (even close to) gaussians. use averaged z position? 
      - better to approximate the beam by a set of gaussians with spline_bunch_length, where amplitude and position are optimised. this is non-trivial, how to do this? 
      - toy study showed that naive approximation below is quite good and that the step size does not have to be equal to spline_bunch_length, but can be relaxed to about 40 steps for the 3 sigma range (not done here), but can be easily implemented in case speedup is needed.
    */
    
    // needs to be optimised for sliced beam
    

    // sort particles by z if necessary (returns the indexes, does not move the particles)
    const std::vector<int> index = sort_index(beam->particle, beam->slices);

    // determine sliceinfo
    double z_slice_start = beam->particle[index[0]].z;
    std::vector<sliceinfo> slice_info;
    // make educated guess of vector size
    // need to be a little bit careful here as in case beam lost can become very negative, and casting to int then becomes INT_MIN and abs(INT_MIN)=INT_MIN!
    // causes a crash hard to debug
    int size_guess_tmp = int(2*std::ceil(z_slice_start/spline_bunch_length)+1);
    int size_guess = std::max(1,std::min(std::abs(size_guess_tmp),beam->slices));
    slice_info.reserve(size_guess);
    double xpos_slice = 0.0, ypos_slice=0.0, weight_slice=0.0;

    // relies on beam sorted in z, starting at negative (front of beam)
    for (int j=0;j<beam->slices;j++) {
      const PARTICLE &particle=beam->particle[index[j]];
      if (particle.z > z_slice_start + spline_bunch_length) {
	// calculate average
	xpos_slice/=weight_slice;
	ypos_slice/=weight_slice;
	sliceinfo slice={xpos_slice,ypos_slice,z_slice_start+0.5*spline_bunch_length,weight_slice};
	slice_info.push_back(slice);
    
	// reset
	z_slice_start+=spline_bunch_length;
	xpos_slice=0.0;
	ypos_slice=0.0;
	weight_slice=0.0;
      }

      xpos_slice+=particle.x*particle.wgt;
      ypos_slice+=particle.y*particle.wgt;
      weight_slice+=particle.wgt;
    }

    // apply kicks

    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[index[i]];
      const double temp = conversion_factor_transv/particle.energy;
      // loop over slice info to apply kicks from all slices on particle
      for (unsigned int j=0; j<slice_info.size(); j++) {
	const sliceinfo & slice = slice_info[j];
	double wakekick_x = spline_x->eval((particle.z-slice.z)*1e-6);
	double wakekick_y = (spline_x==spline_y) ? 
	  wakekick_x : 
	  spline_y->eval((particle.z-slice.z)*1e-6);
	double temp2 = slice.weight*temp;
	particle.xp+=slice.x*wakekick_x*temp2;
	particle.yp+=slice.y*wakekick_y*temp2;
	if (spline_z) {
	  double wakekick_z = spline_z->eval((particle.z-slice.z)*1e-6);
	  particle.energy+= wakekick_z*conversion_factor_long*slice.weight;
	}
      }
    }

#ifdef HTGEN
    int imacro3=0; //haloparticles
    for (int j=0;j<beam->slices_per_bunch;j++) {//loop over slices
      // apply wakefield kick to the halo
      for (int i_m=0;i_m<beam->particle_number_sec[j];i_m++) {
	PARTICLE &particle=beam->particle_sec[imacro3];
	const double temp = conversion_factor_transv/particle.energy;
	// loop over slice info to apply kicks from all slices on particle
	for (unsigned int j=0; j<slice_info.size(); j++) {
	  const sliceinfo & slice = slice_info[j];
	  double wakekick_x = spline_x->eval((particle.z-slice.z)*1e-6);
	  double wakekick_y = (spline_x==spline_y) ? 
	    wakekick_x : 
	    spline_y->eval((particle.z-slice.z)*1e-6);
	  double temp2 = slice.weight*temp;
	  particle.xp+=slice.x*wakekick_x*temp2;
	  particle.yp+=slice.y*wakekick_y*temp2;
	  if (spline_z) {
	    double wakekick_z = spline_z->eval((particle.z-slice.z)*1e-6);
	    particle.energy+= wakekick_z*conversion_factor_long*slice.weight;
	  }
	}
	imacro3++; 
      }
    }
#endif
  } else if (type==2) {
    std::vector<int> index_; // array of valid particles
    for (size_t i=0; i<beam->slices; i++) {
      if (!beam->particle[i].is_lost())
	index_.push_back(i);
    }
    /// checks if the particles are ordered by 'z'
    const std::vector<int> index = sort_index(beam->particle, index_);
    ////
    const size_t Nslices = 64;
    std::pair<std::valarray<double>, std::valarray<double> > data;
    std::valarray<double> data_Wx(Nslices, 0.0);
    std::valarray<double> data_Wy(Nslices, 0.0);
    std::valarray<double> data_Wz(Nslices, 0.0);
    if (spline_x) {
      data = wake_x_convolve(beam, index, length, Nslices);
      data_Wx = data.second;
    }
    if (spline_y) {
      data = wake_y_convolve(beam, index, length, Nslices);
      data_Wy = data.second;
    }
    if (spline_z) {
      data = wake_z_convolve(beam, index, length, Nslices);
      data_Wz = data.second;
    }
    std::valarray<double> z = data.first;
    const double dz = z[1] - z[0];
    //////// apply the kick
    for (int i=0, slice=0; i<beam->slices; i++) {
      PARTICLE &particle = beam->particle[index[i]];
      while (slice<(Nslices-1) && particle.z>z[slice+1])
      	slice++;
      double Kx, Ky, dE;
      if (slice<Nslices-1) {
	double a = (particle.z - z[slice]) / dz;
	Kx = (1.0 - a) * data_Wx[slice] + a * data_Wx[slice+1];
	Ky = (1.0 - a) * data_Wy[slice] + a * data_Wy[slice+1];
	dE = (1.0 - a) * data_Wz[slice] + a * data_Wz[slice+1];
      } else {
	Kx = data_Wx[slice];
	Ky = data_Wy[slice];
	dE = data_Wz[slice];
      }
      particle.xp += Kx / particle.energy;
      particle.yp += Ky / particle.energy;
      particle.energy -= dE;
    }
  } else {
    placet_cout << ERROR << "ShortRangeWake : unacceptable value 'type' = " << type << endmsg;
  }
}
