#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beamline.h"
#include "girder.h"
#include "structures_def.h"

/* Function prototypes */

#include "placeti3.h"
#include "cavity.h"
#include "quadrupole.h"
#include "quadbpm.h"
#include "bpm.h"
#include "random.hh"
#include "survey.h"

extern double zero_point;

void beamline_survey_bpmscale(BEAMLINE *beamline,double sx,double sy)
{
  int i;
  for (i=0;i<beamline->n_elements;i++){
    if (BPM *bpm=beamline->element[i]->bpm_ptr()) {
      bpm->set_scale_y(Survey.Gauss(sx)+1.0);
      bpm->set_scale_x(Survey.Gauss(sy)+1.0);
    }
  }
}

void beamline_survey_tesla2(BEAMLINE *beamline,double sigma)
{
  for (int i=0;i<beamline->n_elements;i++){
    if (CAVITY *cav=beamline->element[i]->cavity_ptr()) {
      cav->set_v_tesla(Survey.Gauss(sigma)+1.0);
    }
  }
}

SURVEY_ERRORS_STRUCT survey_errors;

void beamline_survey_quadroll(BEAMLINE *beamline)
{
  for (int i=0;i<beamline->n_elements;i++) {

    beamline->element[i]->set_offset(0.0, 0.0, 0.0, 0.0);
    
    if (QUADRUPOLE *quad=beamline->element[i]->quad_ptr()) {
      quad->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
    }
  
    if (QUADBPM *qbpm=beamline->element[i]->quadbpm_ptr()) {
      qbpm->set_bpm_offset_x(survey_errors.bpm_error*Misalignments.Gauss());
      qbpm->set_bpm_offset_y(survey_errors.bpm_error*Misalignments.Gauss());
    }

    beamline->element[i]->offset.y+=zero_point;
  }
}

int
wire_survey(FILE *file,BEAMLINE *beamline)
{
  double s1,s2=0.0,z1,z2,x1,x2,y1,y2;
  double px1,px2,py1,py2;
  GIRDER *girder;
  char buffer[1024],*point;

  girder=beamline->first;
  point=fgets(buffer,1024,file);
  if (!point) {
    return 1;
  }
  z1=strtod(point,&point);
  x1=strtod(point,&point)*1e6;
  y1=strtod(point,&point)*1e6;
  point=fgets(buffer,1024,file);
  if (!point) {
    return 1;
  }
  z2=strtod(point,&point);
  x2=strtod(point,&point)*1e6;
  y2=strtod(point,&point)*1e6;
  while (girder) {
    s1=s2;
    s2+=girder->get_length();
    while (s1>z2) {
      z1=z2; x1=x2; y1=y2;
      point=fgets(buffer,1024,file);
      if (!point) {
	return 1;
      }
      z2=strtod(point,&point);
      x2=strtod(point,&point)*1e6;
      y2=strtod(point,&point)*1e6;
    }
    px1=(x1*(z2-s1)+x2*(s1-z1))/(z2-z1);
    py1=(y1*(z2-s1)+y2*(s1-z1))/(z2-z1);

    while (s2>z2) {
      z1=z2; x1=x2; y1=y2;
      point=fgets(buffer,1024,file);
      if (!point) {
	return 1;
      }
      z2=strtod(point,&point);
      x2=strtod(point,&point)*1e6;
      y2=strtod(point,&point)*1e6;
    }
    px2=(x1*(z2-s2)+x2*(s2-z1))/(z2-z1);
    py2=(y1*(z2-s2)+y2*(s2-z1))/(z2-z1);
    girder->move(0.5*(py1+py2),(py2-py1)/(s2-s1));
#ifdef TWODIM
    girder->move_x(0.5*(px1+px2),(px2-px1)/(s2-s1));
#endif
    s2=s2+girder->distance_to_prev_girder();
    girder=girder->next();
  }
  return 0;
}
