#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "histogram.h"

/* Storage and routines for the different histograms */

void histogram_make(HISTOGRAM *histogram,int type,float xmin,float xmax,int n,
		    const char *name)
{
  int i;
  histogram->name=name;
  switch (type)
    {
    case 1:
      histogram->nc=n;
      histogram->xmin=xmin;
      histogram->xmax=xmax;
      histogram->type=type;
      histogram->y=(double*)malloc(sizeof(double)*n);
      for (i=0;i<n;i++)
	{
	  histogram->y[i]=0.0;
	}
      histogram->dx_i=(float)n/(xmax-xmin);
      break;
    case 2:
      histogram->nc=n;
      histogram->xmin=(float)log((double)xmin);
      histogram->xmax=(float)log((double)xmax);
      histogram->type=type;
      histogram->y=(double*)malloc(sizeof(double)*n);
      for (i=0;i<n;i++)
	{
	  histogram->y[i]=0.0;
	}
      histogram->dx_i=(float)n/(histogram->xmax-histogram->xmin);
      break;
    case 3:
      histogram->nc=n;
      histogram->xmin=xmin;
      histogram->xmax=xmax;
      histogram->type=type;
      histogram->y=(double*)malloc(sizeof(double)*n);
      histogram->count=(long*)malloc(sizeof(long)*n);
      for (i=0;i<n;i++)
	{
	  histogram->y[i]=0.0;
	  histogram->count[i]=0;
	}
      histogram->dx_i=(float)n/(xmax-xmin);
      break;
    case 4:
      histogram->nc=n;
      histogram->xmin=xmin;
      histogram->xmax=xmax;
      histogram->type=type;
      histogram->y=(double*)malloc(sizeof(double)*n);
      histogram->y2=(double*)malloc(sizeof(double)*n);
      histogram->count=(long*)malloc(sizeof(long)*n);
      for (i=0;i<n;i++)
	{
	  histogram->y[i]=0.0;
	  histogram->y2[i]=0.0;
	  histogram->count[i]=0;
	}
      histogram->dx_i=(float)n/(xmax-xmin);
      break;
    }
}

void histogram_add(HISTOGRAM *histogram,float x,float y)
{
  int j;
  float jd; 
  switch(histogram->type){
    case 1:
      jd=(x-histogram->xmin)*histogram->dx_i;
      j=(int)jd;
      if ((jd>=0.0)&&(j<histogram->nc)){
	  histogram->y[j]+=y;
	}
/*else{
	placet_printf(INFO,"hist %g %g\n",x,y);
}*/
      break;
    case 2:
      if (x>0.0){
	  jd=(log((double)x)-histogram->xmin)*histogram->dx_i;
          j=(int)jd;
	  if ((jd>=0.0)&&(j<histogram->nc))
	    {
	      histogram->y[j]+=y;
	    }
	}
      break;
    case 3:
      jd=(x-histogram->xmin)*histogram->dx_i;
      j=(int)jd;
      if ((jd>=0.0)&&(j<histogram->nc))
	{
	  histogram->y[j]+=y;
	  histogram->count[j]++;
	}
      break;
    case 4:
      jd=(x-histogram->xmin)*histogram->dx_i;
      j=(int)jd;
      if ((jd>=0.0)&&(j<histogram->nc))
	{
	  histogram->y[j]+=y;
	  histogram->y2[j]+=y*y;
	  histogram->count[j]++;
	}
      break;
    }
}

void histogram_fprint(FILE *datei,HISTOGRAM *histogram)
{
  int i;
  float xstep;
  double sigma;
  switch (histogram->type)
    {
    case 1:
      xstep=(histogram->xmax-histogram->xmin)/((float)histogram->nc);
      fprintf(datei,"%g %g\n",histogram->xmin,0.0);
      for (i=0;i<histogram->nc;i++)
	{
	  fprintf(datei,"%g %g\n",histogram->xmin+i*xstep,
		  histogram->y[i]);
	  fprintf(datei,"%g %g\n",histogram->xmin+(i+1)*xstep,
		  histogram->y[i]);
	}
      fprintf(datei,"%g %g\n",histogram->xmax,0.0);
      break;
    case 2:
      xstep=(histogram->xmax-histogram->xmin)/((float)histogram->nc);
      fprintf(datei,"%g %g\n",exp(histogram->xmin),0.0);
      for (i=0;i<histogram->nc;i++)
	{
	  fprintf(datei,"%g %g\n",exp(histogram->xmin+i*xstep),
		  histogram->y[i]);
	  fprintf(datei,"%g %g\n",exp(histogram->xmin+(i+1)*xstep),
		  histogram->y[i]);
	}
      fprintf(datei,"%g %g\n",exp(histogram->xmax),0.0);
      break;
    case 3:
      xstep=(histogram->xmax-histogram->xmin)/((float)histogram->nc);
      for (i=0;i<histogram->nc;i++)
	{
	  fprintf(datei,"%g %g %ld\n",histogram->xmin+i*xstep,
		  histogram->y[i],histogram->count[i]);
	  fprintf(datei,"%g %g %ld\n",histogram->xmin+(i+1)*xstep,
		  histogram->y[i],histogram->count[i]);
	}
      break;
    case 4:
      xstep=(histogram->xmax-histogram->xmin)/((float)histogram->nc);
      for (i=0;i<histogram->nc;i++)
	{
/*	  sigma=1.0/std::max(eps,histogram->count[i]
				   *(histogram->count[i]-1))
	    *(histogram->count[i]*histogram->y2[i]
	      -histogram->y[i]*histogram->y[i]);*/
          sigma=sqrt(histogram->y2[i]);
	  fprintf(datei,"%g %g %g %ld\n",histogram->xmin+(i+0.5)*xstep,
		  histogram->y[i],sigma,histogram->count[i]);
	}
      break;
    }
}

void histogram_integrate(HISTOGRAM *hist,int sens)
{
  int i;
  if (sens<0) {
    for(i=0;i<hist->nc-1;i++){
      hist->y[i+1]+=hist->y[i];
      //      hist->y2[i+1]+=hist->y2[i];
    }
  }
  else{
    for(i=hist->nc-1;i>0;i--){
      hist->y[i-1]+=hist->y[i];
      //      hist->y2[i-1]+=hist->y2[i];
    }
  }
}

int Tcl_Histogram_add(ClientData clientData,Tcl_Interp * /*interp*/,int argc,
		  char *argv[])
{
  HISTOGRAM *histogram;
  double x,y;
  char *point;

  histogram=(HISTOGRAM*)clientData;
  if (argc!=1) {
    return TCL_ERROR;
  }
  x=strtod(argv[0],&point);
  y=strtod(point,&point);
  histogram_add(histogram,x,y);
  //  placet_printf(INFO,"%g %g\n",x,y);
  return TCL_OK;
}

int Tcl_Histogram_integrate(ClientData clientData,Tcl_Interp *interp,int argc,
			char *argv[])
{
  int error=TCL_OK;
  HISTOGRAM *histogram;
  int i=1;
  Tk_ArgvInfo table[]={
    {(char*)"-direction",TK_ARGV_INT,(char*)NULL,
     (char*)&i,
     (char*)"direction of integration (defaults to 1)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  histogram=(HISTOGRAM*)clientData;
  if (argc!=1) {
    placet_printf(INFO,"%d %s %s\n",argc,argv[0],argv[1]);
    return TCL_ERROR;
  }
  histogram_integrate(histogram,i);
  return TCL_OK;
}

int Tcl_Histogram_print(ClientData clientData,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  HISTOGRAM *histogram;
  FILE *file;
  int norm=0,error;
  Tk_ArgvInfo table[]={
    {(char*)"-normalise",TK_ARGV_INT,(char*)NULL,
     (char*)&norm,
     (char*)"Print histogram density"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,
			  TK_ARGV_DONT_SKIP_FIRST_ARG))!=TCL_OK){
    return error;
  }
  if (argc>1){
    Tcl_SetResult(interp,"Too many arguments to <Histogram_print>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  histogram=(HISTOGRAM*)clientData;
  switch (argc) {
  case 0:
    file=stdout;
    break;
  case 1:
    file=open_file(argv[0]);
    break;
  default:
    return TCL_ERROR;
  }
  histogram_fprint(file,histogram);
  if (argc==1) close_file(file);
  return TCL_OK;
}

int Tcl_Histogram_eval(ClientData clientData,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  if (argc<2){
    return TCL_ERROR;
  }
  if (!strcmp("add",argv[1])){
    return Tcl_Histogram_add(clientData,interp,argc-2,argv+2);
  }
  if (!strcmp("print",argv[1])){
    return Tcl_Histogram_print(clientData,interp,argc-2,argv+2);
  }
  if (!strcmp("integrate",argv[1])){
    return Tcl_Histogram_integrate(clientData,interp,argc-1,argv+1);
  }
  // include error handling
  return TCL_ERROR;
}

int Tcl_Histogram(ClientData /*clientData*/,Tcl_Interp *interp,int argc,char *argv[])
{
  HISTOGRAM *histogram;
  double xmin=0.0,xmax=0.0;
  int n=0,error;
  int log=0;
  Tk_ArgvInfo table[]={
    {(char*)"-n",TK_ARGV_INT,(char*)NULL,
     (char*)&n,
     (char*)"number of intervals for x-axis"},
    {(char*)"-xmin",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&xmin,
     (char*)"minimal value of x-axis"},
    {(char*)"-xmax",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&xmax,
     (char*)"maximal value of x-axis"},
    {(char*)"-log",TK_ARGV_INT,(char*)NULL,
     (char*)&log,
     (char*)"switch for logarithmic x-axis"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2){
    Tcl_SetResult(interp,"Too many arguments to <Histogram>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  log++;
  histogram=(HISTOGRAM*)malloc(sizeof(HISTOGRAM));
  histogram_make(histogram,log,xmin,xmax,n,argv[1]);
  Tcl_CreateCommand(interp,argv[1],Tcl_Histogram_eval,(ClientData)histogram,
		    (Tcl_CmdDeleteProc*)NULL);
  return TCL_OK;
}

int Histogram_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"Histogram",&Tcl_Histogram,(ClientData)NULL,
		    (Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Histogram","1.0");
  return TCL_OK;
}
