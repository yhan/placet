#include <algorithm>
#include <iostream>
#include <fstream>
#include <list>
#include <tk.h>

#include "manual.h"

#include "help.h"

using namespace MANUAL;

std::map<CATEGORY_KEY, const char *> MANUAL::section_titles;

static struct _init_module {
  _init_module();
  ~_init_module() {}
} __init_module; 

_init_module::_init_module() 
{
  section_titles[CATEGORY_KEY(BEAMLINE, CREATE)] = "Creating a Beamline";
  section_titles[CATEGORY_KEY(BEAMLINE, INSPECT)] = "Inspecting a Beamline";
  section_titles[CATEGORY_KEY(BEAMLINE, MODIFY)] = "Modifying a Beamline";
  section_titles[CATEGORY_KEY(ELEMENT, CREATE)] = "Creating a new Element";
  section_titles[CATEGORY_KEY(ELEMENT, INSPECT)] = "Inspecting an Element";
  section_titles[CATEGORY_KEY(ELEMENT, MODIFY)] = "Modifying an Element";
  section_titles[CATEGORY_KEY(BEAM, CREATE)] = "Creating a Beam";
  section_titles[CATEGORY_KEY(BEAM, INSPECT)] = "Inspecting a Beam";
  section_titles[CATEGORY_KEY(BEAM, MODIFY)] = "Modifying a Beam";
  section_titles[CATEGORY_KEY(TRACKING, NONE)] = "Tracking";
  section_titles[CATEGORY_KEY(TOOLS, NONE)] = "Tools";
}

struct cmpCOMMAND_HELPER {
  bool operator () (const COMMAND_HELPER &a, const COMMAND_HELPER &b ) const
  {
    if (a.get_category_key() == b.get_category_key())
      return a.name < b.name;
    else 
      return a.get_category_key() < b.get_category_key();
  }
};

int tk_Manual(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,char *argv[])
{
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Output file name (default : stdout)"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command writes down the placet manual in texinfo format."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error=TCL_OK;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  std::ofstream file;
  if (file_name)
    file.open(file_name);
  std::ostream &stream = file_name==NULL ? std::cout : file;
  stream << 
//            "\\input texinfo   @c -*-texinfo-*-\n"
//            "@node Top\n"
            "@top PLACET Documentation\n";
  std::sort(help_head.begin(), help_head.end(), cmpCOMMAND_HELPER());
  CATEGORY_KEY last_category(-1,-1);
  for (std::vector<COMMAND_HELPER>::const_iterator itr=help_head.begin(); itr!=help_head.end(); ++itr) {
    const COMMAND_HELPER &cmd = *itr;
    if (cmd.get_category_key() != last_category) {
      const char *title = section_titles[CATEGORY_KEY(cmd.category, cmd.subcategory)];
      if (title) {
        stream << "@section " << section_titles[CATEGORY_KEY(cmd.category, cmd.subcategory)] << std::endl;
      }
      last_category = cmd.get_category_key();
    }
    stream << (*itr).get_help_texinfo() << std::endl;
  }    
  stream << "@bye\n";
  return TCL_OK;
}
