#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define NSTART 100000
#define NBUF 1024

int (cmp)(const void *p1, const void *p2)
{
    double *d1,*d2;
    d1=(double*)p1;
    d2=(double*)p2;
    if(d1[0]<d2[0]) {
	return -1;
    }
    else {
	if(d1[0]>d2[0]) {
	    return 1;
	}
    }
    return 0;
}

int main (int argc, char ** argv)
{
  FILE *f;
  double *x,*y,xdiff=0.2,ydiff=0.02;
  int n=0,nmax=NSTART,i,j,jmax=0;
  char buffer[NBUF],*point;

  x=(double*)malloc(sizeof(double)*nmax);
  y=(double*)malloc(sizeof(double)*nmax);
  if (argc==3) f=fopen(argv[1],"r");
  else f=fopen("electron.ini","r");
  if(!f) {
    fprintf(stderr,"ERROR: electron.ini not found\n");
    exit(1);
  }
  while ((point=fgets(buffer,NBUF,f))) {
    strtod(buffer,&point);
    x[n]=strtod(point,&point);
    y[n]=strtod(point,&point);
    n++;
    if (n>=nmax) {
      nmax*=2;
      x=(double*)realloc(x,sizeof(double)*nmax);
      y=(double*)realloc(y,sizeof(double)*nmax);
    }
  }
  fclose(f);
  if (argc==3) f=fopen(argv[2],"r");
  else f=fopen("positron.ini","r");
  if(!f) {
    fprintf(stderr,"ERROR: positron.ini not found\n");
    exit(1);
  }
  while ((point=fgets(buffer,NBUF,f))) {
    strtod(buffer,&point);
    x[n]=strtod(point,&point);
    y[n]=strtod(point,&point);
    n++;
    if (n>=nmax) {
      nmax*=2;
      x=(double*)realloc(x,sizeof(double)*nmax);
      y=(double*)realloc(y,sizeof(double)*nmax);
    }
  }
  fclose(f);
  qsort(x,n,sizeof(double),cmp);
  qsort(y,n,sizeof(double),cmp);
  printf("%g %g\n",1e3*x[(int)(n/100.0)],1e3*x[n-1-(int)(n/100.0)]);
  printf("%g %g\n",1e3*y[(int)(n/100.0)],1e3*y[n-1-(int)(n/100.0)]);

  j=0;
  i=1;
  while (i<n) {
    for (;i<n;++i) {
      if (x[i]-x[j]>xdiff) break;
    }
    j++;
  }
  printf("%g %g %d\n",x[j],x[j]+xdiff,jmax);  
  j=0;
  i=1;
  while (i<n) {
    for (;i<n;++i) {
      if (y[i]-y[j]>ydiff) break;
    }
    j++;
  }
  printf("%g %g %d\n",y[j],y[j]+ydiff,jmax);  
  return 0;
}
