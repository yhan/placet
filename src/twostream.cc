#include <cstdlib>
#include <cmath>
#include <vector>
#include <tcl.h>
#include <tk.h>
 
#include "placet.h"
#include "structures_def.h"
 
#include "lattice.h"
#include "girder.h"
#include "twostream.h"
#include "select.h"
#include "random.hh"
 
class ION : public PARTICLE {
  int s;
public:
  void set_slice(int n) {s=n;};
  int slice() {return s;}
};
 
extern INTER_DATA_STRUCT inter_data;
 
int tk_Twostream(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
                 char *argv[])
{
  int error;
  double length=0.0;
  TWOSTREAM *element;
  Tk_ArgvInfo table[]={
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"length of the cavity in meter"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Twostream>",TCL_VOLATILE);
    return TCL_ERROR;
  }
 
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
 
  element=new TWOSTREAM(length);
  inter_data.girder->add_element(element);
  return TCL_OK;
}
 
void TWOSTREAM::step_4d_0(BEAM* /*beam*/)
{
}
 
void TWOSTREAM::step_4d(BEAM* beam)
{
  int np=100;
  ION *ion=(ION*)alloca(sizeof(ION)*np);
  int n=beam->slices;
 
  std::vector<double> weights;
 
  for (int i=0;i<n;++i)
  {
  weights.push_back(beam->particle[i].wgt);
  }
 
  Select.DiscreteDistributionSet(weights);
 
  for(int i=0;i<np;++i){
    n=Select.DiscreteDistribution();
    one_particle(beam->sigma_xx+n,beam->sigma_xy+n,beam->sigma+n,
                 beam->particle+n,ion+i);
  }
}
 
void TWOSTREAM::list(FILE* f)const
{
  fprintf(f,"Twostream -length %g\n",geometry.length);
}
