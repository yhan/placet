#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beam.h"
#include "beamline.h"
#include "particle.h"

/* Function prototypes */

#include "placet_tk.h"
#include "placeti3.h"
#include "beamenergy.h"

extern INTER_DATA_STRUCT inter_data;

/**********************************************************************/
/*                      beamenergy (not a command)                    */
/**********************************************************************/

double beamenergy(BEAM *beam)
{
  double esum=0.0,wsum=0.0;
  PARTICLE *particle;
  int i,n;
  n=beam->slices;
  particle=beam->particle;
  for (i=0;i<n;i++){
    esum+=(*particle).energy*(*particle).wgt;
    wsum+=(*particle).wgt;
    particle++;
  }
  return esum/wsum;
}

/**********************************************************************/
/*                      beam_energy_plot (not a command)              */
/**********************************************************************/

void beam_energy_plot(BEAMLINE *beamline,BEAM *beam,char *fname)
{
  BEAM *tb;
  int i,n;
  double s=0.0,e;
  FILE *file=open_file(fname);
  tb=bunch_remake(beam);
  beam_copy(beam,tb);
  n=beamline->n_elements;
  e=beamenergy(tb);
  fprintf(file,"%g %g\n",s,e);
  for (i=0;i<n;i++){
    beamline->element[i]->track_0(tb);
    //    element_step_4d_0(beamline->element[i],tb);
    e=beamenergy(tb);
    s+=beamline->element[i]->get_length();
    fprintf(file,"%g %g\n",s,e);
  }
  beam_delete(tb);
  close_file(file);
}

/**********************************************************************/
/*                      beam_energy_profile_plot (not a command)      */
/**********************************************************************/

void beam_energy_profile_plot(BEAM *beam,char *fname)
{
  int nb,n,i,j;
  double w,e,emin,emax;
  PARTICLE *particle;
  FILE *file=open_file(fname);
  particle=beam->particle;
  nb=beam->bunches;
  n=beam->slices/nb;
  for (j=0;j<nb;j++){
    w=0.0;
    e=0.0;
    emin=1e300;
    emax=-1e300;
    for(i=0;i<n;i++){
      e+=particle->energy*particle->wgt;
      w+=particle->wgt;
      emin=std::min(emin,particle->energy);
      emax=std::max(emax,particle->energy);
      particle++;
    }
    fprintf(file,"%d %g %g %g %g\n",j,e/w,emin,emax,w);
  }
  close_file(file);
}


/******   Tcl/Tk commands   ******/

/**********************************************************************/
/*                      BeamEnergyPlot                                */
/**********************************************************************/

int tk_BeamEnergyPlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error;
  char *file_name=NULL,*beam_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  if (beam_name==NULL){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"you have to specify a beam.\n",NULL);
    return TCL_ERROR;
  }
  BEAM* beam=get_beam(beam_name);
  beam_energy_plot(inter_data.beamline,beam,file_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamEnergyPrint                               */
/**********************************************************************/

int tk_BeamEnergyPrint(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[])
{
  int error;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  beamenergy(inter_data.bunch);
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamEnergyProfilePlot                         */
/**********************************************************************/

int tk_BeamEnergyProfilePlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			     char *argv[])
{
  int error;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  beam_energy_profile_plot(inter_data.bunch,file_name);
  return TCL_OK;
}

