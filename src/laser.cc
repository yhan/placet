#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <vector>
#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"
#include "select.h"
#include "placet_tk.h"

#include "random.hh"


int
Tcl_LaserMeasure(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  int n,n1,np,bunch=0;
  double sumw=0.0,sum=0.0,sx=0.0,sy=0.0,x0=0.0,y0=0.0;
  PARTICLE p;
  BEAM *b;
  Tk_ArgvInfo table[]={
    {(char*)"-particles",TK_ARGV_INT,(char*)NULL,
     (char*)&np,
     (char*)"Number of particle to store"},
    {(char*)"-bunch",TK_ARGV_INT,(char*)NULL,
     (char*)&bunch,
     (char*)"Bunch to be used"},
    {(char*)"-sigma_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&sx,
     (char*)"RMS x-dimension of the laser beam"},
    {(char*)"-sigma_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&sy,
     (char*)"RMS y-dimension of the laser beam"},
    {(char*)"-x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&x0,
     (char*)"x-position of the centre of the laser wire"},
    {(char*)"-y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&y0,
     (char*)"y-position of the centre of the laser wire"},
    // {(char*)"-seed",TK_ARGV_INT,(char*)NULL,
    //  (char*)&seed,
    //  (char*)"Seed to be used"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <LaserMeasure>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  b=inter_data.bunch;
  n=b->slices_per_bunch*b->macroparticles;
  n1=n*bunch;
  if (bunch>b->bunches) {
    char buf[100];
    snprintf(buf,100,"beam contains only %d bunches\nbunch %d does not exist",b->bunches,bunch);
    Tcl_SetResult(interp,buf,TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (b->particle_beam) {
    for (int i=0;i<n;++i) {
      sumw+=b->particle[i+n1].wgt;
      sum+=b->particle[i+n1].wgt*exp(-0.5*((b->particle[i+n1].x-x0)
				       *(b->particle[i+n1].x-x0)/(sx*sx)
				       +(b->particle[i+n1].y-y0)
				       *(b->particle[i+n1].y-y0)/(sy*sy)));
    }
    sum/=sumw;
    sum*=np;
  }
  else {
    /* second solution:
      for (int i=0;i<n;++i) {
	sumw+=b->particle[i+n1].wgt;
	sx2=sx*sx+b->sigma_xx[i+n1].r11;
	sy2=sy*sy+b->sigma[i+n1].r11;
	sum+=b->particle[i+n1].wgt*exp(-0.5*((b->particle[i+n1].x-x0)
					   *(b->particle[i+n1].x-x0)/sx2
					   +(b->particle[i+n1].y-y0)
					   *(b->particle[i+n1].y-y0)/sy2));
      }
    sum/=sumw;
    sum*=np;
     */
    //  if(seed) Select.SetSeed(seed);
    std::vector<double> weights;
    
    for(int i=0; i<n; i++)
      {
	weights.push_back(fabs(b->particle[i+n1].wgt));
      }
    Select.DiscreteDistributionSet(weights); 
    for (int i=0;i<np;++i){
      n=Select.DiscreteDistribution()+n1;
      one_particle(b->sigma_xx+n,b->sigma_xy+n,b->sigma+n,b->particle+n,&p);
      if (sx>0.0) {
	  if (sy>0.0) {
	      if (Select.Uniform()<exp(-0.5*((p.x-x0)*(p.x-x0)/(sx*sx)
				      +(p.y-y0)*(p.y-y0)/(sy*sy)))) {
		  sum+=1.0;
	      }
	  }
      }
      else {
	  if (sy>0.0) {
	      if (Select.Uniform()<exp(-0.5*(p.y-y0)*(p.y-y0)/(sy*sy))) {
		  sum+=1.0;
	      }
	  }
      }
    }
  }
  char buf[20];
  snprintf(buf,20,"%g",sum);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

void
LaserMeasure_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"LaserMeasure",Tcl_LaserMeasure,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
}
