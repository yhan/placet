#include <sstream>
#include <iomanip>
#include <typeinfo>
#include <tk.h>

#include "photon.h"

#include "structures_def.h"
#include "placet.h"
#include "placet_tk.h"
#include "placeti3.h"
#include "beam.h"
#include "beamline.h"
#include "cavity.h"
#include "multipole.h"
#include "quadrupole.h"
#include "sbend.h"

using namespace std;

extern INTER_DATA_STRUCT inter_data;
PHOTON *ptracker=NULL;

PHOTON::PHOTON(const char* fname, double r)
{
  nphot = 0;
  vphoton.clear();
  vzpos.clear();
  fsample = 1.;
  rmax = r;
  zcur = 0.;
  icur = 0;
  fulltracking = false;
  of.open(fname);
  outDir = "out_photon/"; // write the photon results from fulltracking into a directory out_photon in the current running directory
  string command = "mkdir "+ outDir;
  system(command.c_str());
}

PHOTON::~PHOTON()
{
  of.close();
}

int PHOTON::add_photon(double x, double y, double xp, double yp, double energy,
		       double z, ELEMENT *elem)
{
  if(fsample<1. && fsample>0.)
  {
    double ran = rand()/(double)RAND_MAX;
    if (ran > fsample) return 0;
  }

  // particles coordinates are in the element frame in [m]
  PARTICLE photon   = fill_part(x,y,xp,yp,energy);

  if (SBEND *sbend=elem->sbend_ptr())
  {
    placet_cout<< DEBUG << "sbend "<<sbend->e1<<"   "<<sbend->e1<<endmsg;
    // changes to lattice frame
    double t2=0.0,offsetx2,offsety2;
    double const eps=1e-200;

    //    offsety=0.5*sbend->get_length()*sbend->offset.yp-sbend->offset.y;
    offsety2=0.5*sbend->get_length()*sbend->offset.yp+sbend->offset.y;
#ifdef TWODIM
    //    offsetx=0.5*sbend->get_length()*sbend->offset.xp-sbend->offset.x;
    offsetx2=0.5*sbend->get_length()*sbend->offset.xp+sbend->offset.x;
#endif
    if ((fabs(sbend->e1)>eps)||(fabs(sbend->e2)>eps)) {
      //      t1=tan(sbend->e1)*sbend->angle0/sbend->get_length()*sbend->ref_energy;
      t2=tan(sbend->e2)*sbend->angle0/sbend->get_length()*sbend->ref_energy;
    }
    photon.xp  = (photon.xp+t2/photon.energy*photon.x)*1e6;
    photon.yp  = (photon.yp-t2/photon.energy*photon.y)*1e6;
    photon.x  *= 1.e6;
    photon.y  *= 1.e6;
    photon.x  += offsetx2;
    photon.xp += sbend->offset.xp;
    photon.y  += offsety2;
    photon.yp += sbend->offset.yp;
  }
  else if (QUADRUPOLE* quad=elem->quad_ptr()) {
    // changes to lattice frame: do not consider quad rotation:
    double offsety=0.5*quad->get_length()*quad->offset.yp+quad->offset.y;
    photon.y  += offsety;
    photon.yp += quad->offset.yp;
#ifdef TWODIM
    double offsetx=0.5*quad->get_length()*quad->offset.xp+quad->offset.x;
    photon.x  += offsetx;
    photon.xp += quad->offset.xp;
#endif
  }
  else if (dynamic_cast<MULTIPOLE*>(elem)!=NULL) {
    MULTIPOLE* mult = (MULTIPOLE*) elem;
    // changes to lattice frame: do not consider mult rotation:
    double offsety=0.5*mult->get_length()*mult->offset.yp+mult->offset.y;
    photon.y  += offsety;
    photon.yp += mult->offset.yp;
#ifdef TWODIM
    double offsetx=0.5*mult->get_length()*mult->offset.xp+mult->offset.x;
    photon.x  += offsetx;
    photon.xp += mult->offset.xp;
#endif
  }
  track_elem(photon, elem, z);
  //  vphoton.insert(vphoton.begin(),photon);
  vphoton.push_back(photon);
  vzpos.push_back(z);
  nphot ++;
  return 0;
}
PARTICLE PHOTON::fill_part(double x, double y, double xp, double yp, double energy)
{
  PARTICLE photon;
  photon.energy = energy;
  photon.x      = x;
  photon.y      = y;
  photon.xp     = xp;
  photon.yp     = yp;
  return photon;
}
int PHOTON::track_elem(PARTICLE& p, ELEMENT* elem, double z)
{
  if (SBEND* sbend = elem->sbend_ptr()) {
    double alpha = sbend->angle0;
    double offsety = 0.5*sbend->get_length()*sbend->offset.yp-sbend->offset.y;
    double offsety2=0.5*sbend->get_length()*sbend->offset.yp+sbend->offset.y;
#ifdef TWODIM
    double offsetx=0.5*sbend->get_length()*sbend->offset.xp-sbend->offset.x;
    double offsetx2=0.5*sbend->get_length()*sbend->offset.xp+sbend->offset.x;
#endif
    double t1=tan(sbend->e1)*sbend->angle0/sbend->get_length()*sbend->ref_energy;
    double t2=tan(sbend->e2)*sbend->angle0/sbend->get_length()*sbend->ref_energy;
    p.x  += offsetx;
    p.xp -= sbend->offset.xp;
    p.y  += offsety;
    p.yp -= sbend->offset.yp;
    p.xp += t1*p.x/p.energy;
    p.yp -= t1*p.y/p.energy;

    p.x  += ((sbend->get_length()-z)*((0.5*alpha*alpha) + (alpha*p.xp*1e-6)/(alpha+p.xp*1e-6)))*1e6;
    p.y  += p.yp*(sbend->get_length()-z);

    p.xp += t2/p.energy*p.x;
    p.yp -= t2/p.energy*p.y;
    p.x  += offsetx2;
    p.xp += sbend->offset.xp;
    p.y  += offsety2;
    p.yp += sbend->offset.yp;
  }
  else {
    p.x += p.xp*(elem->get_length()-z);
    p.y += p.yp*(elem->get_length()-z);
  }
  p.x = min (p.x,25000.);
  p.y = min (p.y,25000.);

  return 0;
}

int PHOTON::add_zelem(double l)
{
  zcur +=l;
  icur ++;
  return 0;
}

int PHOTON::track(ELEMENT* elem)
{
  int nloss = 0;
  double eloss = 0.;
  if(fulltracking==true && vphoton.size()!=0 )
  {
    ostringstream thei; thei << icur;
    string photon_out_name=string(outDir)+"/photon_"+thei.str()+".txt"; // photon_nn.txt
	ofstream photon_out(photon_out_name.c_str());
	//	placet_cout << DEBUG << "write to " << photon_out_name << " with htgen.thebeam->slices=" << htgen.thebeam->slices << endmsg;
	photon_out << "# " << elem->get_name() << "  " << zcur << "  " << vphoton.size() << "  " << elem->get_aperture_type() << "  " << elem->get_aperture_x() << "   " << elem->get_aperture_y() << '\n';
	for(unsigned int i = 0; i<vphoton.size(); i++)
	{
	  photon_out << right << (vphoton[i]).energy << setw(14) << copysign(min(fabs(vphoton[i].x),50000.),vphoton[i].x) << setw(14) << copysign(min(fabs(vphoton[i].y),50000.),vphoton[i].y) << setw(14) << (vphoton[i]).z
		<< setw(14) << (vphoton[i]).xp << setw(14) << (vphoton[i]).yp << setw(14) << zcur << setw(10) << i << '\n';
	}
	photon_out.close();
  }
  // delete any lost photons
  for(vector<PARTICLE>::iterator it=vphoton.begin(); it!=vphoton.end(); )
  {
    track_elem((*it), elem);
    if (is_lost((*it),elem)==true)
    {
      nloss++;
      eloss += (*it).energy;
      it = vphoton.erase(it);
    }
    else ++it;
  }
  add_zelem(elem->get_length());
  if(nloss)
  {
    of<<icur<<"  "<<zcur<<"  "<<typeid(*elem).name()<<"  "<<nphot<<" "<<nloss<<"  "<<eloss<<endl;
  }
  return 0;
}

bool PHOTON::is_lost(PARTICLE p, ELEMENT* elem)
{
  if(elem->get_aperture_type_str()=="none")
  {
    elem->set_aperture_type_str("circular");
    elem->set_aperture_dim(rmax);
  }
  return elem->is_lost(p);
}

int PHOTON::full_tracking(bool flg)
{
  fulltracking = flg;
  return 0;
}

int PHOTON::set_samplingfraction(double f)
{
  fsample = f;
  return 0;
}

// -------------------------------------------------------
// Interpreter functions
// -------------------------------------------------------
int tk_TrackPhoton(ClientData /*clientdata*/, Tcl_Interp *interp,int argc,char *argv[])
{
  int error;
  char *file_name=NULL;
  double aperture=1., fsample=1.;
  int flg_fulltracking=0;
  // Tcl_HashEntry *entry;
  // BEAM *beam=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-aperture",TK_ARGV_FLOAT,(char*)NULL,(char*)&aperture,
     (char*)"Default aperture in m"},
    {(char*)"-sampling",TK_ARGV_FLOAT,(char*)NULL,(char*)&fsample,
     (char*)"Sampling fraction for writing"},
    {(char*)"-fulltracking",TK_ARGV_INT,(char*)NULL,(char*)&flg_fulltracking,
     (char*)"fulltracking: stored into photon directory"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TrackBackground",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (inter_data.track_photon) {
    Tcl_SetResult(interp,"Photon tracking already on",TCL_VOLATILE);
    return TCL_ERROR;
  }
  inter_data.track_photon = 1;

  ptracker = new PHOTON(file_name,aperture*1.e6);
  // JS: should be deleted at end to close file properly
  if(flg_fulltracking) ptracker->full_tracking(true);
  if(fsample>0. && fsample<1.) ptracker->set_samplingfraction(fsample);
  return TCL_OK;
}

// -------------------------------------------------------
int TrackPhoton_Init(Tcl_Interp *interp)
{
  Placet_CreateCommand(interp,"TrackPhoton",tk_TrackPhoton,NULL,NULL);
  return 0;
}
