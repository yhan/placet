#include <cstdio>
#include <poll.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "parallel_tracking.hh"
#include "placet.h"
#include "placeti3.h"
#include "placet_prefix.hh"
#include "track.h"

#include "structures_def.h"
#include "quadrupole.h"
#include "multipole.h"
#include "parallel.h"
#include "dipole.h"
#include "sbend.h"
#include "drift.h"
#include "bpm.h"

#include "beam.h"
#include "element_id.hh"

ParallelTracking &ParallelTracking::operator<<(const QUADRUPOLE &quadrupole )   { ififo << ELEMENT_ID::QUADRUPOLE << quadrupole; return *this; }
ParallelTracking &ParallelTracking::operator<<(const MULTIPOLE &multipole )     { ififo << ELEMENT_ID::MULTIPOLE << multipole; return *this; }
ParallelTracking &ParallelTracking::operator<<(const DIPOLE &dipole )           { ififo << ELEMENT_ID::DIPOLE << dipole; return *this; }
ParallelTracking &ParallelTracking::operator<<(const SBEND &sbend )             { ififo << ELEMENT_ID::SBEND << sbend; return *this; }
ParallelTracking &ParallelTracking::operator<<(const DRIFT &drift )             { ififo << ELEMENT_ID::DRIFT << drift; return *this; }
ParallelTracking &ParallelTracking::operator<<(const BPM &bpm )
{
  ififo << ELEMENT_ID::BPM << bpm; 
  bpms.push(const_cast<BPM*>(&bpm)); ///< later will be modified....
  return *this;
}

bool ParallelTracking::startTracking(const BEAM &beam )
{
  tracking = true;
  return true;
}

void ParallelTracking::endTracking(BEAM &beam )
{
  tracking = false;
  END_PARALLEL end;
  ififo << ELEMENT_ID::SPECIAL << static_cast<const ELEMENT &>(end);
  ififo << beam << Stream::flush;
  ofifo >> beam;
  std::vector<std::pair<double,double> > bpm_readings;
  ofifo >> bpm_readings;
  for (size_t i=0; i<bpm_readings.size(); i++) {
    BPM *bpm_ptr = bpms.front();
    bpm_ptr->set_x_position(bpm_readings[i].first);
    bpm_ptr->set_y_position(bpm_readings[i].second);
    bpm_ptr->set_err_x(0.0); ///< the random error due to the resolution comes from the MPI itself
    bpm_ptr->set_err_y(0.0);
    bpms.pop();
  }
}

double test_no_correction_parallel(BEAMLINE *beamline,
				   BEAM *bunch0, int niter,
				   void (*survey)(BEAMLINE*),
				   const char *name, const char *format, const char *mpirun_cmd )
{
  ParallelTracking parallel_tracking(mpirun_cmd);
  double esumy=0.0;
  if (parallel_tracking) {
    double esumx=0.0,emittx=0.0,emitty=0.0,esumy2=0.0,esumx2=0.0;
    BEAM *tb=bunch_remake(bunch0);
    if (name){
      emitt_data.store_init(beamline->n_quad);
    }
    for (int i=0;i<niter;i++) {
      survey(beamline);
      if ((i==0)||(name)){
	beam_copy(bunch0,tb);
      } else {
	bunch_copy_0(bunch0,tb);
      }
      bunch_track_emitt_start();
      bunch_track_emitt_parallel(parallel_tracking,beamline,tb,0,beamline->n_elements);
      bunch_track_emitt_end(tb,beamline->get_length());
      emitty=emitt_y(tb);
      esumy=(esumy*i+emitty)/(double)(i+1);
      esumy2=(esumy2*i+emitty*emitty)/(double)(i+1);
      double semy = sqrt(std::max(0.0,(esumy2-esumy*esumy)/(double)(i+1)));
#ifdef TWODIM
      emittx=emitt_x(tb);
      esumx=(esumx*i+emittx)/(double)(i+1);
      esumx2=(esumx2*i+emittx*emittx)/(double)(i+1);
      double semx = sqrt(std::max(0.0,(esumx2-esumx*esumx)/(double)(i+1)));
#endif
      placet_cout << VERBOSE << "test_no_correction_parallel" << std::endl;
      if (i>0) placet_cout << INFO << " iteration: " << i << std::endl;
      placet_cout << INFO << "emitt_x " << emittx;
      if (i>0) placet_cout << INFO << " mean " << esumx << " error of mean " << semx;
      placet_cout << std::endl;
      placet_cout << INFO << "emitt_y " << emitty;
      if (i>0) placet_cout << INFO << " mean " << esumy << " error of mean " << semy;
      placet_cout << endmsg;
    }
    if(name){
      emitt_data.print(name,format);
      emitt_data.store_delete();
    }
    beam_delete(tb);
  } else 
    placet_cout << ERROR << "Could not track using MPI" << endmsg;
  return esumy;
}
  
ParallelTracking::ParallelTracking(const char *mpirun_cmd ) : process(NULL), tracking(false), up_and_running(false)
{
  // creates two named pipes, one for input, one for output
  const char *tmpdir = getenv("TMPDIR");
  if (!tmpdir) tmpdir = "/tmp/";
  std::string ififoname_mask = std::string(tmpdir) + std::string("/placet-mpi-ififo.XXXXXX");
  std::string ofifoname_mask = std::string(tmpdir) + std::string("/placet-mpi-ofifo.XXXXXX");
  const int length = ififoname_mask.length(); // length is the same for both
  char ififoname_str[length+1];
  char ofifoname_str[length+1];
  ififoname_mask.copy(ififoname_str,length,0);
  ofifoname_mask.copy(ofifoname_str,length,0);
  ififoname_str[length] = '\0';
  ofifoname_str[length] = '\0';
  int ifd = mkstemp(ififoname_str);
  if (ifd != -1) {
    unlink(ififoname_str);
    close(ifd);
    int ofd = mkstemp(ofifoname_str);
    if (ofd != -1) {
      unlink(ofifoname_str);
      close(ofd);
      if (mkfifo(ofifoname_str, 0666) == 0) {
	if (mkfifo(ififoname_str,  0666) == 0) {
	  // it parses mpirun to retrieve the command name, and the number of processes (default 4)
	  if (!mpirun_cmd)
	    mpirun_cmd = "mpirun -np 4";
	  char str[strlen(mpirun_cmd)+1];
	  strncpy(str, mpirun_cmd, sizeof str);
	  // building argv
	  for (const char *tok = strtok(str, " "); tok; tok = strtok(NULL, " "))
	    argv.push_back(tok);
	  argv.push_back(get_placet_sharepath("modules/mpi_6d-tracking-core"));
	  argv.push_back(ififoname_str);
	  argv.push_back(ofifoname_str);
	  // building _argv
	  const char *_argv[argv.size() + 1];
	  for (size_t i=0; i<argv.size(); i++)
	    _argv[i] = argv[i].c_str();
	  _argv[argv.size()] = NULL;
	  if (process = new Piped_process(_argv[0], const_cast<char * const *>(_argv))) {
	    ofifoname = ofifoname_str;
	    ififoname = ififoname_str;
	    ofifo.open(ofifoname_str, O_RDONLY | O_NONBLOCK);
	    ififo.open(ififoname_str, O_RDWR | O_NONBLOCK);
	    // checks, three times, if MPI starts within 5 seconds
	    pollfd fds = { ofifo.fd(), POLLIN, 0 };
	    for (size_t attempt=0; attempt<3; attempt++) {
	      int p = poll(&fds, 1, 5000);
	      if (p == -1) {
		perror("poll");
		break;
	      } else if (p == 0) {
		placet_cout << ERROR << "MPI subprocess not responding ... (attempt " << attempt+1 << "/3)" << endmsg;
	      } else {
		int oflags = fcntl(ofifo.fd(), F_GETFL, 0);
		int iflags = fcntl(ififo.fd(), F_GETFL, 0);
		fcntl(ofifo.fd(), F_SETFL, (oflags == -1 ? 0 : oflags) & (~O_NONBLOCK));
		fcntl(ififo.fd(), F_SETFL, (iflags == -1 ? 0 : iflags) & (~O_NONBLOCK));
		std::string handshake_str; ofifo >> handshake_str;
		if (handshake_str == "Hello, PLACET!") {
		  up_and_running = true;
		  return;
		}
	      }
	    }
	    placet_cout << ERROR << "Failed to open MPI subprocess" << endmsg;
	    process->kill(SIGTERM);
	    process->wait();
	    delete process;
	  } else placet_cout << ERROR << "Failed to create MPI subprocess" << endmsg;
	  unlink(ififoname.c_str());
	} else placet_cout << ERROR << "Failed to create PLACET to MPI pipe" << endmsg;
	unlink(ofifoname.c_str());
      } else placet_cout << ERROR << "Failed to create MPI to PLACET pipe" << endmsg;
    } else placet_cout << ERROR << "Failed to create a temp file" << endmsg;
  } else placet_cout << ERROR << "Failed to create a temp file" << endmsg;
}

ParallelTracking::~ParallelTracking()
{
  if (up_and_running) {
    if (process) {
      process->kill(SIGTERM);
      process->wait();
      delete process;
    }
    unlink(ififoname.c_str());
    unlink(ofifoname.c_str());
  }
}
