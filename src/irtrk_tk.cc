// this file includes all the functions needed to use and build an Interaction Region
#include "irtracking.h"
#include "track.h"
#include "placet_tk.h"
#include "placeti3.h"
#include <tcl.h>
#include <tk.h>
#include <vector>

using namespace std;

void test_int_region(BEAMLINE *beamline,BEAM *bunch0,BEAM *probe,
                     int niter,void (*survey)(BEAMLINE*),char *emitt_fname, const char *format, char *fmapname,
                     double length,double angle,double step, int synr, int wsynr, int write_first, int backward)
{
    // Sanity checks..
    if (niter<=0) return;
    // End sanity checks..

    // beamline length = integration length
    double int_length = (length==0.0) ? (beamline->get_length()) : (length);
    // build boundlist vector
    vector<double> blist;
    int count_elem_list=0;
    double tmp_len=0.0;
    int nele=0;
    do {
        if(! (beamline->element[nele]->is_tclcall()) ) {
            if( beamline->element[nele]->get_length()> 0.0 ) {
                blist.push_back( beamline->element[nele]->get_s() );
                count_elem_list++;
                tmp_len+=beamline->element[nele]->get_length();
            }
        }
        nele++;
        //    count_elem++;
    } while( tmp_len < int_length && nele<beamline->n_elements);
    if(get_verbosity()>=VERBOSE) {
        placet_cout << VERBOSE << " test_int_region s of IP " << beamline->element[beamline->n_elements-1]->get_s() << endmsg;
        placet_cout << VERBOSE << " test_int_region s of last element " << beamline->element[nele]->get_s() << endmsg;
        placet_cout << VERBOSE << " test_int_region integration length " << tmp_len << endmsg;
        placet_cout << VERBOSE << " test_int_region # of elements " << nele << endmsg;
        placet_cout << VERBOSE << " test_int_region # of good elements " << count_elem_list << endmsg;
        placet_cout << VERBOSE << " test_int_region blist size " << blist.size() << endmsg;
    }

    BEAM *track_bunch=NULL,*probe_bunch=NULL;

    int i;
    double esumy=0.0,esumx=0.0,emittx=0.0,emitty=0.0,esumy2=0.0,esumx2=0.0;
    track_bunch=bunch_remake(bunch0);
    DetectorSolenoid *map;
    map=NULL;
    if ( probe ) probe_bunch=bunch_remake(probe);
    if (emitt_fname) {
        emitt_data.store_init(beamline->n_quad);
    }
    if(fmapname!=NULL) {
      // read map
      map = new DetectorSolenoid(angle);
      map->ReadMapFile1D(fmapname);
    }
    for (i=0; i<niter; i++) {
      survey(beamline);
      if ((i==0)||(emitt_fname)) {
	beam_copy(bunch0,track_bunch);
      }
      else {
	bunch_copy_0(bunch0,track_bunch);
      }
      IRTracking *trk;
      if(fmapname!=NULL){
	trk = new IRTracking(step, int_length, blist,map);
      } else {
	trk = new IRTracking(step, int_length, blist);
      }
      if(write_first) {
	trk->WriteFirstParticle();
      }
      trk->SetSynrad(synr,wsynr);
      if (backward) trk->SetBackwardsTracking(true);
      trk->track(track_bunch,beamline);
      delete trk;
      emitty=emitt_y(track_bunch);
      if (probe!=NULL) {
	beam_copy(probe,probe_bunch);
	bunch_track_line_emitt_new(beamline,probe_bunch);
	emitty=emitt_y(probe_bunch);
      }
      esumy=(esumy*i+emitty)/(double)(i+1);
      esumy2=(esumy2*i+emitty*emitty)/(double)(i+1);
#ifdef TWODIM
      emittx=emitt_x(track_bunch);
      esumx=(esumx*i+emittx)/(double)(i+1);
      esumx2=(esumx2*i+emittx*emittx)/(double)(i+1);
      placet_printf(INFO, "emitt_x %d %.15g %.15g %.15g\n",i,emittx,esumx,
        sqrt(max(0.0,(esumx2-esumx*esumx)/(double)(i+1))));
#endif
      placet_printf(INFO, "emitt_y %d %.15g %.15g %.15g\n",i,emitty,esumy,
        sqrt(max(0.0,(esumy2-esumy*esumy)/(double)(i+1))));
    }
    delete map;
    if(emitt_fname) {
      emitt_data.print(emitt_fname,format);
      emitt_data.store_delete();
    }
    beam_delete(track_bunch);
    if(probe_bunch) beam_delete(probe_bunch);
}


// ************************************************************//
// The create command for the Tcl Interface is in placet_tk.cc //
//*************************************************************//

int tk_TestIntRegion(ClientData clientdata,Tcl_Interp *interp,int argc,
                     char *argv[])
{
    int error;
    char *filename=NULL;
    double length=0.0, angle=0.0;
    int synr = 0;
    int wsynr = 0;
    int write_first=0;
    int backward=0;
    char *file_name=NULL;
    char *format = default_format;
    char *beamname=NULL,*survey_name,sv[]="Zero";
    double step=0.01; // default step size 1 cm
    void (*survey)(BEAMLINE*);
    //Tcl_HashEntry *entry;
    BEAM *beam=NULL;
    double bpm_res=0.0;
    int iter=1;
    Tk_ArgvInfo table[]=
    {
        {   (char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
            (char*)"Number of machines to simulate"
        },
        {   (char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beamname,
            (char*)"Name of the beam to be used for tracking"
        },
        {   (char*)"-survey",TK_ARGV_STRING,(char*)NULL,(char*)&survey_name,
            (char*)"Type of prealignment survey to be used"
        },
        {   (char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
            (char*)"Filename for the results defaults to NULL (no output)"
        },
	{
	  (char*)"-format",TK_ARGV_STRING,(char*)NULL,(char*)&format,
	  (char*)"emittance file format"
	},
        {   (char*)"-bpm_res",TK_ARGV_FLOAT,(char*)NULL,(char*)&bpm_res,
            (char*)"BPM resolution"
        },
        {   (char*)"-filename",TK_ARGV_STRING,(char*)NULL,(char*)&filename,
            (char*)"Name of the field map to use for the calculation"
        },
        {   (char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
            (char*)"Total length of the field map in z [m] "
        },
        {   (char*)"-angle",TK_ARGV_FLOAT,(char*)NULL,(char*)&angle,
            (char*)"Angle between the map and the beamline [rad] "
        },
        {   (char*)"-step",TK_ARGV_FLOAT,(char*)NULL,(char*)&step,
            (char*)"Integration step size [m] "
        },
        {   (char*)"-synrad",TK_ARGV_INT,(char*)NULL,(char*)&synr,
            (char*)"Synchrotron Radiation emission: 0 OFF (default); 1 ON "
        },
        {   (char*)"-wsynrad",TK_ARGV_INT,(char*)NULL,(char*)&wsynr,
            (char*)"Write synrad photons to file: 0 OFF (default); 1 ON "
        },
        {   (char*)"-writefirst",TK_ARGV_INT,(char*)NULL,(char*)&write_first,
            (char*)"Write coordinates of first particle to file singtrk.dat"
        },
        {   (char*)"-backward",TK_ARGV_INT,(char*)NULL,(char*)&backward,
            (char*)"Track the beam backwards through the lattice"
        },
        {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };

    survey_name=sv;
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
            !=TCL_OK)
    {
        return error;
    }

    beamline_survey_hook_interp=interp;
    if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

    if (argc!=1) {
        Tcl_SetResult(interp,"Too many arguments to TestIntRegion",
                      TCL_VOLATILE);
        return TCL_ERROR;
    }
    // check beam info
    if (beamname!=NULL) {
        beam=get_beam(beamname);
    }
    else {
        check_error(interp,argv[0],error);
        Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
        error=TCL_ERROR;
    }

    if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)) {
    }
    else {
        check_error(interp,argv[0],error);
        Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
    }

    if (error) return error;

    test_int_region(inter_data.beamline,beam,NULL,iter,survey,file_name,format,filename,length,angle,step,synr,wsynr,write_first,backward);


    return TCL_OK;
}

