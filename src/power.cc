#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"
#include "errf.h"
#include "power.h"

#define POWER_NSTEP 10000

// power new_point
inline double
new_point(double t[],double w[],int *i,int n,double t0,
	  double k,double beta,double l_cav, double lambda, double Q_long)
{
  int j;
  double sum=0.0, damp=0.0;

  // find first contributing leading particle [j] for which field has not drained out before particle [m] catches up
  while (((t0-t[*i])*beta>l_cav*(1.0-beta))&&((*i)<n)) {
    (*i)++;
  }
  j=*i;

     // damping term
     damp = exp(-PI*(t0-t[j]) / (lambda * Q_long * (1-beta)));

  // loop over wake contribution from leading particles [j]
  while ((t0-t[j]>0.0)&&(j<n)) {
    sum+=cos(k*(t0-t[j]))*w[j]*damp;
    j++;
  }
  return sum;
}


inline double
new_point(BEAM* beam,int *i,double t0,
	  double k,double beta,double l, double /*lambda*/, double /*Q_long*/)
{
  int j,n;
  double sum=0.0;

  n=beam->slices;
  t0*=1e6;l*=1e6;k*=1e-6;
  while (((t0-beam->z_position[*i])>l/(beta/(1.0-beta)))&&((*i)<n)) {
    (*i)++;
  }
  j=*i;
  while ((t0-beam->z_position[j]>0.0)&&(j<n)) {
    sum+=cos(k*(t0-beam->z_position[j]))*beam->particle[j].wgt;
    j++;
  }
  return sum;
}



// maxfield new_point
double
f_new_point(double t[],double w[],int *i,int n,double t0,
	    double k,double beta,double l_cav, int m, double dz, int n_slices, double lambda, double Q_long)
{
  int j;
  double sum=0.0,l_eff, t_eff, damp=0.0;

  // calculate new point ( phase*l_eff ) for particle [m] at point t0
 
  // NB: t[0] is FIRST (leading) particle in beam 


  // find first contributing leading particle [j] which field has not drained out before particle [m] catches up
  while ((l_cav<(t0-t[*i])/(1.0-beta)*beta)&&((*i)<n)) {
    (*i)++;
  }
  j=(*i);

  // loop over wake contribution from leading particles [j]
  while ((j<n)&&(t0-t[j]>0.0)) {
    // calculate the length in which partikle [k] is in the field (drain out)
    l_eff=l_cav-beta*(t0-t[j])/(1.0-beta);

    // modif in order to calculate drain out like PLACET (constant per bunch)
    //   ( this modif can be disabled by calling with "m = -1" )
     if( m != -1 ) {
       t_eff = dz * ((m/n_slices) - (j/n_slices));
       //placet_printf(INFO,"%d  %d  %g\n", m, j, t_eff);
       l_eff=l_cav-beta*(t_eff)/(1.0-beta);
     }
     
     // damping term
     damp = exp(-PI*(t0-t[j]) / (lambda * Q_long * (1-beta)));

    // sum of contribution for all leading particles
    sum+=cos(k*(t0-t[j]))*w[j]*l_eff*damp;
    j++;
  }
  
  // in case of self-wake, add contribution weighted by 0.5
  if (t0==t[j]) {
    l_eff=0.5*l_cav;
    sum+=cos(k*(t0-t[j]))*w[j]*l_eff*damp;
  }
  return sum;
}

//
// Wrapper for old method of calling  
//
double
f_new_point(double t[],double w[],int *i,int n,double t0,
	    double k,double beta,double l_cav, double lambda, double Q_long)
{
  return f_new_point(t,w,i,n,t0,k,beta,l_cav,-1,-1,-1, lambda, Q_long);
}

double power(FILE *file,double lambda,double beta,double l_cav,double r_over_q,
	     double charge,double bunchlength,double distance,
	     double x_min0,double x_max0,double *phase, double lambda_ext,
	     int n_bunches, int n_slices, double gauss_cut, double *t_list_first_bunch, double *w_list_first_bunch, int n_slices_list, double Q_long)
{
  double *t,*w,t0;
  double x_max,x_min,d_x,wgt_sum,tmp;
  int i,j=0,n;
  double k1,sum_s=0.0,sum_c=0.0;
  double k;
  
			     

  //
  // ALLOCATE MEMORY
  //

  if( n_slices_list > 0 ) {
    n_slices = n_slices_list;
  }
 
  t=(double*)malloc(sizeof(double)*n_bunches*n_slices);
  w=(double*)malloc(sizeof(double)*n_bunches*n_slices);
  //  f=(double*)malloc(sizeof(double)*n_bunches*n_slices);
  n=n_bunches*n_slices;

  // if no valid slice list have been given, create a gaussian distribution
  if( n_slices_list < 1 ) {

    //  CREATE FIRST BUNCH: GAUSSIAN BEAM - slices at distances t[0] to t[n_slices-1], with gaussian weight
    d_x=2.0*gauss_cut/(double)n_slices;
    x_max=-gauss_cut;
    wgt_sum=0.0;
    for (i=0;i<n_slices;i++){
      x_min=x_max;
      x_max=x_min+d_x;
      t[i]=0.5*(x_min+x_max)*bunchlength;
      w[i]=gauss_bin(x_min,x_max);
      wgt_sum+=w[i];
      // else: use the input slice distribution
    }
  } else {
    // CREATE FIRST BUNCH: SLICE LIST GIVEN
    wgt_sum=0.0;
    for (i=0;i<n_slices;i++){
      t[i] = t_list_first_bunch[i] / 1e6;
      w[i] = w_list_first_bunch[i];
      wgt_sum+=w[i];
    }
  }
    
  //placet_printf(INFO,"EA: t_slices_pos_list: %g %g\n", t[0],t[1] );
  //placet_printf(INFO,"EA: w_slices_pos_weight: %g %g\n", w[0], w[1]);
  //placet_printf(INFO,"EA: charge wgt_sum: %g\n", wgt_sum); // EA

  // CREATE 2nd to LAST BUNCH (equal to first, each displaced 'distance')
  for (i=1;i<n_bunches;i++){
    for (j=0;j<n_slices;j++){
      t[i*n_slices+j]=distance*i+t[j];
      w[i*n_slices+j]=w[j];
    }
  }


  //
  // CALCULATE POWER PRODUCTION, AND EXTRACT POWER AT SPAT. FREQUENCY k1
  //


  // extraction wavenumber
  k1=2.0*acos(-1.0)/lambda_ext;
  j=0;
  sum_s=0.0;
  sum_c=0.0;

  // cavity wavenumber
  k=2.0*acos(-1.0)/lambda;
  x_min=x_min0;
  x_max=x_max0;
  //    placet_printf(INFO,"EA POWSTART\n"); //EA
  for (i=0;i<POWER_NSTEP;i++){
    // point for calculated power production, goes from beam point xmin and xmax  (e.g. steady state part)
    t0=x_min+((x_max-x_min)*i/POWER_NSTEP);

    // calculate power produced at point t0
    tmp=new_point(t,w,&j,n,t0,k,beta,l_cav, lambda, Q_long)*1e-6*(charge*ECHARGE)*TWOPI
	*C_LIGHT/lambda*r_over_q/(1.0-beta);

    // extract frequency components 
    sum_s+=tmp*sin(k1*t0);
    sum_c+=tmp*cos(k1*t0);

    //placet_printf(INFO,"%d %g %g %g %g %g %g %g\n", i, t0, t[i], tmp, tmp*sin(k1*t0), tmp*cos(k1*t0), sum_s/(i+1),sum_c/(i+1)); //EA

  }
  sum_s/=POWER_NSTEP;
  sum_c/=POWER_NSTEP;

  if (file) {
    j=0;
    for (i=0;i<10000;i++){
      //      t0=2.0+0.01*0.01*i;
      t0=i*(x_max-x_min)/10000.0+x_min;
      tmp=new_point(t,w,&j,n,t0,k,beta,l_cav, lambda, Q_long)*1e-6*(charge*ECHARGE)*TWOPI
	*C_LIGHT/lambda*r_over_q/(1.0-beta);
      fprintf(file,"%g %g %g\n",t0,tmp,tmp-2.0*sin(k1*t0)*sum_s
	      -2.0*cos(k1*t0)*sum_c);
    }
  }

  *phase=atan2(sum_s,sum_c);
  free(t);
  free(w);
  return ((sum_s*sum_s+sum_c*sum_c)
	  *2.0*1e6*beta/(TWOPI/lambda*r_over_q));
}


double power(FILE *file,double lambda,double beta,double l,double r_over_q,
	     BEAM *beam,double x_min,double x_max,double *phase, double lambda_ext, double Q_long)
{
  double t0;
  double tmp;
  int i,j=0;
  double k1,sum_s=0.0,sum_c=0.0;
  double k;
  
  k1=2.0*acos(-1.0)/lambda_ext;
  j=0;
  sum_s=0.0;
  sum_c=0.0;
  k=2.0*acos(-1.0)/lambda;
  for (i=0;i<POWER_NSTEP;i++){
    t0=x_min+((x_max-x_min)*i/POWER_NSTEP);
    //    tmp=new_point(beam,&j,t0,k,beta,l)*1e-6*(charge*ECHARGE)
    tmp=new_point(beam,&j,t0,k,beta,l, lambda, Q_long)*1e-6*(ECHARGE)
	*TWOPI*C_LIGHT/lambda*r_over_q/(1.0-beta);
    sum_s+=tmp*sin(k1*t0);
    sum_c+=tmp*cos(k1*t0);
  }
  sum_s/=POWER_NSTEP;
  sum_c/=POWER_NSTEP;
  if (file) {
    j=0;
    for (i=0;i<=10000;i++){
      t0=i*(x_max-x_min)/10000.0+x_min;
      //      tmp=new_point(beam,&j,t0,k,beta,l)*1e-6*(charge*ECHARGE)
      tmp=new_point(beam,&j,t0,k,beta,l, lambda, Q_long)*1e-6*(ECHARGE)
	  *TWOPI*C_LIGHT/lambda*r_over_q/(1.0-beta);
      fprintf(file,"%g %g %g\n",t0,tmp,tmp-2.0*sin(k1*t0)*sum_s
	      -2.0*cos(k1*t0)*sum_c);
    }
  }
  *phase=atan2(sum_s,sum_c);
  return ((sum_s*sum_s+sum_c*sum_c)
	  *2.0*1e6*beta/(TWOPI/lambda*r_over_q));
}

double max_field(FILE *file,double lambda,double beta,double l_cav,
		 double r_over_q,double charge,double bunchlength,double dz,
		 int n_bunches, int n_slices, double gauss_cut, int bunch_drain_active, double *t_list_first_bunch, double *w_list_first_bunch, int n_slices_list, double Q_long)
{
  double *t,*w,t0;
  double x_max,x_min,d_x,wgt_sum,tmp,f_max=0.0;
  int i,j=0,n;
  double k;
  

  //
  // CREATE BEAM
  //

  if( n_slices_list > 0 ) {
    n_slices = n_slices_list;
  }
 
  t=(double*)malloc(sizeof(double)*n_bunches*n_slices);
  w=(double*)malloc(sizeof(double)*n_bunches*n_slices);
  //  f=(double*)malloc(sizeof(double)*n_bunches*n_slices);
  n=n_bunches*n_slices;

  // if no valid slice list have been given, create a gaussian distribution
  if( n_slices_list < 1 ) {
    
    //  CREATE FIRST BUNCH: GAUSSIAN BEAM - slices at distances t[0] to t[n_slices-1], with gaussian weight
    d_x=2.0*gauss_cut/(double)n_slices;
    x_max=-gauss_cut;
    wgt_sum=0.0;
    
    // Create bunch profile (slice time and weight) with length "bunchlength" and guassian distribution
    //   the bunch is divided in "n_slices" slices, each "dx" long
    //   gauss_cut sets the limit of the guassian distribution (e.g. ( -3sigma, 3sigma) )
    for (i=0;i<n_slices;i++){
      x_min=x_max;
      x_max=x_min+d_x;
      t[i]=0.5*(x_min+x_max)*bunchlength;   // t: point s of slice[i]  ( prop. to time)
      w[i]=gauss_bin(x_min,x_max);          // w: weight of slice[i]
      wgt_sum+=w[i];
    }
  } else {
    // CREATE FIRST BUNCH: SLICE LIST GIVEN
    wgt_sum=0.0;
    for (i=0;i<n_slices;i++){
      t[i] = t_list_first_bunch[i] / 1e6;
      w[i] = w_list_first_bunch[i];
      wgt_sum+=w[i];
    }
  }

  //placet_printf(INFO,"EA: charge wgt_sum: %g\n", wgt_sum); // EA



  // Create beam profile, by multiplying bunch profile (time <- time + n*d,  weight = weight )
  for (i=1;i<n_bunches;i++){
    for (j=0;j<n_slices;j++){
      t[i*n_slices+j]=dz*i+t[j];
      w[i*n_slices+j]=w[j];
    }
  }

  //
  // CALCULATE WAKE
  //

  
  j=0;
  k=2.0*acos(-1.0)/lambda;
  //x_min=x_min0;
  //x_max=x_max0;
  if (file) {
    for (i=0;i<n_bunches*n_slices;i++){
      t0=t[i];
      tmp=f_new_point(t,w,&j,n,t0,k,beta,l_cav, lambda, Q_long)*1e-9*(charge*ECHARGE)*TWOPI
	*C_LIGHT/lambda*r_over_q/(1.0-beta);
      fprintf(file,"%g %g\n",t0,tmp);
      if (tmp>f_max) {
	f_max=tmp;
      }
    }
  }
  else {
    // calculate wake for each particle in beam
    for (i=0;i<n_bunches*n_slices;i++){
      t0=t[i];

      // for particle[i], calculate wake contribution from all other particles in [GeV / particle]
      if(bunch_drain_active) {
	// bunch_drain_active = 1: calculate "exact"
	tmp=f_new_point(t,w,&j,n,t0,k,beta,l_cav, lambda, Q_long)*1e-9*(charge*ECHARGE)*TWOPI
	  *C_LIGHT/lambda*r_over_q/(1.0-beta);
      } else {
	// bunch_drain_active = 0: calcuate like in PLACET tracking
        tmp=f_new_point(t,w,&j,n,t0,k,beta,l_cav,i,dz,n_slices, lambda, Q_long)*1e-9*(charge*ECHARGE)*TWOPI
          *C_LIGHT/lambda*r_over_q/(1.0-beta);
      }

      // store the particle that has felt the largest deceleration
      if (tmp>f_max) {
	f_max=tmp;
      }
    }
  }
  free(t);
  free(w);
  return f_max;
}

int
Tcl_Power(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
	  char *argv[])
{
  int error;
  char *fname=NULL;
  FILE *file=NULL;
  double beta,l,lambda,dz=0.02,charge=1e11,r_over_q=122.0,bl=400.0,lambda_ext=0.01;
  double xmin=0.0,xmax=6.0,tmp,phase;
  double gauss_cut=3, n_bunches=300, n_slices=51;
  char *tcl_slices_list=NULL;
  int n_slices_list=0, dummy, i;
  char **slices_list,**slice;
  double *slices_pos_list=NULL, *slices_weight_list=NULL;
  double Q_long = 7000.0e20;

  BEAM *beam=NULL;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command calculates the steady-state power output from a PETS for a sliced beam."},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"Return value: { power produced at the specified extraction frequency [MW], power phase [rad] }"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"Input parameters:"},
    {(char*)"-beta",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&beta,
     (char*)"group velocity of the structure (fundamental mode)"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l,
     (char*)"length of the structure"},
    {(char*)"-xmin",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&xmin,
     (char*)"start of beam intervall"},
    {(char*)"-xmax",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&xmax,
     (char*)"end of beam intervall)"},
    {(char*)"-lambda",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&lambda,
     (char*)"resonant wavelength of the structure (fundamental mode)"},
    {(char*)"-Q_long",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&Q_long,
     (char*)"Q-factor of the structure (fundamental mode)"},
    {(char*)"-lambda_ext",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&lambda_ext,
     (char*)"wavelength at which power is extracted"},
    {(char*)"-r_over_q",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&r_over_q,
     (char*)"R/Q of the structure in circuit ohms (fundamental mode)"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&charge,
     (char*)"charge per bunch"},
    {(char*)"-bunchlength",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&bl,
     (char*)"length of the bunch"},
    {(char*)"-distance",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dz,
     (char*)"distance between bunches"},
    {(char*)"-n_bunches",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&n_bunches,
     (char*)"Number of bunches in the beam"},
    {(char*)"-n_slices",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&n_slices,
     (char*)"Slices per bunch"},
    {(char*)"-gauss_cut",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gauss_cut,
     (char*)"Cut off (in sigma) for the gaussian bunch distribution"},
    {(char*)"-slices_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&tcl_slices_list,
     (char*)"Charge distribution as list of lists { { s0 w0 } {s1 w1 } ... } [OVERRIDES other beam params if present]"},
     {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&fname,
     (char*)"name of the file where to store results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Power>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (fname) {
    file=open_file(fname);
  }
  else {
    file=NULL;
  }
  bl*=1e-6;


  // Extracts TCL slice distribution
  if( tcl_slices_list != NULL ) { 
    if(error=Tcl_SplitList(interp,tcl_slices_list,&n_slices_list,&slices_list)) {
      return error;
    }
    
    if (n_slices_list<1) {
      Tcl_AppendResult(interp,"Error: wrong slice list given: 0 slices found.",NULL);
      return TCL_ERROR;
    }
    slices_pos_list=(double*)malloc(sizeof(double)
				    *n_slices_list);
    slices_weight_list=(double*)malloc(sizeof(double)
				       *n_slices_list);
    for (i=0;i<n_slices_list;++i) {
      if (error=Tcl_SplitList(interp,slices_list[i],&dummy, &slice)){
	return error;
      }
      if (error=Tcl_GetDouble(interp,slice[0],slices_pos_list+i)){
	return error;
      }
      if (error=Tcl_GetDouble(interp,slice[1],slices_weight_list+i)){
	return error;
      }
    }
    
    //   placet_printf(INFO,"EA: slices_pos_list: %g %g\n", slices_pos_list[0],slices_pos_list[1] );
    //   placet_printf(INFO,"EA: slices_pos_weight: %g %g\n", slices_weight_list[0], slices_weight_list[1]);
    
  }

  if (beam) {
    tmp=power(file,lambda,beta,l,r_over_q,beam,xmin,xmax,&phase,lambda_ext,Q_long);
  }
  else {
    tmp=power(file,lambda,beta,l,r_over_q,charge,bl,dz,
	      xmin,xmax,&phase,lambda_ext,(int) n_bunches,(int) n_slices, gauss_cut, slices_pos_list, slices_weight_list, n_slices_list,Q_long);
  }
  // RETURNS: 1) Power produced at frequency k1 [MW], 2) Phase of power produced
  char buf[50];
  snprintf(buf,50,"%g %g",tmp,phase);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  
  close_file(file);
  return TCL_OK;
}

int
Tcl_MaxField(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
	     char *argv[])
{
  int error;
  char *fname=NULL;
  FILE *file;
  double beta,l,lambda,dz=0.02,charge=1e11,r_over_q=122.0,bl=400.0;
  double xmin,xmax;
  double gauss_cut=3, n_bunches=300, n_slices=51, bunch_drain_active=0;
  char *tcl_slices_list=NULL;
  int n_slices_list=0, dummy, i;
  char **slices_list,**slice;
  double *slices_pos_list=NULL, *slices_weight_list=NULL;
  double Q_long = 7000.0e20;

  Tk_ArgvInfo table[]={
   {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command calculates the PETS deceleration of the most decelerated particle in a sliced beam."},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"Return value: Deceleration of the most decelerated particle [GeV]"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"Input parameters:"},
      {(char*)"-beta",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&beta,
     (char*)"group velocity of the structure (fundamental mode)"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l,
     (char*)"length of the structure"},
    {(char*)"-lambda",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&lambda,
     (char*)"wavelength of the structure (fundamental mode)"},
    {(char*)"-Q_long",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&Q_long,
     (char*)"Q-factor of the structure (fundamental mode)"},
    {(char*)"-r_over_q",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&r_over_q,
     (char*)"R/Q of the structure in circuit ohms (fundamental mode)"},
    {(char*)"-distance",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dz,
     (char*)"distance between bunches"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&charge,
     (char*)"charge per bunch"},
    {(char*)"-bunchlength",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&bl,
     (char*)"length of the bunch"},
    {(char*)"-n_bunches",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&n_bunches,
     (char*)"Number of bunches in the beam"},
    {(char*)"-n_slices",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&n_slices,
     (char*)"Slices per bunch"},
    {(char*)"-gauss_cut",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gauss_cut,
     (char*)"Cut off (in sigma) for the gaussian bunch distribution"},
    {(char*)"-slices_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&tcl_slices_list,
     (char*)"Charge distribution as list of lists { { s0 w0 } {s1 w1 } ... } [OVERRIDES other beam params if present]"},
    {(char*)"-bunch_drain_active",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&bunch_drain_active,
     (char*)"If not 0 calculate different drain out time for each slice [0: calculate like in tracking]"},
    {(char*)"-xmin",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&xmin,
     (char*)"[not used, kept for backwards compability]"},
    {(char*)"-xmax",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&xmax,
     (char*)"[not used, kept for backwards compability]"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&fname,
     (char*)"name of the file where to store results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <MaxField>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (fname) {
    file=open_file(fname);
  }
  else {
    file=NULL;
  }
  bl*=1e-6;


  // Extracts TCL slice distribution
  if( tcl_slices_list != NULL ) { 
    if(error=Tcl_SplitList(interp,tcl_slices_list,&n_slices_list,&slices_list)) {
      return error;
    }
    
    if (n_slices_list<1) {
      Tcl_AppendResult(interp,"Error: wrong slice list given: 0 slices found.",NULL);
      return TCL_ERROR;
    }
    slices_pos_list=(double*)malloc(sizeof(double)
				    *n_slices_list);
    slices_weight_list=(double*)malloc(sizeof(double)
				       *n_slices_list);
    for (i=0;i<n_slices_list;++i) {
      if (error=Tcl_SplitList(interp,slices_list[i],&dummy, &slice)){
	return error;
      }
      if (error=Tcl_GetDouble(interp,slice[0],slices_pos_list+i)){
	return error;
      }
      if (error=Tcl_GetDouble(interp,slice[1],slices_weight_list+i)){
	return error;
      }
    }
    //   placet_printf(INFO,"EA: slices_pos_list: %g %g\n", slices_pos_list[0],slices_pos_list[1] );
    //   placet_printf(INFO,"EA: slices_pos_weight: %g %g\n", slices_weight_list[0], slices_weight_list[1]);
  }

  // RETURNS: Deceleration of the most decelerated particle [GeV]
  char buf[20];
  snprintf(buf,20,"%g",max_field(file,lambda,beta,l,r_over_q,
				 charge,bl,dz,(int) n_bunches,(int) n_slices, gauss_cut, (int) bunch_drain_active,
				 slices_pos_list, slices_weight_list, n_slices_list, Q_long ));
  Tcl_SetResult(interp,buf,TCL_VOLATILE);

  close_file(file);
  return TCL_OK;
}

int
Power_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"Power",Tcl_Power,(ClientData)NULL,
		    (Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(interp,"MaxField",Tcl_MaxField,(ClientData)NULL,
		    (Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Power","0.9");
  return TCL_OK;
}
