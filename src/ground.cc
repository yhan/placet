#include <algorithm>
#include <cstdlib>
#include <functional>
#include <fstream>
#include <map>
#include <string>

#include "ground.h"

#include "placet.h"

#include "girder.h"
#include "random.hh"
#include "spline.h"

//______________________________________________________________________________
GROUND_DATA::GROUND_DATA(char *name,bool syst,double t_st,double tstep,double s_st,double l_st,double s_sgn,int s_ab):
  preisolator(false),pos_qd0(0.0),pos_qf1(0.0),cqd0(0),sqd0(0),cqf1(0),sqf1(0),xpos_qd0_prev(0.),xpos_qf1_prev(0.),do_syst(syst),t_start(t_st),t_step(tstep),s_start(s_st),last_start(l_st),s_sign(s_sgn),s_abs(s_ab)
{
  FILE *file;
  int i,j,m=0;
  char buffer[1000],*point;
  double fmin,fmax,df,kmin,kmax,dk;

  file=read_file(name);

  standing=true;

  point=fgets(buffer,1000,file);
  n_f=strtol(point,&point,10);
  n_k=strtol(point,&point,10);
  fmin=strtod(point,&point);
  fmax=strtod(point,&point);
  df=pow(fmax/fmin,1.0/(n_f-1));
  kmin=strtod(point,&point);
  kmax=strtod(point,&point);
  dk=pow(kmax/kmin,1.0/(n_k-1));

  constructor_init();
  if (standing) {
    s1=new double[n_f*n_k];
    c1=new double[n_f*n_k];
  }
  f[0]=fmin;
  for (i=1;i<n_f;i++) {
    f[i]=f[i-1]*df;
  }
  k[0]=kmin;
  for (i=1;i<n_k;i++) {
    k[i]=k[i-1]*dk;
  }
  for (i=0;i<n_f;i++) {
    sf[i]=0.0;
    cf[i]=1.0;
  }

  m=0;
  for (i=0;i<n_f;i++){
    for (j=0;j<n_k;j++){
      point=fgets(buffer,1000,file);
      strtol(point,&point,10);
      strtol(point,&point,10);
      m=j*n_f+i;
      if (standing) {
	  a[m]=fabs(strtod(point,&point))*0.5;
      }
      else {
	  a[m]=fabs(strtod(point,&point))*0.707106781186548;
      }
    }
  }

  t=-999.;
  // s=0.0;
  if (do_syst) {
    point=fgets(buffer,1000,file);
    n_ks=strtol(point,&point,10);
    kmin=strtod(point,&point);
    kmax=strtod(point,&point);
    dk=pow(kmax/kmin,1.0/(n_ks-1));
    ks= new double[n_ks];
    ph_s= new double[n_ks];
    as= new double[n_ks];
    ks[0]=kmin;
    for (i=1;i<n_ks;i++) {
      ks[i]=ks[i-1]*dk;
    }
    for (i=0;i<n_ks;i++) {
      point=fgets(buffer,1000,file);
      strtol(point,&point,10);
      as[i]=strtod(point,&point);
    }
  }
  else {
    n_ks=0;
    ks=0, ph_s=0, as=0; 
  }
  close_file(file);
}
//___________________________________
void GROUND_DATA::constructor_init(){
  f=new double[n_f];
  sf=new double[n_f];
  cf=new double[n_f];
  k=new double[n_k];
  sk=new double[n_k];
  ck=new double[n_k];
  sk0=new double[n_k];
  ck0=new double[n_k];
  a=new double[n_f*n_k];
  s0=new double[n_f*n_k];
  c0=new double[n_f*n_k];
  gs0.reserve(n_f*n_k); // WARNING: girder vector points must be less than n_f*n_k
  x0.reserve(n_f*n_k);
  //  iter_x1=std::make_pair(-1,0.0);
  iter = -1;
}
// helper functions
void del_ground_data(GROUND_DATA* g){delete g;}
GROUND_DATA* get_ground_data(std::pair<std::string,GROUND_DATA*> p){return p.second;}
void del_ground_data_map(std::pair<std::string,GROUND_DATA*> p){del_ground_data(get_ground_data(p));}
//__________________________
GROUND_DATA::~GROUND_DATA(){
  delete[] f;
  delete[] k;
  delete[] a;
  delete[] s0;
  delete[] c0;
  if (standing) {
    delete[] s1;
    delete[] c1;
  }
  delete[] sf;
  delete[] cf;
  delete[] sk;
  delete[] ck;
  delete[] sk0;
  delete[] ck0;
  if (n_ks>0) {
    delete[] as;
    delete[] ph_s;
    delete[] ks;
  }
  for_each(gm_map.begin(),gm_map.end(),del_ground_data_map);
  if (preisolator) {
    delete[] cqd0;
    delete[] sqd0;
    delete[] cqf1;
    delete[] sqf1;
    delete[] tf_mnt1QD0.first;
    delete[] tf_mnt2QD0.first;
    delete[] tf_mnt1QF1.first;
    delete[] tf_mnt2QF1.first;
    delete[] tf_mnt1QD0.second;
    delete[] tf_mnt2QD0.second;
    delete[] tf_mnt1QF1.second;
    delete[] tf_mnt2QF1.second;
  }
}
// methods
//_____________________________________________
double GROUND_DATA::xpos(double t_e,double s_e)
{
    int j;
    double pos=0.0;
    if (std::abs(t_e-t)>1e-7) {
      update_time(t_e);
    }
    for (j=0;j<n_k;j++){
      pos+=ck[j]*cos(k[j]*s_e)-sk[j]*sin(k[j]*s_e);
      // double damping_factor = 1.0;
      // if (filter) damping_factor = filter->real(f[j]);
      // pos+=damping_factor*(ck[j]*cos(k[j]*s_e)-sk[j]*sin(k[j]*s_e));
      // double phase_shift = 0.0;
      // if (filter) phase_shift = filter->imag(f[j]);
      // pos+=phase_shift*(-ck[j]*sin(k[j]*s_e)+sk[j]*cos(k[j]*s_e));
    }
    for (j=0;j<n_ks;j++){
	pos+=as[j]*cos(ks[j]*s_e+ph_s[j])*t_e;
    }
    return pos;
}
//_____________________________________________________________________
void GROUND_DATA::xpos(double t_e,double s_e,double ds,double *x,int n)
{
    int i,j,m;
    double pos,tmp,s2[n_k],c2[n_k];
    double sd[n_k],cd[n_k];

    if (std::abs(t_e-t)>1e-7) {
	m=0;
	for (j=0;j<n_f;j++){
	    cf[j]=cos(t_e*f[j]);
	    sf[j]=sin(t_e*f[j]);
	}
	for (i=0;i<n_k;i++){
	    sk[i]=0.0;
	    ck[i]=0.0;
	    for (j=0;j<n_f;j++){
		ck[i]+=a[m]*(c0[m]*cf[j]-s0[m]*sf[j]);
		sk[i]+=a[m]*(c0[m]*sf[j]+s0[m]*cf[j]);
		m++;
	    }
	    ck[i]-=ck0[i];
	    sk[i]-=sk0[i];
	}
	t=t_e;
    }
    for (j=0;j<n_k;j++){
	cd[j]=cos(k[j]*ds);
	sd[j]=sin(k[j]*ds);
	c2[j]=cos(k[j]*s_e);
	s2[j]=sin(k[j]*s_e);
    }
    for (i=0;i<n-1;i++) {
	pos=0.0;
	for (j=0;j<n_k;j++){
	    pos+=ck[j]*c2[j]-sk[j]*s2[j];
	    tmp=c2[j]*cd[j]-s2[j]*sd[j];
	    s2[j]=s2[j]*cd[j]+c2[j]*sd[j];
	    c2[j]=tmp;
	}
	x[i]=pos;
    }
    pos=0.0;
    for (j=0;j<n_k;j++){
	pos+=ck[j]*c2[j]-sk[j]*s2[j];
    }
    x[n-1]=pos;
    return;
}
//_________________________________________________
void GROUND_DATA::init(GIRDER* girder, char* filter,GROUND_DATA* nominal)
{
  if (filter) {start_filter(filter,nominal);}
  else {start(nominal);}
  set_gs0(girder);
  //if (!filter) {
  gm_filters.init(this,gs0.size());
  //}
  set_startpos(); 
}
//_____________________________
void GROUND_DATA::init_phases()
{
  for (int i=0;i<n_k*n_f;i++) {
    double phi=TWOPI*Groundmotion.Uniform();
    c0[i]=cos(phi);
    s0[i]=sin(phi);
  }
  if (standing) {
    for (int i=0;i<n_k*n_f;i++) {
      double phi=TWOPI*Groundmotion.Uniform();
      c1[i]=cos(phi);
      s1[i]=sin(phi);
    }
  }
  for (int j=0;j<n_f;j++){
    cf[j]=1.0;
    sf[j]=0.0;
  }
  for (int i=0;i<n_ks;i++){
    ph_s[i]=TWOPI*Groundmotion.Uniform();
  }
}
//__________________________________________________
void GROUND_DATA::copy_phases(const GROUND_DATA* nominal){
  int i,j;
  const double* c0_n = nominal->get_c0();
  const double* s0_n = nominal->get_s0();
  const double* c1_n = nominal->get_c1();
  const double* s1_n = nominal->get_s1();
  const double* ph_s_n = nominal->get_ph_s();
  for (i=0;i<n_k*n_f;i++) {
    c0[i]=c0_n[i];
    s0[i]=s0_n[i];
  }
  if (standing) {
    for (i=0;i<n_k*n_f;i++) {
      c1[i]=c1_n[i];
      s1[i]=s1_n[i];
    }
  }
  for (j=0;j<n_f;j++){
    cf[j]=1.0;
    sf[j]=0.0;
    }
  for (i=0;i<n_ks;i++){
    ph_s[i]=ph_s_n[i];
  }
}
//_______________________
void GROUND_DATA::start(GROUND_DATA* nominal)
{
  int i,j,m=0;
  if (nominal) {
    copy_phases(nominal);
  }
  else {init_phases();}
  
  for (i=0;i<n_k;i++){
    ck0[i]=0.0;
    sk0[i]=0.0;
    if (standing) {
      for (j=0;j<n_f;j++){
	ck0[i]+=a[m]*(c0[m]+c1[m]);
	sk0[i]+=a[m]*(s0[m]-s1[m]);
	m++;
      }
    }
    else {
      for (j=0;j<n_f;j++){
	ck0[i]+=a[m]*c0[m];
	sk0[i]+=a[m]*s0[m];
	m++;
      }
    }
    ck[i]=ck0[i];
    sk[i]=sk0[i];
    //ck[i]=0.0;
    //sk[i]=0.0;
  }
}

//___________________________________________________________________________________
std::pair<double*,double*> GROUND_DATA::read_filter_file(char* name, bool logscale_x, bool logscale_y)const
{
  double *scale_c=new double[n_f];//(double*)alloca(sizeof(double)*n_f);
  double *scale_s=new double[n_f];//(double*)alloca(sizeof(double)*n_f);
  // open filter file:
  std::ifstream file(name);
  if (file.fail()) {
    placet_cout << ERROR << "file: " << name << " not found" << endmsg;
    exit(1);
  }

  // get number of lines (should be improved, so that file is not read twice, e.g. with a vector)
  std::string line;
  int n=0;
  while (getline(file,line)) {
    ++n;
  }
  file.clear();
  file.seekg(0);
  double x[n],y_c[n],y_s[n];

  int i=0;
  while (file.good()) {
    file >> x[i] >> y_c[i] >> y_s[i];
    // change from frequency (Hz) to angular freq. (rad/s)
    x[i]*=TWOPI;
    i++;
  }
  file.close();

  // intrapolate to right frequencies
  SPLINE sp_c(x,logscale_x,y_c,logscale_y,n); // x logarithmic, y linear
  SPLINE sp_s(x,logscale_x,y_s,logscale_y,n);

  for (int j=0;j<n_f;j++){
    scale_c[j]=sp_c.eval(f[j]);
    scale_s[j]=sp_s.eval(f[j]);
  }
  return std::make_pair(scale_c,scale_s);
}

//______________________________________________________________
void GROUND_DATA::start_filter(char *name, GROUND_DATA* nominal)
{
  std::pair<double*,double*> filter = read_filter_file(name);
  double* scale_c = filter.first;
  double* scale_s = filter.second;

  //  reset0=1;
  if (nominal) {
    copy_phases(nominal);
  }
  else {init_phases();}

  int m=0;
  for (int i=0;i<n_k;i++){
    ck0[i]=0.0;
    sk0[i]=0.0;
    if (standing) {
      for (int j=0;j<n_f;j++){
	double tmp=c0[m]*scale_c[j]-s0[m]*scale_s[j];
	s0[m]=s0[m]*scale_c[j]+c0[m]*scale_s[j];
	c0[m]=tmp;
	tmp=c1[m]*scale_c[j]-s1[m]*scale_s[j];
	s1[m]=s1[m]*scale_c[j]+c1[m]*scale_s[j];
	c1[m]=tmp;
	ck0[i]+=a[m]*(c0[m]+c1[m]);
	sk0[i]+=a[m]*(s0[m]-s1[m]);
	m++;
      }
    }
    else {
      for (int j=0;j<n_f;j++){
	double tmp=c0[m]*scale_c[j]-s0[m]*scale_s[j];
	s0[m]=s0[m]*scale_c[j]+c0[m]*scale_s[j];
	c0[m]=tmp;
	ck0[i]+=a[m]*c0[m];
	sk0[i]+=a[m]*s0[m];
	m++;
      }
    }
    ck[i]=ck0[i];
    sk[i]=sk0[i];
    //ck[i]=0.0;
    //sk[i]=0.0;
  }
  delete[] scale_c;
  delete[] scale_s;
}

//_____________________________________________________________________________________
void GROUND_DATA::preisolator_filter(double pos_mnt1, double pos_mnt2, char * mnt1QD0, char * mnt2QD0, char * mnt1QF1, char * mnt2QF1, double _pos_qd0, double _pos_qf1 )
{
  // if ground already has a preisolator, free memory first
  if (preisolator) {
    placet_cout << WARNING << "preisolator was already defined, old preisolator is removed" << endmsg;
    delete[] cqd0;
    delete[] sqd0;
    delete[] cqf1;
    delete[] sqf1;
    delete[] tf_mnt1QD0.first;
    delete[] tf_mnt2QD0.first;
    delete[] tf_mnt1QF1.first;
    delete[] tf_mnt2QF1.first;
    delete[] tf_mnt1QD0.second;
    delete[] tf_mnt2QD0.second;
    delete[] tf_mnt1QF1.second;
    delete[] tf_mnt2QF1.second;
  }

  preisolator = true;
  pos_qd0 = _pos_qd0;
  pos_qf1 = _pos_qf1;

  xpos_qf1_prev = 0.0, xpos_qd0_prev=0.0;

  // s-position for last_start==1, to be fixed for s_start!=0 & last_start==0!
  double spos_mnt1 = -s_sign*pos_mnt1;
  double spos_mnt2 = -s_sign*pos_mnt2;

  tf_mnt1QD0 = read_filter_file(mnt1QD0);
  tf_mnt2QD0 = read_filter_file(mnt2QD0);
  tf_mnt1QF1 = read_filter_file(mnt1QF1);
  tf_mnt2QF1 = read_filter_file(mnt2QF1);

  cqd0=new double[n_f*n_k];
  cqf1=new double[n_f*n_k];
  sqd0=new double[n_f*n_k];
  sqf1=new double[n_f*n_k];

  // im and re parts of e^i(kx)
  double* sk_mnt1 = new double[n_k];
  double* sk_mnt2 = new double[n_k];
  double* ck_mnt1 = new double[n_k];
  double* ck_mnt2 = new double[n_k];
  for (int j=0;j<n_k;j++){
    sk_mnt1[j]=sin(k[j]*spos_mnt1);
    ck_mnt1[j]=cos(k[j]*spos_mnt1);
    sk_mnt2[j]=sin(k[j]*spos_mnt2);
    ck_mnt2[j]=cos(k[j]*spos_mnt2);
  }

  int m=0;
  for (int i=0;i<n_k;i++){
    for (int j=0;j<n_f;j++){
      // include starting phases c0 and c1 (standing)
      double sk_mnt1_phase,ck_mnt1_phase,sk_mnt2_phase,ck_mnt2_phase;
      if (standing) {
	ck_mnt1_phase = ck_mnt1[i]*(c0[m]+c1[m]) - sk_mnt1[i]*(s0[m]-s1[m]);
	sk_mnt1_phase = ck_mnt1[i]*(s0[m]+s1[m]) + sk_mnt1[i]*(c0[m]-c1[m]);
	ck_mnt2_phase = ck_mnt2[i]*(c0[m]+c1[m]) - sk_mnt2[i]*(s0[m]-s1[m]);
	sk_mnt2_phase = ck_mnt2[i]*(s0[m]+s1[m]) + sk_mnt2[i]*(c0[m]-c1[m]);
      } else {
	ck_mnt1_phase = ck_mnt1[i]*c0[m] - sk_mnt1[i]*s0[m];
	sk_mnt1_phase = ck_mnt1[i]*s0[m] + sk_mnt1[i]*c0[m];
	ck_mnt2_phase = ck_mnt2[i]*c0[m] - sk_mnt2[i]*s0[m];
	sk_mnt2_phase = ck_mnt2[i]*s0[m] + sk_mnt2[i]*c0[m];
      }

      cqd0[m]=a[m]*(tf_mnt1QD0.first[j]*ck_mnt1_phase - tf_mnt1QD0.second[j]*sk_mnt1_phase + tf_mnt2QD0.first[j]*ck_mnt2_phase - tf_mnt2QD0.second[j]*sk_mnt2_phase);
      sqd0[m]=a[m]*(tf_mnt1QD0.first[j]*sk_mnt1_phase + tf_mnt1QD0.second[j]*ck_mnt1_phase + tf_mnt2QD0.first[j]*sk_mnt2_phase + tf_mnt2QD0.second[j]*ck_mnt2_phase);

      cqf1[m]=a[m]*(tf_mnt1QF1.first[j]*ck_mnt1_phase - tf_mnt1QF1.second[j]*sk_mnt1_phase + tf_mnt2QF1.first[j]*ck_mnt2_phase - tf_mnt2QF1.second[j]*sk_mnt2_phase);
      sqf1[m]=a[m]*(tf_mnt1QF1.first[j]*sk_mnt1_phase + tf_mnt1QF1.second[j]*ck_mnt1_phase + tf_mnt2QF1.first[j]*sk_mnt2_phase + tf_mnt2QF1.second[j]*ck_mnt2_phase);
      m++;
    }
  }

  delete[] sk_mnt1;
  delete[] sk_mnt2;
  delete[] ck_mnt1;
  delete[] ck_mnt2;
}

//___________________________________________________________
std::pair<double,double> GROUND_DATA::preisolator_dxpos()
{
  int m=0;
  double time = iter*t_step + t_start;
  
  if (std::abs(t-time)>1e-7) {
    update_time(time);
  }

  double xpos_qd0=0.0,xpos_qf1=0.0;
  for (int i=0;i<n_k;i++){
    for (int j=0;j<n_f;j++){
      xpos_qd0+=cqd0[m]*cf[j]-sqd0[m]*sf[j];
      xpos_qf1+=cqf1[m]*cf[j]-sqf1[m]*sf[j];
      m++;
    }
  }

  // long term motion, to be implemented:          
  // for (int j=0;j<n_ks;j++){
  // 	pos+=as[j]*cos(ks[j]*s_e+ph_s[j])*t_e;
  // }

  // normalise to position defined as reference point, s=0.0 (e.g. IP) 
  double x1=0.0;
  // if (iter_x1.first == iter) { // same iteration, retrieve x1
  //   x1 = iter_x1.second;
  // }
  // else { // new iteration, calculate x1
  //   // JS should x1 be at gs0[0] ?
  //   x1=xpos(time,0.0); // calculate phases, as might not be done yet in this class!
  //   iter_x1.first = iter;
  //   iter_x1.second  = x1;
  // }

  double x_qd0  = xpos_qd0 - x1;
  double dx_qd0 = (x_qd0 - xpos_qd0_prev)*1e6;
  xpos_qd0_prev = x_qd0;

  double x_qf1  = xpos_qf1 - x1;
  double dx_qf1 = (x_qf1 - xpos_qf1_prev)*1e6;
  xpos_qf1_prev = x_qf1;

  if (iter==0) {dx_qd0=0; dx_qf1=0;}// xpos_qf1_prev=0.; xpos_qd0_prev=0.;}

  return std::make_pair(dx_qd0,dx_qf1);
}

//________________________________________
void GROUND_DATA::update_time(double time)
{
  int m=0;
  for (int j=0;j<n_f;j++){
    cf[j]=cos(time*f[j]);
    sf[j]=sin(time*f[j]);
  }
  if (standing) {
    for (int i=0;i<n_k;i++){
      double c2=0.0;
      double s2=0.0;
      for (int j=0;j<n_f;j++){
	c2+=a[m]*((c0[m]+c1[m])*cf[j]-(s0[m]+s1[m])*sf[j]);
	s2+=a[m]*((c0[m]-c1[m])*sf[j]+(s0[m]-s1[m])*cf[j]);
	m++;
      }
      //ck[i]=c2-ck0[i];
      //sk[i]=s2-sk0[i];
      ck[i]=c2;
      sk[i]=s2;
    }
  }
  else {
    for (int i=0;i<n_k;i++){
      double c2=0.0;
      double s2=0.0;
      for (int j=0;j<n_f;j++){
	c2+=a[m]*(c0[m]*cf[j]-s0[m]*sf[j]);
	s2+=a[m]*(c0[m]*sf[j]+s0[m]*cf[j]);
	m++;
      }
      //ck[i]=c2-ck0[i];
      //sk[i]=s2-sk0[i];
      ck[i]=c2;
      sk[i]=s2;
    }
  }
  t=time;
}

//___________________________________________________________
void GROUND_DATA::spectrum(char *fn,double dz,double dt)const
{
  FILE *fx;
  int i,j,m=0;
  double *p,*p2;
  p=(double*)alloca(sizeof(double)*n_f);
  p2=(double*)alloca(sizeof(double)*n_f);
  for (j=0;j<n_f;j++){
    p[j]=0.0;
  }
  if (dz>0.0) {
    if (dt<=0.0) {
      for (i=0;i<n_k;i++){
	for (j=0;j<n_f;j++){
	  p[j]+=pow(a[m]*2.0*sin(0.5*dz*k[i]),2);
	  m++;
	}
      }
    }
    else {
      for (i=0;i<n_k;i++){
	for (j=0;j<n_f;j++){
	  p[j]+=pow(a[m]*2.0*sin(0.5*dz*k[i])*2.0*sin(0.5*dt*f[j]),2);
	  m++;
	}
      }
    }
  }
  else {
    if (dt<=0.0) {
      for (i=0;i<n_k;i++){
	for (j=0;j<n_f;j++){
	  p[j]+=pow(a[m],2);
	  m++;
	}
      }
    }
    else {
      for (i=0;i<n_k;i++){
	for (j=0;j<n_f;j++){
	  p[j]+=pow(a[m]*2.0*sin(0.5*dt*f[j]),2);
	  m++;
	}
      }
    }
  }
  if (standing) {
    for (j=0;j<n_f;j++){
      p[j]*=0.5*2.0*0.707106781186548;
    }
  }
  else {
    for (j=0;j<n_f;j++){
      p[j]*=0.5;
    }
  }
  for (j=0;j<n_f;j++){
    p2[j]=p[j];
  }
  for (j=n_f-2;j>=0;j--){
    p2[j]+=p2[j+1];
  }
  for (j=0;j<n_f;j++){
    p[j]=sqrt(p[j]);
    p2[j]=sqrt(p2[j]);
  }
  fx=open_file(fn);
  for (j=0;j<n_f;j++){
    fprintf(fx,"%g %g %g\n",f[j]/(2.0*acos(-1.0)),p[j],p2[j]);
  }
  close_file(fx);
}

//_____________________________________________
void GROUND_DATA::set_gs0(GIRDER *g)
{
  double s=0.0;
  if (s_abs) { // s_abs can be discarded, it is the same as s_start=0, s_sign=1, last_start=0
    if (g) {
      s+=g->distance_to_prev_girder();
      gs0.push_back(s);
    }
    while(g) {
      s+=g->get_length();
      gs0.push_back(s);
      g=g->next();
     }
  }
  else {
    if (last_start) gs0.push_back(0.0);
    else gs0.push_back(s_start);
    int n=1;
    while(g) {
      s+=g->get_length();
      gs0.push_back(s*s_sign); //+gs0[n-1]);
      g=g->next();
      n++;
    }
    if (last_start) {
      for (int i=0;i<n;i++){
	gs0[i]-=gs0[n-1]-s_start;
      }
    }
  }
}

//______________________________
void GROUND_DATA::set_startpos()
{
  //double x1=xpos(t_start,0.0);
  //  for (unsigned int j=0;j<gs0.size();j++) {
    //    x0.push_back(xpos(t_start,gs0[j]));//-x1);
  //}
  x0.resize(gs0.size());
}

// //________________________________________________________
// void GROUND_DATA::set_startpos(double *z_pos, int s_abs)
// {
//   // z_pos = list of position along the beamline where to apply gm

// }

//______________________________
double GROUND_DATA::dxpos(int g, bool use_filter)
{
  // select proper filtered ground data
  if (use_filter) {
    GROUND_DATA* filter = gm_filters.get_ground_data(g);
    if (this != filter) {
      return filter->dxpos(g);
    }
  }

  double x1=0.0;

  // if (iter_x1.first == iter) { // same iteration, retrieve x1
  //   x1 = iter_x1.second;
  // }
  // else { // new iteration, calculate x1
  //   // JS should x1 be at gs0[0] ?
  //   x1=xpos(t_step*iter+t_start,0.0);
  //   iter_x1.first = iter;
  //   iter_x1.second  = x1;
  // }

  double x=xpos(t_step*iter+t_start,gs0[g])-x1;
  double dx=(x-x0[g])*1e6;
  x0[g]=x;
  if (iter==0) {dx=0.;}
  return dx;
}

//___________________________
double GROUND_DATA::dxpos_0()
{
  double x_old=xpos(t_step*(iter-1)+t_start,0.0); // could be stored
  double x_new=xpos(t_step*iter+t_start,0.0);
  double dx = (x_new - x_old)*1e6;
  return dx;
}

void GROUND_DATA::move_girder_ends(GIRDER* girder, bool vertical, double scale){
  int gn=0; // girder count
  next_iteration();
  double girder_begin = dxpos(gn) * scale;
  while (girder) {
    gn++;
    double girder_end = dxpos(gn) * scale;
    if (vertical) {girder->move_ends(0.0,0.0,girder_begin,girder_end);}
    else {girder->move_ends(girder_begin,girder_end,0.0,0.0);}
    // prepare for next girder:
    girder_begin=girder_end;
    girder=girder->next();
  }
}

void GROUND_DATA::add_filter(char* name, GIRDER* girder, char* filter_name, int filter_start, int filter_end, double low_freq, double high_freq) {
  // search if filter is already in map
  std::string filter_name_str = std::string(filter_name);
  std::map<std::string, GROUND_DATA*>::iterator map_it = gm_map.find(filter_name_str);
  GROUND_DATA* filter = 0;
  if (map_it!=gm_map.end()) { // filter already in map
    filter = (*map_it).second;
  } else { // new filter
    // better use a copy constructor
    filter = new GROUND_DATA(name,do_syst,t_start,t_step,s_start,last_start,s_sign,s_abs);
    filter->apply_freq_cut(low_freq,high_freq);
    filter->init(girder,filter_name,this);
    gm_map[filter_name_str] = filter;
  }
  gm_filters.add_filter(filter, filter_start, filter_end);
}

int GROUND_DATA::next_iteration(){
  // increase iteration for each filter
  std::map<std::string, GROUND_DATA*>::iterator it = gm_map.begin();
  for (;it!=gm_map.end(); ++it) {
    (*it).second->next_iteration();
  }
  return ++iter;
}

void GROUND_DATA::apply_freq_cut(double low, double high){
  if (low < 0 && high < 0) return;
  if (low < 0) low = 0;
  if (high < 0) high = 1e20 + low; // make sure high > low, if not set
  // loop over all frequencies and set its subsequent amplitude to 0 / 1
  for (int i=0;i<n_f;i++){
    if (low<high) {
      if (f[i] > low && f[i] < high) continue;
    } else {
      if (f[i] < high || f[i] > low) continue;
    }
    for (int j=0;j<n_k;j++){
      int m=j*n_f+i;
      a[m]=0.0;
    }
  }
}
