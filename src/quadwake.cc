#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <tcl.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "malloc.h"
#include "matrix.h"
#include "quadwake.h"

extern Tcl_HashTable ModeTable;

#ifdef TWODIM

QUAD_KICK_DATA* quad_kick_data(BEAM *beam,int n_cell)
{
  int i,n;
  QUAD_KICK_DATA *res;

  n=beam->bunches*beam->slices_per_bunch;
  res=(QUAD_KICK_DATA*)xmalloc(sizeof(QUAD_KICK_DATA));
  res->n_cell=n_cell;
  res->n_part=n;
  res->n_bunch=beam->bunches;
  res->c=(double**)xmalloc(sizeof(double*)*n_cell);
  res->s=(double**)xmalloc(sizeof(double*)*n_cell);
  res->a=(double**)xmalloc(sizeof(double*)*n_cell);
  for (i=0;i<n_cell;i++){
    res->c[i]=(double*)xmalloc(sizeof(double)*n);
    res->s[i]=(double*)xmalloc(sizeof(double)*n);
    res->a[i]=(double*)xmalloc(sizeof(double)*beam->bunches);
  }
  return res;
}

QUAD_KICK_DATA*
quad_kick_data_const(BEAM *beam,int n_cell)
{
  int i,n;
  QUAD_KICK_DATA *res;

  n=beam->bunches*beam->slices_per_bunch;
  res=(QUAD_KICK_DATA*)xmalloc(sizeof(QUAD_KICK_DATA));
  res->n_cell=n_cell;
  res->n_part=n;
  res->n_bunch=beam->bunches;
  res->c=(double**)xmalloc(sizeof(double*)*n_cell);
  res->s=(double**)xmalloc(sizeof(double*)*n_cell);
  res->a=(double**)xmalloc(sizeof(double*)*n_cell);
  for (i=0;i<n_cell;i++){
    res->c[i]=(double*)xmalloc(sizeof(double)*2*beam->bunches);
    res->s[i]=(double*)xmalloc(sizeof(double)*2*beam->bunches);
    res->a[i]=(double*)xmalloc(sizeof(double)*beam->bunches);
  }
  return res;
}

void
quad_kick_data_delete(QUAD_KICK_DATA *quad_kick_data)
{
  int i;

  if (quad_kick_data==NULL) return;
  for (i=0;i<quad_kick_data->n_cell;i++){
    xfree(quad_kick_data->c[i]);
    xfree(quad_kick_data->s[i]);
    xfree(quad_kick_data->a[i]);
  }
  xfree(quad_kick_data->c);
  xfree(quad_kick_data->s);
  xfree(quad_kick_data->a);
}

void
quad_kick_data_fill(QUAD_KICK_DATA *quad_kick_data,BEAM *beam,int i_cell,
		    double lambda,double loss,double q,double charge)
{
  int i;
  double *c,*s,*a;

  if (i_cell>quad_kick_data->n_cell){
    placet_printf(ERROR,"error i_cell too large %d\n",i_cell);
    exit(1);
  }
  if (i_cell<0){
    placet_printf(ERROR,"error i_cell too small\n");
    exit(1);
  }
  c=quad_kick_data->c[i_cell];
  s=quad_kick_data->s[i_cell];
  a=quad_kick_data->a[i_cell];
  for (i=0;i<quad_kick_data->n_part;i++){
    c[i]=cos(beam->z_position[i]/lambda*TWOPI);
    s[i]=sin(beam->z_position[i]/lambda*TWOPI);
  }
  for(i=1;i<quad_kick_data->n_bunch;i++){
    /*
    a[i]=exp(-PI*(beam->drive_data->bunch_z[i]-beam->drive_data->bunch_z[i-1])
	     /(lambda*q));
	     */
    a[i]=exp(-PI*(beam->drive_data->param.inject->bunch[i].z
		  -beam->drive_data->param.inject->bunch[i-1].z)
	     /(lambda*q));
  }
  a[0]=-loss*(charge*ECHARGE*1e12)*1e-9*1e-12;
}

void
quad_kick_data_fill_const(QUAD_KICK_DATA *quad_kick_data,BEAM *beam,int i_cell,
			  double lambda,double loss,double q,double charge)
{
  int i;
  double *c0,*s0,*cd,*sd,*a;

  if (i_cell>quad_kick_data->n_cell){
    exit(1);
  }
  if (i_cell<0){
    exit(1);
  }
  c0=quad_kick_data->c[i_cell];
  cd=quad_kick_data->c[i_cell]+beam->bunches;
  s0=quad_kick_data->s[i_cell];
  sd=quad_kick_data->s[i_cell]+beam->bunches;
  a=quad_kick_data->a[i_cell];
  for (i=0;i<beam->bunches;i++){
    c0[i]=cos(beam->z_position[i*beam->slices_per_bunch]/lambda*TWOPI);
    s0[i]=sin(beam->z_position[i*beam->slices_per_bunch]/lambda*TWOPI);
    cd[i]=cos((beam->z_position[i*beam->slices_per_bunch+1]
	       -beam->z_position[i*beam->slices_per_bunch])/lambda*TWOPI);
    sd[i]=sin((beam->z_position[i*beam->slices_per_bunch+1]
	       -beam->z_position[i*beam->slices_per_bunch])/lambda*TWOPI);
    //    placet_printf(INFO,"fill: %g %g %g %g\n",c0[i],s0[i],cd[i],sd[i]);
  }
  for(i=1;i<quad_kick_data->n_bunch;i++){
    a[i]=exp(-PI*(beam->drive_data->param.inject->bunch[i].z
		  -beam->drive_data->param.inject->bunch[i-1].z)
	     /(lambda*q));
  }
  a[0]=-loss*(charge*ECHARGE*1e12)*1e-9*1e-12;
}

double
quadrupole_moment(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
		  R_MATRIX * /*sigma_xy*/)
{
  return sigma->r11-sigma_xx->r11
    +particle->y*particle->y-particle->x*particle->x;
}

void
quadrupole_step_kick(double k,PARTICLE *particle,R_MATRIX *sigma,
		     R_MATRIX *sigma_xx,R_MATRIX *sigma_xy)
{
  double eps=1e-200;
  R_MATRIX r;
  R_MATRIX rxx;

  /* If kick is too small ignore it */

  if (fabs(k)<eps){
    return;
  }

  k/=particle->energy;
  r.r11=1.0;
  r.r12=0.0;
  r.r21=-k;
  r.r22=1.0;
  particle->yp-=k*particle->y;
  rxx.r11=1.0;
  rxx.r12=0.0;
  rxx.r21=k;
  rxx.r22=1.0;
  particle->xp+=k*particle->x;
  mult_M_M(&rxx,sigma_xx,sigma_xx);
  rxx.transpose();
  mult_M_M(sigma_xx,&rxx,sigma_xx);
  mult_M_M(&r,sigma_xy,sigma_xy);      
  mult_M_M(sigma_xy,&rxx,sigma_xy);
  mult_M_M(&r,sigma,sigma);
  r.transpose();
  mult_M_M(sigma,&r,sigma);
}

void
slice_rotate(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
	     R_MATRIX *sigma_xy,double theta)
{
  double c,s,tmp;
  R_MATRIX tmp1,tmp2;

  if (fabs(theta)<=1e-20) return;
  s=sin(theta);
  c=cos(theta);

  tmp=c*particle->x-s*particle->y;
  particle->y=s*particle->x+c*particle->y;
  particle->x=tmp;
  tmp=c*particle->xp-s*particle->yp;
  particle->yp=s*particle->xp+c*particle->yp;
  particle->xp=tmp;
  
  tmp1.r11=c*c*sigma_xx->r11-2.0*c*s*sigma_xy->r11
    +s*s*sigma->r11;
  tmp1.r12=c*c*sigma_xx->r12-c*s*(sigma_xy->r12+sigma_xy->r21)
    +s*s*sigma->r12;
  tmp1.r22=c*c*sigma_xx->r22-2.0*c*s*sigma_xy->r22+s*s*sigma->r22;
  
  tmp2.r11=c*c*sigma->r11+2.0*c*s*sigma_xy->r11
    +s*s*sigma_xx->r11;
  tmp2.r12=c*c*sigma->r12+c*s*(sigma_xy->r12+sigma_xy->r21)
    +s*s*sigma_xx->r12;
  tmp2.r22=c*c*sigma->r22+2.0*c*s*sigma_xy->r22+s*s*sigma_xx->r22;
  
  sigma_xy->r11=(c*c-s*s)*sigma_xy->r11
    +c*s*(sigma_xx->r11-sigma->r11);
  tmp=c*c*sigma_xy->r12-s*s*sigma_xy->r21
    +c*s*(sigma_xx->r12-sigma->r12);
  sigma_xy->r21=c*c*sigma_xy->r21
    -s*s*sigma_xy->r12+c*s*(sigma_xx->r21-sigma->r21);
  sigma_xy->r12=tmp;
  sigma_xy->r22=(c*c-s*s)*sigma_xy->r22+c*s*(sigma_xx->r22-sigma->r22);
  
  sigma->r11=tmp2.r11;
  sigma->r12=tmp2.r12;
  sigma->r21=tmp2.r12;
  sigma->r22=tmp2.r22;
  sigma_xx->r11=tmp1.r11;
  sigma_xx->r12=tmp1.r12;
  sigma_xx->r21=tmp1.r12;
  sigma_xx->r22=tmp1.r22;
}

void
slice_rotate_45(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
		R_MATRIX *sigma_xy)
{
  slice_rotate(particle,sigma,sigma_xx,sigma_xy,PI/4.);
}

void
slice_rotate_back_45(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
		     R_MATRIX *sigma_xy)
{
  slice_rotate(particle,sigma,sigma_xx,sigma_xy,-PI/4.);
}

void
quadrupole_fill_mode(BEAM *beam,int j,WAKE_MODE *wake,double charge)
{
  quad_kick_data_fill(beam->quad_kick_data,beam,j,wake->lambda*1e6,wake->a0,
		      wake->Q,charge);
}

void
quadrupole_fill_mode_const(BEAM *beam,int j,WAKE_MODE *wake,double charge)
{
  quad_kick_data_fill_const(beam->quad_kick_data,beam,j,wake->lambda*1e6,
			    wake->a0,wake->Q,charge);
}

void
quadrupole_wake_prepare(BEAM *beam,double charge)
{
  Tcl_HashEntry *entry;
  Tcl_HashSearch search;
  WAKE_MODE *wake;
  int nmax=-1,j;

  /* count the number of modes */

  entry=Tcl_FirstHashEntry(&ModeTable,&search);
  while(entry){
    wake=(WAKE_MODE*)Tcl_GetHashValue(entry);
    if(wake->number>nmax) nmax=wake->number;
    entry=Tcl_NextHashEntry(&search);
  }

  /* If no mode was found we are done */

  if (nmax<0) return;

  /* Create the necessary data structure */

  //  placet_printf(INFO,"bunch_info1 = %d\n",beam->drive_data->param.inject->bunch);
  beam->quad_kick_data=quad_kick_data(beam,nmax+1);
  //  placet_printf(INFO,"bunch_info1 = %d\n",beam->drive_data->param.inject->bunch);

  /* For each mode produce the necessary data */

  entry=Tcl_FirstHashEntry(&ModeTable,&search);
  while(entry){
    wake=(WAKE_MODE*)Tcl_GetHashValue(entry);
    j=wake->number;
    //  placet_printf(INFO,"bunch_info2 = %d\n",beam->drive_data->param.inject->bunch);
    quadrupole_fill_mode(beam,j,wake,charge);
    //  placet_printf(INFO,"bunch_info2 = %d\n",beam->drive_data->param.inject->bunch);
    entry=Tcl_NextHashEntry(&search);
  }
}

/*x*/
void
quadrupole_wake_prepare_const(BEAM *beam,double charge)
{
  Tcl_HashEntry *entry;
  Tcl_HashSearch search;
  WAKE_MODE *wake;
  int nmax=-1,j;

  /* count the number of modes */

  entry=Tcl_FirstHashEntry(&ModeTable,&search);
  while(entry){
    wake=(WAKE_MODE*)Tcl_GetHashValue(entry);
    if(wake->number>nmax) nmax=wake->number;
    entry=Tcl_NextHashEntry(&search);
  }

  /* If no mode was found we are done */

  if (nmax<0) return;

  /* Create the necessary data structure */

  beam->quad_kick_data=quad_kick_data_const(beam,nmax+1);

  /* For each mode produce the necessary data */

  entry=Tcl_FirstHashEntry(&ModeTable,&search);
  while(entry){
    wake=(WAKE_MODE*)Tcl_GetHashValue(entry);
    j=wake->number;
    quadrupole_fill_mode_const(beam,j,wake,charge);
    entry=Tcl_NextHashEntry(&search);
  }
}

void
quadrupole_kick(BEAM *beam,double length,int mode)
{
  int i,k,j;
  double quad_1_c=0.0,quad_1_s=0.0,quad_2_c=0.0,quad_2_s=0.0;
  double *damp,*sine,*cosine;
  double factor,wgt;

  if (beam->quad_kick_data==NULL){
    return;
  }

  /* Add kick values */

  factor=length;
  cosine=beam->quad_kick_data->c[mode];
  sine=beam->quad_kick_data->s[mode];
  damp=beam->quad_kick_data->a[mode];
  i=0;
  for (k=0;k<beam->bunches;k++){
    /*
      Apply the necessary damping from bunch to bunch
    */
    if (k>0){
      quad_1_c*=damp[k];
      quad_1_s*=damp[k];
      quad_2_c*=damp[k];
      quad_2_s*=damp[k];
    }
    for (j=0;j<beam->slices_per_bunch;j++){
      /* Calculate wakefield produced by the particle */
      wgt=beam->particle[i].wgt*quadrupole_moment(beam->particle+i,
						  beam->sigma+i,
						  beam->sigma_xx+i,
						  beam->sigma_xy+i);
      quad_1_c-=wgt*sine[i];
      quad_1_s+=wgt*cosine[i];
      /* Apply the kick of previous particles */
      quadrupole_step_kick(damp[0]*factor
			   *(quad_1_c*cosine[i]+quad_1_s*sine[i]),
			   beam->particle+i,beam->sigma+i,beam->sigma_xx+i,
			   beam->sigma_xy+i);
      
      slice_rotate_45(beam->particle+i,beam->sigma+i,beam->sigma_xx+i,
		      beam->sigma_xy+i);

      wgt=beam->particle[i].wgt*quadrupole_moment(beam->particle+i,
						  beam->sigma+i,
						  beam->sigma_xx+i,
						  beam->sigma_xy+i);
      quad_2_c-=wgt*sine[i];
      quad_2_s+=wgt*cosine[i];
      /* Apply the kick of previous particles */
      quadrupole_step_kick(damp[0]*factor
			   *(quad_2_c*cosine[i]+quad_2_s*sine[i]),
			   beam->particle+i,beam->sigma+i,beam->sigma_xx+i,
			   beam->sigma_xy+i);
      
      /* Rotate beam back into original orientation */

      slice_rotate_back_45(beam->particle+i,beam->sigma+i,beam->sigma_xx+i,
			   beam->sigma_xy+i);

      i++;
    }
  }
}

/*
 This routine is the same as above but it assumes that the slices within each
bunch have a constant distance.
*/

/*x*/
void
quadrupole_kick_const(BEAM *beam,double length,int mode)
{
  int i,k,j;
  double quad_1_c=0.0,quad_1_s=0.0,quad_2_c=0.0,quad_2_s=0.0;
  double *damp,*sine,*cosine,*c0,*s0;
  double factor,wgt,s,c,cd,sd,tmp;

  if (beam->quad_kick_data==NULL){
    return;
  }

  /* Add kick values */

  factor=length;
  c0=beam->quad_kick_data->c[mode];
  cosine=beam->quad_kick_data->c[mode]+beam->bunches;
  s0=beam->quad_kick_data->s[mode];
  sine=beam->quad_kick_data->s[mode]+beam->bunches;
  damp=beam->quad_kick_data->a[mode];
  i=0;
  for (k=0;k<beam->bunches;k++){
    /*
      Apply the necessary damping from bunch to bunch
    */
    if (k>0){
      quad_1_c*=damp[k];
      quad_1_s*=damp[k];
      quad_2_c*=damp[k];
      quad_2_s*=damp[k];
    }
    s=s0[k];
    c=c0[k];
    sd=sine[k];
    cd=cosine[k];
    for (j=0;j<beam->slices_per_bunch;j++){
      /* Calculate wakefield produced by the particle */
      wgt=beam->particle[i].wgt*quadrupole_moment(beam->particle+i,
						  beam->sigma+i,
						  beam->sigma_xx+i,
						  beam->sigma_xy+i);
      quad_1_c-=wgt*s;
      quad_1_s+=wgt*c;
      /* Apply the kick of previous particles */
      quadrupole_step_kick(damp[0]*factor
			   *(quad_1_c*c+quad_1_s*s),
			   beam->particle+i,beam->sigma+i,beam->sigma_xx+i,
			   beam->sigma_xy+i);
      
      slice_rotate_45(beam->particle+i,beam->sigma+i,beam->sigma_xx+i,
		      beam->sigma_xy+i);

      wgt=beam->particle[i].wgt*quadrupole_moment(beam->particle+i,
						  beam->sigma+i,
						  beam->sigma_xx+i,
						  beam->sigma_xy+i);
      quad_2_c-=wgt*s;
      quad_2_s+=wgt*c;
      /* Apply the kick of previous particles */
      quadrupole_step_kick(damp[0]*factor
			   *(quad_2_c*c+quad_2_s*s),
			   beam->particle+i,beam->sigma+i,beam->sigma_xx+i,
			   beam->sigma_xy+i);
      
      /* Rotate beam back into original orientation */

      slice_rotate_back_45(beam->particle+i,beam->sigma+i,beam->sigma_xx+i,
			   beam->sigma_xy+i);
      tmp=c*cd-s*sd;
      s=s*cd+c*sd;
      c=tmp;
      i++;
    }
  }
}

#else

QUAD_KICK_DATA*
quad_kick_data(BEAM *beam,int n_cell)
{
  return NULL;
}

QUAD_KICK_DATA*
quad_kick_data_const(BEAM *beam,int n_cell)
{
  return NULL;
}

void
quad_kick_data_delete(QUAD_KICK_DATA *quad_kick_data)
{
}

void
quad_kick_data_fill(QUAD_KICK_DATA *quad_kick_data,BEAM *beam,int i_cell,
		    double lambda,double loss,double q,double charge)
{
}

void
quad_kick_data_fill_const(QUAD_KICK_DATA *quad_kick_data,BEAM *beam,int i_cell,
			  double lambda,double loss,double q,double charge)
{
}

double
quadrupole_moment(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
		  R_MATRIX *sigma_xy)
{
}

void
quadrupole_step_kick(double k,PARTICLE *particle,R_MATRIX *sigma,
		     R_MATRIX *sigma_xx,R_MATRIX *sigma_xy)
{
}

void
slice_rotate(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
	     R_MATRIX *sigma_xy,double theta)
{
}

void
slice_rotate_45(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
		R_MATRIX *sigma_xy)
{
}

void
slice_rotate_back_45(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
		     R_MATRIX *sigma_xy)
{
}

void
quadrupole_fill_mode(BEAM *beam,int j,WAKE_MODE *wake,double charge)
{
}

void
quadrupole_fill_mode_const(BEAM *beam,int j,WAKE_MODE *wake,double charge)
{
}

void
quadrupole_wake_prepare(BEAM *beam,double charge)
{
}

/*x*/
void
quadrupole_wake_prepare_const(BEAM *beam,double charge)
{
}

void
quadrupole_kick(BEAM *beam,double length,int mode)
{
}

/*
 This routine is the same as above but it assumes that the slices within each
bunch have a constant distance.
*/

/*x*/
void
quadrupole_kick_const(BEAM *beam,double length,int mode)
{
}

#endif
