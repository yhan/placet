// included by cavity.cc

#include "dipole_kick.h"
#include "quadwake.h"
#include "wakekick.h"

#ifdef SECOND_ORDER
#ifdef RFCAV
void CAVITY::step_rf(BEAM *beam)
{
#else
void CAVITY::step_4d(BEAM *beam)
{
#endif
#else
#ifdef RFCAV
void CAVITY::step_rf_0(BEAM *beam)
{
#else
void CAVITY::step_4d_0(BEAM *beam)
{
#endif
#endif

#ifdef HTGEN
  //sorting halo particles
  //particle_cmp is defined in particle.h
  size_t nhalo = beam->nhalo; 
  qsort(beam->particle_sec,nhalo,sizeof(PARTICLE),cmp_func_t(&particle_cmp));
#endif
  if (beam->particle_beam) {
#ifdef SECOND_ORDER
    step_many_x(beam);
    return;
#else
    step_many_0_x(beam);
    return;
#endif
  }

  // no halo tracking implemented, because ifdef LONGITUDINAL is never called
#ifdef LONGITUDINAL
  if (beam->macroparticles==1) {
    for (int i=0;i<beam->slices;i++){
      beam->z_position[i]+=geometry.length*0.5e-6
	*particle.yp*particle.yp;
      beam->z_position[i]+=geometry.length*0.5*1e6
	*(EMASS*EMASS)/(particle.energy*particle.energy);
    }
  }
#endif
  //  placet_printf(INFO,"angle %g\n",offset.yp);
  double half_length=0.5*geometry.length;
  double length_i=1.0/geometry.length;
  //  de=beam->acc_field[element->field];
  double *de=beam->field[field].de;
/*changed to allow different structures */
  double *s_beam=beam->s_long[field];
  double *c_beam=beam->c_long[field];
  if (s_beam[0]>1.0) {
    for (int i=0;i<beam->slices_per_bunch*beam->bunches;++i) {
      sincos(beam->z_position[i]/injector_data.lambda[0]*1e-6*TWOPI,s_beam+i,
	      c_beam+i);
    }
  }
  /*
  s_long=sin(element->v5);
  c_long=cos(element->v5);
  */
  double s_long,c_long;
  sincos(phase,&s_long,&c_long);
  
#ifdef RFCAV
  /*
    Move beam to the centre of the structure
  */
  double wgt=0.0;
  double x1=0.0;
  double y1=0.0;
#endif
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
#ifdef RFCAV
    wgt+=fabs(particle.wgt);
    x1+=particle.x*fabs(particle.wgt);
    y1+=particle.y*fabs(particle.wgt);
#endif
    int ics=i/beam->macroparticles;// ics = slice number
    double de0=(c_beam[ics]*c_long + s_beam[ics]*s_long)*gradient + beam->factor*de[ics];// RF gradient G_i
    double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
    double gammap=delta*length_i;
    particle.yp-=gammap*particle.y;
#endif
    double lndelta;
    if (delta>0.01) {
      lndelta=log1p(delta)/delta;
    }
    else {
      lndelta=1.0-delta*(0.5+delta*0.3308345316809);
    }
    double tmp=1.0/(1.0+delta);
#else
    //    gammap=0.5*de0/(particle.energy*geometry.length);
    gammap=delta*length_i;
#ifdef END_FIELDS
    particle.yp-=gammap*particle.y;
#endif
    /* scd 1.0-delta ? */
    tmp=(1.0-gammap*length);
#endif
    
#ifdef CAV_PRECISE
    particle.y+=half_length*particle.yp*lndelta;
#else
    particle.y+=half_length*particle.yp;
#endif
    particle.yp*=tmp;
    
#ifdef END_FIELDS
    particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
    particle.x+=half_length*particle.xp*lndelta;
#else
    particle.x+=half_length*particle.xp;
#endif
    particle.xp*=tmp;
    particle.energy+=half_length*de0;
    
#ifdef SECOND_ORDER
    sigma_step_x(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
		 2.0*delta);
#endif
  }
 
#ifdef HTGEN
  /* 
     Move Halo to the center of the structure
  */
  int i_part_sec=0; 
  //loop over bunches
  for(int i_b=0;i_b<beam->bunches;i_b++) {
    //loop over slices
    for (int i_s=0;i_s<beam->slices_per_bunch;i_s++) {
      int i_z=i_b*beam->slices_per_bunch+i_s;// slice number
      double de0=(c_beam[i_z]*c_long + s_beam[i_z]*s_long)*gradient + beam->factor*de[i_z];// RF gradient G_i
      //loop over haloparticles in this slice
      for (int i_m=0;i_m<beam->particle_number_sec[i_z];i_m++) {
	PARTICLE &particle=beam->particle_sec[i_part_sec];
	double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
	double gammap=delta*length_i;
	particle.yp-=gammap*particle.y;
#endif
	double lndelta;
	if (delta>0.01) {
	  lndelta=log1p(delta)/delta;
	}
	else {
	  lndelta=1.0-delta*(0.5+delta*0.3308345316809);
	}
	double tmp=1.0/(1.0+delta);
#else
	gammap=delta*length_i;
#ifdef END_FIELDS
	particle.yp-=gammap*particle.y;
#endif
	tmp=(1.0-gammap*length);
#endif
	
#ifdef CAV_PRECISE
	particle.y+=half_length*particle.yp*lndelta;
#else
	particle.y+=half_length*particle.yp;
#endif
	particle.yp*=tmp;
	
#ifdef END_FIELDS
	particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
	particle.x+=half_length*particle.xp*lndelta;
#else
	particle.x+=half_length*particle.xp;
#endif
	particle.xp*=tmp;
	particle.energy+=half_length*de0;
	i_part_sec++; 
      }
    }
  }
#endif

  // Apply a constant dipole kick
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
    particle.xp += _dipole_kick.x/particle.energy;
    particle.yp += _dipole_kick.y/particle.energy;
  }

  /*
    Apply the wakefield kick
  */
  
  if ((book.x!=0.0)||(book.y!=0.0)) { // unsafe! JS
    bookshelf(beam);
  }
  
  if(wakefield_data.transv){
    if ((beam->which_field==1)||(beam->which_field==3)
	||(beam->which_field==4)||(beam->which_field==123)) {
      wake_kick(this,beam,geometry.length);//no halo tracking included
    } else {
      dipole_kick(this,wake_data,beam);
    }
    if (modes.size()>0){
      for (int i=0;i<modes[0];i++){
	switch (modes[i+1]) {
	case 1:
	  break;
	case 2:
	  quadrupole_kick(beam,geometry.length,modes[i+1]);// no halo tracking included
	  //	quadrupole_kick_const(beam,geometry.length,element->modes[i+1]);
	  break;
	}
      }
    }
  }
  
#ifdef RFCAV
/*
  Move beam to the end of the structure
*/
  double x2=0.0;
  double y2=0.0;
#endif
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
    int ics=i/beam->macroparticles;
    double de0=(c_beam[ics]*c_long + s_beam[ics]*s_long)*gradient + beam->factor*de[ics];
#ifdef CAV_PRECISE
    double delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
    double  gammap=delta/geometry.length;
#endif
    double lndelta;
    if (delta>0.01) {
      lndelta=log1p(delta)/delta;
    } else {
      lndelta=1.0-delta*(0.5+delta*0.3308345316809);
    }
    double tmp=1.0/(1.0+delta);
#else
    //    gammap=0.5*de0/(particle.energy*geometry.length);
    double  gammap=half_length*de0/(particle.energy*length);
    double tmp=(1.0-gammap*length);
#endif
    
#ifdef CAV_PRECISE
    particle.y+=half_length*particle.yp*lndelta;
#else
    particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
    particle.yp+=gammap*particle.y;
#endif
    particle.yp*=tmp;
    
#ifdef CAV_PRECISE
    particle.x+=half_length*particle.xp*lndelta;
#else
    particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
    particle.xp+=gammap*particle.x;
#endif
    particle.xp*=tmp;
    
    particle.energy+=half_length*de0;
    
#ifdef RFCAV
    x2+=particle.x*fabs(particle.wgt);
    y2+=particle.y*fabs(particle.wgt);
#endif
    
#ifdef EARTH_FIELD
    particle.yp+=earth_field.y*geometry.length/particle.energy;
    particle.xp+=earth_field.x*geometry.length/particle.energy;
#endif
  }
#ifdef HTGEN
/*
  Move halo to the end of the structure
*/
  i_part_sec=0;
  //loop over bunches
  for(int i_b=0;i_b<beam->bunches;i_b++) {
    //loop over slices
    for (int i_s=0;i_s<beam->slices_per_bunch;i_s++) {
      int i_z=i_b*beam->slices_per_bunch+i_s;// slice number
      double de0=(c_beam[i_z]*c_long + s_beam[i_z]*s_long)*gradient + beam->factor*de[i_z];// RF gradient G_i
      //loop over haloparticles in this slice
      for (int i_m=0;i_m<beam->particle_number_sec[i_z];i_m++) {
	PARTICLE &particle=beam->particle_sec[i_part_sec];
#ifdef CAV_PRECISE
    double delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
    double gammap=delta/geometry.length;
#endif
    double lndelta;
    if (delta>0.01) {
      lndelta=log1p(delta)/delta;
    } else {
      lndelta=1.0-delta*(0.5+delta*0.3308345316809);
    }
    double tmp=1.0/(1.0+delta);
#else
    double gammap=half_length*de0/(particle.energy*length);
    double tmp=(1.0-gammap*length);
#endif
    
#ifdef CAV_PRECISE
    particle.y+=half_length*particle.yp*lndelta;
#else
    particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
    particle.yp+=gammap*particle.y;
#endif
    particle.yp*=tmp;
    
#ifdef CAV_PRECISE
    particle.x+=half_length*particle.xp*lndelta;
#else
    particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
    particle.xp+=gammap*particle.x;
#endif
    particle.xp*=tmp;
    
    particle.energy+=half_length*de0;
#ifdef EARTH_FIELD
    particle.yp+=earth_field.y*geometry.length/particle.energy;
    particle.xp+=earth_field.x*geometry.length/particle.energy;
#endif
    i_part_sec++; 
      }
    }
  }
#endif
  

#ifdef RFCAV
  bpm.set_x_position(0.5*(x1+x2)/wgt);
  bpm.set_y_position(0.5*(y1+y2)/wgt);
#endif



}
