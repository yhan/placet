#include "cavity_pets.h"

#include "beam.h"

/*
  The routine steps through a PETS cavity using second order transport
*/


#define SECOND_ORDER
//#include "cavity_strange_pets.cc"
#include "cavity_multi_pets2.cc"
#undef SECOND_ORDER

/*
  The routine steps through a PETS cavity using first order transport
*/

//#include "cavity_strange_pets.cc"
#include "cavity_multi_pets2.cc"

CAVITY_PETS::_WAKE_TABLE CAVITY_PETS::wake_table;


CAVITY_PETS::CAVITY_PETS() : ELEMENT()
{
  v1=1.0;
  v3=0.0;
  v4=0.0;
  v5=0.0;
  field=0;
  t=field;
  rf_seed=0.0;
  wake=NULL;
}

CAVITY_PETS::CAVITY_PETS(double length,int _field,double rotation) : ELEMENT(length), field(_field)
{
  v1=1.0;
  v3=0.0;
  v4=0.0;
  v5=rotation*PI/180.0;
  t=field;
  rf_seed=0.0;
  wake=NULL;
}

#define SECOND_ORDER
void CAVITY_PETS::step_twiss(BEAM *beam,FILE * file,double /*step0*/,int iel, double s0, int n1, int n2, void (* callback)(FILE*,BEAM*,int,double,int,int)){
  // perform only tracking without twiss parameter calculation
  placet_cout << VERBOSE << " CAVITY_PETS:: step_twiss:  twiss computed at the entrace and the exit of the element " << endmsg;
  callback(file,beam,iel,s0,n1,n2);
#ifdef SECOND_ORDER
  step_4d(beam);
#endif
  s0+=geometry.length;
  callback(file,beam,iel,s0,n1,n2);
}
#undef SECOND_ORDER

void CAVITY_PETS::step_twiss_0(BEAM *beam,FILE * file,double /*step0*/,int iel,double s0,int n1,int n2, void (* callback)(FILE*,BEAM*,int,double,int,int))
{
  placet_cout << VERBOSE << " CAVITY_PETS:: step_twiss_0: twiss computed at the entrace and the exit of the element" << endmsg;
  callback(file,beam,iel,s0,n1,n2);
  track_0(beam);
  s0+=geometry.length;
  callback(file,beam,iel,s0,n1,n2);
}
