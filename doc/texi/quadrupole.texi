@deffn {Command} Quadrupole
This command places a quadrupole in the current girder.
Valid options for @code{Quadrupole} are listed in the following table.

@table @code
@item -help
 Print summary of command-line options and abort
@item -name                 Element name [STRING]
@item -s                    Longitudinal position [m] [READ-ONLY]
@item -x                    Horizontal offset [um]
@item -y                    Vertical offset [um]
@item -xp                   Horizontal offset in angle [urad]
@item -yp                   Vertical offset in angle [urad]
@item -roll                 Roll angle [urad]
@item -length               Element length [m]
@item -synrad               Synchrotron Radiation emission [BOOL]
@item -six\_dim              Longitudinal motion (6d) [BOOL]
@item -thin\_lens            Thin Lens approximation [INT]
@item -e0                   Reference energy [GeV]
@item -gas\_A                Atomic number [amu]
@item -gas\_Z                Atomic charge [e]
@item -gas\_pressure         Gas pressure [torr]
@item -gas\_temperature      Gas temperature [K]
@item -gas\_minumum\_theta    Minimum scattering angle [rad]
@item -radiation\_length     Radiation length [m]
@item -aperture\_x           Horizontal aperture [m]
@item -aperture\_y           Vertical aperture [m]
@item -aperture\_losses      Fraction of particles which are lost [\#] [READ-ONLY]
@item -aperture\_shape       Aperture shape [STRING]
@item -strength             Quadrupole strength [GeV/m]
@item -tilt                 Tilt angle [rad]
@item -hcorrector           Horizontal correction leverage [STRING] [READ-ONLY]
@item -hcorrector\_step\_size Horizontal corrector step size [DOUBLE]
@item -vcorrector           Vertical correction leverage [STRING] [READ-ONLY]
@item -vcorrector\_step\_size Vertical corrector step size [DOUBLE]
@end table
@end deffn
