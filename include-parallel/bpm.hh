#ifndef bpm_hh
#define bpm_hh

#include "element.hh"

class Bpm : public Element {

  size_t n_bunches;

  double resolution;
	
  mutable double x_err;
  mutable double y_err;

  mutable double x_sum;
  mutable double y_sum;

  mutable double sum;

public:

  explicit Bpm(const std::string &name, double length=0.0, size_t _n_bunches=0, double _resolution=0.0 );
  explicit Bpm(double length=0.0, size_t _n_bunches=0, double _resolution=0.0 );

  int get_id() const { return ELEMENT_ID::BPM; }
	
  void	 set_resolution(double r ) { resolution = r; }
  double get_resolution() const { return resolution; }

  double get_x_reading() const { return x_sum / sum + x_err; }
  double get_y_reading() const { return y_sum / sum + y_err; }

  double get_x_mean() const { return x_sum / sum; }
  double get_y_mean() const { return y_sum / sum; }

  double get_x_sum() const { return x_sum; }
  double get_y_sum() const { return y_sum; }

  double get_sum() const { return sum; }

  void set_x_sum(double _x_sum ) const { x_sum = _x_sum; }
  void set_y_sum(double _y_sum ) const { y_sum = _y_sum; }

  void set_sum(double _sum ) const { sum = _sum; }

  void get_readings(double &x_reading, double &y_reading ) const
  {
    x_reading = x_sum / sum + x_err;
    y_reading = y_sum / sum + y_err;
  }

  void get_readings(std::pair<double, double> &reading ) const
  {
    reading.first	= x_sum / sum + x_err;
    reading.second	= y_sum / sum + y_err;
  }
	
  void get_mean(double &_x_mean, double &_y_mean ) const
  {
    _x_mean = x_sum / sum;
    _y_mean = y_sum / sum;
  }

  void get_mean(std::pair<double, double> &mean ) const
  {
    mean.first	= x_sum / sum;
    mean.second	= y_sum / sum;
  }

  void transport(std::vector<Particle> &beam ) const;
  void transport_synrad (std::vector<Particle> &beam ) const
  {
    transport(beam);
  }

  friend OStream &operator<<(OStream &stream, const Bpm &bpm )
  {
    return stream	<< static_cast<const Element&>(bpm) 
			<< bpm.n_bunches
			<< bpm.resolution
			<< bpm.x_err
			<< bpm.y_err
			<< bpm.x_sum
			<< bpm.y_sum
			<< bpm.sum;
  }
	
  friend IStream &operator>>(IStream &stream, Bpm &bpm )
  {
    return stream 	>> static_cast<Element&>(bpm)
			>> bpm.n_bunches
			>> bpm.resolution
			>> bpm.x_err
			>> bpm.y_err
			>> bpm.x_sum
			>> bpm.y_sum
			>> bpm.sum;
  }

};

#endif /* bpm_hh */
