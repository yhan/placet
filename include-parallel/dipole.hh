#ifndef dipole_hh
#define dipole_hh

#include "element.hh"

class Dipole : public Element {

  double a_x, a_y;

public:

  Dipole(double a_x, double a_y, double length=0.0 ) : Element(length) {}
  Dipole() : a_x(0.0), a_y(0.0) {}

  int get_id() const { return ELEMENT_ID::DIPOLE; }
		
  void transport(std::vector<Particle> &beam ) const;
  void transport_synrad (std::vector<Particle> &beam ) const
  {
    transport(beam);
  }

  friend OStream &operator<<(OStream &stream, const Dipole &d )	{ return stream << static_cast<const Element&>(d) << d.a_x << d.a_y; }
  friend IStream &operator>>(IStream &stream, Dipole &d ) { return stream >> static_cast<Element&>(d) >> d.a_x >> d.a_y; }

};

#endif /* dipole_hh */
