#ifndef sbend_hh
#define sbend_hh

#include "element.hh"

class SBend : public Element {

  double	e1;	// rad
  double	e2;	// rad
	
  double	angle0;	// rad
  double 	k;	// GeV / m^2
	
public:

  SBend(const std::string &name, double _e1, double _e2, double _angle0, double _length, double _ref_energy, double _k ) : Element(name, _length, _ref_energy), e1(_e1), e2(_e2), angle0(_angle0), k(_k) {}
  SBend(double _e1, double _e2, double _angle0, double _length, double _ref_energy, double _k ) : Element(_length, _ref_energy), e1(_e1), e2(_e2), angle0(_angle0), k(_k) {}
  SBend() {}

  int get_id() const { return ELEMENT_ID::SBEND; }

  void transport(std::vector<Particle> &beam ) const;
  void transport_synrad(std::vector<Particle> &beam ) const;

  friend OStream &operator<<(OStream &stream, const SBend &sbend )
  { 
    return stream << static_cast<const Element&>(sbend)
		  << sbend.e1
		  << sbend.e2
		  << sbend.angle0
		  << sbend.k;
  }
  friend IStream &operator>>(IStream &stream, SBend &sbend )
  {
    return stream >> static_cast<Element&>(sbend) 
		  >> sbend.e1 
		  >> sbend.e2
		  >> sbend.angle0 
		  >> sbend.k;
  }

};

#endif /* sbend_hh */
