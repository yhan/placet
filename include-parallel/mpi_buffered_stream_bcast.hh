#ifndef mpi_buffered_stream_bcast_hh
#define mpi_buffered_stream_bcast_hh

#include "mpi_stream_bcast.hh"
#include "fifo_stream.hh"

#define BUFFER_SIZE (1L<<18)

class MPI_Buffered_OStream_Bcast : public OStream {

  MPI_OStream_Bcast stream;
  FIFO_Stream buffer;
				
  MPI_Buffered_OStream_Bcast(const MPI_Buffered_OStream_Bcast & );
  void operator = (const MPI_Buffered_OStream_Bcast & );
	
protected:

  bool writable() const { return stream && buffer.free() > 0; }

public:

  MPI_Buffered_OStream_Bcast(MPI_Comm _comm = MPI_COMM_WORLD, size_t _size = BUFFER_SIZE ) : stream(_comm), buffer(_size) {}
  MPI_Buffered_OStream_Bcast(size_t _size ) : stream(MPI_COMM_WORLD), buffer(_size) { stream << _size; }
	
  ~MPI_Buffered_OStream_Bcast() { flush(); }
	
  void flush() { broadcast_buffer(); }

  // write methods
#define WRITE(TYPE)				\
  size_t write(const TYPE *ptr, size_t n )	\
  {						\
    size_t __t=buffer.write(ptr,n);		\
    while(__t<n) {				\
      flush();					\
      size_t __l=buffer.write(ptr+__t,n-__t);	\
      if (__l==0) break;			\
      __t+=__l;					\
    }						\
    return __t;					\
  }						\
  size_t write(const TYPE &ref )		\
  {						\
    if (buffer.write(ref)) return 1;		\
    flush();					\
    return buffer.write(ref);			\
  }
  
  WRITE(char)
  WRITE(bool)
  WRITE(float)
  WRITE(double)
  WRITE(long double)
  WRITE(signed short)
  WRITE(signed int)
  WRITE(signed long int)
  WRITE(unsigned short)
  WRITE(unsigned int)
  WRITE(unsigned long int)
#undef WRITE

  void broadcast_buffer()
  {
    size_t size=buffer.size();
    if (size>0) {
      stream.write(size);
      stream.write(buffer.c_ptr(),size);
      buffer.clear();
    }
  }	

};

class MPI_Buffered_IStream_Bcast : public IStream {

  MPI_IStream_Bcast stream;
  FIFO_Stream buffer;
		
  MPI_Buffered_IStream_Bcast();
  MPI_Buffered_IStream_Bcast(const MPI_Buffered_IStream_Bcast & );
	
  void operator = (const MPI_Buffered_IStream_Bcast & );
	
protected:

  bool readable() const { return stream && buffer.size() > 0; }

public:


  MPI_Buffered_IStream_Bcast(int _root, MPI_Comm _comm = MPI_COMM_WORLD, size_t _size = BUFFER_SIZE ) : stream(_root, _comm), buffer(_size) {}
  ~MPI_Buffered_IStream_Bcast() {}
	
  // read methods
#define READ(TYPE)				\
  size_t read(TYPE *ptr, size_t n )		\
  {						\
    size_t __t=buffer.read(ptr,n);		\
    while(__t<n) {				\
      broadcast_buffer();			\
      size_t __l=buffer.read(ptr+__t,n-__t);	\
      if (__l==0) break;			\
      __t+=__l;					\
    }						\
    return __t;					\
  }						\
  size_t read(TYPE &ref )			\
  {						\
    if (buffer.read(ref)) return 1;		\
    broadcast_buffer();				\
    return buffer.read(ref);			\
  }

  READ(char)
  READ(bool)
  READ(float)
  READ(double)
  READ(long double)
  READ(signed short)
  READ(signed int)
  READ(signed long int)
  READ(unsigned short)
  READ(unsigned int)
  READ(unsigned long int)
#undef READ

  void broadcast_buffer()
  {
    size_t size;
    buffer.clear();
    stream.read(size);
    stream.read(buffer.c_ptr(),size);
    buffer.in(size);
  }

};

#endif /* mpi_buffered_stream_bcast_hh */
