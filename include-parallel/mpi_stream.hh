#ifndef mpi_stream_hh
#define mpi_stream_hh

#ifndef mpi_buffered_stream_hh
#warning "Why using `mpi_stream.hh' when `mpi_buffered_stream.hh' is much faster?"
#endif

#include <cstdio>
#include <string>
#include <vector>
#include <list>

#include <mpi.h>

#include "stream.hh"

class MPI_OStream : public OStream {

  int dest;
  int tag;
  MPI_Comm comm;
	
protected:

  bool writable() const { return true; }

public:

  MPI_OStream(int _dest=0, int _tag=0, MPI_Comm _comm=MPI_COMM_WORLD ) : dest(_dest), tag(_tag), comm(_comm) {}
	
  int get_dest() const { return dest; }
  int get_tag() const { return tag; }
  MPI_Comm get_comm() const { return comm; }
	
  void set_dest(int d ) { dest=d; }
  void set_tag(int t ) { tag=t; }
  void set_comm(MPI_Comm c ) { comm=c; }
	
  void set_dest(int _dest, int _tag, MPI_Comm _comm=MPI_COMM_WORLD )
  {
    dest=_dest;
    tag=_tag;
    comm=_comm;
  }
	
  // writing methods
#define WRITE(TYPE, MPI_TYPE)						\
  inline size_t write(const TYPE *ptr, size_t n ) { MPI_Send(const_cast<TYPE*>(ptr),  n, MPI_TYPE, dest, tag, comm); return n; } \
  inline size_t write(const TYPE &ref )           { MPI_Send(const_cast<TYPE*>(&ref), 1, MPI_TYPE, dest, tag, comm); return 1; }
  
  WRITE(char,		   MPI_CHAR)
  WRITE(bool,	           MPI_INT)
  WRITE(signed short,	   MPI_SHORT)
  WRITE(signed int,	   MPI_INT)
  WRITE(signed long int,   MPI_LONG)
  WRITE(unsigned short,    MPI_UNSIGNED_SHORT)
  WRITE(unsigned int,	   MPI_UNSIGNED)
  WRITE(unsigned long int, MPI_UNSIGNED_LONG)
  WRITE(float,		   MPI_FLOAT)
  WRITE(double,		   MPI_DOUBLE)
  WRITE(long double,	   MPI_LONG_DOUBLE)
#undef WRITE
	
};

class MPI_IStream : public IStream {
	
  int src;
  int tag;
  MPI_Comm comm;
	
protected:
	
  bool readable()	const { return true; }
	
public:
	
  MPI_IStream(int _src=0, int _tag=0, MPI_Comm _comm=MPI_COMM_WORLD ) : src(_src), tag(_tag), comm(_comm) {}
	
  int get_src() const { return src; }
  int get_tag() const { return tag; }
  MPI_Comm get_comm() const { return comm; }
	
  void set_src(int d ) { src=d; }
  void set_tag(int t ) { tag=t; }
  void set_comm(MPI_Comm c ) { comm=c; }
	
  void set(int _src, int _tag=0, MPI_Comm _comm=MPI_COMM_WORLD )
  {
    src=_src;
    tag=_tag;
    comm=_comm;
  }
	
  // reading methods
#define READ(TYPE, MPI_TYPE)						\
  inline size_t read(TYPE *ptr, size_t n )        { MPI_Recv(const_cast<TYPE*>(ptr),  n, MPI_TYPE, src, tag, comm, MPI_STATUS_IGNORE); return n; } \
  inline size_t read(TYPE &ref )                  { MPI_Recv(const_cast<TYPE*>(&ref), 1, MPI_TYPE, src, tag, comm, MPI_STATUS_IGNORE); return 1; }

  READ(char,		  MPI_CHAR)
  READ(bool,	          MPI_INT)
  READ(signed short,	  MPI_SHORT)
  READ(signed int,	  MPI_INT)
  READ(signed long int,	  MPI_LONG)
  READ(unsigned short,	  MPI_UNSIGNED_SHORT)
  READ(unsigned int,	  MPI_UNSIGNED)
  READ(unsigned long int, MPI_UNSIGNED_LONG)
  READ(float,	          MPI_FLOAT)
  READ(double,		  MPI_DOUBLE)
  READ(long double,	  MPI_LONG_DOUBLE)
#undef READ

};

#endif /* mpi_stream_hh */
