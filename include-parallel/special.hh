#ifndef special_hh
#define special_hh

#include "element.hh"

struct Special : public Element {

  explicit Special(const std::string &name, int id ) : Element(name, id) {}
  Special() {}

  int get_id() const { return ELEMENT_ID::SPECIAL; }

  void transport(std::vector<Particle> &beam ) const { std::cerr << "warning: tracking through a special element" << std::endl; }
  void transport_synrad (std::vector<Particle> &beam ) const
  {
    transport(beam);
  }

};

#endif /* special_hh */
