## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_response_matrix_optics (@var{beamline}, @var{bpms}, @var{correctors}, @var{axis})
## Return the optics response matrix for 'beamline' using selected 'bpms' and 'correctors', in the direction specified by 'axis' ("x" or "y", "xy" or "yx").
## @end deftypefn

function R = placet_get_response_matrix_optics (beamline, B, C, Axis)
	if nargin==4 && ischar(beamline) && isvector(B) && isvector(C)
		R=zeros(length(B),length(C));
		Axis=tolower(Axis);
		if Axis == "x" || Axis == "xx"
			i1=1;
			i2=2;
		elseif Axis == "y" || Axis == "yy"
			i1=3;
			i2=4;
		elseif Axis == "xy"
			i1=1;
			i2=4;
		elseif Axis == "yx"
			i1=3;
			i2=2;
		end
		for i=1:length(C)
			J=find(C(i)<B);
			if length(J)>0
				Ei=placet_element_get_attribute(beamline, C(i), "e0");
				for j=1:length(J)
					T=placet_get_transfer_matrix(beamline, C(i), B(J(j))-1)(1:4,1:4);
					Ej=placet_element_get_attribute(beamline, B(J(j)), "e0");
					R(J(j),i)=T(i1,i2)/sqrt(Ei*Ej);
				end
			end
		end
	else  
		help placet_get_response_matrix_optics
	endif
endfunction
