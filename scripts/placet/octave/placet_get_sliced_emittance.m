## Return the sliced horizontal and vertical normalized emittances of beam `B', where `B' is a 6-columns matrix in guinea-pig format
## N is the number of slices

function [E,Z]=placet_get_sliced_emittance(B,N)
    E = [];
    Z = [];
    sigmaz = std(B(:,4));
    z = linspace(-3*sigmaz,3*sigmaz,N+1);
    for index =1:N
        Bmask = B(:,4)>z(index) & B(:,4)<z(index+1);
        Emit = placet_get_emittance(B(Bmask,:));
        pos = (z(index) + z(index+1))/2;
        E = [ E ; Emit(1), Emit(2) ];
        Z = [ Z ; pos ];
    endfor
endfunction
