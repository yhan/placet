#ifndef latex_picture_h
#define latex_picture_h

#include <cstdio>

class LaTeX_Picture {

  FILE *file;

  void header(double sizex=30., double sizey=20., const char *unitlength=NULL )
  {
    if (unitlength)
      fprintf(file, "\\setlength{\\unitlength}{%s}\n", unitlength);
    fprintf(file, "\\begin{picture}(%g,%g)\n", sizex, sizey);
  }

public:

  LaTeX_Picture(double sizex=30., double sizey=20., const char *unitlength=NULL ) : file(stdout)
  {
    header(sizex, sizey, unitlength);
  }
  LaTeX_Picture(const char *filename, double sizex=30., double sizey=20., const char *unitlength=NULL )
  {
    file=fopen(filename, "w");
    header(sizex, sizey, unitlength);
  }

  ~LaTeX_Picture()
  {
    fputs("\\end{picture}\n", file);
    if (file!=stdout)
      fclose(file);
  }

  void linethickness(const char *line1 )
  {
    fprintf(file, "\\linethickness{%s}\n", line1);
  }

  void put(double x, double y )
  {
    fprintf(file, "\\put(%g,%g){\n", x, y);
  }

  void multiput(double x, double y, double dx, double dy, int n )
  {
    fprintf(file, "\\multiput(%g,%g)(%g,%g){%d}{\n", x, y, dx, dy, n);
  }

  void line(double tx, double ty, double length )
  {
    fprintf(file, "\\line(%g,%g){%g}}\n", tx, ty, length);
  }

  void vector(double tx, double ty, double length )
  {
    fprintf(file, "\\vector(%g,%g){%g}}\n", tx, ty, length);
  }

 private:
  /// private copy constructor (not implemented, rule of three)
  LaTeX_Picture(const LaTeX_Picture&);
  /// private assign operator (not implemented, rule of three)
  LaTeX_Picture& operator=(const LaTeX_Picture&);
};

#endif /* latex_picture_h */
