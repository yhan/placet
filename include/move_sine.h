#ifndef move_sine_h
#define move_sine_h

int tk_MoveInitSine(ClientData ,Tcl_Interp *,int ,char **);
int tk_MoveStepSine(ClientData ,Tcl_Interp *,int ,char **);

#endif // move_sine_h
