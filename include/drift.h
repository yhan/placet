#ifndef drift_h
#define drift_h

#include "element.h"

class DRIFT : public ELEMENT {

  void step_out(BEAM *beam );

  void step_4d(BEAM *beam) { drift_step_4d(beam); }
  void step_4d_0(BEAM *beam) { drift_step_4d_0(beam); }
  void step_4d_sr(BEAM *beam) { drift_step_4d(beam); }
  void step_4d_sr_0(BEAM *beam) { drift_step_4d_0(beam); }

  void step_6d(BEAM *beam) { drift_step_6d(beam); }
  void step_6d_0(BEAM *beam) { drift_step_6d_0(beam); }
  void step_6d_sr(BEAM *beam) { drift_step_6d(beam); }
  void step_6d_sr_0(BEAM *beam) { drift_step_6d_0(beam); }

/*   void drift_step_twiss_0(BEAM *beam) { drift_step_twiss_0(beam); } */
/*   void drift_step_twiss(BEAM *beam) { drift_step_twiss(beam); } */

  DRIFT();

public:

  DRIFT(int &argc, char **argv) : ELEMENT() {set_attributes(argc,argv);}
  explicit DRIFT(double l) : ELEMENT(l) {}

  bool is_drift() const { return true; }

  DRIFT *drift_ptr() { return this; }
  const DRIFT *drift_ptr() const { return this; }

  friend OStream &operator<<(OStream &stream, const DRIFT &drift )
  {
    return stream << static_cast<const ELEMENT &>(drift);
  }
   
};

#endif /* drift_h */
