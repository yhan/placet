#ifndef rndm2_h
#define rndm2_h

class RANDOM {
 public:
  virtual ~RANDOM(){}
  virtual float value()=0;
  virtual void init(int)=0;
};

class RANDOM8 : public RANDOM {
  float r[97];
  int ix1,ix2,ix3;
 public:
  virtual ~RANDOM8(){}
  virtual float value();
  virtual void init(int);
};

class RANDOM_GAUSS : public RANDOM {
  RANDOM8 rndm;
  float v1,v2,low,high,mean,sigma;
  int iset;
  virtual float value_int();
 public:
  virtual ~RANDOM_GAUSS(){}
  virtual float value();
  virtual void init(int);
  void init(int,double,double);
  void set_sigma(double s) {sigma=s;}; 
};

#endif //rndm2_h
