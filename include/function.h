/** 
 * @file
 * collection of fast math routines 
 */

#ifndef function_h
#define function_h

#include <limits>
#include <vector>

static inline double inverse(double x )
{
  return 1/x;
}

static inline double opposite(double x )
{
  return -x;
}

/** 
 * fast way to calculate both sinh and cosh
 * syntax similar to sincos(phi,s,c),
 * except sincosh expects doubles and no pointers!
 */
static inline void sincosh(const double a, double &s, double &c) {

  if (std::abs(a) <= 0.05) {
    s = sinh(a);
    c = sqrt(1.0 + s*s);
  } else {
    double ea = exp(a);
    double inv_ea = 1./(ea);
    s = (ea - inv_ea)*0.5;
    c = (ea + inv_ea)*0.5;
  }
}

/**
 * calculates power n of double x
 */
template <class T>
static inline T pow_i(T x,int n)
{
  T res=T(1);
  while(n){
    if(n&1) res*=x;
    x*=x;
    n>>=1;
  }
  return res;
}

/** 
 * fast way to calculate sin(n*phi) and cos(n*phi) 
 * with c0 = cos(phi) and s0 = sin (phi)
 * based on the identities
 * cos(A+B)=cos(A)cos(B)+sin(A)sin(B)
 * sin(A+B)=sin(A)cos(B)+sin(B)cos(A)

 * s/c store intermediate factors of two
 * e.g. for n=7 = 4+2+1 = 111, 
 * 1st step: s/c->2 (1+1)
 * 2nd step: s/c->4 (2+2), s0/c0->3 (2+1)
 * 3rd step: s0/c0->7 (4+3)
 */
static inline void pow_angle(double &c0,double &s0,int n)
{
  double s=s0;
  double c=c0;
  n--;
  while(n){
    if (n&1){ // check if n is odd
      double c_old=c0;
      c0=c*c0 - s*s0;
      s0=s*c_old + c*s0;
    }
    if (n==1) break; // already finished?
    // next factor of 2
    {
      double c_old=c;
      c=c*c-s*s;
      s=s*c_old+c_old*s;
      n>>=1; // bitshift n by 1, equivalent to dividing by 2
    }
  }
}

/// turn angle counter clockwise
static inline void turn_angle(double c,double s,double &c0,double &s0)
{
  double tmp=c0;
  c0=c*c0  - s*s0;
  s0=s*tmp + c*s0;
}

static inline void multipole_kick(int n,double k,double c0,double s0,double x,double y, double &kx, double &ky )
{
  const double eps=std::numeric_limits<double>::epsilon();
  // when n=1 (dipole kick) the kick is uniform, also for on-axis particles
  if (n==1&&x*x+y*y<eps) {
    x = 1;
    y = 0;
  }
  double c=y;
  double s=x;
  turn_angle(c0,s0,c,s);
  pow_angle(c,s,n);
  if (x*x+y*y>eps) {
    k/=(x*x+y*y);
    kx=k*(c*x-s*y);
    ky=k*(c*y+s*x);
  } else {
    kx=0;
    ky=0;
  }
}

#endif /* function_h */



