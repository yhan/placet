/************************************************************************
*  Header file for the implementation of the function phase_advance		*
*  																		*
*  Juergen Pfingster													*
*  6. Jan 2009															*
*																		*
*  Phase advance calculates the phase advance of a given beam line and 	*
*  a given beam.														*
*************************************************************************/


#ifndef phase_advance_h
#define phase_advance_h

#include "beam.h"
#include "beamline.h"
#include "element.h"

void calc_beta(BEAM *bunch, double *_beta_new_x, double *_beta_new_y);
void phase_advance_calc(ELEMENT **element,int element_intex, double *phase_advance_x, double *phase_advance_y, 
				double beta_x_old, double beta_y_old, double beta_x_new, double beta_y_new);
void phase_advance_write(double phase_advance_x, double phase_advance_y, char *file_name);
void phase_advance(BEAMLINE *beamline,BEAM *bunch0, char *file_result, char *file_detail);

#endif
