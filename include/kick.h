#ifndef kick_h
#define kick_h

// A kick, in urad

#include "photon_spectrum.h"

struct KICK {
  double xp; // urad
  double yp; // urad
  double h; // 1/m, curvature of the reference frame (in bending magnets)
  KICK() : xp(0.), yp(0.), h(0.) {}
  KICK(double _xp, double _yp, double _h=0. ) : xp(_xp), yp(_yp), h(_h) {}
  
  double get_synrad_average_energy_loss(double length, double particle_energy ) const
  {
    double k0 = h*length*1e6; // urad
    double kick = sqrt((xp+k0)*(xp+k0)+yp*yp) * 1e-6;
    return SYNRAD::get_average_energy_loss(kick, length, particle_energy);
  }
  
  double get_synrad_mean_free_path(double length, double particle_energy ) const 
  {
    double k0 = h*length*1e6; // urad
    double kick = sqrt((xp+k0)*(xp+k0)+yp*yp) * 1e-6;
    return SYNRAD::get_mean_free_path(kick, length, particle_energy);
  }

  double get_synrad_free_path(double length, double particle_energy ) const 
  {
    double k0 = h*length*1e6; // urad
    double kick = sqrt((xp+k0)*(xp+k0)+yp*yp) * 1e-6;
    return SYNRAD::get_free_path(kick, length, particle_energy);
  }
  
  double get_synrad_energy_loss(double length, double particle_energy ) const 
  {
    double k0 = h*length*1e6; // urad
    double kick = sqrt((xp+k0)*(xp+k0)+yp*yp) * 1e-6;
    return SYNRAD::get_energy_loss(kick, length, particle_energy);
  }
  
  friend KICK operator-(const KICK &a ) { return KICK(-a.xp, -a.yp, -a.h); }

};

#endif
