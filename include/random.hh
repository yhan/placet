#ifndef random_hh
#define random_hh

#include <vector>
#include <string>
#include <cmath>
#include <climits>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>

class RANDOM_NEW
{
  gsl_rng * rng;
  gsl_ran_discrete_t * disc;

public:
	
  // default constructor
  RANDOM_NEW(int s = 0, const gsl_rng_type * T = gsl_rng_taus2) : disc(NULL)
  {
    rng = gsl_rng_alloc(T);	// Random Number Generator type
    gsl_rng_set(rng,s);		// sets seed
  }
	
  // copy constructor
  RANDOM_NEW(const RANDOM_NEW &r )
  {
    rng = gsl_rng_clone (r.rng);
    if (r.disc) {
      size_t K = r.disc->K;	
      std::vector<double> weights(K);
      for (size_t i=0; i<K; i++)
	weights[i]= gsl_ran_discrete_pdf(i, r.disc);
      disc = gsl_ran_discrete_preproc (weights.size(), weights.data());
    } else {
      disc = NULL;
    }
  }
	
  //destructor
  ~RANDOM_NEW()
  {	  	
    gsl_rng_free(rng);
    if (disc) {
      gsl_ran_discrete_free (disc);
    }
  }
	
  // assignment operator
  RANDOM_NEW &operator=(const RANDOM_NEW &r )
  {
    if (this!=&r) {
      gsl_rng_free(rng);
      if (disc) {
	gsl_ran_discrete_free (disc);
	disc = NULL;
      }
      rng = gsl_rng_clone (r.rng);
      if (r.disc) {
	size_t K = r.disc->K;	
	std::vector<double> weights(K);
	for (size_t i=0; i<K; i++)
	  weights[i]= gsl_ran_discrete_pdf(i, r.disc);
	disc = gsl_ran_discrete_preproc (weights.size(), weights.data());
      }
    }
    return *this;
  }

  //METHODS:	
	
  //save states
  int Save(FILE *file)
  {
    int ret = -1;
    if (file!=NULL) {
      ret = gsl_rng_fwrite(file, rng);
    }
    return ret;
  }

  int Save(char *filename)
  {
    FILE *file = fopen(filename,"w");
    if(file!=NULL) {              
      Save(file);
      fclose(file);
      return 0;
    }
    return -1;
  }
	
  //load states	
  int Load(FILE *file)
  {
    int ret = -1;
    if(file!=NULL) {
      ret = gsl_rng_fread(file, rng);
    }
    return ret;
  }

  int Load(char *filename)
  {
    FILE *file = fopen(filename,"r");
    if(file!=NULL) {              
      Load(file);
      fclose(file);
      return 0;
    }
    return -1;
  }
  
  //centralized save/load functions		
  static int SaveAll(char *filename);
  static int LoadAll(char *filename);

  //change seed
  void SetSeed(unsigned long int s = 0)
  {
    gsl_rng_set(rng,s);
  }
	
  //change Generator and seed
  void SetGenerator(const gsl_rng_type * T, int s = 0)
  {
    rng = gsl_rng_alloc(T);
    gsl_rng_set(rng,s);
  }
	
  //reset
  static int reset(std::string r);
  	
  // returns a random floatingpoint number
  double Uniform()		
  {
    return gsl_rng_uniform(rng);
  }
	
  // returns a random floatingpoint weight by gaussian distribution	
  double Gauss(double sigma = 1.0, double cut = -1.0)	
  {
    if(cut == -1.0) {
      return gsl_ran_gaussian(rng,sigma);
    }
    float tmp;
    while(fabs(tmp=gsl_ran_gaussian(rng,sigma))>cut*sigma);
    return tmp;
  }
	
  // returns values of Gaussian Distribution
  double GaussTable(double sigma, int k)
  {
    return gsl_ran_gaussian_pdf(k,sigma);
  }
	
  // returns a random floatingpoint weight by exponential power distribution 	
  double Exponential(double mu=1.)
  {
    return gsl_ran_exponential(rng, mu);
  }
	
  // returns values of exponential Distribution
  double ExponentialTable(double mu, int k)
  {
    return gsl_ran_exponential_pdf(k,mu);
  }
	
  // returns a random number weight by discrete distributions, weights provided by vector 'weights'
  bool DiscreteDistributionSet(std::vector<double> weights)
  {
    if (disc) {
      gsl_ran_discrete_free (disc);
    }
    disc = gsl_ran_discrete_preproc (weights.size(), weights.data());
    return disc != NULL;
  }
	
  size_t DiscreteDistribution()
  {	
    return gsl_ran_discrete (rng,disc);	
  }
	
  // returns normalised discrete distribution created via vector 'weights'	
  double DiscreteTable(int j)
  {	
    return gsl_ran_discrete_pdf(j,disc);
  }

};	

//________________________________________________________________________________

class PARALLEL_RNG {

  std::vector<RANDOM_NEW> rng;

  int get_num_threads();

  int get_thread_num();

public:


  // default constructor
  PARALLEL_RNG(int s = 0)
  {
    int threads;
    threads = get_num_threads();
    rng.resize(threads);
    RANDOM_NEW Seed(s);
    for (int i=0; i<threads; i++) {
      rng[i].SetSeed((unsigned long int)(UINT_MAX*Seed.Uniform()));
    }
  }
  
  //METHODS  
  
  //change seed
  void SetSeed(unsigned long int s = 0)
  {
    int threads;
    threads = get_num_threads();
    rng.resize(threads);
    RANDOM_NEW Seed(s, gsl_rng_mt19937);
    for (int i=0; i<threads; i++) {
      rng[i].SetSeed((unsigned long int)(UINT_MAX*Seed.Uniform()));
    }
  }
	
  //change Generator and seed
  void SetGenerator(const gsl_rng_type * T, int s = 0)
  {
    int threads;
    threads = get_num_threads();
    rng.resize(threads);
    RANDOM_NEW Seed(s,T);
		
    for (int i=0; i<threads; i++) {
      rng[i].SetGenerator(T,(int)(INT_MAX*Seed.Uniform()));
    }
  }

  //reset
  void Reset()
  {
    int threads;
    threads = get_num_threads();
    rng.resize(threads);
    RANDOM_NEW Seed(0);
    for (int i=0; i<threads; i++) {
      rng[i].SetSeed((unsigned long int)(UINT_MAX*Seed.Uniform()));
    }
  } 
  
  //save states
  int Save(FILE *file)
  {
    int ret = -1;
    if (file!=NULL) {
      int _size = rng.size();
      fwrite(&_size, sizeof(_size), 1, file);
      for (int i=0; i < _size; i++) {
	ret = rng[i].Save(file);
	if (ret!=0)
	  break;
      }
    }
    return ret;
  }

  int Save(const char *filename)
  {
    FILE *file = fopen(filename, "w");
    if(file!=NULL) {
      int ret = Save(file);
      fclose(file);
      return ret;
    }
    return -1;    
  }
  
  //load states
  int Load(FILE *file)
  {
    int ret = -1;
    if(file!=NULL) {
      int _size = fread(&_size, sizeof(_size), 1, file);
      rng.resize(_size);
      for (int i=0; i < _size; i++) {
	ret = rng[i].Load(file);
	if (ret!=0)
	  break;
      }
    }
    return ret;
  }

  int Load(const char *filename )
  {
    FILE *file = fopen(filename, "r");
    if(file!=NULL) {
      int ret = Load(file);
      fclose(file);
      return ret;
    }
    return -1;
  }
  	
  // returns a random floatingpoint number
  double Uniform()		
  {
    int tid = get_thread_num();
    return rng[tid].Uniform();
  }		
		
	
  double Gauss(double sigma = 1.0, double cut = -1.0)	
  {	
    int tid = get_thread_num();
    return rng[tid].Gauss(sigma,cut);
  }
	
  double Exponential(double mu=1.)	
  {	
    int tid = get_thread_num();
    return rng[tid].Exponential(mu);
  }
  
  // returns discrete distribution	
  int DiscreteDistribution(std::vector<double> weights)
  {	
    int tid = get_thread_num();
    if(rng[tid].DiscreteDistributionSet(weights))
      return (int)rng[tid].DiscreteDistribution();	
    return -1; 
  }
  
};

//streams of random numbers
extern RANDOM_NEW Misalignments;
extern RANDOM_NEW Instrumentation;
extern RANDOM_NEW Groundmotion;
extern RANDOM_NEW Cavity;
extern RANDOM_NEW User;
extern RANDOM_NEW Default;
extern RANDOM_NEW Survey;
extern RANDOM_NEW Select;
extern PARALLEL_RNG Radiation;

//initialisation of TCL commands
struct Tcl_Interp;
extern int Rndm_Init(Tcl_Interp*);

#endif
