#ifndef parallel_h
#define parallel_h

#include <cstdio>
#include <tcl.h>

#include "element.h"

class BEGIN_PARALLEL : public ELEMENT {

  void step_4d_0(BEAM *beam );
  void step_4d(BEAM *beam ) { step_4d_0(beam); }
  void step_4d_sr(BEAM *beam ) { step_4d_0(beam); }
  void step_4d_sr_0(BEAM *beam ) { step_4d_0(beam); }

  void step_6d(BEAM *beam ) { step_4d_0(beam); }
  void step_6d_0(BEAM *beam ) { step_4d_0(beam); } 
  void step_6d_sr(BEAM *beam ) { step_4d_0(beam); }
  void step_6d_sr_0(BEAM *beam ) { step_4d_0(beam); }

public:
};

class END_PARALLEL : public ELEMENT {
  
  bool reslice;
  bool center;
  
  void step_6d_0(BEAM *beam );
  void step_6d(BEAM *beam ) { step_6d_0(beam); }
  void step_6d_sr(BEAM *beam ) { step_6d_0(beam); }
  void step_6d_sr_0(BEAM *beam ) { step_6d_0(beam); }
  
public:

  END_PARALLEL() : reslice(false), center(false) {}
  END_PARALLEL(int &argc, char **argv );

};

extern int tk_BeginParallel(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[]);
extern int tk_EndParallel(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[]);

#endif /* parallel_h */
