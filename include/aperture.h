#ifndef aperture_h
#define aperture_h

#include "element.h"
#include "placet_cout.hh"

class APERTURE: public ELEMENT {

  void step_in(BEAM*) {}
  void step_out(BEAM*) {}

  void step_4d(BEAM*);
  void step_4d_0(BEAM*);

public:

  APERTURE(ELEMENT::APERTURE::TYPE type, double x, double y ) { set_aperture(type, x, y); }
  APERTURE(const char *shape, double x, double y ) { set_aperture(shape, x, y); }
  APERTURE() { set_aperture_type(ELEMENT::APERTURE::NONE); }
  APERTURE(int &argc, char **argv ) : ELEMENT()
  {
    set_attributes(argc,argv);
    if (geometry.length!=0) {
      placet_cout << WARNING << "aperture's length should be zero. Zeroing it." << endmsg;
      geometry.length=0.0;
    }
  }

  double get_weight() const { return aperture.weight; }

  bool is_aperture() const { return true; }
  
  APERTURE *aperture_ptr() { return this; }
  const APERTURE *aperture_ptr() const { return this; }

};

#endif
