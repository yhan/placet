#ifndef error_hh
#define error_hh

#include <iostream>
#include <string>

struct Error {

	std::string	message;
	int		id;
	
	Error(const std::string &_message="", int _id=0 ) : message(_message), id(_id) {}

	friend std::ostream &operator << (std::ostream &stream, const Error &error ) { return stream << error.message; }
	
};

// defines TYPENAME  as inheriting from Error
#define TYPEDEF_ERROR(TYPENAME)\
	struct TYPENAME : public Error\
	{\
		TYPENAME(const std::string &_message="", int _id=0 ) : Error(_message, _id) {}\
	};


#endif /* error_hh */
