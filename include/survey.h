#ifndef survey_h
#define survey_h

void beamline_survey_quadroll(BEAMLINE *beamline);
int wire_survey(FILE *file,BEAMLINE *beamline);

#endif
