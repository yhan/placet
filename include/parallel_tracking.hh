#ifndef parallel_tracking_hh
#define parallel_tracking_hh

#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <queue>

#include "piped_process.hh"
#include "file_buffered_stream.hh"
#include "file_stream.hh"
#include "parallel.h"

class ELEMENT;
class QUADRUPOLE;
class MULTIPOLE;
class DIPOLE;
class BPM;
class DRIFT;
class SBEND;

class BEGIN_PARALLEL;
class END_PARALLEL;
class BEAM;

class ParallelTracking {
  
  Piped_process *process;
  std::vector<std::string> argv;
  std::string ififoname;
  std::string ofifoname;
  File_Buffered_OStream ififo;
  File_IStream ofifo;
  bool tracking;
  bool up_and_running;

  std::queue<BPM*> bpms; ///< queue of BPMS that need to receive a BPM reading information

public:
    
  ParallelTracking(const char *mpirun_cmd = NULL );
  ~ParallelTracking();

  operator bool() const { return up_and_running; }

  bool is_tracking() const { return tracking; }

  bool startTracking(const BEAM &beam );
  void endTracking(BEAM &beam );
	
  ParallelTracking &operator<<(const QUADRUPOLE &quadrupole );
  ParallelTracking &operator<<(const MULTIPOLE &multipole );
  ParallelTracking &operator<<(const SBEND &sbend );
  ParallelTracking &operator<<(const DIPOLE &dipole );
  ParallelTracking &operator<<(const BPM &bpm );
  ParallelTracking &operator<<(const DRIFT &bpm );

};

class BEAMLINE;
class BEAM;

extern double test_no_correction_parallel(BEAMLINE *beamline, BEAM *bunch0, int niter, void (*survey)(BEAMLINE*), const char *name, const char *format, const char *mpirun );

#endif /* parallel_tracking_hh */
