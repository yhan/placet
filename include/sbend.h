#ifndef sbend_h
#define sbend_h

#include "conversion.h"
#include "element.h"
#include "particle.h"
#include "photon.h"
#include "savitzky_golay.h"
#include "placet.h"

class SBEND : public ELEMENT {
  double angle0,e1,e2,k,k2;
  bool csr, csr_savesectors, csr_enforce_steady_state,csr_enable_driftwake;
  int csr_nbins, csr_nhalffilter, csr_filterorder, csr_nsectors;
  double csr_charge, csr_attenuation_length;

  bool csr_shielding;
  int csr_shielding_n_images;
  double csr_shielding_height;
  bool enable_csr_shielding_width;
  double csr_shielding_width;
  std::vector<SAVITZKY_GOLAY> SG;
  double hgap, fint, fintx; // see equivalent MADX parameters
  
  void step_in(BEAM *beam );
  void step_out(BEAM *beam );

  void step_partial(BEAM* beam, double l, void (ELEMENT::*step_function)(BEAM*));

  friend class Printer;
  friend class PHOTON;
 
  struct CSR_wake {
    std::vector<int> nlambda;   // histogram distribution 
    std::vector<double> lambda;   // distribtuion  
    std::vector<double> dlambda;  // differential distribution  
    std::vector<double> dE_ds; // CSR wake
    int csr_nbins;
  CSR_wake(size_t n = 0) : nlambda(n, 0), lambda(n, 0.0), dlambda(n, 0.0), dE_ds(n, 0.0), csr_nbins(n) {}
  };

  CSR_wake apply_csr_wake(double sector_angle, double sector_length, int nsector, BEAM* beam);
  void prepare_csr_drift_wake(double sector_angle, CSR_wake &wake, BEAM* beam);
    
 protected:

  KICK get_kick(const PARTICLE &particle ) const
  {
    const int N=flags.thin_lens;
    const double dS=geometry.length/N; // m
    const double rho_i=angle0/geometry.length; // 1/m
    const double h=rho_i; // 1/m
    const double k0=h*ref_energy/particle.energy; // 1/m
    const double k1=k/particle.energy/geometry.length; // 1/m^2
    const double
      x  = particle.x, // um
      y  = particle.y, // um
      xp = particle.xp, // urad
      yp = particle.yp, // urad
      vz_i = hypot(1.0, xp/1e6, yp/1e6); // rad
    KICK kick; // obatined from the exact Hamiltonian
    kick.xp = (-((k1 + h*k0) * x + 1e6*k0) * vz_i + 1e6*h) * dS; // urad
    kick.yp = k1 * y * vz_i * dS; // urad
    kick.h = h; // 1/m, curvature of the reference system
    return kick;
  }
  
 public:


  explicit SBEND(double _length=0., double _angle0=0., double _e0=0., double _e1=0., double _e2=0., double _k=0. ) : ELEMENT(_length), angle0(_angle0), e1(_e1), e2(_e2), k(_k), k2(0.0), csr(false), csr_savesectors(false), csr_enforce_steady_state(false), csr_enable_driftwake(false), csr_nbins(510), csr_nhalffilter(10), csr_filterorder(2), csr_nsectors(20), csr_charge(1.0e-9), csr_attenuation_length(-2.0), csr_shielding(false), csr_shielding_n_images(0), csr_shielding_height(1.0), enable_csr_shielding_width(false), csr_shielding_width(0.0) { ref_energy=_e0; }
 SBEND(int &argc, char **argv ) : ELEMENT(), angle0(0.0), e1(0.0), e2(0.0), k(0.0), k2(0.0), csr(false), csr_savesectors(false), csr_enforce_steady_state(false), csr_enable_driftwake(false), csr_nbins(510), csr_nhalffilter(10), csr_filterorder(2), csr_nsectors(20), csr_charge(1.0e-9), csr_attenuation_length(-2.0), csr_shielding(false), csr_shielding_n_images(0), csr_shielding_height(1.0), enable_csr_shielding_width(false), csr_shielding_width(0.0), hgap(0.0), fint(0.0), fintx(-1.0)
  {
    attributes.add("angle", "Bend angle [rad]", OPT_DOUBLE, &angle0);
    attributes.add("E1", "Rotation angle for the entrance pole face [rad]", OPT_DOUBLE, &e1);
    attributes.add("E2", "Rotation angle for the exit pole face [rad]", OPT_DOUBLE, &e2);
    attributes.add("K", "Integrated quadrupole strength [GeV/m]", OPT_DOUBLE, &k);
    attributes.add("K2", "Integrated sextupole strength [GeV/m^2]", OPT_DOUBLE, &k2);
    // fringe fields (see madx implementation)
    attributes.add("hgap", "The half gap of the magnet (default 0) [m]", OPT_DOUBLE, &hgap);
    attributes.add("fint", "Field integral for the entrance fringe field (default 0)", OPT_DOUBLE, &fint);
    attributes.add("fintx", "Field integral for the exit fringe field (default = as entrance)", OPT_DOUBLE, &fintx);
    //
    attributes.add("csr", "Coherent Synchrotron Radiation (CSR) [BOOL]", OPT_BOOL, &csr);
    attributes.add("csr_charge", "CSR: [REQUIRED] total charge of input distribution [C] ", OPT_DOUBLE, &csr_charge);
    attributes.add("csr_enable_driftwake", "Enable csr wake propagation into trailing drift [BOOL]\n     drift wake will be applied until 1% remains or until next SBend", OPT_BOOL, &csr_enable_driftwake);
    attributes.add("csr_nbins", "CSR: # of bins ( >= 10 ) [#]", OPT_INT, &csr_nbins);
    attributes.add("csr_nsectors", "CSR: # of dipole sectors ( >= 1 )  [#]", OPT_INT, &csr_nsectors);
    attributes.add("csr_nhalffilter", "CSR: Savitzky-Golay filter half-width ( >= 1 )  [# of bins]", OPT_INT, &csr_nhalffilter);
    attributes.add("csr_filterorder", "CSR: Savitzky-Golay filter order ( >= 1 ), 1 implies MA  [-]", OPT_INT, &csr_filterorder);
    attributes.add("csr_savesectors", "CSR: save data for each sector  [nbin  s   lambda   dlambda   dE_ds [GeV/m] ]   [BOOL]", OPT_BOOL, &csr_savesectors);
    attributes.add("csr_attenuation_length", "CSR: attenuation length for csr drift (default value is 1.5*overtaking length) [m] ", OPT_DOUBLE, &csr_attenuation_length);
    attributes.add("csr_enforce_steady_state", "CSR: enforce steady state mode (infinite slippage length)  [BOOL]", OPT_BOOL, &csr_enforce_steady_state);

    attributes.add("csr_shielding", "CSR_shielding, parallel plates switch [BOOL]", OPT_BOOL, &csr_shielding);
    attributes.add("csr_shielding_n_images", "CSR_shielding: Number of images on each side of plates used in shielding calculation [INT]", OPT_INT, &csr_shielding_n_images);
    attributes.add("csr_shielding_height", "CSR_shielding:  Distance between parallel plates [m]", OPT_DOUBLE, &csr_shielding_height);
    attributes.add("enable_csr_shielding_width", "CSR_shielding: Enable finite chamber width", OPT_BOOL, &enable_csr_shielding_width);
    attributes.add("csr_shielding_width", "CSR_shielding:  Width of rectangular beam pipe [m]", OPT_DOUBLE, &csr_shielding_width);

    set_attributes(argc, argv);
  }

  Matrix<6,6> get_transfer_matrix_6d(double _energy ) const;

  double get_angle() const { return angle0; }

  void step_6d_sr_0(BEAM *beam );
  void step_6d_0(BEAM *beam );
  void step_4d_sr_0(BEAM*);
  void step_4d_0(BEAM*);
  void step_4d(BEAM*);
  // void step_6d_tl_0(BEAM*);
  // void step_6d_tl_sr_0(BEAM *beam );
  //  void calculate_savitzky_golay_coeffs(int SG_order, int SG_length, double *SG_coeffs, double *SG_norm);

  double r_alpha_n(double alpha,double kappa,double nH);
  double s_alpha_n(double z,double alpha,double kappa,double r_alpha);
  void find_alpha_limit(double z,double* alpha1,double kappa,double nH);
  void calculate_shielding_kick(int csr_nbins, double csr_charge, double sector_angle, double radius0,double traversed_angle,CSR_wake &wake, int nsector);
  //  double lambda_temp(double x);
  //  double dlambda_temp(double x);
 
  bool is_sbend() const { return true; }

  SBEND *sbend_ptr() { return this; }
  const SBEND *sbend_ptr() const { return this; }

  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  
  void GetMagField(PARTICLE *particle, double sector_length, double *bfield);

  friend OStream &operator<<(OStream &stream, const SBEND &sbend )
  {
    return stream << static_cast<const ELEMENT &>(sbend)
		  << double(sbend.e1)
		  << double(sbend.e2)
		  << double(sbend.angle0)
		  << double(sbend.k);
  }

};

int tk_Sbend(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[]);

#endif /* sbend_h */
