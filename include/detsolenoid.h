#include <vector>

#include "particle.h"


class DetectorSolenoid { 
  

  std::vector<double> z,bz,br,bz_dz;
  /**
   * The current index in the z array
   * 
   * By having this index in the class,
   * we may speed up the code (while loop 
   * is shortened)
   */
  unsigned int z_i;

  /// The half crossing angle...
  double xangle;
  /// Store these two as well for performance.
  double cos_xangle;
  double sin_xangle;

 public:

  /**
   * @brief Detector Solenoid class constructor
   * 
   * @param crossing_angle The half crossing angle of beamline wrt. detector
   */
  DetectorSolenoid(double crossing_angle=0.0);
  ~DetectorSolenoid(); // destructor
  
  int ReadMapFile1D(char *file);
  /**
   * Returns the pointer to the field components Bx, By, and Bz.
   * 
   * The convention is the following:
   *  - electrons move towards positive z, positrons towards negative
   *  - a positive angle means that the particles move towards a
   *    NEGATIVE x-value.
   * @return B-field (x,y,z)
   */
  int GetMapField1D(PARTICLE* particle, double posz, double int_length, double* bfield);
  void rotate_in(double* vec, double energy);
  void rotate_out(double* vec, double energy);

//  protected:
 
//  private:
};
