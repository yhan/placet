#ifndef curvature_h
#define curvature_h

#include <cstdio>
#include <tcl.h>

#include "element.h"

class CURVATURE : public ELEMENT {

	double angley, anglex;

public:

	CURVATURE(double _angley = 0, double _anglex = 0);

	void step_4d_0(BEAM *beam );
	void step_4d(BEAM *beam) { step_4d_0(beam); }

};

extern int tk_Curvature(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[]);

#endif /* curvature_h */
