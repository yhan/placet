#! /usr/bin/perl

## Andrea Latina <andrea.latina@cern.ch>
## October 20: added support for default arguments

if (@ARGV < 1) { die "usage:\tcreate_interface.pl FILE_API.hh\n"; }

open FILE, $ARGV[0] or die "Could not open file $ARGV[0]\n";
@line = <FILE>;
close FILE;

use File::Basename;

$basename = basename($ARGV[0]);
$basename =~ s/\.[^.]+$//;
# $basename =~ s/_api$//;

$_ = "@line";
s!//.*\n!!g;     # removes C++-like comments
s!\n!!g;         # chomps \n 
s!/\*.*?\*/!!g;  # removes C-like comments
s![\s\t]+! !g;   # removes tabs and white spaces

if (/^.*?(extern)(.*)(\);).*$/) {
  # retains the file from the first 'extern'
  # to the last ');'
  $_ = "$1$2$3";
  my @functions = split /;/ ;
  my @functions_name;
  my @functions_args;
  my @commands_name;
  foreach (@functions) {
    /^\s*extern\s+\w+\s+(\w+)\((.*)\)\s*$/;
    my $function_name = $1;
    my $function_args = $2;
    my $command_name  = $function_name;
    $command_name =~ tr/a-z/A-Z/;
    push @functions_name, $function_name;
    push @functions_args, $function_args;
    push @commands_name,  $command_name;
  }
  my %commands_hash;
  for (my $i=0; $i<@commands_name,; $i++) {
    $_ = $commands_name[$i];
    $commands_hash{$_}++;
    $commands_name[$i] .= "$commands_hash{$_}" if ($commands_hash{$_} > 1);
  }
  # we are done with reading the input file

  # starts working on the output files
  open SERVER_H, ">${basename}_server.hh";
  print SERVER_H "#ifndef ${basename}_server_hh\n#define ${basename}_server_hh\n\n";
  print SERVER_H "#include \"${basename}.hh\"\n\nenum {\n";
  for (my $i=0; $i<@commands_name; $i++) {
    $_ = $commands_name[$i];
    print SERVER_H ($i < (@functions_name - 1)) ? "  $_,\n" : "  $_\n";
  }
  print SERVER_H "};\n\n";
  print SERVER_H "extern int ${basename}_dispatcher(int ifiledes, int ofiledes );\n\n";
  print SERVER_H "#endif /* ${basename}_server_hh */\n";
  close SERVER_H;

  # header file for the client
  open CLIENT_H, ">${basename}_client.hh";
  print CLIENT_H "#ifndef ${basename}_client_hh\n#define ${basename}_client_hh\n\n";
  print CLIENT_H "#include \"${basename}_server.hh\"\n\n";
  print CLIENT_H "extern int init(int ifiledes, int ofiledes );\n";
  print CLIENT_H "extern int finalize();\n\n";
  print CLIENT_H "#endif /* ${basename}_client_hh */\n";
  close CLIENT_H;

  # here we write the body of these files
  open SERVER_C, ">${basename}_server.cc";
  print SERVER_C "#include \"file_buffered_stream.hh\"\n";
  print SERVER_C "#include \"${basename}_server.hh\"\n\n";
  print SERVER_C "int ${basename}_dispatcher(int ifiledes, int ofiledes )\n";
  print SERVER_C "{\n";
  print SERVER_C "  File_Buffered_OStream OSTREAM(ofiledes);\n";
  print SERVER_C "  File_IStream ISTREAM(ifiledes);\n";
  print SERVER_C "  if (ISTREAM && OSTREAM) {\n";
  print SERVER_C "    int command;\n";
  print SERVER_C "    ISTREAM >> command;\n";
  print SERVER_C "    switch(command) {\n";
  for (my $i=0; $i<@functions_name; $i++) {
    my $function_name = $functions_name[$i];
    my $function_args = $functions_args[$i];
    my $command_name  = $commands_name[$i];
    print SERVER_C "      case ${command_name}: {\n";
    # declare all variables
    foreach (split(/,[^\w]/, $function_args)) {
      s/const|&//g;
      s/=.*$//g;
      s/^\s*|\s*$//g;
      print SERVER_C "        $_;\n";
    }
    # read the input parameters
    foreach (split(/,/, $function_args)) {
      if (/(in_\w+)/) {
	print SERVER_C "        ISTREAM >> $1;\n";
      }
    }
    # invoke the function
    my @call_args = ($function_args =~ /in_\w+|out_\w+/g);
    my $call_args = join ",", @call_args;
    print SERVER_C "        ${function_name}($call_args);\n";
    my $out_args = @{[ $function_args =~ /out_\w*/g ]};
    # return all return values
    my $nret_values = 0;
    foreach (split(/,/, $function_args)) {
      s/^\s*|\s*$//g;
      if (/(out_.*)/) {
	print SERVER_C "        OSTREAM << $1;\n";
	$nret_values++;
      }
    }
    if ($nret_values == 0) {
      print SERVER_C "        int dummy=0;\n";
      print SERVER_C "        OSTREAM << dummy;\n";
    }
    print SERVER_C "      }\n";
    print SERVER_C "      break;\n";
  }
  print SERVER_C "    }\n";
  print SERVER_C "    OSTREAM.flush();\n";
  print SERVER_C "  }\n";
  print SERVER_C "  return 0;\n";
  print SERVER_C "}\n";
  close SERVER_C;

  # CLIENT side
  open CLIENT_C, ">${basename}_client.cc";
  print CLIENT_C "#include \"file_buffered_stream.hh\"\n";
  print CLIENT_C "#include \"${basename}_server.hh\"\n\n";
  print CLIENT_C "static File_Buffered_OStream OSTREAM;\n";
  print CLIENT_C "static File_IStream ISTREAM;\n\n";
  print CLIENT_C "int init(int ifiledes, int ofiledes )\n";
  print CLIENT_C "{\n";
  print CLIENT_C "  ISTREAM.open(ifiledes);\n";
  print CLIENT_C "  OSTREAM.open(ofiledes);\n";
  print CLIENT_C "  return ISTREAM && OSTREAM ? 0 : 1;\n";
  print CLIENT_C "}\n\n";
  print CLIENT_C "int finalize()\n";
  print CLIENT_C "{\n";
  print CLIENT_C "  if (ISTREAM)\n";
  print CLIENT_C "    ISTREAM.close();\n";
  print CLIENT_C "  if (OSTREAM)\n";
  print CLIENT_C "    OSTREAM.close();\n";
  print CLIENT_C "  return 0;\n";
  print CLIENT_C "}\n";
  #
  for (my $i=0; $i<@functions_name; $i++) {
    my $function_name = $functions_name[$i];
    my $function_args = $functions_args[$i];
    my $command_name  = $commands_name[$i];
    print CLIENT_C "\nvoid ${function_name}(";
    my @call_args;
    foreach (split(/,/, $function_args)) {
      s/^\s*(.*)\s+(&in_\w+|in_\w+).*/$1 $2/g;
      s/^\s*//g;
      push @call_args, $_;
    }
    my $call_args = join ", ", @call_args;
    print CLIENT_C "$call_args )\n";
    print CLIENT_C "{\n";
    print CLIENT_C "  OSTREAM << int(${command_name});\n";
    foreach (split(/,/, $function_args)) {
      s/^\s*|\s*$//g;
      if (/(in_\w+)/) {
	print CLIENT_C "  OSTREAM << $1;\n";
      }
    }
    print CLIENT_C "  OSTREAM.flush();\n";
    my $nret_values = 0;
    foreach (split(/,/, $function_args)) {
      s/^\s*|\s*$//g;
      if (/(out_.*)/) {
	print CLIENT_C "  ISTREAM >> $1;\n";
	$nret_values++;
      }
    }
    if ($nret_values == 0) {
      print CLIENT_C "  int dummy;\n";
      print CLIENT_C "  ISTREAM >> dummy;\n";
    }
    print CLIENT_C "}\n";
  }
  close CLIENT_C;
}
