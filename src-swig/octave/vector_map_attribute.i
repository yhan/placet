#if defined(SWIGOCTAVE)
%typemap(in, numinputs=0) std::vector<std::map<std::string,Attribute> > & (std::vector<std::map<std::string,Attribute> > temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<std::map<std::string,Attribute> > & {
  Cell res($1->size(),1);
  for (size_t i=0; i<$1->size(); i++) {
    octave_map table;
    const std::map<std::string,Attribute> &attributes = (*$1)[i];
    for (std::map<std::string,Attribute>::const_iterator itr=attributes.begin(); itr!=attributes.end(); ++itr) {
      const std::string &key = (*itr).first;
      const Attribute &attribute = (*itr).second;
      octave_value value;
      switch(attribute.type()) {
      case OPT_INT: value = int(attribute); break;
      case OPT_UINT: value = size_t(attribute); break;
      case OPT_BOOL: value = bool(attribute); break;
      case OPT_DOUBLE: value = double(attribute); break;
      case OPT_CHAR_PTR:
      case OPT_STRING: value = std::string(attribute); break;
      case OPT_COMPLEX: value = attribute.get_complex(); break;
      default:
	value = 0;
      }
      table.assign(key, Cell(value));
    }
    res(i,0) = table;
  }
  $result->append(octave_value(res));
}

#endif
